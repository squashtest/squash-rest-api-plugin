/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.core.web;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpInputMessage;
import org.squashtest.tm.plugin.rest.core.jackson.DeserializationHints;

import java.io.IOException;
import java.io.InputStream;

/**
 * That entity is created by the {@link DeserializationConfigurationRequestBodyAdvice}, and will help the {@link PersistentEntityJacksonHttpMessageConverter}
 * to configure the object reader with {@link com.fasterxml.jackson.databind.cfg.ContextAttributes}
 *
 * Created by bsiri on 11/07/2017.
 */
public class DeserializationConfigurationInputMessage implements HttpInputMessage {

    private InputStream body;

    private HttpHeaders headers;

    private DeserializationHints hints;


    public DeserializationConfigurationInputMessage(HttpInputMessage original, DeserializationHints hints) {
        try {
            this.body = original.getBody();
            this.headers = original.getHeaders();
        }catch(IOException ex){
            throw new RuntimeException(ex);
        }
        this.hints = hints;
    }

    @Override
    public InputStream getBody() throws IOException {
        return body;
    }

    @Override
    public HttpHeaders getHeaders() {
        return headers;
    }

    public DeserializationHints getHints() {
        return hints;
    }

    public void setHints(DeserializationHints hints) {
        this.hints = hints;
    }
}
