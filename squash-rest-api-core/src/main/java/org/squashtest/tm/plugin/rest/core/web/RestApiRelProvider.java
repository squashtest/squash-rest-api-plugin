/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.core.web;

import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.hateoas.RelProvider;
import org.springframework.util.StringUtils;
import org.squashtest.tm.plugin.rest.core.jackson.RestDtoName;

/**
 * Provides the rel of a test. Currently works by returning lowercase version of the simple class name joined with hyphens, and ending 
 * with an 's'.
 * @author bsiri
 */
/*
 * TODO : if the simple implementation here isn't enough, consider something more substantial : 
 * - add a mandatory 'rel' attribute in annotation RestApiController (in addition to the existing 'value')
 * - locate and process those annotations 
 * 
 * much like in BasePathAwareLinkBuildingService. Which would call for factoring out the plumbing in a new Mapping object 
 * dedicated for maintaining classes and their name, controller handlers etc
 * 
 */
public class RestApiRelProvider implements RelProvider {

	@Override
	public boolean supports(Class<?> delimiter) {
		return true;
	}

	@Override
	public String getItemResourceRelFor(Class<?> type) {
		return hyphenatedClassname(type);
	}

	@Override
	public String getCollectionResourceRelFor(Class<?> type) {
		// check if type is a dto
		RestDtoName annotation = AnnotationUtils.findAnnotation(type, RestDtoName.class);

		if(annotation != null) {
			return annotation.value()
					// and now append an 's'
					+ "s";
		} else {
			return hyphenatedClassname(type)
					// and now append an 's'
					+ "s";
		}
	}

	private String hyphenatedClassname(Class<?> type){
		
		// uncapitalize the first later of the simple name
		return StringUtils.uncapitalize(type.getSimpleName())	
				// prepend a hyphen '-' before every other uppercase letter
				.replaceAll("(?=[A-Z])","-$0")					
				// then lower-case it
				.toLowerCase();
		
	}

}
