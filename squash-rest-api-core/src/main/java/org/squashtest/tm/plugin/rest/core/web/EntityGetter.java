/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.core.web;

import org.springframework.core.annotation.AliasFor;
import org.squashtest.tm.domain.Identified;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Methods on subclasses of {@link BaseRestController} annotated with this will be identified as main handlers for fetching entities of that type.
 * The value is optional : if left blank the entity type will be resolved from RestApiController. If that entity type has several subclasses served by 
 * the annotated handler, they should be listed in attribute {@link #subclasses()}.
 * 
 * @author bsiri
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface EntityGetter {

	@AliasFor("subclasses")
	Class<? extends Identified>[] value() default {};
	
	@AliasFor("value")
	Class<? extends Identified>[] subclasses() default {};
}
