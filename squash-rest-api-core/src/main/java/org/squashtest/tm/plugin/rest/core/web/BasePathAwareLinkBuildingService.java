/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.core.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.LinkBuilder;
import org.springframework.hateoas.RelProvider;
import org.springframework.hateoas.core.LinkBuilderSupport;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.hateoas.mvc.ControllerLinkBuilderFactory;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.web.util.UriComponentsBuilder;
import org.squashtest.tm.domain.Identified;
import org.squashtest.tm.plugin.rest.core.configuration.RestApiProperties;
import org.squashtest.tm.plugin.rest.core.exception.ProgrammingError;
import org.squashtest.tm.plugin.rest.core.utils.ProxyUtils;
import org.squashtest.tm.service.internal.configuration.CallbackUrlProvider;
import org.squashtest.tm.service.testautomation.spi.BadConfiguration;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.servlet.ServletContext;
import java.lang.reflect.Method;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

/**
 * <p>
 * That service will ensure that the base path of the API is present
 * in a HATEOAS link.
 * </p>
 *
 * <p>
 * The usual toolkit to create HATEOAS links is {@link ControllerLinkBuilder},
 * that unfortunately would not take care of the base path of the api. That bug
 * was initially detected with Spring Data Rest, and originates with the way the
 * Controllers are registered by the custom HandlerMapping (long story short,
 * non-standard way).
 * </p>
 *
 * <p>
 * 	It also offers handy functions that will generate the self URI for the entities managed by the controllers, if 
 * 	they expose a method annotated with  
 * </p>
 *
 * <p>
 * Thanks to https://github.com/spring-projects/spring-hateoas/issues/434 I can
 * work around the private static final services in
 * {@link ControllerLinkBuilder} and {@link ControllerLinkBuilderFactory}, that
 * would not treat the basepath.
 * </p>
 *
 *
 * @author bsiri
 *
 */
public class BasePathAwareLinkBuildingService {

    private static final Logger LOGGER = LoggerFactory.getLogger(BasePathAwareLinkBuildingService.class);

    private final URI contextBaseURI;
    private final String restBasePath;
    private final ApplicationContext context;

    private Map<Class<? extends Identified>, Method> entityGetterHandlerMap = new HashMap<>();

    private RelProvider relProvider;

    @Inject
    private CallbackUrlProvider callbackUrlProvider;


    public BasePathAwareLinkBuildingService(ServletContext servletContext, RestApiProperties properties, RelProvider relProvider, ApplicationContext context) {

        contextBaseURI = URI.create(servletContext.getContextPath());
        restBasePath = properties.getNormalizedBasePath();
        this.context = context;
        this.relProvider = relProvider;

        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("initializing link building service with base url : " + contextBaseURI + restBasePath);
        }

    }

    @PostConstruct
    public void afterPropertiesSet() {
        LOGGER.trace("initializing entity getter handler map");
        initHandlerMap();
    }


    /**
     * Prepends the base path of the api to the URI being built by the linkBuilder.
     *
     *
     * @param linkBuilder
     * @return
     */
    public LinkBuilder fromBasePath(ControllerLinkBuilder linkBuilder) {

        try {
            URI linkURI = linkBuilder.toUri();

            URI squashURI = new URI(linkURI.getScheme(), linkURI.getUserInfo(), linkURI.getHost(), linkURI.getPort(), contextBaseURI.getPath(), null, null);

            URI relativeLinkUri = squashURI.relativize(linkURI);

            URI returnedUri;
            try {
                returnedUri = callbackUrlProvider.getCallbackUrl().toURI();
            } catch (BadConfiguration badConfiguration) {
                returnedUri = squashURI;
            }

            return BasePathLinkBuilder.create(returnedUri)
                    .slash(restBasePath)
                    .slash(relativeLinkUri);
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Returns a LinkBuilder configured to point to the given entity. Remember that the link will require a call to  {@link #fromBasePath(ControllerLinkBuilder)}
     *
     * @param clazz
     * @param id
     * @param <ENTITY>
     * @return
     */
    public <ENTITY extends Identified> ControllerLinkBuilder initializeBuilderFor(Class<ENTITY> clazz, long id) {
        // find the handler or die trying
        Method handler = findHandlerFor(clazz);
        return linkTo(handler, id);
    }

    /**
     * Returns a LinkBuilder configured to point to the given entity. Remember that the link will require a call to  {@link #fromBasePath(ControllerLinkBuilder)}
     *
     * @param entity
     * @param <ENTITY>
     * @return
     */
    public <ENTITY extends Identified> ControllerLinkBuilder initializeBuilderFor(ENTITY entity) {
        return initializeBuilderFor((Class<ENTITY>) ProxyUtils.getClass(entity), entity.getId());
    }

    /**
     * Creates the absolute link to the given entity, using the given relname
     *
     * @param clazz
     * @param id
     * @param relname
     * @return
     */
    public <ID extends Identified> Link createLinkTo(Class<ID> clazz, Long id, String relname) {
        return fromBasePath(initializeBuilderFor(clazz, id)).withRel(relname);
    }


    /**
     * Creates the absolute link to the given entity, using the default rel for that entity
     *
     * @param entity
     * @return
     */
    public <ENTITY extends Identified> Link createLinkTo(ENTITY entity) {
        Class<ENTITY> clazz = (Class<ENTITY>) ProxyUtils.getClass(entity);
        // find the 'rel' for that entity
        String relname = relProvider.getItemResourceRelFor(clazz);
        return createLinkTo(clazz, entity.getId(), relname);
    }


    /**
     * Creates the self-link for the argument if it can, or null if cannot.
     *
     * @param entity
     * @return
     */
    public <ENTITY extends Identified> Link createSelfLink(ENTITY entity) {
        Class<ENTITY> clazz = (Class<ENTITY>) ProxyUtils.getClass(entity);
        return createLinkTo(clazz, entity.getId(), Link.REL_SELF);
    }


    public <ENTITY extends Identified> Link createSelfLink(Class<ENTITY> clazz, Long id) {
        return createLinkTo(clazz, id, Link.REL_SELF);
    }

    /**
     * Creates the absolute link to the given entity, using the specified rel
     *
     * @param entity
     * @param relname
     * @return
     */
    public <ENTITY extends Identified> Link createLinkTo(ENTITY entity, String relname) {
        Class<ENTITY> clazz = (Class<ENTITY>) ProxyUtils.getClass(entity);
        return createLinkTo(clazz, entity.getId(), relname);
    }


    public <ID extends Identified> Link createLinkTo(Class<ID> clazz, Long id) {
        String relname = relProvider.getItemResourceRelFor(clazz);
        return createLinkTo(clazz, id, relname);
    }


    /**
     * appends '/relname' to the current URL and creates a link with it
     *
     * @param relname
     * @return
     */
    public <ENTITY extends Identified> Link createRelationRelativeToCurrentUri(String relname) {
        try {
            UriComponentsBuilder builder = ServletUriComponentsBuilder.fromCurrentRequestUri();
            builder.path("/" + relname);
            return new Link(builder.toUriString(), relname);
        } catch (Exception ex) {
            LOGGER.error("attempted to retrieve the current http request, but it seems this thread isn't processing one");
            // this error is non-fatal, let not crash the whole thing
            return new Link("", relname);
        }
    }

    /**
     * appends '/relname' to the selflink of the given entity. relname is also the name of the link
     *
     * @param entity
     * @return
     */
    public <ENTITY extends Identified> Link createRelationTo(ENTITY entity, String relname) {
        return fromBasePath(initializeBuilderFor(entity)).slash(relname).withRel(relname);
    }

    public <ENTITY extends Identified> Link createRelationTo(Class<ENTITY> entityClass, long id, String relname) {
        return fromBasePath(initializeBuilderFor(entityClass, id)).slash(relname).withRel(relname);
    }

    public <ENTITY extends Identified> Link createRelationTo(Class<ENTITY> entityClass, long id, String relname, String relpath) {
        return fromBasePath(initializeBuilderFor(entityClass, id)).slash(relpath).withRel(relname);
    }

    private Method findHandlerFor(Object entity) {

        Class<?> unproxied = ProxyUtils.getClass(entity);
        return entityGetterHandlerMap.get(unproxied);

    }

    private Method findHandlerFor(Class<? extends Identified> clazz) {
        Method method = entityGetterHandlerMap.get(clazz);
        if (method != null) {
            return method;
        } else {
            // notify the developper early !
            throw new ProgrammingError("Somehow no controller handler could be found for entity class '" + clazz + "'. " +
                    "Please ensure that there is a controller dedicated to that class, with proper " +
                    "use of @RestApiController and @EntityGetter");
        }
    }


    /**
     * Ripped of org.springframework.data.rest.webmvc.support.BaseUriLinkBuilder
     *
     * @author bsiri
     *
     */
    private static final class BasePathLinkBuilder extends LinkBuilderSupport<BasePathLinkBuilder> {


        public BasePathLinkBuilder(UriComponentsBuilder builder) {
            super(builder);
        }

        public static BasePathLinkBuilder create(URI baseUri) {
            return new BasePathLinkBuilder(UriComponentsBuilder.fromUri(baseUri));
        }

        @Override
        protected BasePathLinkBuilder getThis() {
            return this;
        }

        @Override
        protected BasePathLinkBuilder createNewInstance(UriComponentsBuilder builder) {
            return new BasePathLinkBuilder(builder);
        }

    }


    @SuppressWarnings({"rawtypes"})
    private final void initHandlerMap() {

        Map<String, BaseRestController> controllers = context.getBeansOfType(BaseRestController.class);

        for (Entry<String, BaseRestController> entry : controllers.entrySet()) {

            String beanName = entry.getKey();
            BaseRestController controller = entry.getValue();

            // get the controller class, unproxy if required
            Class<?> contCls = ProxyUtils.getClass(controller);

            if (LOGGER.isTraceEnabled()) {
                LOGGER.trace("looking for @EntityGetter method on bean '" + beanName + "' of class " + contCls);
            }

            // locate the entity type
            // note : if the annotation is missing let the NPE fly wild and crash the initialization
            // so the dev knows (s)he forgot something
            RestApiController racAnnot = AnnotationUtils.findAnnotation(contCls, RestApiController.class);
            Class<? extends Identified> mainType = racAnnot.value();

            // when the controller is not rely on a Identified Entity (AutomatedSuite for exemple)
            // no need to initialize the following entityGetter
            if(mainType.equals(UndefinedIdentified.class)){
                continue;
            }

            // locate the main getter method (if exist), and potential extra subclasses
            Method entityGetter = null;
            Class<? extends Identified>[] subclasses = null;
            for (Method m : contCls.getDeclaredMethods()) {
                EntityGetter annot = AnnotationUtils.getAnnotation(m, EntityGetter.class);

                if (annot != null) {

                    entityGetter = m;
                    // TODO : additional checks to assert that the arguments are compatible too

                    subclasses = annot.value();

                    break;
                }

            }

            // if a main method was found, register it
            if (entityGetter != null) {

                entityGetterHandlerMap.put(mainType, entityGetter);

                for (Class<? extends Identified> cls : subclasses) {
                    entityGetterHandlerMap.put(cls, entityGetter);
                }


                if (LOGGER.isTraceEnabled()) {

                    LOGGER.trace("found @EntityGetter on method '" + entityGetter.getName() + "' on class '" + contCls + "', registering as entity getter for entity : '" + mainType + "'");

                    if (subclasses.length > 0) {
                        StringBuilder builder = new StringBuilder();

                        for (Class cls : subclasses) {
                            builder.append(cls.getName());
                        }

                        LOGGER.trace("also registering for subclasses : " + builder);
                    }

                }

            }

            // else nothing
            else {
                if (LOGGER.isTraceEnabled()) {
                    LOGGER.trace("could not find @EntityGetter on class ");
                }
            }

        }
    }

}
