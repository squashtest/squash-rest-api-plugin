/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.core.jackson;

import com.fasterxml.jackson.databind.BeanProperty;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.util.NameTransformer;
import com.fasterxml.jackson.datatype.hibernate5.Hibernate5Module;
import com.fasterxml.jackson.datatype.hibernate5.HibernateProxySerializer;
import org.hibernate.engine.spi.Mapping;
import org.hibernate.proxy.HibernateProxy;

import java.io.IOException;


/**
 * 
 * <p>That serializer attempts honors the unwrapping feature, when the value to be serialized should be unwrapped.</p>
 * 
 * <p>
 * 	The regular {@link HibernateProxySerializer} cannot really unwrap the value because it ultimately delegates to another serializer
 * (which depends on the actual type of the proxy). If that delegate cannot unwrap, no way that HibernateProxySerializer could do that too.
 * However it's a shame that it doesn't even try. 
 * </p>
 * 
 * <p>	
 * 	Note : That serializer must be registered before {@link Hibernate5Module} can register its own, otherwise the other implementation will 
 * kick in first and no unwrapping would occur.
 * </p>
 * 
 * @author bsiri
 *
 */
public class TryUnwrappingWhenPossibleHibernateProxySerializer extends HibernateProxySerializer {

	private boolean isUnwrapping = false;
	
	public TryUnwrappingWhenPossibleHibernateProxySerializer(boolean forceLazyLoading, boolean serializeIdentifier, Mapping mapping,
			BeanProperty property, boolean isUnwrapping) {
		super(forceLazyLoading, serializeIdentifier, mapping, property);
		this.isUnwrapping = isUnwrapping;
	}
	
	public TryUnwrappingWhenPossibleHibernateProxySerializer(boolean forceLazyLoading, boolean serializeIdentifier, Mapping mapping,
			BeanProperty property) {
		super(forceLazyLoading, serializeIdentifier, mapping, property);
	}

	public TryUnwrappingWhenPossibleHibernateProxySerializer(boolean forceLazyLoading, boolean serializeIdentifier, Mapping mapping) {
		super(forceLazyLoading, serializeIdentifier, mapping);
	}

	public TryUnwrappingWhenPossibleHibernateProxySerializer(boolean forceLazyLoading, boolean serializeIdentifier) {
		super(forceLazyLoading, serializeIdentifier);
	}

	public TryUnwrappingWhenPossibleHibernateProxySerializer(boolean forceLazyLoading) {
		super(forceLazyLoading);
	}

	@Override
	protected JsonSerializer<Object> findSerializer(SerializerProvider provider, Object value)
	        throws IOException {
		JsonSerializer<Object> serializer = super.findSerializer(provider, value);
		return isUnwrapping ? serializer.unwrappingSerializer(NameTransformer.NOP) : serializer;
	}
	
	@Override
	public JsonSerializer<HibernateProxy> unwrappingSerializer(NameTransformer unwrapper) {
		return new TryUnwrappingWhenPossibleHibernateProxySerializer(
					_forceLazyLoading,
					_serializeIdentifier,
	                _mapping,
	                _property, 
	                true);
		
	}
	
	@Override
	public JsonSerializer<?> createContextual(SerializerProvider prov, BeanProperty property) {
        return new TryUnwrappingWhenPossibleHibernateProxySerializer(_forceLazyLoading, _serializeIdentifier,
                _mapping, property, isUnwrapping);
	}
	
	
	@Override
	public boolean isUnwrappingSerializer() {
		return isUnwrapping;
	}
	
	
}
