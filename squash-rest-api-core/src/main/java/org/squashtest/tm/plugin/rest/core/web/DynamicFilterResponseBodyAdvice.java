/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.core.web;

import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.servlet.mvc.method.annotation.AbstractMappingJacksonResponseBodyAdvice;
import org.squashtest.tm.plugin.rest.core.jackson.DynamicFilterExpression;
import org.squashtest.tm.plugin.rest.core.jackson.SerializationDynamicFilter;
import org.squashtest.tm.plugin.rest.core.utils.URIUtils;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.Map;


/**
 * If a filter expression is defined at the method level (via annotation {@link DynamicFilterExpression}) or as a request parameter (?fields=expression), 
 * will set the serialization context with a dynamic filter. If none of them is defined, no filter will be included. If both are defined, the request 
 * parameter takes precedence.
 * 
 * @author bsiri
 *
 */
public class DynamicFilterResponseBodyAdvice extends AbstractMappingJacksonResponseBodyAdvice {
	
	public static final String PARAM_NAME = "fields";

	private static final Logger LOGGER = LoggerFactory.getLogger(DynamicFilterResponseBodyAdvice.class);
	
	@Override
	protected void beforeBodyWriteInternal(MappingJacksonValue bodyContainer, MediaType contentType,
			MethodParameter returnType, ServerHttpRequest request, ServerHttpResponse response) {

		
		String filterExpression = extractFilterExpression(returnType, request);
		
		if (filterExpression == null){
			filterExpression = "*";
		}
		
		SimpleFilterProvider filterProvider = new SimpleFilterProvider();
		
		filterProvider.addFilter(SerializationDynamicFilter.FILTER_ID, new SerializationDynamicFilter(filterExpression));
		
		bodyContainer.setFilters(filterProvider);
	

	}
	

	private String extractFilterExpression(MethodParameter returnType, ServerHttpRequest request){
		String filterExpression = fromUri(request);
		if (filterExpression == null){
			filterExpression = fromAnnotation(returnType);
		}
		return filterExpression;
		
	}
	
	private String fromUri(ServerHttpRequest request){		
		try{	
			URI uri = request.getURI();			
			URL url = uri.toURL();
			Map<String, String> params = URIUtils.findQueryParameters(url);
			return params.get(PARAM_NAME);
		}
		catch(MalformedURLException | UnsupportedEncodingException ex){
			LOGGER.warn("could not extract query parameters because an exception occured : ",ex);
			return null;
		}
	}
	
	private String fromAnnotation(MethodParameter returnType){
		DynamicFilterExpression annot = returnType.getMethodAnnotation(DynamicFilterExpression.class);
		if (annot != null){
			return annot.value();
		}
		else{
			return null;
		}
	}

}
