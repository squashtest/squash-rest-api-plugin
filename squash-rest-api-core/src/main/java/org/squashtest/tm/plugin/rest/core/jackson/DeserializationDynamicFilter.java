/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.core.jackson;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by benoit on 16/07/17.
 */
public class DeserializationDynamicFilter {

    private static final Logger LOGGER = LoggerFactory.getLogger(DeserializationDynamicFilter.class);

    private BaseDynamicFilter filter;


    public DeserializationDynamicFilter(BaseDynamicFilter filter){
        super();
        this.filter = filter;
        this.filter.defineMandatoryProperties("_type");// "id" is no longer mandatory
    }

    public DeserializationDynamicFilter(){
        this("");
    }


    public DeserializationDynamicFilter(String filterExpression){
        this(new BaseDynamicFilter(FilterExpressionBuilder.from(filterExpression)));
    }

    public void setCurrentProperty(String pptName){
        filter.setCurrentProperty(pptName);
    }

    public boolean include(String pptName){
        return filter.include(pptName);
    }

    public FilterExpression getPointer(){
        return filter.getPointer();
    }

    public void advance(){
        filter.advancePointer();
    }

    public void reset(){
        filter.resetPointer();
    }

    public boolean isRoot() { return filter.isRootObject(); }
}
