/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.core.web;

import org.springframework.core.Conventions;
import org.springframework.core.MethodParameter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.ModelAndViewContainer;
import org.springframework.web.servlet.mvc.method.annotation.AbstractMessageConverterMethodArgumentResolver;
import org.springframework.web.servlet.mvc.method.annotation.RequestBodyAdvice;
import org.squashtest.tm.plugin.rest.core.jackson.DeserializationHints;
import org.squashtest.tm.plugin.rest.core.utils.DeserializationConfigHelper;
import org.squashtest.tm.plugin.rest.core.validation.HintedValidator;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.List;

/**
 * That argument resolver will process arguments annotated with {@link PersistentEntity}. This allows to invoke the ObjectMapper
 * with additional {@link com.fasterxml.jackson.databind.cfg.ContextAttributes} in the {@link com.fasterxml.jackson.databind.DeserializationContext},
 * that may then be used by deserialization utilities.
 *
 * Created by bsiri on 11/07/2017.
 */

/*
 * That class is a hack, that allows to convey bits information extracted from the original http request and consumed later in the
 * process (by the {@link DeserializationConfigurerRequestBodyAdvice} for instance). Indeed, at the time the advice is invoked the original http request
 * is no longer accessible because {@link org.springframework.web.servlet.mvc.method.annotation.AbstractMessageConverterMethodArgumentResolver}
 * wraps it into a  {@link org.springframework.web.servlet.mvc.method.annotation.AbstractMessageConverterMethodArgumentResolver.EmptyBodyCheckingHttpInputMessage}.
 *
 * Implementation is partially taken from RequestResponseBodyMethodProcessor.
 */
public class PersistentEntityArgumentResolver extends AbstractMessageConverterMethodArgumentResolver {

    private DeserializationConfigHelper helper;

    public PersistentEntityArgumentResolver(List<HttpMessageConverter<?>> converters, List<RequestBodyAdvice> requestResponseBodyAdvice) {
        super(converters, (List)requestResponseBodyAdvice);
    }

    public void setConfigHelper(DeserializationConfigHelper helper) {
        this.helper = helper;
    }

    @Override
    public boolean supportsParameter(MethodParameter parameter) {
        return parameter.hasParameterAnnotation(PersistentEntity.class);
    }


    @Override
    public Object resolveArgument(MethodParameter parameter, ModelAndViewContainer mavContainer, NativeWebRequest webRequest, WebDataBinderFactory binderFactory) throws Exception {

        HttpServletRequest servletRequest = webRequest.getNativeRequest(HttpServletRequest.class);

        DeserializationHints hints = helper.createHints(servletRequest, parameter);

        ContextualizedMethodParameter contextualizedParam = new ContextualizedMethodParameter(parameter,hints);

        Object arg = readWithMessageConverters(webRequest, contextualizedParam, parameter.getGenericParameterType());


        // prepare for the validation
        String modelName = Conventions.getVariableNameForParameter(parameter);
        WebDataBinder binder = binderFactory.createBinder(webRequest, arg, modelName);

        if (arg != null) {

            // add validation errors encountered during deserialization into the main bindingresult
            mergeBindingResults(binder, hints);

            // validate with regular and HintedValidator
            validateIfApplicable(binder, contextualizedParam);

            if (binder.getBindingResult().hasErrors() && isBindExceptionRequired(binder, parameter)) {
                throw new MethodArgumentNotValidException(parameter, binder.getBindingResult());
            }
        }
        mavContainer.addAttribute(BindingResult.MODEL_KEY_PREFIX + modelName, binder.getBindingResult());

        return arg;
    }

    @Override
    protected <T> Object readWithMessageConverters(NativeWebRequest webRequest, MethodParameter methodParam,
                                                   Type paramType) throws IOException, HttpMediaTypeNotSupportedException, HttpMessageNotReadableException {

        HttpServletRequest servletRequest = webRequest.getNativeRequest(HttpServletRequest.class);
        ServletServerHttpRequest inputMessage = new ServletServerHttpRequest(servletRequest);

        Object arg = readWithMessageConverters(inputMessage, methodParam, paramType);

        return arg;
    }


    private void mergeBindingResults(WebDataBinder binder, DeserializationHints hints){

        BindingResult deserializationResult = hints.getBindingResult();
        BindingResult binderResult = binder.getBindingResult();

        // the following method doesn't check for the model name of the binding results
        // if the merging seems incorrect, consider using : #addAllError, but you
        // should then ensure that the names matches (and build an appropriate
        // BindingResult in the deserializtaion hints
        for (ObjectError err : deserializationResult.getAllErrors()){
            binderResult.addError(err);
        }
    }


    @Override
    protected void validateIfApplicable(WebDataBinder binder, MethodParameter methodParam) {
        Class<?> targetClass = binder.getTarget().getClass();

        // first, the regular validators
        super.validateIfApplicable(binder, methodParam);

        // now the hinted validators
        Validated validatedAnn = methodParam.getParameterAnnotation(Validated.class);
        if (validatedAnn == null){
            return;
        }

        if (methodParam instanceof  ContextualizedMethodParameter){
            DeserializationHints hints = ((ContextualizedMethodParameter)methodParam).getHints();
            for (Validator validator : binder.getValidators()){

                if (HintedValidator.class.isAssignableFrom(validator.getClass()) &&
                    validator.supports(targetClass)){

                    ((HintedValidator)validator).validateWithHints(binder.getTarget(), binder.getBindingResult(), hints);

                }
            }
        }
    }
}
