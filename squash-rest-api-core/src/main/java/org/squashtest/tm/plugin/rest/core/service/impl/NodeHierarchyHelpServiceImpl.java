/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.core.service.impl;

import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.domain.campaign.CampaignLibraryNode;
import org.squashtest.tm.domain.library.LibraryNode;
import org.squashtest.tm.domain.library.NodeContainer;
import org.squashtest.tm.domain.requirement.Requirement;
import org.squashtest.tm.domain.requirement.RequirementLibraryNode;
import org.squashtest.tm.domain.testcase.TestCaseFolder;
import org.squashtest.tm.domain.testcase.TestCaseLibraryNode;
import org.squashtest.tm.plugin.rest.core.service.NodeHierarchyHelpService;
import org.squashtest.tm.service.internal.repository.CampaignFolderDao;
import org.squashtest.tm.service.internal.repository.CampaignLibraryDao;
import org.squashtest.tm.service.internal.repository.RequirementDao;
import org.squashtest.tm.service.internal.repository.RequirementFolderDao;
import org.squashtest.tm.service.internal.repository.RequirementLibraryDao;
import org.squashtest.tm.service.internal.repository.TestCaseFolderDao;
import org.squashtest.tm.service.internal.repository.TestCaseLibraryDao;

import javax.inject.Inject;

//@Service
// now created by ApiWebConfig
@Transactional(readOnly=true)
public class NodeHierarchyHelpServiceImpl implements NodeHierarchyHelpService{

	@Inject
	private TestCaseLibraryDao tclibDao;
	
	@Inject
	private TestCaseFolderDao tcfoldDao;

	@Inject
	private CampaignLibraryDao campaignLibraryDao;
	
	@Inject
	private RequirementLibraryDao reqlibDao;
	
	@Inject
	private RequirementFolderDao reqfoldDao;

	@Inject
	private CampaignFolderDao campaignFolderDao;
	
	@Inject
	private RequirementDao reqDao;
	
	
	@Override
	public NodeContainer<? extends LibraryNode> findParentFor(TestCaseLibraryNode tcnode) {
		TestCaseFolder fold = tcfoldDao.findByContent(tcnode);
		return (fold != null) ? fold : tclibDao.findByRootContent(tcnode);
	}

	
	@Override
	public NodeContainer<? extends LibraryNode> findParentFor(RequirementLibraryNode<?> reqNode) {
		
		NodeContainer<? extends LibraryNode> container = null;

		container = reqfoldDao.findByContent(reqNode);
		
		if (container == null){
			container =  reqlibDao.findByRootContent(reqNode);
		}
		
		if (container == null && Requirement.class.isAssignableFrom(reqNode.getClass())){
			container = reqDao.findByContent((Requirement)reqNode);
		}
		
		return container;
	}

	@Override
	public NodeContainer<? extends LibraryNode> findParentFor(CampaignLibraryNode cln) {
		NodeContainer<? extends LibraryNode> container = campaignFolderDao.findByContent(cln);
		return (container != null) ? container : campaignLibraryDao.findByRootContent(cln);
	}

}
