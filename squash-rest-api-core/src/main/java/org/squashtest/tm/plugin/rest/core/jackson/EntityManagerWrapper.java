/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.core.jackson;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Created a wrapper around EntityManager for testability, because @{@link PersistenceContext} is too hard to mock while
 * mocking that service is easier.
 *
 * Created by bsiri on 10/07/2017.
 */
public class EntityManagerWrapper {

    @PersistenceContext
    private EntityManager em;

    public <TYPE> TYPE find(Class<TYPE> clazz, Object primaryKey){
        return em.find(clazz, primaryKey);
    }

}
