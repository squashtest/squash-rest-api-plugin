/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.core.configuration;

import org.springframework.stereotype.Service;
import org.squashtest.tm.plugin.rest.core.utils.ExceptionUtils;
import org.squashtest.tm.service.security.PermissionEvaluationService;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Class used to perform existence and permission checks for API services.
 *  Standard hasPermission spel expression were not suited because in the API we want to make distinction between :
 *  Entity not found witch must return a 404 and a user friendly error message
 *  Access denied witch must return a 403 and a standard Access Denied error message
 *
 *  Even if this behavior can be considered as a security problem we choose to promote user experience on this point.
 *
 *  You can use this class by direct injection or with a spel expression like "@apiSecurity.check(#testCaseId,'org.squashtest.tm.domain.testcase.TestCase' , 'READ')"
 *
 * Created by jthebault on 31/05/2017.
 */
@Service("apiSecurity")
public class ApiPermissionEvaluationService {

    @PersistenceContext
    private EntityManager entityManager;

    @Inject
    private PermissionEvaluationService permissionEvaluationService;

    public boolean hasPermission(Object target, String permission){
        return permissionEvaluationService.hasRoleOrPermissionOnObject("ROLE_ADMIN", permission, target);
    }

    public boolean hasPermission(long entityId, String entityClassName, String permission){
        try {
            Class<?> aClass = Thread.currentThread().getContextClassLoader().loadClass(entityClassName);
            Object entity = entityManager.find(aClass, entityId);
            if (entity == null){
                throw ExceptionUtils.entityNotFoundException(aClass, entityId);
            }
            if (! permissionEvaluationService.hasRoleOrPermissionOnObject("ROLE_ADMIN",permission,entity)){
                return false;
            }
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
        return true;
    }
}
