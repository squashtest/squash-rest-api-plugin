/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.core.hateoas;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.BeanProperty;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.springframework.hateoas.hal.Jackson2HalModule.HalResourcesSerializer;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>That class process HAL resources, with a particular treatment for instances of {@link SingleRelHateoasResources}. </p>
 *
 * <p>
 *     When the resource under processing is a regular Hateoas resource, the treatment is delegated to an instance of
 * 		{@link HalResourcesSerializer} (ie Spring defaults).
 * 	</p>
 *
 * <p>
 *     However, when the resource is a collection of heterogeneous entities, the default HalResourcesSerializer will serialize
 *     it as a map in which entities will be partitioned by type. This is not always desirable for us, mostly because
 *     such collections often represent polymorphic entities belonging to some container, eg TestCase and TestCaseFolder
 *     that belong to a folder. It makes more sense in that case to serialize them all within a single collection, using
 *     a rel name that conveys that nuance better. This alternate behavior is triggered when the resource implements
 *     the interface {@link SingleRelHateoasResources}, and the rel name used is that returned by {@link SingleRelHateoasResources#getRel()}
 * </p>
 *
 * 
 * @author bsiri
 *
 */
@SuppressWarnings("serial")
public class SingleRelHalResourcesSerializer extends HalResourcesSerializer {

	private HalResourcesSerializer delegate;
	private BeanProperty property;
	
	public SingleRelHalResourcesSerializer(HalResourcesSerializer delegate){
		this(delegate, null);
	}
	
	public SingleRelHalResourcesSerializer(HalResourcesSerializer delegate, BeanProperty property){
		super(null);
		this.delegate = delegate;
		this.property = property;
	}
	
	@Override
	public void serialize(Collection<?> value, JsonGenerator jgen, SerializerProvider provider)
			throws IOException, JsonGenerationException {
		
		Object currentValue = jgen.getCurrentValue();
		
		if (currentValue instanceof SingleRelHateoasResources){
			SingleRelHateoasResources<?> res = (SingleRelHateoasResources<?>)currentValue;
			Map<String, Collection<?>> embeddeds = new HashMap<>();
			
			embeddeds.put(res.getRel(), value);
			
			provider.findValueSerializer(Map.class, property).serialize(embeddeds, jgen, provider);
			
		}
		else{
			delegate.serialize(value, jgen, provider);
		}
		
	}
	
	@Override
	public JsonSerializer<?> createContextual(SerializerProvider prov, BeanProperty property)
			throws JsonMappingException {
		HalResourcesSerializer contextualized = (HalResourcesSerializer)delegate.createContextual(prov, property);
		return new SingleRelHalResourcesSerializer(contextualized, property);
	}
	
}
