/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.core.web;

import org.springframework.core.MethodParameter;
import org.squashtest.tm.plugin.rest.core.jackson.DeserializationHints;

/**
 * Basically, it is a {@link MethodParameter} that carries the {@link org.squashtest.tm.plugin.rest.core.jackson.DeserializationHints} so that
 * the {@link DeserializationConfigurationRequestBodyAdvice} can put them in the {@link DeserializationConfigurationInputMessage}.
 *
 * Created by bsiri on 11/07/2017.
 */
public class ContextualizedMethodParameter extends MethodParameter {

    private DeserializationHints hints;

    public ContextualizedMethodParameter(MethodParameter original) {
        super(original);
    }

    public ContextualizedMethodParameter(MethodParameter original, DeserializationHints hints) {
        super(original);
        this.hints = hints;
    }

    public DeserializationHints getHints() {
        return hints;
    }

    public void setHints(DeserializationHints hints) {
        this.hints = hints;
    }
}
