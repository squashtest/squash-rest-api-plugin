/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.core.web;

import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

/**
 * @author qtran - created on 02/04/2020
 */
public class TestCaseTypeFilterArgumentResolver implements HandlerMethodArgumentResolver {
    private TestCaseTypeFilter defaultFilter = TestCaseTypeFilter.ALL;
    private String testCaseTypeFilter = UriComponents.TEST_CASE_TYPE_FILTER;

    @Override
    public boolean supportsParameter(MethodParameter parameter) {
        return TestCaseTypeFilter.class.equals(parameter.getParameterType());
    }

    @Override
    public Object resolveArgument(MethodParameter parameter, ModelAndViewContainer mavContainer,
              NativeWebRequest webRequest, WebDataBinderFactory binderFactory) throws Exception {
        String value = webRequest.getParameter(testCaseTypeFilter);

        if (value == null) {
            return defaultFilter;
        }

        try {
            return TestCaseTypeFilter.valueOf(value.toUpperCase());
        } catch (IllegalArgumentException ex) {
            String testCaseTypeFilterAll = UriComponents.TEST_CASE_TYPE_FILTER_ALL;
            String testCaseTypeFilterStandard = UriComponents.TEST_CASE_TYPE_FILTER_STANDARD;
            String testCaseTypeFilterScripted = UriComponents.TEST_CASE_TYPE_FILTER_SCRIPTED;
            String testCaseTypeFilterKeyword = UriComponents.TEST_CASE_TYPE_FILTER_KEYWORD;
            throw new IllegalArgumentException("Invalid value '"+value+"' for request parameter '" + testCaseTypeFilter+
                                                "'. Must be one of these values: '"+ testCaseTypeFilterAll +"', '"+ testCaseTypeFilterStandard +
                                                "', '"+ testCaseTypeFilterScripted +"', '"+ testCaseTypeFilterKeyword +"'");
        }
    }

    public TestCaseTypeFilter getDefaultFilter() {
        return defaultFilter;
    }

    public void setDefaultFilter(TestCaseTypeFilter defaultFilter) {
        this.defaultFilter = defaultFilter;
    }

    public String getTestCaseTypeFilter() {
        return testCaseTypeFilter;
    }

    public void setTestCaseTypeFilter(String testCaseTypeFilter) {
        this.testCaseTypeFilter = testCaseTypeFilter;
    }
}
