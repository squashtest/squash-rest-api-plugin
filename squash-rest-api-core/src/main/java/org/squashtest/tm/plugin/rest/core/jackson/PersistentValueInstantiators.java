/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.core.jackson;

import com.fasterxml.jackson.databind.BeanDescription;
import com.fasterxml.jackson.databind.DeserializationConfig;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.PropertyMetadata;
import com.fasterxml.jackson.databind.PropertyName;
import com.fasterxml.jackson.databind.deser.CreatorProperty;
import com.fasterxml.jackson.databind.deser.SettableBeanProperty;
import com.fasterxml.jackson.databind.deser.ValueInstantiator;
import com.fasterxml.jackson.databind.deser.ValueInstantiators;
import com.fasterxml.jackson.databind.deser.impl.PropertyValueBuffer;
import org.springframework.core.annotation.AnnotationUtils;
import org.squashtest.tm.domain.Identified;
import org.squashtest.tm.plugin.rest.core.jackson.DeserializationHints.Mode;

import javax.persistence.Entity;
import javax.persistence.EntityNotFoundException;
import java.io.IOException;




@SuppressWarnings("serial")
public class PersistentValueInstantiators extends ValueInstantiators.Base{

	private EntityManagerWrapper em;

	public PersistentValueInstantiators(EntityManagerWrapper emw){
		super();
		this.em = emw;
	}

	/*
	 * This value instantiator can apply only if it is a persistent entity, which implements Identified
	 */
	private boolean canApply(DeserializationConfig config, Class<?> beanClass){
		return (Identified.class.isAssignableFrom(beanClass) &&
					AnnotationUtils.findAnnotation(beanClass, Entity.class) != null
			);
	}

	@Override
	public ValueInstantiator findValueInstantiator(DeserializationConfig config, BeanDescription beanDesc,
			ValueInstantiator defaultInstantiator) {

		Class<?> beanClass = beanDesc.getBeanClass();
		if (canApply(config, beanClass)){
			return new PersistentValueInstantiator(em, beanClass, defaultInstantiator);
		}
		else{
			return defaultInstantiator;
		}
	}
    
	
	private static final class PersistentValueInstantiator extends ValueInstantiator {
		private EntityManagerWrapper em;
		private ValueInstantiator defaultInstantiator;
		private Class<?> beanClass;


		public PersistentValueInstantiator(EntityManagerWrapper em, Class<?> beanClass, ValueInstantiator defaultInstantiator) {
			this.em = em;
			this.defaultInstantiator = defaultInstantiator;
			this.beanClass = beanClass;
		}

		@Override
		public boolean canInstantiate() {
			return true;
		}

		@Override
		public boolean canCreateFromObjectWith() {
			return true;
		}

		@Override
		public Object createFromObjectWith(DeserializationContext ctxt, SettableBeanProperty[] props,
				PropertyValueBuffer buffer) throws IOException {

			DeserializationHints hints = (DeserializationHints) ctxt.getAttribute(DeserializationHints.HINTS_KEY);

			// Guard : if no hints available, take no risk and fall back to default behavior
			if (hints == null){
				return defaultInstantiator.createUsingDefault(ctxt);
			}

			Object instance = null;
			SettableBeanProperty idProp = props[0];
			Mode mode = hints.getMode();
			DeserializationDynamicFilter filter = hints.getFilter();

			//boolean isRoot = ctxt.getParser().getParsingContext().getParent() == null;
			boolean isRoot = filter.isRoot();

			// check if we can find it from the database (if an ID was supplied and ID is allowed at this location by the filter)
			if (buffer.hasParameter(idProp) && filter.include("id")){
				Long id = (Long)buffer.getParameter(idProp);
				instance = em.find(beanClass, id);
				if (instance == null){
					throw new EntityNotFoundException("No entity " + beanClass.getName() + " found with id : " + id);
				}
			}

			/*
			 * Otherwise, maybe we are patching an entity (in which case the id was in the URL, not the json payload).
			 * We must then check if the following is true :
			 * - the mode is DESER_UPDATE,
			 * - the deserialization context is the root context (no parent)
			 * - the target entity was set
			 */
			else if (hints.getMode() == DeserializationHints.Mode.DESERIALIZE_UPDATE && isRoot){
				instance = hints.getTargetEntity();
			}


			// else we are certainly creating a new instance, let's return one
			else{
				instance = defaultInstantiator.createUsingDefault(ctxt);
			}

			return instance;
		}

		@Override
		public Class<?> getValueClass() {
			return beanClass;
		}


		@Override
		// big thanks to JsonLocationInstantiator for explaining what to do with it
		public SettableBeanProperty[] getFromObjectArguments(DeserializationConfig config) {
			SettableBeanProperty[] properties = new SettableBeanProperty[1];
			properties[0] = new CreatorProperty(
				PropertyName.construct("id"),
				config.constructType(Long.TYPE),
				null,
				null,
				null,
				null,
				0,
				null,
				PropertyMetadata.STD_OPTIONAL
			);
			return properties;
		}


	}
	
}
