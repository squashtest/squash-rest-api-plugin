/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.core.jackson;

import org.apache.commons.lang3.StringUtils;
import org.squashtest.tm.plugin.rest.core.exception.UnbalancedFilterExpressionException;

import java.util.LinkedList;


/**
 *
 * <p>Utility class that builds {@link FilterExpression} from a string representation. That representation defines a white-list of properties that should be serialized or deserialized.</p>
 *
 * <p>NOTE : although the rest of this documentation is written with serialization in mind, the mechanic applies to deserialization too.</p>
 *
 * <p>
 * 		<em>Note : to prevent any confusion here we define 'relation' as the target of a functional bond between two entities, not the bond itself. For example, if A and B are bound, 
 * 		then the term 'relation' applies to B, and not the bond. It reads : 'B is a relation of A'.</em>
 * </p>
 * 
 * <p>
 * 		A string expression is a comma-separated list of property names. A star '*' is a wild card that stands for all the properties. If a property name is prefixed with a 
 * 		minus '-', that property will not be included even if the wildcard was specified. 
 * </p>
 * 
 * <p>
 * 		Filters can be nested using brackets '[' and ']'. If a property defines a nested filter, it will apply to the value(s) of that property. Of course this makes sense 
 * 		only if the property designates a complex object, a relation, or a collection of such : primitive types will remain unaffected. You can nest filters within nested filters 
 * 		and so on. 
 * </p>
 * 
 * <p>
 * 		When a filter is empty, every properties will be serialized if the filtered object is the root object, or none if it is a relation. This is different from the star operator '*' 
 * 		where every properties are serialized no matter what (root or not). This rule prevents Jackson running wild when serializing a circular object graph. However, because of this 
 * 		you must explicitly specify	a nested filter expression for properties that require it, otherwise they will appear empty (except for the mandatory properties, see below). You can
 * 		redeclare a property for that purpose even if the star operator is present.
 * </p>
 * 
 * <p>
 * 		In all cases, the properties '_type', 'id' and '_links' of an object will always be included for serialization(if relevant), and always ignored for deserialization.
 * 		Also, the properties that can be serialized are still bounded by the Jackson mixins and adding it to a filter cannot make it appear if it was not mixed in.
 * </p>
 * 
 * 
 *  <p>
 *  	Valid examples are : 
 *  
 *  	<table border="1px solid black;">
 *  		<tr>
 *  			<td>id,name,age</td>
 *  			<td>serializes only id, name and age.</td>
 *  		</tr>
 *  		<tr>
 *  			<td>id, age, games[name,clans[tagname, website]]</td>
 *  			<td>serializes the id, age, and game list. The games will be serialized with name and clan list. Clans are serialized with tagname and website.</td>
 *  		</tr>
 *  		<tr>
 *  			<td>*</td>
 *  			<td>serialize all the properties. However, the relation 'games' will remain empty because no subfilter was explicited for it.</td>
 *  		</tr>
 *  		<tr>
 *  			<td>*,games[name]</td>
 *  			<td>serialize every properties, and in the case of the games serialize its name.</td>
 *  		</tr>
 *  		<tr>
 *  			<td>*,-age</td>
 *  			<td>serialize all properties except the age </td>
 *  		</tr>
 *  	</table>
 *  </p>
 *  
 *  <p>
 *  	Invalid examples are :
 *  
 *  	<table border="1px solid black">
 *  		<tr>
 *  			<td>age,games[name</td><td> missing closing bracket, the expression is unbalanced</td>
 *  		</tr>
 *  		<tr>
 *  			
 *  			<td>age,games[name]],id</td><td>the root expression exited early</td>
 *  		</tr>
 *  	</table>
 *  </p>
 * 
 * @author bsiri
 *
 */
public class FilterExpressionBuilder {

	private static final String SPLIT_EXPR = "((?<=[\\[\\],])|(?=[\\[\\],]))";

	private static final String OPEN_SUBCONTEXT = "[";
	private static final String CLOSE_SUBCONTEXT = "]";
	private static final String SEPARATOR = ",";
	
	// input variables
	private FilterExpression properties;
	private int depth = 0;
	private LinkedList<String> tokens;

	// work variables
	private String currentProperty = null;
	private FilterExpression currentValue = null;
	private boolean subcontextClosed = false;

	

	public static FilterExpression from(String expression){
		FilterExpressionBuilder parser = new FilterExpressionBuilder(tokenize(expression));
		parser.process();
		return parser.getProperties();
	}
	
	private FilterExpressionBuilder(){
		super();
	}

	
	private static final LinkedList<String> tokenize(String dirtyExpression){
		String expression = dirtyExpression.replaceAll("\\s", ""); 
		if (StringUtils.isBlank(expression)){
			return new LinkedList<>();
		}
		
		String[] tokens = expression.split(SPLIT_EXPR);
		LinkedList<String> tokenList = new LinkedList<>();
		for (String tok : tokens) {
			tokenList.add(tok);
		}
		return tokenList;
	}
	
	private FilterExpressionBuilder(LinkedList<String> tokens) {
		this(tokens, 0);
	}

	private FilterExpressionBuilder(LinkedList<String> tokens, int depth) {
		this.tokens = tokens;
		this.depth = depth;
	}
	

	public FilterExpression getProperties() {
		return properties;
	}

	public boolean hasTerminatedNormally() {
		if (depth == 0){
			return tokens.isEmpty();
		}
		else{
			return subcontextClosed;
		}
	}

	public void process() {

		looping: while (!tokens.isEmpty()) {

			String token = tokens.removeFirst();

			switch (token) {

			// separator : store the property and value, then move to a new property
			case SEPARATOR:
				storeProperty();
				break;

			// subcontext encountered : parse it
			case OPEN_SUBCONTEXT:
				FilterExpressionBuilder subcontext = new FilterExpressionBuilder(tokens, depth + 1);
				subcontext.process();
				currentValue = subcontext.getProperties();
				break;

			// ending of the subcontext : this instance has finished its job
			case CLOSE_SUBCONTEXT:
				subcontextClosed = true;
				break looping;

			// default case is that we encountered a property name : set it
			default:
				currentProperty = token;
				currentValue = null;
				break;
			}

		}
	
		// store the property that was being processed here, otherwise the 
		// last property before termination will be missing
		storeProperty();

		// check that the parsing has ended normally
		if (! hasTerminatedNormally()) {
			throw new UnbalancedFilterExpressionException("unbalanced expression");
		}
	}
	
	
	private void storeProperty(){
		if (currentProperty == null){
			return;
		}
		
		if (properties == null){
			properties = new FilterExpression();
		}
		
		properties.put(currentProperty, currentValue);
		
		currentProperty = null;
		currentValue = null;
	}
	

	/**
	 * PostProcessing step
	 */
	private void postProcess(){
		
	}

}
