/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.core.web;

import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.AbstractJackson2HttpMessageConverter;
import org.springframework.web.context.request.WebRequestInterceptor;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.handler.AbstractHandlerMapping;
import org.springframework.web.servlet.mvc.condition.PatternsRequestCondition;
import org.springframework.web.servlet.mvc.condition.ProducesRequestCondition;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;
import org.squashtest.tm.plugin.rest.core.configuration.RestApiProperties;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;


/**
 * Code here heavily inspired by BasePathAwareHandlerMapping and RepositoryRestHandlerMapping, both from Spring Data Rest project.
 * 
 * @author bsiri
 *
 */
public class RestApiHandlerMapping extends RequestMappingHandlerMapping{

	private static final MediaType EVERYTHING_JSON_MEDIA_TYPE = new MediaType("application", "*+json",
			AbstractJackson2HttpMessageConverter.DEFAULT_CHARSET);
	
	private RestApiProperties properties;
	private List<Object> extraInterceptors = new ArrayList<>();
	
	private String basePath;
	
	public RestApiHandlerMapping(RestApiProperties properties){
		super();
		this.properties = properties;
	}

	
	
	/**
	 * Fetched and adapted from BasePathAwareHandlerMapping#getMappingForMethod()
	 * 
	 */
	@Override
	protected RequestMappingInfo getMappingForMethod(Method method, Class<?> handlerType) {
		
		RequestMappingInfo info = super.getMappingForMethod(method, handlerType);
		
		if (info == null){
			return null;
		}
		
		PatternsRequestCondition patternsCondition = customize(info.getPatternsCondition(), basePath);
		ProducesRequestCondition producesCondition = info.getProducesCondition();
		
		/*
		 * add extra configuration on the produce condition, only if the developper explicitly asked for it 
		 */
		if (AnnotationUtils.findAnnotation(handlerType, UseDefaultRestApiConfiguration.class) != null){
			producesCondition = customize(info.getProducesCondition());
		}
		
		return new RequestMappingInfo(patternsCondition, info.getMethodsCondition(), info.getParamsCondition(),
				info.getHeadersCondition(), info.getConsumesCondition(), producesCondition, info.getCustomCondition());
	}
	
	
	/**
	 * Copy/pasted from BasePathAwareHandlerMapping#customize
	 * 
	 * @param condition
	 * @param prefix
	 * @return
	 */
	protected PatternsRequestCondition customize(PatternsRequestCondition condition, String prefix) {

		Set<String> patterns = condition.getPatterns();
		String[] augmentedPatterns = new String[patterns.size()];
		int count = 0;

		for (String pattern : patterns) {
			augmentedPatterns[count++] = prefix.concat(pattern);
		}

		return new PatternsRequestCondition(augmentedPatterns, getUrlPathHelper(), getPathMatcher(),
				useSuffixPatternMatch(), useTrailingSlashMatch(), getFileExtensions());
	}
	
	
	/**
	 * adapted from RepositoryRestHandlerMapping#customize
	 * 
	 * @param condition
	 * @return
	 */
	
	protected ProducesRequestCondition customize(ProducesRequestCondition condition) {

		if (!condition.isEmpty()) {
			return condition;
		}

		HashSet<String> mediaTypes = new LinkedHashSet<String>();
		mediaTypes.add(MediaType.APPLICATION_JSON_VALUE);
		mediaTypes.add(EVERYTHING_JSON_MEDIA_TYPE.toString());

		return new ProducesRequestCondition(mediaTypes.toArray(new String[mediaTypes.size()]));
	}
	

	//**************** configuration methods ***********************
	
	/**
	 * According to the code, the type of "interceptor" is either {@link HandlerInterceptor} or {@link WebRequestInterceptor} 
	 * (@see {@link AbstractHandlerMapping#adaptInterceptor(Object)}
	 * 
	 * @param interceptor
	 */
	public void addInterceptor(Object interceptor){
		extraInterceptors.add(interceptor);
	}
	
	/**
	 * Only classes explicitly annotated {@link RestApiController} will be considered
	 * 
	 */
	@Override
	protected boolean isHandler(Class<?> beanType){
		return AnnotationUtils.findAnnotation(beanType, RestApiController.class) != null;
	}
	
	@Override
	protected void extendInterceptors(List<Object> interceptors) {
		interceptors.addAll(extraInterceptors);
	}
	
	@Override
	public void afterPropertiesSet() {		
		this.basePath = properties.getNormalizedBasePath();
		super.afterPropertiesSet();		
	}
	
}
