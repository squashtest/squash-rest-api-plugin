/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.core.web;

import org.springframework.data.domain.Page;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.LinkBuilder;
import org.springframework.hateoas.PagedResources;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.squashtest.tm.domain.Identified;
import org.squashtest.tm.plugin.rest.core.hateoas.SingleRelPagedResources;

import javax.inject.Inject;

public abstract class BaseRestController {
	
	@Inject
	protected BasePathAwareLinkBuildingService linkService;

	@Inject
	protected PagedResourcesAssembler pageAssembler;
	
	@Inject
	private BasicResourceAssembler resAssembler;
	
	
	protected <ENTITY extends Identified> LinkBuilder fromBasePath(ControllerLinkBuilder builder){
		return linkService.fromBasePath(builder);
	}
	
	protected <ENTITY extends Identified> Link createSelfLink(ENTITY entity){
		return linkService.createSelfLink(entity);
	}
	
	protected <ENTITY extends Identified> Link createLinkTo(ENTITY entity){
		return linkService.createLinkTo(entity);
	}
	
	protected <ENTITY extends Identified> Link createLinkTo(Class<ENTITY> clazz, Long id){
		return linkService.createLinkTo(clazz, id);
	}
	
	protected <ENTITY extends Identified> Link createRelationTo(String relname){
		return linkService.createRelationRelativeToCurrentUri(relname);
	}

	protected <ENTITY extends Identified> Link createRelationTo(ENTITY entity, String relname){
		return linkService.createRelationTo(entity, relname);
	}


	protected <ENTITY extends Identified> Resource<ENTITY> toResource(ENTITY object){
		return (Resource<ENTITY>) resAssembler.toResource((Identified)object);
	}
	
	protected <T> PagedResources<Resource> toPagedResources(Page page){
		return pageAssembler.toResource(page,resAssembler);
	}
	
	protected <T> SingleRelPagedResources<Resource> toPagedResourcesWithRel(Page page, String rel){
		PagedResources<Resource> res = pageAssembler.toResource(page,resAssembler);
		return new SingleRelPagedResources<Resource>(res, rel);
	}


}
