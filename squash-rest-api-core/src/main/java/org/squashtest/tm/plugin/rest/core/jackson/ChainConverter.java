/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.core.jackson;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.fasterxml.jackson.databind.util.Converter;
import com.fasterxml.jackson.databind.util.StdConverter;
import org.squashtest.tm.plugin.rest.core.exception.ProgrammingError;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 		Will chain multiple {@link Converter} together, when an annotation {@link SerializeChainConverter} or 
 * {@link DeserializeChainConverter} 
 * is encountered by the {@link StackableAnnotationIntrospector}. 
 * </p>
 * 
 * @author bsiri
 *
 * @param <IN>
 * @param <OUT>
 */

/*
 *  	Note : the annotation introspector could theoretically create the 
 *  	instance itself, but it would need a handle on the beanfactory. That's why the job is handled by 
 *  the SquashRestHalHandlerInstantiator instead.
 */
public abstract class ChainConverter<IN,OUT> extends  StdConverter<IN, OUT>{
	
	@SuppressWarnings("unchecked")
	private static final Class<? extends Converter>[] EMPTY_CONVERTERS = new Class[0];
	
	private List<Converter> chain = new ArrayList<>();
	
	public ChainConverter(){
		super();
	}

	public ChainConverter(List<Converter> converters){
		super();
		chain = converters;
	}
	
	public void addConverter(Converter converter){
		chain.add(converter);
	}
	
	/**
	 * Given a {@link SerializeChainConverter} and a {@link DeserializeChainConverter}, returns which converter this chain is made of. Specializations of 
	 * ChainConverter will extract different informations.
	 * 
	 * @param annotation
	 * @return
	 */
	public abstract Class<? extends Converter>[] convertersFrom(SerializeChainConverter ser, DeserializeChainConverter deser);


	@Override
	@SuppressWarnings({"unchecked", "rawtypes"})
	public OUT convert(IN value) {
		Object res = value;
		for (Converter converter : chain){
			res = converter.convert(res);
		}
		return (OUT)res;
	}

    @Override
    public JavaType getInputType(TypeFactory typeFactory) {
    	checkIsWellFormed();
    	Converter firstConv = chain.get(0);
        return _findConverterType(firstConv, typeFactory).containedType(0);
    }

    @Override
    public JavaType getOutputType(TypeFactory typeFactory) {
    	checkIsWellFormed();
    	Converter lastConv = chain.get(chain.size()-1);
        return _findConverterType(lastConv, typeFactory).containedType(1);
    }
	
	// extracted and modified from StdConverter
    protected JavaType _findConverterType(Converter conv, TypeFactory tf) {
        JavaType thisType = tf.constructType(conv.getClass());
        JavaType convType = thisType.findSuperType(Converter.class);
        if (convType == null || convType.containedTypeCount() < 2) {
            throw new IllegalStateException("Can not find OUT type parameter for Converter of type "+getClass().getName());
        }
        return convType;
    }

	public void checkIsWellFormed(){
		if (chain.isEmpty()){
			throw new ProgrammingError("declared an empty ChainConverter");
		}
	}

	// ******************** specializations ************************
	
	/*
	 * The various null checks below are probably superflous
	 */
	
	public static final class SerialChainConverter<IN,OUT> extends ChainConverter<IN, OUT>{
		@Override
		public Class<? extends Converter>[] convertersFrom(SerializeChainConverter ser,
				DeserializeChainConverter deser) {
			return (ser != null) ? ser.converters() : EMPTY_CONVERTERS;
		}

	}
	
	public static final class SerialContentChainConverter<IN,OUT> extends ChainConverter<IN, OUT>{
		@Override
		public Class<? extends Converter>[] convertersFrom(SerializeChainConverter ser,
				DeserializeChainConverter deser) {
			return (ser != null) ? ser.contentConverters() : EMPTY_CONVERTERS;
		}
	}
	
	
	public static final class DeserialChainConverter<IN,OUT> extends ChainConverter<IN, OUT>{
		@Override
		public Class<? extends Converter>[] convertersFrom(SerializeChainConverter ser,
				DeserializeChainConverter deser) {
			return (deser != null) ? deser.converters() : EMPTY_CONVERTERS;
		}
	}
	
	
	public static final class DeserialContentChainConverter<IN,OUT> extends ChainConverter<IN, OUT>{
		@Override
		public Class<? extends Converter>[] convertersFrom(SerializeChainConverter ser,
				DeserializeChainConverter deser) {
			return (deser != null) ? deser.contentConverters() : EMPTY_CONVERTERS;
		}
	}
	
}
