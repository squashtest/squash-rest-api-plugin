/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.core.jackson;

import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.PropertyMetadata;
import com.fasterxml.jackson.databind.PropertyName;
import com.fasterxml.jackson.databind.annotation.JsonAppend;
import com.fasterxml.jackson.databind.cfg.HandlerInstantiator;
import com.fasterxml.jackson.databind.cfg.MapperConfig;
import com.fasterxml.jackson.databind.introspect.Annotated;
import com.fasterxml.jackson.databind.introspect.AnnotatedClass;
import com.fasterxml.jackson.databind.introspect.AnnotatedMember;
import com.fasterxml.jackson.databind.introspect.NopAnnotationIntrospector;
import com.fasterxml.jackson.databind.introspect.VirtualAnnotatedMember;
import com.fasterxml.jackson.databind.ser.BeanPropertyWriter;
import com.fasterxml.jackson.databind.ser.VirtualBeanPropertyWriter;
import com.fasterxml.jackson.databind.util.ClassUtil;
import com.fasterxml.jackson.databind.util.SimpleBeanPropertyDefinition;
import org.squashtest.tm.plugin.rest.core.jackson.ChainConverter.DeserialChainConverter;
import org.squashtest.tm.plugin.rest.core.jackson.ChainConverter.DeserialContentChainConverter;
import org.squashtest.tm.plugin.rest.core.jackson.ChainConverter.SerialChainConverter;
import org.squashtest.tm.plugin.rest.core.jackson.ChainConverter.SerialContentChainConverter;

import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * <p>
 * The default policy regarding annotations and inheritance is the following :
 * <ul>
 * <li>annotations in subclasses stack with annotations in superclasses, as long
 * as they are different.</li>
 * <li>if annotations are redeclared, the new definition erases the former
 * one</li>
 * </ul>
 * </p>
 * 
 * <p>
 * The late point poses us a problem because we actually want some annotations
 * (at least {@link JsonAppend}) to stack, hence this extra introspector.
 * </p>
 * 
 * @author bsiri
 *
 */
@SuppressWarnings("serial")
public class StackableAnnotationIntrospector extends NopAnnotationIntrospector {
	
	@Override
	public Version version() {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * This method handles the case of @JsonAppend, aka virtual (extra) properties.
	 * 
	 * Implementation adapted from JacksonAnnotationIntrospector#findAndAddVirtualProperties. 
	 * Here we do not consider the annotation on the current class itself, and also we consider
	 * only JsonAppend that uses @Prop definitions, not @Attr.
	 */
	@Override
	public void findAndAddVirtualProperties(MapperConfig<?> config, AnnotatedClass ac,
			List<BeanPropertyWriter> properties) {
		
		List<JsonAppend> allAppends = findAllJsonAppend(config, ac);
		
		if (allAppends.isEmpty()){
			return;
		}
		
		/*
		 * Because this introspector would potentially collect @JsonAppend.Prop that were already processed 
		 * by the primary introspector, we must defend against double-inclusion of the same virtual properties.
		 * The Set declared here will  help test that a property writer will not be added if it exist already. 
		 * In effect, it means that the most specific declaration of a virtual property will remain. 
		 */
		Set<PropertyName> propertyNames = getPropertyNames(properties);
		
		for (JsonAppend ann : allAppends){
			final boolean prepend = ann.prepend();
	
			JsonAppend.Prop[] props = ann.props();
			for (int i = 0, len = props.length; i < len; ++i) {
				
				JsonAppend.Prop prop = props[i];
				
				// check against double inclusion
				PropertyName name = _propertyName(prop.name(), prop.namespace());
				if (propertyNames.contains(name)){
					continue;
				}
				
				// create and add the property writer
				BeanPropertyWriter bpw = _constructVirtualProperty(props[i], config, ac);
				
				if (prepend) {
					properties.add(i, bpw);
				} else {
					properties.add(bpw);
				}
			}
		}

	}
	

	@Override
	public Object findSerializationConverter(Annotated a) {
		SerializeChainConverter ann = _findAnnotation(a, SerializeChainConverter.class);
		if (ann == null || ann.converters().length ==0){
			return null;
		}
		else{
			return SerialChainConverter.class;
		}
	}
	

	@Override
	public Object findSerializationContentConverter(AnnotatedMember a) {
		SerializeChainConverter ann = _findAnnotation(a, SerializeChainConverter.class);
		if (ann == null || ann.contentConverters().length ==0){
			return null;
		}
		else{
			return SerialContentChainConverter.class;
		}
	}
	
	@Override
	public Object findDeserializationConverter(Annotated a) {
		DeserializeChainConverter ann = _findAnnotation(a, DeserializeChainConverter.class);
		if (ann == null || ann.converters().length ==0){
			return null;
		}
		else{
			return DeserialChainConverter.class;
		}
	}
	
	@Override
	public Object findDeserializationContentConverter(AnnotatedMember a) {
		DeserializeChainConverter ann = _findAnnotation(a, DeserializeChainConverter.class);
		if (ann == null || ann.contentConverters().length ==0){
			return null;
		}
		else{
			return DeserialContentChainConverter.class;
		}
	}
	
	// ********************* private : JsonAppend *************************
	

	
	/*
	 * Look for all instances of {@link JsonAppend} in the ancestry of that
	 * class. Cautious : by ancestry, we actually mean : the ancestry of the
	 * mixins, not the class itself. Indeed We leverage the assumption that
	 * Squash domain classes do not have Jackson annotation on them directly,
	 * and all serialization informations are held by mixins.
	 * 
	 * 
	 * @param config
	 * 
	 * @param ac
	 * 
	 * @return
	 */
	/*
	 * Note : if some day we require to consider Jackson annotations on the
	 * class hierarchy of the actual class itself (not just the mixins), please
	 * look at the implementation of AnnotatedClass._resolveClassAnnotations
	 */
	/*
	 * The code below is liberally adapted from AnnotatedClass._addClassMixIns.
	 */
	private List<JsonAppend> findAllJsonAppend(MapperConfig<?> config, AnnotatedClass ac) {

		Class<?> underIntrospection = ac.getAnnotated();

		Class<?> mixin = config.findMixInClassFor(underIntrospection);
		if (mixin == null) {
			return Collections.emptyList();
		}

		List<JsonAppend> appends = new ArrayList<>();
		for (Class<?> parent : ClassUtil.findSuperClasses(mixin, underIntrospection, false)) {
			for (Annotation an : ClassUtil.findClassAnnotations(parent)) {
				if (an instanceof JsonAppend) {
					appends.add((JsonAppend) an);
				}
			}
		}

		return appends;
	}
	
	
	/*
	 * Copy/Pasta of the same method in JacksonAnnotationIntrospector
	 */
	protected BeanPropertyWriter _constructVirtualProperty(JsonAppend.Prop prop, MapperConfig<?> config,
			AnnotatedClass ac) {
		PropertyMetadata metadata = prop.required() ? PropertyMetadata.STD_REQUIRED : PropertyMetadata.STD_OPTIONAL;
		PropertyName propName = _propertyName(prop.name(), prop.namespace());
		JavaType type = config.constructType(prop.type());
		// now, then, we need a placeholder for member (no real Field/Method):
		AnnotatedMember member = new VirtualAnnotatedMember(ac, ac.getRawType(), propName.getSimpleName(), type);
		// and with that and property definition
		SimpleBeanPropertyDefinition propDef = SimpleBeanPropertyDefinition.construct(config, member, propName,
				metadata, prop.include());

		Class<?> implClass = prop.value();

		HandlerInstantiator hi = config.getHandlerInstantiator();
		VirtualBeanPropertyWriter bpw = (hi == null) ? null : hi.virtualPropertyWriterInstance(config, implClass);
		if (bpw == null) {
			bpw = (VirtualBeanPropertyWriter) ClassUtil.createInstance(implClass, config.canOverrideAccessModifiers());
		}

		// one more thing: give it necessary contextual information
		return bpw.withConfig(config, ac, propDef, type);
	}

	/*
	 * Copy/Pasta of the same method in JacksonAnnotationIntrospector
	 */
	protected PropertyName _propertyName(String localName, String namespace) {
		if (localName.isEmpty()) {
			return PropertyName.USE_DEFAULT;
		}
		if (namespace == null || namespace.isEmpty()) {
			return PropertyName.construct(localName);
		}
		return PropertyName.construct(localName, namespace);
	}
	
	
	private Set<PropertyName> getPropertyNames(List<BeanPropertyWriter> writers){
		Set<PropertyName> names = new HashSet<>();
		for (BeanPropertyWriter writer : writers){
			names.add(writer.getFullName());
		}
		return names;
	}

}
