/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.core.web;

import com.fasterxml.jackson.databind.JavaType;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.http.converter.json.MappingJacksonInputMessage;
import org.squashtest.tm.plugin.rest.core.jackson.DeserializationDynamicFilter;
import org.squashtest.tm.plugin.rest.core.jackson.DeserializationHints;

import java.io.IOException;
import java.lang.reflect.Type;

/**
 * That message converter works like a regular {@link MappingJackson2HttpMessageConverter}, except that
 * it can also process {@link org.squashtest.tm.plugin.rest.core.jackson.DeserializationHints}.
 *
 * Created by bsiri on 11/07/2017.
 */
public class PersistentEntityJacksonHttpMessageConverter extends MappingJackson2HttpMessageConverter{

    @Override
    protected Object readInternal(Class<?> clazz, HttpInputMessage inputMessage)
        throws IOException, HttpMessageNotReadableException {

        JavaType javaType = getJavaType(clazz, null);
        return readJavaType(javaType, inputMessage);
    }

    @Override
    public Object read(Type type, Class<?> contextClass, HttpInputMessage inputMessage)
        throws IOException, HttpMessageNotReadableException {

        JavaType javaType = getJavaType(type, contextClass);
        return readJavaType(javaType, inputMessage);
    }

    private Object readJavaType(JavaType javaType, HttpInputMessage inputMessage) {
        try {
            if (inputMessage instanceof DeserializationConfigurationInputMessage){

                DeserializationHints hints = ((DeserializationConfigurationInputMessage) inputMessage).getHints();

                DeserializationDynamicFilter filter = hints.getFilter();

                /*
                 * Note : there is a method .withValueToUpdate(toUpdate) that allows to directly set the entity that must
                 * be updated. But it has a disappointing result when the expected type is WraperDTO, because in that case it asks the
                 * PersistentValueInstantiator to load it anyway. So I just removed the code and let PersistentValueInstantiator
                 * do the job.
                 */
               return this.objectMapper
                        .reader()
                        .withAttribute(DeserializationHints.HINTS_KEY, hints)
                        .forType(javaType)
                        .readValue(inputMessage.getBody());


            }

            else if (inputMessage instanceof MappingJacksonInputMessage) {
                Class<?> deserializationView = ((MappingJacksonInputMessage) inputMessage).getDeserializationView();
                if (deserializationView != null) {
                    return this.objectMapper
                               .readerWithView(deserializationView)
                               .forType(javaType)
                               .readValue(inputMessage.getBody());
                }
            }
            return this.objectMapper.readValue(inputMessage.getBody(), javaType);
        }
        catch (IOException ex) {
            throw new HttpMessageNotReadableException("Could not read document: " + ex.getMessage(), ex);
        }
    }

}
