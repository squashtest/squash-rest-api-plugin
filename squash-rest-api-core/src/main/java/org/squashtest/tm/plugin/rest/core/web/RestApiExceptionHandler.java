/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.core.web;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.persistence.EntityNotFoundException;

@ControllerAdvice(assignableTypes=BaseRestController.class)
public class RestApiExceptionHandler {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(RestApiExceptionHandler.class);

	/**
	 * Catch-all exceptions, if no other more specific handler could do the job
	 * 
	 * @param ex
	 * @return
	 */
	@ExceptionHandler({RuntimeException.class})
	ResponseEntity<?> catchAll(Exception ex){
		LOGGER.error("wrapped-exception handler invoked, attempting to resolve the root exception");
		
		return dispatch(ex);
	}
	
	@ExceptionHandler
	ResponseEntity<?> handleNotFound(EntityNotFoundException ex){
		LOGGER.error(ex.getMessage(), ex);
		return ResponseEntity
					.status(HttpStatus.NOT_FOUND)
					.body(new JsonException(ex));
	}
	
	@ExceptionHandler
	ResponseEntity<?> handleAccessDenied(AccessDeniedException ex){
		LOGGER.error(ex.getMessage(), ex);
		return ResponseEntity
				.status(HttpStatus.FORBIDDEN)
				.body(new JsonException(ex));
	}
	
	private ResponseEntity<?> handleAnyException(Exception ex){
		if (LOGGER.isErrorEnabled()){
			LOGGER.error("resolving exception '"+ex.getClass().getName()+"' with the catch-all handler",ex);
		}
		return ResponseEntity
					.status(HttpStatus.INTERNAL_SERVER_ERROR)
					.body(new JsonException(ex));
	}
	
	
	private ResponseEntity<?> dispatch(Exception unqualifiedException){
		Throwable root = findRootException(unqualifiedException);
		
		if (EntityNotFoundException.class.isAssignableFrom(root.getClass())){
			return handleNotFound((EntityNotFoundException)root);
		}
		
		else{
			return handleAnyException((Exception)root);
		}
	}
	
	private Throwable findRootException(Exception caught){
		Throwable root = caught;
		
		while (root.getCause() != null && root.getCause() != root){
			root = root.getCause();
		}
		
		return root;
	}
	
}
