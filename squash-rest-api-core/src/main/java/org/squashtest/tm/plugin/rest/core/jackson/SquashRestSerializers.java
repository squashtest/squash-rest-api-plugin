/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.core.jackson;

import com.fasterxml.jackson.databind.BeanDescription;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializationConfig;
import com.fasterxml.jackson.databind.ser.Serializers.Base;
import com.fasterxml.jackson.datatype.hibernate5.HibernateProxySerializer;
import org.hibernate.proxy.HibernateProxy;

/**
 * That implementation will bypass the regular {@link HibernateProxySerializer} and return a custom implementation that honors the unwrapping serializer contract
 * (see documentation on {@link TryUnwrappingWhenPossibleHibernateProxySerializer} )
 *
 *
 * @author bsiri
 *
 */
public class SquashRestSerializers extends Base {

	public SquashRestSerializers(){

	}

    /** The forceLazyLoading is set to "true" because {@link HibernateProxySerializer#findProxied(HibernateProxy)}
     * returns null when trying to find a proxy value for an object's lazy collections. We presume that is due to
     * incompatibility related to jackson.datatype.hibernate5 2.9 and hibernate-core 5.2.
     *
     * We have verified that the lazy collections are only serialized when needed (on demand).
     * */
	@Override
	public JsonSerializer<?> findSerializer(SerializationConfig config, JavaType type, BeanDescription beanDesc) {
        Class<?> raw = type.getRawClass();
        if (HibernateProxy.class.isAssignableFrom(raw)) {
            return new TryUnwrappingWhenPossibleHibernateProxySerializer(true);
        }
        return null;
	}
	
}
