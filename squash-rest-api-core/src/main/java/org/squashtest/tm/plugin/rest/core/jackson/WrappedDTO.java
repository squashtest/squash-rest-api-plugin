/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.core.jackson;

import com.fasterxml.jackson.annotation.JsonUnwrapped;

/**
 * DTO intended for deserialization and that use @{@link com.fasterxml.jackson.annotation.JsonUnwrapped} should inherit from this class.
 *
 * Created by bsiri on 11/07/2017.
 */
public abstract class WrappedDTO<ENTITY> {

    @JsonUnwrapped
    private ENTITY wrapped;


    public void setWrapped(ENTITY wrapped){
        this.wrapped = wrapped;
    }


    public ENTITY getWrapped(){
        return wrapped;
    }

}
