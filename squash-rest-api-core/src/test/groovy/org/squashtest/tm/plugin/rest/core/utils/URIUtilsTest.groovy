/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.core.utils

import spock.lang.Specification

public class URIUtilsTest  extends Specification{

	def "should return no parameters if the url has no parameters"(){
		
		given :
			def url = new URL("http://whatever.com/path")
		
		when :
			def params = URIUtils.findQueryParameters(url)
		
		then :
			params == [:]		
	}
	
	def "should return two parameters : one without value and another with value"(){
		
		given :
			def url = new URL("http://whatever.com/path?novalue&has=value")
		
		when :
			def params = URIUtils.findQueryParameters(url)
		
		then :
			params == [novalue:null, has:"value"]
		
		
	}
	
}
