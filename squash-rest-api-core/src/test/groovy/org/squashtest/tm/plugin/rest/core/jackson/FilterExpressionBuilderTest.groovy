/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.core.jackson

import org.squashtest.tm.plugin.rest.core.exception.UnbalancedFilterExpressionException
import org.squashtest.tm.plugin.rest.core.jackson.FilterExpression
import org.squashtest.tm.plugin.rest.core.jackson.FilterExpressionBuilder
import spock.lang.Specification

class FilterExpressionBuilderTest extends Specification{

	def "should tokenize an expression"(){
		given :
			def expr = "_type,steps[id,action],custom-fields"
		
		when :
			def res = FilterExpressionBuilder.tokenize expr 
		
		then :
			res == ["_type", "," , "steps", "[", "id", "," , "action" , "]" , "," , "custom-fields"] as LinkedList<String>
	}
	
	def "should tokenize a more complex and dirty (ie, having spaces) expression"(){
		
		given :
			def expr = "_type ,   steps [id ,action  ,\t\tcalled-test-case[id  ]  ] , custom-fields"
		
		when :
			def res = FilterExpressionBuilder.tokenize expr
		
		then :
			res == ["_type" , "," , "steps" , "[" , "id" , "," , "action" , "," , "called-test-case" , "[" , "id" , "]" , "]" , "," , "custom-fields"] as LinkedList<String>
	}
	
	
	def "should parse a correct expression"(){
		
		given :
			def expr = "_type,id,custom-fields"
		
		when :
			FilterExpression expression = FilterExpressionBuilder.from expr
		
		
		then : 
			expression == ["_type" : null, "id" : null, "custom-fields" : null] as FilterExpression
		
	}
	
	
	
	def "should parse a complex expression"(){
		given :
			def expr = "_type,steps[id,action,called-test-case[id]],custom-fields"
		
		when :
			FilterExpression expression = FilterExpressionBuilder.from expr
		
		
		then :
			expression == [
						"_type" : null, 
						"steps" : [
								"id" : null, 
								"action" : null, 
								"called-test-case" : [
									"id" : null
								] as FilterExpression
						] as FilterExpression,
						"custom-fields" : null
					] as FilterExpression
	}
	
	def "should complain because expression is unbalanced (1)"(){
		
		given :
			def expr = "I,am[unbalanced[duh]"		
			
		when :
			FilterExpression expression = FilterExpressionBuilder.from expr
		
		then :
			thrown UnbalancedFilterExpressionException
		
	}
	
	
	def "should complain because expression is unbalanced (2)"(){
		
		given :
			def expr = "I,am,unbalanced]duh"
			
		when :
			FilterExpression expression = FilterExpressionBuilder.from expr
		
		then :
			thrown UnbalancedFilterExpressionException
		
	}
 	
}
