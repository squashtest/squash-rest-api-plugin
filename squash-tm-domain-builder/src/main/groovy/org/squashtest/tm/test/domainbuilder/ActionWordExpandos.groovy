/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.test.domainbuilder

import org.squashtest.tm.domain.bdd.ActionWord
import org.squashtest.tm.domain.bdd.ActionWordParameter
import org.squashtest.tm.domain.bdd.ActionWordParameterValue
import org.squashtest.tm.domain.bdd.ActionWordText
import org.squashtest.tm.domain.bdd.BddImplementationTechnology

class ActionWordExpandos {

    private ActionWordExpandos() {
        super()
    }

    static class ActionWordExpando extends BaseExpando<ActionWord> implements AuditableFeatures {
        ActionWordExpando() {
            super()
            id = 1L
        }

        // methods below are used in Action word library Api
        def lastImplementationTechnology(String technology) {
            this.lastImplementationTechnology = BddImplementationTechnology.valueOf technology
        }

        def lastImplementationDate(String date) {
            this.lastImplementationDate = parseDate(date)
        }
    }

    static class ActionWordTextExpando extends BaseExpando<ActionWordText> {
        ActionWordTextExpando() {
            super()
            id = 1L
        }
    }

    static class ActionWordParameterExpando extends BaseExpando<ActionWordParameter> {
        ActionWordParameterExpando() {
            super()
            id = 1L
        }
    }

    static class ActionWordParameterValueExpando extends BaseExpando<ActionWordParameterValue> {
        ActionWordParameterValueExpando() {
            super()
            id = 1L
        }
    }
}
