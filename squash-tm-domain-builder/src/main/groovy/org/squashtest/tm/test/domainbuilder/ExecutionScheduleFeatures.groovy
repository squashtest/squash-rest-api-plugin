/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.test.domainbuilder

import org.squashtest.tm.domain.campaign.ActualTimePeriod
import org.squashtest.tm.domain.campaign.ScheduledTimePeriod

/**
 * Created by bsiri on 05/07/2017.
 */
trait ExecutionScheduleFeatures {

    // that class uses the date utilities defined in BaseExpando

    def scheduledStartDate(String datestr){
        if (this.scheduledPeriod == null){
            this.scheduledPeriod = new ScheduledTimePeriod()
        }
        this.scheduledPeriod.scheduledStartDate = parseDate(datestr)
    }

    def scheduledEndDate(String datestr){
        if (this.scheduledPeriod == null){
            this.scheduledPeriod = new ScheduledTimePeriod()
        }
        this.scheduledPeriod.scheduledEndDate = parseDate(datestr)
    }

    def actualStartDate(String datestr){
        if (this.actualPeriod == null){
            this.actualPeriod = new ActualTimePeriod()
        }
        this.actualPeriod.actualStartDate = parseDate(datestr)
    }

    def actualEndDate(String datestr){
        if (this.actualPeriod == null){
            this.actualPeriod = new ActualTimePeriod()
        }
        this.actualPeriod.actualEndDate = parseDate(datestr)
    }

    def actualStartAuto(boolean flag){
        if (this.actualPeriod == null){
            this.actualPeriod = new ActualTimePeriod()
        }
        this.actualPeriod.actualStartAuto = flag
    }

    def actualEndAuto(boolean flag){
        if (this.actualPeriod == null){
            this.actualPeriod = new ActualTimePeriod()
        }
        this.actualPeriod.actualEndAuto = flag
    }

}