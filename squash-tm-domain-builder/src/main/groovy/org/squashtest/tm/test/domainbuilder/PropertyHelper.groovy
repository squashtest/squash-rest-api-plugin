/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.test.domainbuilder

import org.springframework.util.ReflectionUtils

import java.lang.reflect.Field
import java.util.concurrent.ConcurrentHashMap

class PropertyHelper {

	
	private static Map<Class<?>, IndexedFields> CACHE = new ConcurrentHashMap<>()
	
	
	private PropertyHelper(){}
	
	
	public static final assign (Object target, String pptName, Object value) {
		def targetClass = target.class
		if (! CACHE[targetClass]){
			CACHE[targetClass] = new IndexedFields()
		}
		
		CACHE[targetClass].assign target, pptName, value
	}
	
	public static final append(Object target, String pptName, Object value){
		def targetClass = target.class
		if (! CACHE[targetClass]){
			CACHE[targetClass] = new IndexedFields()
		}
		
		CACHE[targetClass].append target, pptName, value
	}

	
	private static final class IndexedFields extends ConcurrentHashMap<String, FieldInfo>{
		
		// warning : argument must be of the type for which the fields are indexed
		// in this class
		def assign(Object target, String pptName, Object value){
			if (! get(pptName)){
				map(target.class, pptName)
			}
			def info = get(pptName)
			
			/*
			 * If the attribute exists for real, set its value
			 * otherwise ignore, it might be one of the special attributes of the expando (see their doc)
			 */
			if (info.exists){
				info.field.set target, value
			}
		}
		
		def append(Object target, String pptName, Object value){
			if (! get(pptName)){
				map(target.class, pptName)
			}
			def info = get(pptName)
			
			
			/*
			 * If the attribute exists for real, set its value
			 * otherwise ignore, it might be one of the special attributes of the expando (see their doc)
			 */
			if (info.exists){
				
				// if needed, replace the collection implementation by a sorted implementation
				if (shouldReplaceImplementation(target, info.field)){
					replaceImplementation(target, info.field)
				} 
				
				
				def collection = info.field.get target
				collection << value
			}
		}

		def map(Class<?> clazz, String pptName){
			def field = ReflectionUtils.findField clazz, pptName
			def info
			if (field){
				field.accessible = true
				info = new FieldInfo(field:field)
			}
			else{
				info = new FieldInfo(exists:false)
			}
			put(pptName, info)
			info
		}
		
		/*
		 * Unsorted collections are a pain to test
		 * This method asserts whether we can (and should) use 
		 * a sorted implementation
		 */
		private def shouldReplaceImplementation(Object target, Field field){
			def value = field.get target
			def type = field.type			
			def finalType = value?.class ?: type
			
			// implementation should be replaced if the value is null or empty, 
			// and if the type is not already suitable
			def isEmpty = (value == null || value.isEmpty())
			def isUnsorted =(finalType in [Set, HashSet, Collection, List, Map, HashMap])  
			
			return isEmpty && isUnsorted
			
		}
		
		private def replaceImplementation(Object target, Field field){
			def type = field.type
			def newvalue
			switch(type){
				case Set :
				case HashSet : 
					newvalue = new LinkedHashSet()
					break;
				case Map :
				case HashMap : 
					newvalue = new LinkedHashMap() 
					break
				case List :
					newvalue = new ArrayList()
					break;
				default : throw new IllegalArgumentException("don't know what sorted Collection implementation should be used for type "+type)
			}			
			field.set target, newvalue
		}
		
	}
	
	private static final class FieldInfo {
		private Field field = null
		private boolean exists = true
	}

}
