/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.test.domainbuilder

import org.squashtest.tm.domain.bdd.Keyword
import org.squashtest.tm.domain.infolist.ListItemReference
import org.squashtest.tm.domain.testcase.ActionTestStep
import org.squashtest.tm.domain.testcase.CallTestStep
import org.squashtest.tm.domain.testcase.Dataset
import org.squashtest.tm.domain.testcase.DatasetParamValue
import org.squashtest.tm.domain.testcase.KeywordTestCase
import org.squashtest.tm.domain.testcase.KeywordTestStep
import org.squashtest.tm.domain.testcase.Parameter
import org.squashtest.tm.domain.testcase.RequirementVersionCoverage
import org.squashtest.tm.domain.testcase.ScriptedTestCase
import org.squashtest.tm.domain.testcase.TestCase
import org.squashtest.tm.domain.testcase.TestCaseFolder
import org.squashtest.tm.domain.testcase.TestCaseImportance
import org.squashtest.tm.domain.testcase.TestCaseStatus
import org.squashtest.tm.domain.testcase.TestStep

import static org.squashtest.tm.test.domainbuilder.PropertyHelper.append
import static org.squashtest.tm.test.domainbuilder.PropertyHelper.assign

class TestCaseExpandos {

    private TestCaseExpandos() {
        super();
    }


    static class TestCaseFolderExpando extends BaseExpando<TestCaseFolder> implements AuditableFeatures {

        TestCaseFolderExpando() {
            super()

            id = 1l
            createdBy "admin"
            createdOn "2017/06/15"
            lastModifiedBy "admin"
            lastModifiedOn "2017/06/15"

        }


        @Override
        def create() {
            TestCaseFolder folder = super.create()

            // if a project is defined, ensure the content knows which project it is
            if (this.project) {
                assign folder, "project", null
                // need to nullify because of FolderSupport#notSameProject (go and look)
                folder.notifyAssociatedWithProject(this.project)
            }

            folder
        }


    }


    /**
     * The TestCaseExpando allows a special attribute :
     *
     *  <ul>
     *  	<li>'requirementVersions' : is an array of RequirementVersion
     * and will automatically wrap them in RequirementVersionCoverage when the test case is created </li>
     *  </ul>
     *
     * @author bsiri*
     */
    static class TestCaseExpando extends BaseExpando<TestCase> implements AuditableFeatures {

        TestCaseExpando() {
            super()

            id = 1l
            createdBy "admin"
            createdOn "2017/06/15"
            lastModifiedBy "admin"
            lastModifiedOn "2017/06/15"

            nature = new ListItemReference("NAT_FUNCTIONAL_TESTING")
            type = new ListItemReference("TYP_EVOLUTION_TESTING")
            steps = [] as List<TestStep>
        }

        // ********* creation override **************

        @Override
        def create() {
            def tc = super.create()

            // iterate over steps to bind them to this test case
            tc.steps.each {
                assign it, "testCase", tc
            }

            // iterate over requirement version and create coverages
            this?.requirementVersions.each {
                def cov = new RequirementVersionCoverage()
                append tc, "requirementVersionCoverages", cov
                append it, "requirementVersionCoverages", cov
                cov.verifyingTestCase = tc
                cov.verifiedRequirementVersion = it
            }

            // same for parameters
            tc.parameters.each {
                assign it, "testCase", tc
            }

            // same for datasets
            tc.datasets.each {
                assign it, "testCase", tc
            }

            // return
            tc
        }


        // ********** helpers ********

        def nature(String strnat) {
            this.nature = new ListItemReference(strnat)
        }

        def type(String strtyp) {
            this.type = new ListItemReference(strtyp)
        }

        def importance(TestCaseImportance imp) {
            this.importance = imp
        }

        def importance(String strimp) {
            this.importance = TestCaseImportance.valueOf strimp
        }

        def status(TestCaseStatus stat) {
            this.status = stat
        }

        def status(String strstat) {
            this.status = TestCaseStatus.valueOf strstat
        }

    }


    /**
     * The TestCaseExpando allows a special attribute :
     *
     * 	<ul>
     * 	   	<li>'script' : is the Gherkin script of the current test case.</li>
     * 	 </ul>
     *
     *  <ul>
     *  	<li>'requirementVersions' : is an array of RequirementVersion
     * and will automatically wrap them in RequirementVersionCoverage when the test case is created </li>
     *  </ul>
     *
     * @author qtran*
     */
    static class ScriptedTestCaseExpando extends BaseExpando<ScriptedTestCase> implements AuditableFeatures {

        ScriptedTestCaseExpando() {
            super()

            id = 2l
            createdBy "admin"
            createdOn "2020/04/02"
            lastModifiedBy "admin"
            lastModifiedOn "2020/04/02"

            nature = new ListItemReference("NAT_FUNCTIONAL_TESTING")
            type = new ListItemReference("TYP_EVOLUTION_TESTING")
            steps = [] as List<TestStep>

            script = "This is default script"

        }

        // ********* creation override **************

        @Override
        def create() {
            def tc = super.create()

            // iterate over steps to bind them to this test case
            //NOOP

            // iterate over requirement version and create coverages
            this?.requirementVersions.each {
                def cov = new RequirementVersionCoverage()
                append tc, "requirementVersionCoverages", cov
                append it, "requirementVersionCoverages", cov
                cov.verifyingTestCase = tc
                cov.verifiedRequirementVersion = it
            }

            // same for parameters
            tc.parameters.each {
                assign it, "testCase", tc
            }

            // same for datasets
            tc.datasets.each {
                assign it, "testCase", tc
            }

            // return
            tc
        }


        // ********** helpers ********

        def nature(String strnat) {
            this.nature = new ListItemReference(strnat)
        }

        def type(String strtyp) {
            this.type = new ListItemReference(strtyp)
        }

        def importance(TestCaseImportance imp) {
            this.importance = imp
        }

        def importance(String strimp) {
            this.importance = TestCaseImportance.valueOf strimp
        }

        def status(TestCaseStatus stat) {
            this.status = stat
        }

        def status(String strstat) {
            this.status = TestCaseStatus.valueOf strstat
        }

    }


    /**
     *
     *
     * @author qtran*
     */
    static class KeywordTestCaseExpando extends BaseExpando<KeywordTestCase> implements AuditableFeatures {

        KeywordTestCaseExpando() {
            super()

            id = 3l
            createdBy "admin"
            createdOn "2020/04/03"
            lastModifiedBy "admin"
            lastModifiedOn "2020/04/03"

            nature = new ListItemReference("NAT_FUNCTIONAL_TESTING")
            type = new ListItemReference("TYP_EVOLUTION_TESTING")
            steps = [] as List<TestStep>

        }

        // ********* creation override **************

        @Override
        def create() {
            def tc = super.create()

            // iterate over steps to bind them to this test case
            tc.steps.each {
                assign it, "testCase", tc
            }

            // iterate over requirement version and create coverages
            this?.requirementVersions.each {
                def cov = new RequirementVersionCoverage()
                append tc, "requirementVersionCoverages", cov
                append it, "requirementVersionCoverages", cov
                cov.verifyingTestCase = tc
                cov.verifiedRequirementVersion = it
            }

            // same for parameters
            tc.parameters.each {
                assign it, "testCase", tc
            }

            // same for datasets
            tc.datasets.each {
                assign it, "testCase", tc
            }

            // return
            tc
        }


        // ********** helpers ********

        def nature(String strnat) {
            this.nature = new ListItemReference(strnat)
        }

        def type(String strtyp) {
            this.type = new ListItemReference(strtyp)
        }

        def importance(TestCaseImportance imp) {
            this.importance = imp
        }

        def importance(String strimp) {
            this.importance = TestCaseImportance.valueOf strimp
        }

        def status(TestCaseStatus stat) {
            this.status = stat
        }

        def status(String strstat) {
            this.status = TestCaseStatus.valueOf strstat
        }

    }

    static class ActionStepExpando extends BaseExpando<ActionTestStep> {

        ActionStepExpando() {
            super();

            id = 1l

        }

        // ********* creation override **************

        @Override
        def create() {
            def step = super.create()

            // fix the test case if needed
            if (step.testCase) {
                append step.testCase, "steps", step
            }

            step
        }
    }


    static class CallStepExpando extends BaseExpando<CallTestStep> {

        CallStepExpando() {
            super();

            id = 1l

            delegateParameterValues = false
        }

        // ********* creation override **************

        @Override
        def create() {
            def step = super.create()


            if (step.testCase) {
                append step.testCase, "steps", step
            }

            step
        }
    }

    static class KeywordStepExpando extends BaseExpando<KeywordTestStep> {

        KeywordStepExpando() {
            super();

            id = 1l

        }

        // ********* creation override **************

        @Override
        def create() {
            def step = super.create()

            // fix the test case if needed
            if (step.testCase) {
                append step.testCase, "steps", step
            }

            step
        }

        // ********** helpers ********

        def keyword(String keywordStr) {
            this.keyword = Keyword.valueOf(keywordStr)
        }

    }

    static class ParameterExpando extends BaseExpando<Parameter> {

        ParameterExpando() {
            super()

            id = 1l
            name = "parameter"

        }

        @Override
        def create() {
            Parameter param = super.create()

            param.datasetParamValues?.each { assign it, "parameter", param }

            if (param.testCase) {
                append param.testCase, "parameters", param
            }

            return param
        }

    }

    static class DatasetExpando extends BaseExpando<Dataset> {

        DatasetExpando() {
            super()

            id = 1l
            name = "dataset"
        }

        @Override
        def create() {
            Dataset ds = super.create()

            ds.parameterValues?.each { assign it, "dataset", ds }

            if (ds.testCase) {
                append ds.testCase, "datasets", ds
            }

            return ds
        }
    }

    static class DatasetParamValueExpando extends BaseExpando<DatasetParamValue> {

        DatasetParamValueExpando() {
            super()

            id = 1L
            paramValue = "value"
        }

        @Override
        def create() {
            DatasetParamValue value = super.create()

            if (value.dataset) {
                append value.dataset, "parameterValues", value
            }

            if (value.parameter) {
                append value.parameter, "datasetParamValues", value
            }

            return value
        }
    }

}
