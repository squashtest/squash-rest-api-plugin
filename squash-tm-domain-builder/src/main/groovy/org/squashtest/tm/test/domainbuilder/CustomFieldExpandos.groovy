/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.test.domainbuilder

import org.squashtest.tm.domain.customfield.BindableEntity
import org.squashtest.tm.domain.customfield.CustomFieldBinding
import org.squashtest.tm.domain.customfield.CustomFieldValue
import org.squashtest.tm.domain.customfield.InputType
import org.squashtest.tm.domain.customfield.RawValue
import org.squashtest.tm.domain.denormalizedfield.DenormalizedFieldValue

import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.cufBinding

class CustomFieldExpandos {
	
	private CustomFieldExpandos(){
		super();
	}
	
	
	/**
	 * Expando for a custom field. Note : available options should be given as a Map (label, code). Example :
	 * 
	 * <pre>
	 * 		{
	 *	id = 1l
	 *	options [
	 *		"label1" : "code1",
	 *		"label2" : "code2"
	 *	]
	 * 
	 * 	}
	 * </pre>
	 * 
	 * 
	 */
	static class CustomFieldExpando extends BaseExpando<NewCustomField>{
		CustomFieldExpando(){
			super()
			
			id = 1l
			name = "text field"
			label = "text field"
			inputType "PLAIN_TEXT"
			code = "CF_1"
			options [:]
			defaultValue = ""
		}
		
		def inputType( InputType type){
			this.inputType = type
		}
		
		def inputType(String strtype){
			this.inputType = InputType.valueOf strtype
		}
		
		def options(Map map){
			map.collect { k,v -> [k,v] as String[] } as String[]
		}
		
		@Override
		def create(){
			def newcuf = super.create()
			if (newcuf.inputType == InputType.CHECKBOX && newcuf.defaultValue?.trim() == ""){
				newcuf.defaultValue = "false"
			}
			
			newcuf.createTransientEntity()
		}
		
	}
	
	
	static class CustomFieldBindingExpando extends BaseExpando<CustomFieldBinding>{
		
		CustomFieldBindingExpando(){
			super()
			
			id = 1l
			boundEntity "TEST_CASE"
		}
		
		def boundEntity(BindableEntity et){
			this.boundEntity = et
		}
		
		def boundEntity(String et){
			this.boundEntity = BindableEntity.valueOf et
		}
		
	}
	
	/**
	 * Expando for a custom field value. To make life easier, if you find that declaring the custom field binding and custom field is too cumbersome you can inline the properties of a CustomFieldBinding and CustomFieldExpando instead and the rest will be created for you.
	 *
	 * Basically you can specify the inputType, code, value and the bound entity (via the method #bound(string), see below)
	 * and you'll usually get what you want.
	 * 
	 * 
	 * @author bsiri
	 *
	 */
	static class CustomFieldValueExpando extends BaseExpando<CustomFieldValue>{
		
		CustomFieldValueExpando(){
			super();
			
			cufId = 1L
			value = ""
			bound "TEST_CASE:1"
		}

		@Override
		def create(){
			
			// create a custom field and a binding if none was set
			if (! this.binding){
				
				def properties = this.properties
			
				def cufexpando = new CustomFieldExpando()
				properties.each { k,v -> cufexpando.setProperty (k,v) }
				def cuf = cufexpando.create()
				
				this.binding = cufBinding {
					boundEntity (properties["boundEntityType"])
					customField = cuf
				}
				
			}
			
			def value = this.binding.createNewValue()
			
			def raw = (this.value instanceof Collection) ? new RawValue(this.value as ArrayList) : new RawValue(this.value as String) 
			
			raw.setValueFor value
			value.boundEntityId = properties.boundEntityId
			value.boundEntityType = properties.boundEntityType
			
			value
		}
		
		// ************ helpers ********
		
		
		def bound(String typeAndId){
			def matcher = (typeAndId =~ /(\w+):(\d+)/)
			this.boundEntityType = BindableEntity.valueOf(matcher[0][1])
			this.boundEntityId = Long.valueOf(matcher[0][2])
		}

		// ************* other helpers, coming from the other expandos *************
				
		def inputType( InputType type){
			this.inputType = type
		}
		
		def inputType(String strtype){
			this.inputType = InputType.valueOf strtype
		}
		
		def options(Map map){
			map.collect { k,v -> [k,v] as String[] } as String[]
		}
		
	}

	static class DenormalizedFieldValueExpando extends BaseExpando<DenormalizedFieldValue> {

        DenormalizedFieldValueExpando() {
            super()
            code = "code"
            label = "label"
            value ="value"
        }
    }
}
