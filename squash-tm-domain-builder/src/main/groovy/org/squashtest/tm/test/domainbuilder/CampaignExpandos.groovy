/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.test.domainbuilder

import org.squashtest.tm.domain.campaign.Campaign
import org.squashtest.tm.domain.campaign.CampaignFolder
import org.squashtest.tm.domain.campaign.CampaignTestPlanItem
import org.squashtest.tm.domain.campaign.Iteration
import org.squashtest.tm.domain.campaign.IterationTestPlanItem
import org.squashtest.tm.domain.campaign.TestSuite
import org.squashtest.tm.domain.execution.Execution
import org.squashtest.tm.domain.execution.ExecutionStatus
import org.squashtest.tm.domain.execution.ExecutionStep
import org.squashtest.tm.domain.execution.KeywordExecution
import org.squashtest.tm.domain.execution.ScriptedExecution
import org.squashtest.tm.domain.infolist.DenormalizedNature
import org.squashtest.tm.domain.infolist.DenormalizedType
import org.squashtest.tm.domain.testcase.TestCaseExecutionMode
import org.squashtest.tm.domain.testcase.TestCaseImportance
import org.squashtest.tm.domain.testcase.TestCaseStatus

import static org.squashtest.tm.test.domainbuilder.PropertyHelper.append
import static org.squashtest.tm.test.domainbuilder.PropertyHelper.assign

/**
 * Created by bsiri on 05/07/2017.
 */
class CampaignExpandos {

    private CampaignExpandos() {
        super();
    }


    static class CampaignFolderExpando extends BaseExpando<CampaignFolder> implements AuditableFeatures {

        CampaignFolderExpando() {
            super()

            id = 1L
            createdBy "admin"
            createdOn "2017/06/15"
            lastModifiedBy "admin"
            lastModifiedOn "2017/06/15"

        }

        @Override
        def create() {
            CampaignFolder folder = super.create()

            // if a project is defined, ensure the content knows which project it is
            if (this.project) {
                assign folder, "project", null
                // need to nullify because of FolderSupport#notSameProject (go and look)
                folder.notifyAssociatedWithProject(this.project)
            }

            folder
        }

    }

    static class CampaignExpando extends BaseExpando<Campaign> implements AuditableFeatures, ExecutionScheduleFeatures {

        CampaignExpando() {
            super()

            id = 1L
            createdBy "admin"
            createdOn "2017/06/15"
            lastModifiedBy "admin"
            lastModifiedOn "2017/06/15"
        }
    }

    static class CampaignTestPlanItemExpando extends BaseExpando<CampaignTestPlanItem> {

        CampaignTestPlanItemExpando() {
            super()
            id = 1L
        }

        def assignedUser(String userstr) {
            this.user = SquashEntityBuilder.user {
                login = userstr
            }
        }
    }

    static class IterationExpando extends BaseExpando<Iteration> implements AuditableFeatures, ExecutionScheduleFeatures {

        IterationExpando() {
            super()

            id = 1L
            uuid = "2f7194ca-eb2e-4379-f82d-ddc207c866bd"
            createdBy "admin"
            createdOn "2012/12/12"
            lastModifiedBy "admin"
            lastModifiedOn "2017/05/12"

        }
    }

    static class TestSuiteExpando extends BaseExpando<TestSuite> implements AuditableFeatures {

        TestSuiteExpando() {
            super()

            id = 1L
            uuid = "2f7198zd-eb2e-4379-f82d-ddc207c866bd"
            createdBy "admin"
            createdOn "2017/07/12"
            lastModifiedBy "admin"
            lastModifiedOn "2017/07/12"
        }

        def executionStatus(String status) {
            this.executionStatus = ExecutionStatus.valueOf(status.toUpperCase())
        }
    }

    static class IterationTestPlanItemExpando extends BaseExpando<IterationTestPlanItem> implements AuditableFeatures {

        IterationTestPlanItemExpando() {
            super();

            id = 1L
            createdBy "admin"
            createdOn "2017/07/12"
            lastModifiedBy "admin"
            lastModifiedOn "2017/07/12"

        }

        @Override
        def create() {
            IterationTestPlanItem item = super.create();

            item.executions.each {
                assign it, "testPlan", item
            }

            item
        }

        def lastExecutedOn(String datestr) {
            this.lastExecutedOn = parseDate(datestr)
        }

        def executionStatus(String statstr) {
            this.executionStatus = ExecutionStatus.valueOf(statstr.toUpperCase())
        }

        def assignedUser(String userstr) {
            this.user = SquashEntityBuilder.user {
                login = userstr
            }
        }
    }

    static class ExecutionExpando extends BaseExpando<Execution> implements AuditableFeatures {

        ExecutionExpando() {
            super()

            id = 1L
            createdBy "admin"
            createdOn "2017/07/12"
            lastModifiedBy "admin"
            lastModifiedOn "2017/07/12"

        }

        @Override
        def create() {
            Execution exec = super.create()

            if (exec.testPlan) {
                append exec.testPlan, "executions", exec
            }

            exec
        }


        def lastExecutedOn(String datestr) {
            this.lastExecutedOn = parseDate(datestr)
        }

        def executionStatus(String status) {
            this.executionStatus = ExecutionStatus.valueOf(status.toUpperCase())
        }

        def nature(String naturestr) {
            this.nature = new DenormalizedNature(null, naturestr, null)
        }

        def type(String typestr) {
            this.type = new DenormalizedType(null, typestr, null)
        }

        def importance(String impstr) {
            this.importance = TestCaseImportance.valueOf impstr
        }

        def executionMode(String modestr) {
            this.executionMode = TestCaseExecutionMode.valueOf modestr
        }

        def status(String status) { this.status = TestCaseStatus.valueOf status }
    }

    static class KeywordExecutionExpando extends BaseExpando<KeywordExecution> implements AuditableFeatures {

        KeywordExecutionExpando() {
            super()

            id = 1L
            createdBy "admin"
            createdOn "2017/07/12"
            lastModifiedBy "admin"
            lastModifiedOn "2017/07/12"

        }

        @Override
        def create() {
            def exec = super.create()

            if (exec.testPlan) {
                append exec.testPlan, "executions", exec
            }

            exec
        }


        def lastExecutedOn(String datestr) {
            this.lastExecutedOn = parseDate(datestr)
        }

        def executionStatus(String status) {
            this.executionStatus = ExecutionStatus.valueOf(status.toUpperCase())
        }

        def nature(String naturestr) {
            this.nature = new DenormalizedNature(null, naturestr, null)
        }

        def type(String typestr) {
            this.type = new DenormalizedType(null, typestr, null)
        }

        def importance(String impstr) {
            this.importance = TestCaseImportance.valueOf impstr
        }

        def executionMode(String modestr) {
            this.executionMode = TestCaseExecutionMode.valueOf modestr
        }

        def status(String status) { this.status = TestCaseStatus.valueOf status }
    }

    static class ScriptedExecutionExpando extends BaseExpando<ScriptedExecution> implements AuditableFeatures {

        ScriptedExecutionExpando() {
            super()

            id = 1L
            createdBy "admin"
            createdOn "2017/07/12"
            lastModifiedBy "admin"
            lastModifiedOn "2017/07/12"

            scriptName = "This is the default script name"

        }

        @Override
        def create() {
            def exec = super.create()

            if (exec.testPlan) {
                append exec.testPlan, "executions", exec
            }

            exec
        }


        def lastExecutedOn(String datestr) {
            this.lastExecutedOn = parseDate(datestr)
        }

        def executionStatus(String status) {
            this.executionStatus = ExecutionStatus.valueOf(status.toUpperCase())
        }

        def nature(String naturestr) {
            this.nature = new DenormalizedNature(null, naturestr, null)
        }

        def type(String typestr) {
            this.type = new DenormalizedType(null, typestr, null)
        }

        def importance(String impstr) {
            this.importance = TestCaseImportance.valueOf impstr
        }

        def executionMode(String modestr) {
            this.executionMode = TestCaseExecutionMode.valueOf modestr
        }

        def status(String status) { this.status = TestCaseStatus.valueOf status }
    }

    static class ExecutionStepExpando extends BaseExpando<ExecutionStep> implements AuditableFeatures {

        ExecutionStepExpando() {
            super()

            id = 1L
            createdBy "admin"
            createdOn "2017/07/24"
            lastModifiedBy "admin"
            lastModifiedOn "2017/07/25"
        }

        def executionStatus(String status) {
            this.executionStatus = ExecutionStatus.valueOf(status.toUpperCase())
        }

        def lastExecutedOn(String datestr) {
            this.lastExecutedOn = parseDate(datestr)
        }

    }

}
