/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.test.domainbuilder

import org.squashtest.tm.domain.customfield.BindableEntity
import org.squashtest.tm.domain.customfield.CustomFieldValue
import org.squashtest.tm.domain.customfield.InputType
import org.squashtest.tm.domain.customfield.NumericCustomFieldValue
import org.squashtest.tm.domain.denormalizedfield.DenormalizedFieldHolderType
import org.squashtest.tm.domain.denormalizedfield.DenormalizedFieldValue
import org.squashtest.tm.domain.denormalizedfield.DenormalizedMultiSelectField
import org.squashtest.tm.domain.denormalizedfield.DenormalizedNumericValue
import org.squashtest.tm.domain.denormalizedfield.DenormalizedRichValue
import org.squashtest.tm.domain.denormalizedfield.DenormalizedSingleSelectField

class DenoCustomFieldExpandos {

    private DenoCustomFieldExpandos(){
        super()
    }

    /**
     * <p>Documentation is the same that for the custom field values, see {@link org.squashtest.tm
     * .test.domainbuilder.CustomFieldExpandos.CustomFieldValueExpando}. It will notably create
     * the original custom field value for you.</p>
     *
     * <p>Note : you can specify which entity hold the denormalized field with #bound(str), and
     * the original entity bound to the custom field value this denormalized value originates from
     * using #boundOriginal(str)</p>
     *
     */
    static class DenoCustomFieldValueExpando extends BaseExpando<DenormalizedFieldValue>{

        DenoCustomFieldValueExpando(){
            super();

            value = ""
            bound "EXECUTION:83"

        }

        @Override
        def create(){

            // create the original custom field value etc
            def properties = this.properties
            def cufValueExpando = new CustomFieldExpandos.CustomFieldValueExpando()
            properties.each { k,v -> cufValueExpando.setProperty(k,v)}
            CustomFieldValue cfv = cufValueExpando.create()

            // now we can work on creating our denormalized field value
            DenormalizedFieldValue dfv

            switch(cfv.customField.inputType){
                case InputType.DROPDOWN_LIST :
                    dfv = new DenormalizedSingleSelectField(cfv, this.denormalizedFieldHolderId, this.denormalizedFieldHolderType)
                    break
                case InputType.RICH_TEXT :
                    dfv = new DenormalizedRichValue(cfv, this.denormalizedFieldHolderId, this.denormalizedFieldHolderType)
                    break;
                case InputType.TAG :
                    dfv = new DenormalizedMultiSelectField(cfv, this.denormalizedFieldHolderId, this.denormalizedFieldHolderType)
                    break;
                case InputType.NUMERIC:
                    NumericCustomFieldValue numericValueCustomFieldValue = (NumericCustomFieldValue) cfv
                    dfv = new DenormalizedNumericValue(numericValueCustomFieldValue, this.denormalizedFieldHolderId, this.denormalizedFieldHolderType)
                    break;

                default :
                    dfv = new DenormalizedFieldValue(cfv, this.denormalizedFieldHolderId, this.denormalizedFieldHolderType)

            }

            return dfv

        }

        // ************ helpers ********


        def bound(String typeAndId){
            def matcher = (typeAndId =~ /(\w+):(\d+)/)
            this.denormalizedFieldHolderType = DenormalizedFieldHolderType.valueOf(matcher[0][1])
            this.denormalizedFieldHolderId = Long.valueOf(matcher[0][2])
        }


        def boundOriginal(String typeAndId){
            def matcher = (typeAndId =~ /(\w+):(\d+)/)
            this.boundEntityType = BindableEntity.valueOf(matcher[0][1])
            this.boundEntityId = Long.valueOf(matcher[0][2])
        }

        // ************* other helpers, coming from the other expandos *************

        def inputType(InputType type){
            this.inputType = type
        }

        def inputType(String strtype){
            this.inputType = InputType.valueOf strtype
        }

        def options(Map map){
            map.collect { k,v -> [k,v] as String[] } as String[]
        }

    }
}
