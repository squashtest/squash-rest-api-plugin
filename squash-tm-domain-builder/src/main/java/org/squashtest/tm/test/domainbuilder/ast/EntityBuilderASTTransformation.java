/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.test.domainbuilder.ast;

import groovy.transform.CompileStatic;
import org.apache.commons.lang3.ClassUtils;
import org.codehaus.groovy.ast.ASTNode;
import org.codehaus.groovy.ast.AnnotationNode;
import org.codehaus.groovy.ast.ClassHelper;
import org.codehaus.groovy.ast.ClassNode;
import org.codehaus.groovy.ast.Parameter;
import org.codehaus.groovy.ast.tools.GenericsUtils;
import org.codehaus.groovy.control.CompilePhase;
import org.codehaus.groovy.control.SourceUnit;
import org.codehaus.groovy.transform.ASTTransformation;
import org.codehaus.groovy.transform.AbstractASTTransformation;
import org.codehaus.groovy.transform.GroovyASTTransformation;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

import static org.codehaus.groovy.ast.tools.GeneralUtils.assignS;
import static org.codehaus.groovy.ast.tools.GeneralUtils.constX;
import static org.codehaus.groovy.ast.tools.GeneralUtils.indexX;
import static org.codehaus.groovy.ast.tools.GeneralUtils.params;
import static org.codehaus.groovy.ast.tools.GeneralUtils.returnS;
import static org.codehaus.groovy.ast.tools.GeneralUtils.varX;

/**
 * Created by bsiri on 26/06/17.
 */
@CompileStatic
@GroovyASTTransformation(phase = CompilePhase.SEMANTIC_ANALYSIS)
public class EntityBuilderASTTransformation extends AbstractASTTransformation implements ASTTransformation {

    private static final String ERR_HEADER = "@EntityBuilder AST Transformation : ";
    private static final String ERR_GROOVY_VERSION = " This error might be related to incompatible groovy version.";

    private static final String ANNOTATED_NOT_CLASS = "annotated element is not a class definition.";
    private static final String ANNOTATION_MALFORMED = "expected annotation @EntityBuilder, but got some malformed AST node instead !";
    private static final String ANNOTATION_INCORRECT = "expected annotation @EntityBuilder, but got some other annotation instead. ";
    private static final String WRONG_PHASE = "it seems that AST transform phase SEMANTIC_ANALYSIS is too early : the class referenced by @EntityBuilder has not been resolved yet.";

    // **************** check methods **********************
    // the tutorial said to be extra-defensive so let's do this

    private boolean check(ASTNode[] astNodes, SourceUnit source) {

        // check that the annotated element is a classnode
        if (! (astNodes[1] instanceof ClassNode)){
            addError(ERR_HEADER + ANNOTATED_NOT_CLASS, astNodes[1]);
            return false;
        }

        AnnotationNode annot = (AnnotationNode)astNodes[0];
        ClassNode clazz = (ClassNode) astNodes[1];

        // is the annotation node correctly initialized ?
        if ( annot.getClassNode() == null){
            addError(ERR_HEADER + ANNOTATION_MALFORMED + ERR_GROOVY_VERSION, annot);
            return false;
        }

        // is the annotation @EntityBuilder ?
        if (! isEntityBuilderAnnotation(annot)){
            addError(ERR_HEADER + ANNOTATION_INCORRECT + ERR_GROOVY_VERSION, annot);
            return false;
        }

        // has @EntityBuilder a value ?
        // note : in case of problems, the called method already reported it
        ClassNode annotValue = getMemberClassValue(annot, "value");
        if (annotValue == null){
            return false;
        }

        // has the annotation value been resolved ?
        if (annotValue.getTypeClass() == null){
            addError(ERR_HEADER + WRONG_PHASE + ERR_GROOVY_VERSION, annot);
        }

        // OK, we're in business !
        return true;
    }

    // check that the annotation has the correct type
    private boolean isEntityBuilderAnnotation(AnnotationNode annotNode){
        ClassNode annotClass = annotNode.getClassNode();
        return EntityBuilder.class.getName().equals(annotClass.getName());
    }

    // ********************** transformation methods ***********************

    @Override
    public void visit(ASTNode[] nodes, SourceUnit source) {

        // ***** init ******

        // see super method : checks for nullity etc
        init(nodes, source);

        if (!check(nodes, source)) {
            return;
        }

        // let's add getters and setters
        AnnotationNode annotation = (AnnotationNode)nodes[0];
        ClassNode target = (ClassNode)nodes[1];

        List<Field> fields = getNodesFromAnnotation(annotation);

        addMethods(target, fields);

    }


    protected void addMethods(ClassNode target, List<Field> fields){

       for (Field field : fields){

            ClassNode typeNode = new ClassNode(field.getType());
            if (field.getType().isPrimitive()){
               typeNode = new ClassNode(ClassUtils.primitiveToWrapper(field.getType()));
            }
            String fieldName = field.getName();

            addSetters(target, fieldName, typeNode);
            addGetters(target, fieldName, typeNode);
        }

    }


    protected void addSetters(ClassNode target, String fieldName, ClassNode fieldType){
        //String setterName = "set"+ Verifier.capitalize(fieldName);
        String setterName = fieldName;

        // big thank to DelegateASTTransformation#addSetterIfNeeded
        target.addMethod(
                setterName,
                ACC_PUBLIC,
                ClassHelper.VOID_TYPE,
                params(new Parameter(GenericsUtils.nonGeneric(fieldType), fieldName)),
                null,
                assignS(
                        indexX(varX("properties"), constX(fieldName)),
                        varX(fieldName)
                ));

    }

    protected void addGetters(ClassNode target, String fieldName, ClassNode fieldType){
        //String getterName = "get"+ Verifier.capitalize(fieldName);
        String getterName = fieldName;


        // big thank to DelegateASTTransformation#addGetterIfNeeded
        target.addMethod(
                getterName,
                ACC_PUBLIC,
                fieldType,
                params(),
                null,
                returnS(
                    indexX(varX("properties"), constX(fieldName))
                ));
    }


    // too bad, it seems GeneralUtils.getAllProperties etc are useless
    // because they return empty collections (except for getAllMethods)
    protected  List<Field> getNodesFromAnnotation(AnnotationNode annotNode){

        ClassNode classNode = getMemberClassValue(annotNode, "value");
        Class<?> clazz = classNode.getTypeClass();

        final List<Field> fields = new ArrayList<>();

        ReflectionUtils.doWithFields(clazz, new ReflectionUtils.FieldCallback() {
            @Override
            public void doWith(Field field) throws IllegalArgumentException, IllegalAccessException {
                fields.add(field);
            }
        }, new ReflectionUtils.FieldFilter() {
            @Override
            public boolean matches(Field field) {
                return (! (Modifier.isStatic(field.getModifiers()) ||
                            field.getDeclaringClass().equals(Object.class)));
            }
        });


        return fields;

    }



}
