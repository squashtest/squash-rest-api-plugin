/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.test.domainbuilder

import org.squashtest.tm.domain.testautomation.TestAutomationServerKind
import spock.lang.Specification

class AutomatedTestExpandosTest extends Specification {


    def "should create a automated test"(){

        when :

        def res = SquashEntityBuilder.automatedTest {
            id = 569L
            name = "auto test"
            project = SquashEntityBuilder.testAutomatioProject {
                id = 569L
                jobName = "job 1"
                label = "wan wan"
                server = SquashEntityBuilder.testAutomationServer {
                    id = 569L
                    name = "TA server"
                    url = "http://1234:4567/jenkins/"
                    login = "ajenkins"
                    password = "password"
                    kind = TestAutomationServerKind.jenkins
                    description = "<p>nice try</p>"
                }
                tmProject = SquashEntityBuilder.project {
                    id = 367L
                    description = "<p>This project is the main sample project</p>"
                    label = "Main Sample Project"
                    active = true
                }
                slaves = "no slavery"
            }
        }

        then :
        res.name == "auto test"
        res.project.server.kind == TestAutomationServerKind.jenkins
        res.project.tmProject.active == true
    }
}
