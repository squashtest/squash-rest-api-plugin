/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.test.domainbuilder

import org.squashtest.tm.domain.customfield.BindableEntity
import org.squashtest.tm.domain.customfield.CustomField
import org.squashtest.tm.domain.customfield.CustomFieldValue
import org.squashtest.tm.domain.customfield.InputType
import org.squashtest.tm.domain.denormalizedfield.DenormalizedFieldHolderType
import org.squashtest.tm.domain.denormalizedfield.DenormalizedFieldValue
import spock.lang.Specification

class DenoCustomFieldExpandosTest extends Specification{

    def "should create a text denormalized field value"(){

        when :
            def deno = SquashEntityBuilder.denoValue {
                bound "EXECUTION:83"
                boundOriginal "TEST_CASE:20"
                inputType "PLAIN_TEXT"
                code = "SOME_CODE"
                value = "my value"
                label = "my label"
                name = "my name"
            }

        then :
            deno instanceof DenormalizedFieldValue

            deno.code == "SOME_CODE"
            deno.value == "my value"
            deno.label == "my label"
            deno.inputType == InputType.PLAIN_TEXT
            deno.denormalizedFieldHolderId == 83L
            deno.denormalizedFieldHolderType == DenormalizedFieldHolderType.EXECUTION

            CustomFieldValue value = deno.customFieldValue
            value.value == "my value"
            value.boundEntityType == BindableEntity.TEST_CASE
            value.boundEntityId == 20L

            CustomField cuf = value.customField
            cuf.name == "my name"
            cuf.label == "my label"
            cuf.inputType == InputType.PLAIN_TEXT


    }

}
