/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.test.domainbuilder

import org.squashtest.tm.domain.infolist.ListItemReference
import spock.lang.Specification

import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.actionStep
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.callStep
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.dataset
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.parameter
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.project
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.requirementVersion
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.testCase
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.testCaseFolder


class TestCaseExpandosTest extends Specification {

	def "should build a test case"(){
		
		when :
			def tc = testCase {
				id = 1l
				name = "Bob"
				
				type = new ListItemReference("WHATEVER")
			}
		
		then :
			tc.id == 1l
			tc.name == "Bob"
			tc.type.code == "WHATEVER"
	}
	
	
	def "should build an elaborate test case"(){
		
		
		when :
			def tc = testCase{				
				project = project {
					id = 14l
					name = "sample project"
				}
				
				id = 238l				
				name = "walking test"
				
				createdBy = "User-1"
				createdOn = "2017/06/15"
				lastModifiedBy "User-1"
				lastModifiedOn "2017/06/15"
				
				reference = "TC1"
				importance "LOW"
				nature "NAT_USER_TESTING"
				type "TYP_EVOLUTION_TESTING"
				
				prerequisite = "<p>You must have legs with feet attached to them (one per leg)</p>\n"
				description = "<p>check that you can walk through the API (literally)</p>\n"
				
				steps = [
					actionStep {
						id = 165l
						action = "<p><p>move \${first_foot} forward</p>\n"
						expectedResult = "<p>I just advanced by one step</p>\n"
					},
					actionStep {
						id = 166l
						action = "<p>move \${second_foot}&nbsp;forward</p>\n"
						expectedResult = "<p>and another step !</p>\n"
					},
					callStep {
						id = 167l
						calledTestCase = testCase{
							id = 239l
							name = "victory dance"
						}
					},
					callStep {
						id = 168l
						calledTestCase = testCase{
							id = 240l
							name = "unauthorized"
						}
					}
				]
				
				parameters = [
					parameter {
						id = 1l
						name = "first_foot"
					},
					parameter {
						id = 2l
						name = "second_foot"
					}
				] as Set
				
				datasets = [
					dataset {
						id = 1l
						name = "right handed people"
					},
					dataset {
						id = 2l
						name = "left handed people"
					}
					
				] as Set
				
				
				requirementVersions = [
					requirementVersion {
						id = 255l
						name = "Must have legs"
						withDefaultRequirement()
					},
					requirementVersion {
						id = 256l
						name = "unauthorized"
						withDefaultRequirement()
					}	
					
				]
				
			}
		
		then :
		
			tc.id == 238l
			// TODO : more tests
	}

	def "should create a folder with some content"(){

		when :
			def folder = testCaseFolder {
				id = 2L
				name = "bob"
				project = project{
					id = 1L
					name = "project"
				}
				content = [
				    testCase{
						id = 200L
						name = "test 1"
					},
					testCase{
						id = 201L
						name = "test 2"
					}
				]
			}

		then :
			folder.id == 2L
			folder.name == "bob"
			folder.project.id == 1L
			folder.content.collect { [id : it.id, name : it.name, pid : it.project.id ] } ==
							[[id : 200L, name:"test 1", pid : 1L],
							[id : 201L, name : "test 2", pid : 1L]]
	}

}
