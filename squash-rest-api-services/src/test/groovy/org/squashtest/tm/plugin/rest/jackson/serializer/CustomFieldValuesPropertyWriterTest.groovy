/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.jackson.serializer

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.ObjectWriter
import com.fasterxml.jackson.databind.SerializerProvider
import com.fasterxml.jackson.databind.ser.DefaultSerializerProvider
import com.fasterxml.jackson.databind.ser.FilterProvider
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider
import org.squashtest.tm.domain.customfield.CustomField
import org.squashtest.tm.domain.customfield.CustomFieldBinding
import org.squashtest.tm.domain.customfield.CustomFieldValue
import org.squashtest.tm.plugin.rest.core.jackson.SerializationDynamicFilter
import org.squashtest.tm.service.customfield.CustomFieldValueFinderService
import spock.lang.Specification

class CustomFieldValuesPropertyWriterTest extends Specification{

	CustomFieldValueFinderService service = Mock()
	
	CustomFieldValuesPropertyWriter writer = new CustomFieldValuesPropertyWriter()
	

	
	def setup(){
		writer.cufServices = service
								
	}
	
	
	// ***** test the filtering *****

	// * locating the filter *
	
	def "should not find a filter because no provider was set"(){
		
		when :
			def provider = withFilter(null)
			def filter = writer.findFilter([], provider)
		
		then :
			filter == null
		
	}
	
	
	def "should not find a filter because the provider doesn't know the filter"(){
		
		given :
			def provider = withFilter(new SimpleFilterProvider())
		
		when :
			def filter = writer.findFilter([], provider)
		
		then :
			filter == null
		
	}
	
	def "should not find a filter because the filter is not of the right type"(){
		
		
		given :
			SimpleFilterProvider prov = new SimpleFilterProvider()
			prov.addFilter(SerializationDynamicFilter.FILTER_ID, (SimpleBeanPropertyFilter) SimpleBeanPropertyFilter.serializeAllExcept("whatever"))
			
			def provider = withFilter(prov)
		
		when :
			def filter = writer.findFilter([], provider)
		
		then :
			filter == null
		
	}
	
	def "should find and return our filter"(){
		
		given :
			SerializationDynamicFilter dynfilter = new SerializationDynamicFilter("whatever")
			SimpleFilterProvider prov = new SimpleFilterProvider()
			prov.addFilter(SerializationDynamicFilter.FILTER_ID,  (SimpleBeanPropertyFilter)dynfilter)
			
			def provider = withFilter(prov)
		
		when :
			def filter = writer.findFilter([], provider)
					
		then :
			filter == dynfilter
	}

	// * filtering *
	
	def "should not filter anything if no filter was found"(){
		
		given :
			def provider = withFilter(null)
	
		and :
			def cufs = [
				mockCuf("code_A", "aaa"),
				mockCuf("code_B", "bbb")
			]
			
		when :
			def filtered = writer.applyFilter(cufs, provider)
		
		
		then :
			filtered as Set == cufs as Set
		
	}
	
	def "should filter nothing because the filter is empty (exception to the empty filter and root bean rule)"(){
		
		given :
			SerializationDynamicFilter dynfilter = new SerializationDynamicFilter("")
			SimpleFilterProvider prov = new SimpleFilterProvider()
			prov.addFilter(SerializationDynamicFilter.FILTER_ID,  (SimpleBeanPropertyFilter)dynfilter)
			
			def provider = withFilter(prov)
		
		and : 
			def cufs = [
				mockCuf("code_A", "aaa"),
				mockCuf("code_B", "bbb")
			]
		
		when :
			def filtered = writer.applyFilter(cufs, provider)

		then :
			filtered as Set == cufs as Set
		
	}
	
	def "should filter nothing because the filter is a star"(){
		
		given :
			SerializationDynamicFilter dynfilter = new SerializationDynamicFilter("*")
			SimpleFilterProvider prov = new SimpleFilterProvider()
			prov.addFilter(SerializationDynamicFilter.FILTER_ID,  (SimpleBeanPropertyFilter)dynfilter)
			
			def provider = withFilter(prov)
		
		and :
			def cufs = [
				mockCuf("code_A", "aaa"),
				mockCuf("code_B", "bbb")
			]
		
		when :
			def filtered = writer.applyFilter(cufs, provider)

		then :
			filtered as Set == cufs as Set
		
	}
	
	def "should filter one customfield and let the other one pass"(){
		given :
			SerializationDynamicFilter dynfilter = new SerializationDynamicFilter("code_A")
			SimpleFilterProvider prov = new SimpleFilterProvider()
			prov.addFilter(SerializationDynamicFilter.FILTER_ID,  (SimpleBeanPropertyFilter)dynfilter)
			
			def provider = withFilter(prov)
		
		and :
			def cufA = mockCuf("code_A", "aaa")
			def cufB = mockCuf("code_B", "bbb")
			def cufs = [ cufA, cufB ]
		
		when :
			def filtered = writer.applyFilter(cufs, provider)
	
		then :
			filtered as Set == [ cufA ] as Set 
			! filtered.contains(cufB)
		
	}
	
	
	def "should include all the custom fields except one"(){
		given :
			SerializationDynamicFilter dynfilter = new SerializationDynamicFilter("*,-code_B")
			SimpleFilterProvider prov = new SimpleFilterProvider()
			prov.addFilter(SerializationDynamicFilter.FILTER_ID,  (SimpleBeanPropertyFilter)dynfilter)
			
			def provider = withFilter(prov)
		
		and :
			def cufA = mockCuf("code_A", "aaa")
			def cufB = mockCuf("code_B", "bbb")
			def cufs = [ cufA, cufB ]
		
		when :
			def filtered = writer.applyFilter(cufs, provider)
	
		then :
			filtered as Set == [ cufA ] as Set
			! filtered.contains(cufB)
		
	}
	
	
	// ******* utils ******
	
	SerializerProvider withFilter(FilterProvider filProv){
		ObjectMapper mapper = new ObjectMapper()
		ObjectWriter writer =  mapper.writer((FilterProvider)filProv)
		SerializerProvider fake = mapper.getSerializerProvider()
		
		return new DefaultSerializerProvider.Impl(fake, writer.getConfig(), mapper.getSerializerFactory())
		
	}
	
	def mockCuf(code,value){
		new CustomFieldValue(
			value : value,
			binding : new CustomFieldBinding(
				customField : new CustomField(code : code)	
			)	
		)
	}
	
}
