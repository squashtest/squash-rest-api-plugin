/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.jackson.deserializer

import com.fasterxml.jackson.annotation.JsonAutoDetect
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.ObjectMapper
import org.squashtest.csp.core.bugtracker.domain.BugTracker
import org.squashtest.tm.domain.execution.Execution
import org.squashtest.tm.plugin.rest.jackson.mixin.RestBugTrackerMixin
import org.squashtest.tm.plugin.rest.jackson.model.ExecutionAndCustomFields
import spock.lang.Specification

class CustomFieldValuesMergerDeserializerTest extends Specification{


    def "should deserialize a bunch of custom field values"(){

        given :
            def mapper = new ObjectMapper()
        mapper.addMixIn(Execution, TestRestExecMixin)
                .addMixIn(BugTracker, RestBugTrackerMixin)


        and :

            def json = """{
    "id" : 80,
    "custom_fields" : [{
            "code" : "CF_TXT",
            "value" : "some text" 
        },
        {
            "code" : "CF_TAGS",
             "value" : ["tagA", "tagB"]
        }
    ]

}"""

        
        when :
            ExecutionAndCustomFields res = mapper
                    .readerFor(ExecutionAndCustomFields)
                    .readValue(json)

        then :
            res.wrapped.id == 80L
            res.customFields.size == 2
    }


    @JsonAutoDetect(fieldVisibility= JsonAutoDetect.Visibility.NONE, getterVisibility= JsonAutoDetect.Visibility.NONE, isGetterVisibility= JsonAutoDetect.Visibility.NONE)
    public static abstract class TestRestExecMixin{

        @JsonProperty
        Long id;

    }

}
