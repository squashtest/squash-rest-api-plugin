/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.service.impl

import org.apache.commons.lang3.StringUtils
import org.squashtest.tm.domain.infolist.ListItemReference
import org.squashtest.tm.domain.testcase.TestCaseImportance
import org.squashtest.tm.domain.testcase.TestCaseStatus
import org.squashtest.tm.plugin.rest.jackson.model.ActionTestStepDto
import org.squashtest.tm.plugin.rest.jackson.model.TestCaseDto
import org.squashtest.tm.test.domainbuilder.SquashEntityBuilder
import spock.lang.Specification

class AllPatchersTest extends Specification{


    def "(abstract) should update only the patchable properties"(){

        given :
            SampleObject bob = new SampleObject(name:"Bob", address: "dad's flat", adult: false)
            def dto = new SampleObject(name: "Bob was renamed ??", address: "15 Mike St.", adult: true)
            def patcher = new SampleObjectPatcher()

        when :
            patcher.patch(bob, dto)

        then :
            bob.name == "Bob"
            bob.address == "15 Mike St."
            bob.adult == true

    }


    def "should update test case properties"(){

        given :
            def bob = SquashEntityBuilder.testCase {
                id = 5L
                name = "Bob"
                reference = "ref Bob"
                description = "desc Bob"
                importance "HIGH"
                status "WORK_IN_PROGRESS"
                nature "TC_NAT_NATURAL"
                type "TC_TYP_COOL"
                prerequisite = "prerequisite Bob"
                automatedTestReference = "repo01/src/resources/script_auto.robot#Test_case_5"
                automatedTestTechnology = SquashEntityBuilder.automatedTestTechnology {
                    id = 8L
                    name = "Ranorex"
                }
                scmRepository = SquashEntityBuilder.scmRepository {
                    id = 6L
                    name = "repo01"
                    workingBranch = "master"
                    repository_path = "/home/repo01"
                    working_folder_path = "src/resources"
                    scmServer = SquashEntityBuilder.scmServer {
                        id = 2L
                        kind = "git"
                        committerMail = "test@henix.fr"
                    }
                }
            }
            def dto = new TestCaseDto(
                    id : 10L,
                    name: "changed Bob",
                    reference: "changed ref", description: "changed desc",
                    importance : TestCaseImportance.VERY_HIGH,
                    status : TestCaseStatus.APPROVED,
                    nature: new ListItemReference("TC_NAT_SUPERNATURAL"),
                    type: new ListItemReference("TC_TYP_SUPERCOOL"),
                    prerequisite: "changed prerequisite",
                    automatedTestReference: "repo01/src/resources/script_auto.robot#Test_case_5",
                    automatedTestTechnology: "Ranorex",
                    scmRepositoryId: 6L
            )
            def patcher = new TestCasePatcher()

        when :
           patcher.patch(bob, dto)

        then :
            bob.id == 5L    // not modified
            bob.name == "Bob" // not modified
            bob.reference == "changed ref"
            bob.importance.name() == "VERY_HIGH"
            bob.status.name() == "APPROVED"
            bob.nature.code == "TC_NAT_SUPERNATURAL"
            bob.type.code == "TC_TYP_SUPERCOOL"
            bob.prerequisite == "changed prerequisite"
            bob.automatedTestReference == "repo01/src/resources/script_auto.robot#Test_case_5"
            bob.automatedTestTechnology.name == "Ranorex"
            bob.scmRepository.id == 6L
    }

    def "should update action test step properties"(){

        given :
        def bob = SquashEntityBuilder.actionStep {
            id = 5L
            action = "Bob"
            expectedResult = "result Bob"
        }
        def dto = new ActionTestStepDto(
                id : 10L,
                action: "changed Bob",
                expectedResult: "changed result",

        )
        def patcher = new ActionTestStepPatcher()

        when :
        patcher.patch(bob, dto)

        then :
        bob.id == 5L    // not modified
        bob.action == "changed Bob"
        bob.expectedResult == "changed result"

    }


    // ***************** for the abstract patcher tests ********************


    public static final class SampleObject{
        private String name
        private String address
        private boolean adult

        public String getName() {
            return name
        }

        public void setName(String name) {
            this.name = name
        }

        public String getAddress() {
            return address
        }

        public void setAddress(String address) {
            this.address = address
        }

        public boolean getAdult() {
            return adult
        }

        public void setAdult(boolean adult) {
            this.adult = adult
        }
    }

    private static final class SampleObjectPatcher extends AbstractPatcher{
        EnumSet getPatchableAttributes() { return EnumSet.allOf(SamplePatchable)}
    }

    private static enum SamplePatchable implements PatchableAttributes{
        ADDRESS("address", String.class),
        ADULT("adult", boolean.class);

        SamplePatchable(String ppt, Class clazz){
            this.propertyName = ppt
            this.propertyClass = clazz
        }

        private String propertyName;
        private Class propertyClass;
        private String getterName;
        private String setterName;

        @Override
        public String getPropertyName() {
            return propertyName;
        }

        @Override
        public Class getPropertyClass() {
            return propertyClass;
        }

        @Override
        public String getGetterName() {
            if(StringUtils.isBlank(getterName)){
                return "get" + StringUtils.capitalize(getPropertyName());
            }
            return getterName;
        }

        @Override
        public String getSetterName() {
            if(StringUtils.isBlank(setterName)){
                return "set" + StringUtils.capitalize(getPropertyName());
            }
            return setterName;
        }


    }

}
