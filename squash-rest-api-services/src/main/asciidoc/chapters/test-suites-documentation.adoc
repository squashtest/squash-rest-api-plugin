== Test Suites

This chapter focuses on services for the test suites.

*Note* : A Test Suite is a partition of an Iteration, more specifically its test plan is a partition of that of the Iteration it belongs to. Technically they share the same instances of IterationTestPlanItem; however the lifecycle of those items is managed by the Iteration only. Therefore, in the context of a Test Suite the semantic of PUTing or DELETEing an item changes slightly : those operations becomes 'attach' or 'detach' items. An item is available to a Test Suite only if it has  been created first within the Iteration.

=== Get test suite

A `GET` to `/test-suites/{id}` returns the test suite with the given id.

operation::RestTestSuiteControllerIT/get-test-suite[snippets='path-parameters,http-request,request-parameters,http-response,response-fields,links']

=== Create test suite

A `POST` to `/test-suites` creates a new test suite with or without test plan .

operation::RestTestSuiteControllerIT/post-test-suite[snippets='http-request,request-fields,http-response']

=== Modify test suite

A `PATCH` to `/test-suites/{id}` modifies the test suite with de given id. You can modify description, status, and/or custom fields.

operation::RestTestSuiteControllerIT/patch-test-suite[snippets='path-parameters,http-request,http-response']

=== Attach item to test suite

A `POST` to `/test-suites/{idTestSuite}/test-plan/{ids}` attach one or several test plan item to a test suite with the given id(s), and return the content of the test suite.

If among the targeted items some cannot be attached due to validation errors (see below) they won't be processed; only those that pass the validation will be attached. The operation will return no error message : the client will receive the modified test suite instead, and can then check that the final state is consistent with its expectations.

The possible validation errors are :
* the item does not exist,
* the item and the test suite belong to different iterations,
* the user does not have the required permissions.

operation::RestTestSuiteControllerIT/attach-item-from-test-suite[snippets='path-parameters,http-response']

=== Detach item of test suite

A `DELETE` to `/test-suites/{idTestSuite}/test-plan/{ids}` Detached one or several test plan item of test suite with the given id(s).

operation::RestTestSuiteControllerIT/detach-item-from-test-suite[snippets='path-parameters,http-response']

=== Delete test suite

A `DELETE` to `/test-suites/{ids}` deletes one or several test suites with the given id(s).

operation::RestTestSuiteControllerIT/delete-test-suite[snippets='path-parameters,http-request']

=== Get plans of test suite

A `GET` to `/test-suites/{id}/test-plan` returns all the test plans of the test suite with the given id.

operation::RestTestSuiteControllerIT/get-test-suite-test-plan[snippets='http-request,request-parameters,http-response']