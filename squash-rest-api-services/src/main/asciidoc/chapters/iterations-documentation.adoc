== Iterations

This chapter focuses on services for iterations.

=== Create iteration

A `POST` to `/campaigns/{campaignId}/iterations` creates a new iteration.

operation::RestIterationControllerIT/post-iteration[snippets='http-request,request-fields,request-parameters,http-response,response-fields,links']

=== Get iteration

A `GET` to `/iterations/{id}` returns the iteration with the given id.

operation::RestIterationControllerIT/get-iteration[snippets='path-parameters,http-request,request-parameters,http-response,response-fields,links']

=== Get iteration by name

A `GET` to `/iterations` with a request parameter `iterationName` returns the iteration with the given name.
Be careful, both the name of the parameter `iterationName` and the value of the iteration name are case-sensitive.

operation::RestIterationControllerIT/get-iteration-by-name[snippets='http-request,request-parameters,http-response,response-fields,links']

=== Modify iteration

A `PATCH` to `/iterations/{id}` modifies the iteration with de given id.

operation::RestIterationControllerIT/patch-iteration[snippets='path-parameters,http-request,request-fields,request-parameters,http-response,response-fields,links']

=== Get test plans of an iteration

A `GET` to `/iterations/{id}/test-plan` returns the test plans of the iteration with the given id.

operation::RestIterationControllerIT/get-iteration-test-plan[snippets='path-parameters,http-request,request-parameters,http-response,response-fields,links']

=== Get test suites of an iteration

A `GET` `/iterations/{id}/test-suites` returns all the test-suites of the iteration with the given id.

operation::RestIterationControllerIT/get-iteration-test-suites[snippets='path-parameters,http-request,request-parameters,http-response,response-fields,links']
