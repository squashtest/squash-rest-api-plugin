== Iteration Test Plan Items

This chapter focuses on services for iteration test plan items. An iteration test plan item represents a test case that has been planned in an iteration test plan. It belongs to an iteration, and binds together the test case to execute with a dataset (optional) and an assigned
user (optional).

=== Get iteration test plan item

A `GET` to `/iteration-test-plan-items/{id}` returns the iteration test plan item with the given id.

operation::RestIterationTestPlanItemControllerIT/get-iteration-test-plan-item[snippets='path-parameters,http-request,request-parameters,http-response,response-fields,links']

=== Create iteration test plan item

A `POST` to `/iterations/{iterationId}/test-plan` creates a new entry in the test plan of the iteration with the given id. The entry must
reference a test case, and optionally for which dataset and which assignee. If specified, the dataset must belong to the referenced Test Case.
The dataset and/or assignee may be undefined or null if you don't want to set them yet.

operation::RestIterationTestPlanItemControllerIT/post-iteration-test-plan-item[snippets='path-parameters,http-request,request-fields,http-response']

=== Modify iteration test plan item

A `Patch` to `/iteration-test-plan-items/{id}` modifies the iteration test plan item with the given id. You can modify the planned dataset, the assignee, or both. A property left absent from the json payload will not be altered, if present with a null value they will be reset. You cannot change the planned test case.

operation::RestIterationTestPlanItemControllerIT/patch-modify-iteration-test-plan-item[snippets='path-parameters,http-request,request-fields,http-response']

=== Delete iteration test plan item

A `DELETE` to `/iteration-test-plan-items/{testPlanItemsIds}` deletes one or several iteration test plan items with the given id(s).

operation::RestIterationTestPlanItemControllerIT/delete-iteration-test-plan-item[snippets='path-parameters,http-request']

=== Get executions of iteration test plan item

A `GET` to `/iteration-test-plan-items/{id}/executions` returns all the executions of the iteration test plan item with the given id.

operation::RestIterationTestPlanItemControllerIT/get-iteration-test-plan-item-executions[snippets='path-parameters,http-request,request-parameters,http-response,response-fields,links']
