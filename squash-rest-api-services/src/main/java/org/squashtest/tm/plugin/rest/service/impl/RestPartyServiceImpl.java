/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.service.impl;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.domain.users.Party;
import org.squashtest.tm.domain.users.PartyVisitor;
import org.squashtest.tm.domain.users.Team;
import org.squashtest.tm.domain.users.User;
import org.squashtest.tm.plugin.rest.jackson.model.PartyDto;
import org.squashtest.tm.plugin.rest.jackson.model.PartyDtoVisitor;
import org.squashtest.tm.plugin.rest.jackson.model.TeamDto;
import org.squashtest.tm.plugin.rest.jackson.model.UserDto;
import org.squashtest.tm.plugin.rest.repository.RestPartyRepository;
import org.squashtest.tm.plugin.rest.repository.RestTeamRepository;
import org.squashtest.tm.plugin.rest.repository.RestUserRepository;
import org.squashtest.tm.plugin.rest.service.RestPartyService;
import org.squashtest.tm.service.project.GenericProjectManagerService;
import org.squashtest.tm.service.security.acls.model.ObjectAclService;
import org.squashtest.tm.service.user.TeamModificationService;
import org.squashtest.tm.service.user.UserManagerService;

import javax.inject.Inject;
import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

import static org.squashtest.tm.service.security.Authorizations.HAS_ROLE_ADMIN;

@Service
@Transactional
public class RestPartyServiceImpl implements RestPartyService {

    @Inject
    private UserManagerService userManagerService;

    @Inject
    private TeamModificationService teamModificationService;

    @Inject
    private GenericProjectManagerService projectManagerService;

    @Inject
    private ObjectAclService aclService;

    @Inject
    private RestTeamRepository teamDao;

    @Inject
    private RestUserRepository userDao;

    @Inject
    private RestPartyRepository partyRepository;

    @Inject
    private UserPatcher userPatcher;

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    @Transactional(readOnly=true)
    public Party findById(long id) {
        Optional<Party> party = partyRepository.findById(id);
        if (!party.isPresent()) {
            throw new EntityNotFoundException("The user/team with id: " + id + " does not exist.");
        }

        final Party[] result = new Party[1];
        PartyVisitor visitor = new PartyVisitor() {
            @Override
            public void visit(User user) {
                result[0] = user;
            }

            @Override
            public void visit(Team team) {
                result[0] = team;
            }
        };
        party.get().accept(visitor);

        return result[0];
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    @Transactional(readOnly=true)
    public Party getOne(long id) {
        Party party = projectManagerService.findPartyById(id);
        if(party == null){
            throw new EntityNotFoundException("The user/team with id: " + id + " does not exist.");
        }
        return party;
    }

    @Override
    public User findUserByLogin(String login) {
        return userManagerService.findByLogin(login);
    }

    @Override
    @PostAuthorize(HAS_ROLE_ADMIN)
    public Page<User> findAllUsers(Pageable pageable) {
        return userDao.findAll(pageable);
    }

    @Override
    @PostAuthorize(HAS_ROLE_ADMIN)
    public Page<Team> findAllTeams(Pageable pageable) {
        return teamDao.findAll(pageable);
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public Page<Team> findTeamsByUser(long userId, Pageable pageable) {
        return teamDao.findTeamsByUserId(userId, pageable);
    }

    @Override
    public Party createParty(PartyDto partyDto) {
        final Party[] parties = new Party[1];
        PartyDtoVisitor visitor = new PartyDtoVisitor() {
            @Override
            public void visit(UserDto userDto) {
                User user = new User();
                user.setFirstName(userDto.getFirstName());
                user.setLastName(userDto.getLastName());
                user.setLogin(userDto.getLogin());
                user.setEmail(userDto.getEmail());
                user.setActive(userDto.getActive());
                userManagerService.addUser(user, userDto.getGroupId(), userDto.getPassword());
                parties[0] = user;
            }

            @Override
            public void visit(TeamDto teamDto) {
                Team team = new Team();
                team.setName(teamDto.getName());
                team.setDescription(teamDto.getDescription());
                teamModificationService.persist(team);
                parties[0] = team;
            }
        };

        partyDto.accept(visitor);
        return parties[0];
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public Party patchParty(PartyDto patch, long id) {
        Party party = findById(id);

        PartyDtoVisitor visitor = new PartyDtoVisitor() {
            @Override
            public void visit(UserDto userDto) {
                if(userDto.getPassword() != null){
                    userManagerService.resetUserPassword(id, userDto.getPassword());
                }
                if(userDto.getLogin() != null){
                    userManagerService.modifyUserLogin(id, userDto.getLogin());
                }
                if(userDto.getGroupId() != null){
                    userManagerService.setUserGroupAuthority(id, userDto.getGroupId());
                }
                userPatcher.patch(party, userDto);
            }

            @Override
            public void visit(TeamDto teamDto) {
                if (teamDto.getName() != null) {
                    teamModificationService.changeName(id, teamDto.getName());
                }
                if(teamDto.getDescription()!= null) {
                    teamModificationService.changeDescription(id, teamDto.getDescription());
                }
            }
        };

        patch.accept(visitor);

        return party;
    }

    @Override
    public void deleteUsers(List<Long> ids) {
        userManagerService.deleteUsers(ids);
    }

    @Override
    public void deleteTeams(List<Long> ids) {
        teamModificationService.deleteTeam(ids);
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public Page<User> findAllTeamMembers(long teamId, Pageable pageable) {
        return userDao.findMembersByTeamId(teamId, pageable);
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public void addMembersToTeam(long teamId, List<Long> userIds) {
        try {
            Team team = (Team) findById(teamId);
            List<User> users = userDao.findByIdIn(userIds);
            // team members are a set collection, so no need to check if the member already exists in the team.
            team.addMembers(users);
            userIds.forEach(userId -> aclService.updateDerivedPermissions(userId));
        } catch (ClassCastException e){
            throw new EntityNotFoundException("The team with id: " + teamId + " does not exist.");
        }


    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public void removeMembersFromTeam(long teamId, List<Long> userIds) {
        try {
            Team team = (Team) findById(teamId);
            teamModificationService.removeMembers(teamId, userIds);
        } catch (ClassCastException e){
            throw new EntityNotFoundException("The team with id: " + teamId + " does not exist.");
        }

    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public void addTeamsToUser(long userId, List<Long> teamIds) {

        try {
            User user = (User) findById(userId);

            List<Team> teams = teamDao.findByIdIn(teamIds);

            teams.forEach(team -> team.addMember(user));

            aclService.updateDerivedPermissions(userId);
        } catch (ClassCastException e){
            throw new EntityNotFoundException("The user with id: " + userId + " does not exist.");
        }

    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public void disassociateUserFromTeams(long userId, List<Long> teamIds) {
        try {
           userManagerService.deassociateTeams(userId, teamIds);
        } catch (ClassCastException e){
            throw new EntityNotFoundException("The user with id: " + userId + " does not exist.");
        }

    }
}
