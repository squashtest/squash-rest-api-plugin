/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.jackson.deserializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.beans.factory.annotation.Configurable;
import org.squashtest.tm.domain.testcase.Dataset;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.plugin.rest.jackson.model.CalledTestStepDto;

import javax.inject.Inject;
import java.io.IOException;
import java.net.BindException;

/**
 * Created by jthebault on 08/06/2017.
 */
@Configurable
public class CallTestStepDeserializer extends JsonDeserializer<CalledTestStepDto> {

    @Inject
    private JsonCrawler jsonCrawler;

    @Override
    public CalledTestStepDto deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        ObjectCodec codec = p.getCodec();
        JsonNode jsonCallStep = codec.readTree(p);

        CalledTestStepDto callTestStep = new CalledTestStepDto();

        JsonNode jsonDelegateParameter = jsonCallStep.get("delegate_parameter_values");
        if (jsonDelegateParameter != null) {
            callTestStep.setDelegateParameterValues(jsonDelegateParameter.asBoolean());
        }

        JsonNode index = jsonCallStep.get("index");
        if (index != null) {
            callTestStep.setIndex(index.asInt());
        }

        deserializeCalledTestCase(jsonCallStep, callTestStep);
        deserializeCalledDataset(jsonCallStep, callTestStep);
        return callTestStep;
    }

    private void deserializeCalledDataset(JsonNode jsonCallStep, CalledTestStepDto callTestStep) throws BindException {
        JsonNode jsonCalledDataset = jsonCallStep.get("called_dataset");
        if (jsonCalledDataset != null) {
            if (callTestStep.isDelegateParameterValues()) {
                throw new BindException("Invalid dataset call. You cannot call a dataset from the called test case if you delegate parameters to the calling test case.");
            }

            Dataset dataset = jsonCrawler.findMandatoryEntity(jsonCalledDataset, Dataset.class);
            callTestStep.setCalledDataset(dataset);
        }
    }

    private void deserializeCalledTestCase(JsonNode jsonCallStep, CalledTestStepDto callTestStep) throws BindException {
        JsonNode jsonCalledTestCase = jsonCallStep.get("called_test_case");
        // When updating a called test step, one doesn't need to give a called test case
        if(jsonCalledTestCase != null) {
            TestCase calledTestCase = jsonCrawler.findMandatoryEntity(jsonCalledTestCase, TestCase.class);
            callTestStep.setCalledTestCase(calledTestCase);
        }
    }
}
