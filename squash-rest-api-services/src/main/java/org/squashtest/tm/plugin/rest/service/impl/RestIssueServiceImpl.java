/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.service.impl;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.csp.core.bugtracker.domain.BugTracker;
import org.squashtest.tm.domain.bugtracker.Issue;
import org.squashtest.tm.domain.bugtracker.IssueList;
import org.squashtest.tm.domain.execution.Execution;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.exception.IssueAlreadyBoundException;
import org.squashtest.tm.plugin.rest.core.utils.ExceptionUtils;
import org.squashtest.tm.plugin.rest.jackson.model.IssueDto;
import org.squashtest.tm.plugin.rest.repository.RestIssueRepository;
import org.squashtest.tm.plugin.rest.service.RestIssueService;
import org.squashtest.tm.service.bugtracker.BugTrackersLocalService;
import org.squashtest.tm.service.execution.ExecutionProcessingService;
import org.squashtest.tm.service.internal.repository.BugTrackerBindingDao;

import javax.inject.Inject;

/**
 * Created by jthebault on 23/06/2017.
 */
@Service
@Transactional
public class RestIssueServiceImpl implements RestIssueService {

    @Inject
    private RestIssueRepository issueRepository;

    @Inject
    private RestIssueService restIssueService;

    @Inject
    private BugTrackerBindingDao bugTrackerBindingDao;

    @Inject
    private BugTrackersLocalService bugTrackersLocalService;

    @Inject
    private ExecutionProcessingService executionProcessingService;

    @Override
    @Transactional(readOnly=true)
    public Issue getOne(long id) {
        Issue issue = issueRepository.getOne(id);

        if (issue == null) {
            throw ExceptionUtils.entityNotFoundException(Issue.class, id);
        }

//        Iteration iteration = issue.getExecution().getIteration();
//        boolean canRead = permissionEvaluationService.hasRoleOrPermissionOnObject("ROLE_ADMIN","READ",  iteration);
//
//        if(!canRead){
//            throw new AccessDeniedException("Access Denied");
//        }

        Execution testExec = new Execution();
        testExec.setDescription("llolololo");

        //issue.setExecution(testExec);
        return issue;
    }

    @Override
    @PreAuthorize("@apiSecurity.hasPermission(#idExecution,'org.squashtest.tm.domain.execution.Execution' , 'WRITE')")
    public  Issue attachIssue(IssueDto issueDto,Long idExecution ){

        Execution exc = executionProcessingService.findExecution(idExecution);

        IssueList issueList = exc.getIssueList();

        BugTracker bt= exc.getBugTracker();

        Issue issue = null;

        if (issueList.hasRemoteIssue(issueDto.getRemoteIssueId())) {
            throw new IssueAlreadyBoundException();
        } else {
            issue = new Issue();
            issue.setBugtracker(bt);
            issue.setRemoteIssueId(issueDto.getRemoteIssueId());
            issueList.addIssue(issue);
            issueRepository.save(issue);

            TestCase testCase = bugTrackersLocalService.findTestCaseRelatedToIssue(issue.getId());

        }

        return issue;

    }




}
