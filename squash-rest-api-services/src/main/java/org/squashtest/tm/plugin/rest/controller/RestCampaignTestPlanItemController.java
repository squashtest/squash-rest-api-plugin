/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.controller;

import org.springframework.hateoas.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.squashtest.tm.domain.campaign.CampaignTestPlanItem;
import org.squashtest.tm.domain.testcase.Dataset;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.plugin.rest.controller.helper.ResourceLinksHelper;
import org.squashtest.tm.plugin.rest.core.jackson.DynamicFilterExpression;
import org.squashtest.tm.plugin.rest.core.web.BaseRestController;
import org.squashtest.tm.plugin.rest.core.web.EntityGetter;
import org.squashtest.tm.plugin.rest.core.web.RestApiController;
import org.squashtest.tm.plugin.rest.core.web.UseDefaultRestApiConfiguration;
import org.squashtest.tm.plugin.rest.jackson.model.CampaignTestPlanItemDto;
import org.squashtest.tm.plugin.rest.service.RestCampaignTestPlanItemService;
import org.squashtest.tm.plugin.rest.validators.CampaignTestPlanItemValidator;

import javax.inject.Inject;
import java.util.List;

/**
 * Created by jthebault on 22/06/2017.
 */
@RestApiController(CampaignTestPlanItem.class)
@UseDefaultRestApiConfiguration
public class RestCampaignTestPlanItemController extends BaseRestController {

    public static final String CTPI_DYNAMIC_FILTER = "*,referenced_test_case[name, reference], referenced_dataset[name], campaign[name,reference]";

    @Inject
    private RestCampaignTestPlanItemService service;

    @Inject
    private CampaignTestPlanItemValidator campaignTestPlanItemValidator;

    @Inject
    private ResourceLinksHelper linksHelper;

    @RequestMapping(value = "/campaign-test-plan-items/{id}", method = RequestMethod.GET)
    @EntityGetter
    @ResponseBody
    @DynamicFilterExpression(CTPI_DYNAMIC_FILTER)
    public ResponseEntity<Resource<CampaignTestPlanItem>> findIteration(@PathVariable("id") long id) {

        CampaignTestPlanItem itpi = service.getOne(id);

        Resource<CampaignTestPlanItem> res = toResource(itpi);

        res.add(linkService.createLinkTo(itpi.getCampaign().getProject()));

        TestCase referencedTestCase = itpi.getReferencedTestCase();
        if (referencedTestCase != null) {
            res.add(linkService.createLinkTo(referencedTestCase));
        }

        Dataset referencedDataset = itpi.getReferencedDataset();
        if (referencedDataset != null) {
            res.add(linkService.createLinkTo(referencedDataset));
        }

        res.add(linkService.createLinkTo(itpi.getCampaign()));

        return ResponseEntity.ok(res);
    }

    /*AMK: add test plan item to campaign*/

    @RequestMapping(value = "/campaign/{id}/test-plan", method = RequestMethod.POST)
    @ResponseBody
    @DynamicFilterExpression(CTPI_DYNAMIC_FILTER)
    public ResponseEntity<Resource<CampaignTestPlanItem>> addTestPlanItemToCampaign(@RequestBody CampaignTestPlanItemDto ctpiDto,
                                                                                    @PathVariable("id") long campaignId) throws BindException {
        //validation DTO DataSet assigned to
        campaignTestPlanItemValidator.validatePostTestPlanItem(ctpiDto,campaignId);

        //add test Plan item
        CampaignTestPlanItem ctpi = service.addTestCaseToCampaign(ctpiDto,campaignId);

        Resource<CampaignTestPlanItem> res = toResource(ctpi);

        linksHelper.addAllLinksForCampaignTestPlanItem(res);

        return ResponseEntity.status(HttpStatus.CREATED).body(res);
    }

    /*AMK: Modify test plan item to campaign*/

    @RequestMapping(value = "/campaign-test-plan-items/{id}", method = RequestMethod.PATCH)
    @ResponseBody
    @DynamicFilterExpression(CTPI_DYNAMIC_FILTER)
    public ResponseEntity<Resource<CampaignTestPlanItem>> modifyTestPlanItemToCampaign(@RequestBody CampaignTestPlanItemDto ctpiDto,
                                                                                    @PathVariable("id") long testPlanId) throws BindException {
        //validation DTO DataSet assigned to
        campaignTestPlanItemValidator.validatePatchTestPlanItem(ctpiDto,testPlanId);

        //change test Plan item
        CampaignTestPlanItem ctpi = service.modifyCampaignTestPlan(ctpiDto,testPlanId);

        Resource<CampaignTestPlanItem> res = toResource(ctpi);

        linksHelper.addAllLinksForCampaignTestPlanItem(res);

        return ResponseEntity.ok(res);
    }

    /*AMK: delete campaign test plan item*/
    @ResponseBody
    @RequestMapping(value = "/campaign-test-plan-items/{testPlanItemsIds}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> removeTestPlanItemsFromIteration(@PathVariable("testPlanItemsIds") List<Long> testPlanItemsIds) {
        // check if a test plan item was already executed and therefore not removed
        service.deleteCampaignTestPlan(testPlanItemsIds);
        return ResponseEntity.noContent().build();
    }

}
