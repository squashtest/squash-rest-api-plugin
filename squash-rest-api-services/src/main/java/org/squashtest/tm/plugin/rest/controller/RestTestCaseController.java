/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.PagedResources;
import org.springframework.hateoas.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.squashtest.tm.domain.testcase.Dataset;
import org.squashtest.tm.domain.testcase.KeywordTestCase;
import org.squashtest.tm.domain.testcase.Parameter;
import org.squashtest.tm.domain.testcase.ScriptedTestCase;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.domain.testcase.TestStep;
import org.squashtest.tm.plugin.rest.controller.helper.ErrorHandlerHelper;
import org.squashtest.tm.plugin.rest.controller.helper.ResourceLinksHelper;
import org.squashtest.tm.plugin.rest.core.jackson.DynamicFilterExpression;
import org.squashtest.tm.plugin.rest.core.web.BaseRestController;
import org.squashtest.tm.plugin.rest.core.web.DisableSort;
import org.squashtest.tm.plugin.rest.core.web.EntityGetter;
import org.squashtest.tm.plugin.rest.core.web.RestApiController;
import org.squashtest.tm.plugin.rest.core.web.TestCaseTypeFilter;
import org.squashtest.tm.plugin.rest.core.web.UriComponents;
import org.squashtest.tm.plugin.rest.core.web.UseDefaultRestApiConfiguration;
import org.squashtest.tm.plugin.rest.jackson.model.TestCaseDto;
import org.squashtest.tm.plugin.rest.jackson.model.TestStepDto;
import org.squashtest.tm.plugin.rest.service.RestDatasetService;
import org.squashtest.tm.plugin.rest.service.RestParameterService;
import org.squashtest.tm.plugin.rest.service.RestTestCaseService;
import org.squashtest.tm.plugin.rest.service.RestVerifyingRequirementManagerService;
import org.squashtest.tm.plugin.rest.validators.ParameterValidator;
import org.squashtest.tm.plugin.rest.validators.TestCasePatchValidator;
import org.squashtest.tm.plugin.rest.validators.TestCasePostValidator;
import org.squashtest.tm.plugin.rest.validators.TestStepValidator;

import javax.inject.Inject;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Set;


@RestApiController(TestCase.class)
@UseDefaultRestApiConfiguration
public class RestTestCaseController extends BaseRestController {

    private static final Logger LOGGER = LoggerFactory.getLogger(RestTestCaseController.class);

    private static final String POST_TEST_CASE = "post-test-case";
    private static final String PATCH_TEST_CASE = "patch-test-case";
    public static final String LINK_REQUIREMENTS = "link-requirements";
    public static final String UNLINK_REQUIREMENTS = "unlink-requirements";

    @Inject
    private RestTestCaseService restTestCaseService;

    @Inject
    private RestParameterService restParameterService;

    @Inject
    private RestDatasetService restDatasetService;

    @Inject
    private TestCasePostValidator testCasePostValidator;

    @Inject
    private TestCasePatchValidator testCasePatchValidator;

    @Inject
    private ParameterValidator parameterValidator;

    @Inject
    private TestStepValidator testStepValidator;

    @Inject
    private ResourceLinksHelper linksHelper;

    @Inject
    private RestVerifyingRequirementManagerService restVerifyingRequirementManagerService;

    /**
     * Returns all readable test case in all projects.
     *
     * @param pageable paging object
     * @param type  value is either {@link UriComponents#TEST_CASE_TYPE_FILTER_ALL} or {@link UriComponents#TEST_CASE_TYPE_FILTER_STANDARD},
     *                           or {@link UriComponents#TEST_CASE_TYPE_FILTER_SCRIPTED} or {@link UriComponents#TEST_CASE_TYPE_FILTER_KEYWORD},
     *                            if null defaults to {@link UriComponents#TEST_CASE_TYPE_FILTER_ALL}.
     * @return a responseEntity object to be use for viewing
     */
    @RequestMapping("/test-cases")
    @ResponseBody
    @DynamicFilterExpression("name,reference")
    @SuppressWarnings("rawtypes")
    public ResponseEntity<PagedResources<Resource>> getAllReadableTestCases(Pageable pageable, TestCaseTypeFilter type) {
        Page<TestCase> tcs = null;
        switch (type) {
            case STANDARD:
                tcs = restTestCaseService.getAllStandardTestCases(pageable);
                break;
            case SCRIPTED:
                tcs = restTestCaseService.getAllScriptedTestCases(pageable);
                break;
            case KEYWORD:
                tcs = restTestCaseService.getAllKeywordTestCases(pageable);
                break;
            default:
                tcs = restTestCaseService.getAllReadableTestCases(pageable);
        }

        PagedResources<Resource> res = toPagedResourcesWithRel(tcs, "test-cases");

        return ResponseEntity.ok(res);

    }

    @RequestMapping(value = "/test-cases/{id}", method = RequestMethod.GET)
    @EntityGetter({TestCase.class, ScriptedTestCase.class, KeywordTestCase.class})
    @ResponseBody
    @DynamicFilterExpression("*, parent[name], project[name],  verified_requirements[name], parameters[name], " +
            "datasets[name], automated_test[name],steps[*, called_test_case[name], called_dataset[name], -test_case], " +
            "attachments[name]")
    public ResponseEntity<Resource<TestCase>> findTestCase(@PathVariable("id") long id) {

        TestCase tc = restTestCaseService.getOne(id);

        Resource<TestCase> res = toResource(tc);

        linksHelper.addAllLinksForTestCase(res);

        return ResponseEntity.ok(res);
    }

    @RequestMapping("/test-cases/{id}/steps")
    @ResponseBody
    @DynamicFilterExpression("*, called_test_case[name], -test_case ")
    public ResponseEntity<PagedResources<Resource>> findTestCaseSteps(@PathVariable("id") long tcid, @DisableSort Pageable pageable) {

        Page<TestStep> steps = restTestCaseService.getTestCaseSteps(tcid, pageable);

        PagedResources<Resource> res = toPagedResourcesWithRel(steps, "steps");

        return ResponseEntity.ok(res);

    }

    @RequestMapping("/test-cases/{id}/parameters")
    @ResponseBody
    @DynamicFilterExpression("*, test_case[name]")
    public ResponseEntity<PagedResources<Resource>> findTestCaseParameters(@PathVariable("id") long id, Pageable pageable) {

        Page<Parameter> parameters = restParameterService.findAllByTestCaseId(id, pageable);

        PagedResources<Resource> res = toPagedResources(parameters);

        return ResponseEntity.ok(res);

    }

    @RequestMapping("/test-cases/{id}/datasets")
    @ResponseBody
    @DynamicFilterExpression("*,parameters[name],-test_case")
    public ResponseEntity<PagedResources<Resource>> findTestCaseDatasets(@PathVariable("id") long id, Pageable pageable) {

        Page<Dataset> datasets = restDatasetService.findAllByTestCaseId(id, pageable);

        PagedResources<Resource> res = toPagedResources(datasets);

        return ResponseEntity.ok(res);

    }

    @RequestMapping(value = "/test-cases", method = RequestMethod.POST)
    @ResponseBody
    @DynamicFilterExpression("*, parent[name], project[name], verified_requirements[name], parameters[name], steps[*, called_test_case[name], -test_case ]")
    public ResponseEntity<Resource<TestCase>> createTestCase(@RequestBody TestCaseDto testCaseDto) throws BindException, InvocationTargetException, IllegalAccessException {


        validatePostTestCase(testCaseDto);

        TestCase tc = restTestCaseService.createTestCase(testCaseDto);

        Resource<TestCase> res = toResource(tc);

        linksHelper.addAllLinksForTestCase(res);

        return ResponseEntity.status(HttpStatus.CREATED).body(res);
    }


    @RequestMapping(value = "/test-cases/{id}", method = RequestMethod.PATCH)
    @ResponseBody
    @DynamicFilterExpression("*, parent[name], project[name], verified_requirements[name], parameters[name], steps[*, called_test_case[name], -test_case ]")
    public ResponseEntity<Resource<TestCase>> patchTestCase(@RequestBody TestCaseDto patch, @PathVariable("id") long id) throws BindException {

        patch.setId(id);

        validatePatchTestCase(patch);

        TestCase tc = restTestCaseService.patchTestCase(patch, id);

        Resource<TestCase> res = toResource(tc);

        linksHelper.addAllLinksForTestCase(res);

        return ResponseEntity.ok(res);
    }

    /*AMK: delete test case */

    @ResponseBody
    @RequestMapping(value = "/test-cases/{ids}", method = RequestMethod.DELETE)
    public ResponseEntity<List<String>> deleteTestCase(@PathVariable("ids") List<Long> testCaseIds,
                                                                         @RequestParam(name="dry-run", required = false) Boolean dryRun,
                                                                         Locale locale) {

        List<String> reportMessages = restTestCaseService.deleteTestCase(testCaseIds,dryRun,locale);
        return ResponseEntity.ok(reportMessages);
    }

    @ResponseBody
    @PostMapping(value= "/test-cases/{id}/coverages/{requirementIds}")
    @DynamicFilterExpression("*, parent[name], project[name], verified_requirements[name], parameters[name], steps[*, called_test_case[name], -test_case ]")
    public ResponseEntity<Resource<TestCase>> linkRequirements(@PathVariable("id") Long testCaseId, @PathVariable("requirementIds") List<Long> requirementIds) throws BindException {
        testCasePostValidator.validateRequirements(testCaseId, requirementIds, LINK_REQUIREMENTS);
        TestCase testCase = restTestCaseService.getOne(testCaseId);
        restVerifyingRequirementManagerService.linkRequirementsToTestCase(requirementIds, testCaseId);
        Resource<TestCase> res = toResource(testCase);
        linksHelper.addAllLinksForTestCase(res);
        return ResponseEntity.ok(res);
    }

    @RequestMapping(value = "/test-cases/{id}/coverages/{requirementIds}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> unlinkRequirementFromTestCase(@PathVariable("id") Long testCaseId, @PathVariable("requirementIds") List<Long> requirementIds) throws BindException {
        testCasePostValidator.validateRequirements(testCaseId, requirementIds, UNLINK_REQUIREMENTS);
        restVerifyingRequirementManagerService.unlinkRequirementsFromTestCase(requirementIds, testCaseId);
        return ResponseEntity.noContent().build();
    }

    // ************** test case validation ******************


    private void validatePostTestCase(TestCaseDto testCaseDto) throws BindException {
        List<Errors> errors = new ArrayList<>();

        BindingResult validation = new BeanPropertyBindingResult(testCaseDto, POST_TEST_CASE);
        testCasePostValidator.validate(testCaseDto, validation);

        if (validation.hasErrors()) {
            errors.add(validation);
        }

        validateParameters(testCaseDto, errors);
        validateTestSteps(testCaseDto, errors);

        ErrorHandlerHelper.throwIfError(testCaseDto, errors, POST_TEST_CASE);
    }


    private void validatePatchTestCase(TestCaseDto patch) throws BindException {
        List<Errors> errors = new ArrayList<>();

        BindingResult validation = new BeanPropertyBindingResult(patch, PATCH_TEST_CASE);
        testCasePatchValidator.validate(patch, validation);

        if (validation.hasErrors()) {
            errors.add(validation);
        }

        ErrorHandlerHelper.throwIfError(patch, errors, PATCH_TEST_CASE);
    }

    private void validateParameters(TestCaseDto testCaseDto, List<Errors> errors) {
        Set<Parameter> parameters = testCaseDto.getParameters();
        if (parameters == null) {
            return;
        }
        for (Parameter parameter : parameters) {
            BindingResult validation = new BeanPropertyBindingResult(testCaseDto, POST_TEST_CASE);
            parameterValidator.validate(parameter, validation);
            if (validation.hasErrors()) {
                errors.add(validation);
            }
        }
    }

    private void validateTestSteps(TestCaseDto testCaseDto, List<Errors> errors) {
        List<TestStepDto> stepDtos = testCaseDto.getSteps();
        if (stepDtos == null) {
            return;
        }
        for (TestStepDto stepDto : stepDtos) {
            //must have set project before in test case validator
            stepDto.setProjectId(testCaseDto.getProject().getId());
            BindingResult validation = new BeanPropertyBindingResult(testCaseDto, POST_TEST_CASE);
            testStepValidator.validate(stepDto, validation);
            if (validation.hasErrors()) {
                errors.add(validation);
            }
        }
    }
}
