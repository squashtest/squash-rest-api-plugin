/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.jackson.model;

import org.squashtest.tm.domain.EntityReference;
import org.squashtest.tm.domain.Identified;
import org.squashtest.tm.domain.library.LibraryNode;

import java.util.List;

/**
 * Created by jthebault on 13/06/2017.
 */
public interface RestNode extends LibraryNode, Identified {
    ParentEntity getParent();

    List<CustomFieldValueDto> getCustomFields();

    RestType getRestType();

    @Override
    default EntityReference toEntityReference() {
        throw new UnsupportedOperationException("This method should not be called in API");
    }
}
