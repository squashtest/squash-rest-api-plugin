/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.squashtest.tm.domain.campaign.CampaignFolder;
import org.squashtest.tm.domain.campaign.CampaignLibraryNode;

import java.util.Collection;

/**
 * Created by jthebault on 19/06/2017.
 */
public interface RestCampaignFolderRepository extends JpaRepository<CampaignFolder, Long> {

	@Query("from CampaignFolder cfold where cfold.project.id in (:projectIds)")
	Page<CampaignFolder> findAllInProjects(@Param("projectIds") Collection<Long> projectIds, Pageable pageable);
	
	 @Query("select nodes from CampaignFolder fold join fold.content nodes where fold.id = :id")
    Page<CampaignLibraryNode> findFolderContent(@Param("id") long folderId, Pageable pageable);
    
	@Query("select nodes from CampaignLibraryNode nodes, CampaignPathEdge path where path.ancestorId = :id and path.descendantId = nodes.id "
			+ "and path.ancestorId != path.descendantId")
	Page<CampaignLibraryNode> findFolderAllContent(@Param("id") long folderId, Pageable pageable);


}
