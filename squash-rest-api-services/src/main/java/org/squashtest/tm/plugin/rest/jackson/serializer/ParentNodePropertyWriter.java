/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.jackson.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.cfg.MapperConfig;
import com.fasterxml.jackson.databind.introspect.AnnotatedClass;
import com.fasterxml.jackson.databind.introspect.BeanPropertyDefinition;
import com.fasterxml.jackson.databind.ser.VirtualBeanPropertyWriter;
import com.fasterxml.jackson.databind.util.Annotations;
import org.springframework.stereotype.Component;
import org.squashtest.tm.domain.EntityType;
import org.squashtest.tm.domain.Identified;
import org.squashtest.tm.domain.campaign.CampaignLibraryNode;
import org.squashtest.tm.domain.campaign.Iteration;
import org.squashtest.tm.domain.campaign.TestSuite;
import org.squashtest.tm.domain.library.Library;
import org.squashtest.tm.domain.library.LibraryNode;
import org.squashtest.tm.domain.library.NodeContainer;
import org.squashtest.tm.domain.library.TreeNode;
import org.squashtest.tm.domain.library.WhichNodeVisitor;
import org.squashtest.tm.domain.requirement.RequirementLibraryNode;
import org.squashtest.tm.domain.testcase.TestCaseLibraryNode;
import org.squashtest.tm.plugin.rest.core.service.NodeHierarchyHelpService;
import org.squashtest.tm.plugin.rest.core.web.BasicResourceAssembler;

import javax.inject.Inject;

@SuppressWarnings("serial")
@Component
public class ParentNodePropertyWriter extends VirtualBeanPropertyWriter {

    @Inject
    private NodeHierarchyHelpService service;

    @Inject
    private BasicResourceAssembler assembler;

    public ParentNodePropertyWriter() {
        super();
    }

    public ParentNodePropertyWriter(BeanPropertyDefinition propDef,
                                    Annotations contextAnnotations,
                                    JavaType declaredType,
                                    NodeHierarchyHelpService service,
                                    BasicResourceAssembler assembler) {

        super(propDef, contextAnnotations, declaredType);
        this.service = service;
        this.assembler = assembler;

    }

    @Override
    protected Object value(Object bean, JsonGenerator gen, SerializerProvider prov) throws Exception {

        NodeContainer<? extends LibraryNode> container;

        EntityType beanType = new WhichNodeVisitor().getTypeOf((TreeNode) bean);

        switch (beanType) {
            case TEST_CASE_FOLDER:
            case TEST_CASE:
                container = service.findParentFor((TestCaseLibraryNode) bean);
                break;

            case REQUIREMENT_FOLDER:
            case REQUIREMENT:
                container = service.findParentFor((RequirementLibraryNode) bean);
                break;
            case CAMPAIGN:
            case CAMPAIGN_FOLDER:
                container = service.findParentFor((CampaignLibraryNode) bean);
                break;

            case ITERATION:
                Iteration iteration = (Iteration) bean;
                container = (NodeContainer) iteration.getCampaign();
                break;

            case TEST_SUITE:
                TestSuite testSuite = (TestSuite) bean;
                container = (NodeContainer) testSuite.getIteration();
                break;

            default:
                container = null;
        }

		/*
		 *  also, if the parent happens to be a library, let's skip it and return the project 
		 *  instead (because we want to hide the Library objects from our REST representation)
		 */
        Identified containerBean;
        if (Library.class.isAssignableFrom(container.getClass())) {
            containerBean = ((Library<?>) container).getProject();
        } else {
            containerBean = container;
        }

        return assembler.toResource(containerBean);


    }

    @Override
    public VirtualBeanPropertyWriter withConfig(MapperConfig<?> config, AnnotatedClass declaringClass,
                                                BeanPropertyDefinition propDef, JavaType type) {
        return new ParentNodePropertyWriter(propDef, declaringClass.getAnnotations(), type, service, assembler);
    }


}
