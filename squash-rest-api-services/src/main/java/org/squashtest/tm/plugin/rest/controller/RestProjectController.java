/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.controller;

import com.google.common.base.CaseFormat;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.PagedResources;
import org.springframework.hateoas.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.squashtest.tm.domain.campaign.Campaign;
import org.squashtest.tm.domain.campaign.CampaignLibraryNode;
import org.squashtest.tm.domain.project.GenericProject;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.domain.project.ProjectTemplate;
import org.squashtest.tm.domain.requirement.Requirement;
import org.squashtest.tm.domain.requirement.RequirementLibraryNode;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.domain.testcase.TestCaseLibraryNode;
import org.squashtest.tm.domain.users.Party;
import org.squashtest.tm.plugin.rest.controller.helper.ErrorHandlerHelper;
import org.squashtest.tm.plugin.rest.controller.helper.ResourceLinksHelper;
import org.squashtest.tm.plugin.rest.core.jackson.DynamicFilterExpression;
import org.squashtest.tm.plugin.rest.core.web.BaseRestController;
import org.squashtest.tm.plugin.rest.core.web.ContentInclusion;
import org.squashtest.tm.plugin.rest.core.web.EntityGetter;
import org.squashtest.tm.plugin.rest.core.web.RestApiController;
import org.squashtest.tm.plugin.rest.core.web.UriComponents;
import org.squashtest.tm.plugin.rest.core.web.UseDefaultRestApiConfiguration;
import org.squashtest.tm.plugin.rest.jackson.model.GenericProjectDto;
import org.squashtest.tm.plugin.rest.jackson.model.RestPartyPermission;
import org.squashtest.tm.plugin.rest.service.RestPartyService;
import org.squashtest.tm.plugin.rest.service.RestProjectService;
import org.squashtest.tm.plugin.rest.validators.GenericProjectPostValidator;
import org.squashtest.tm.security.acls.PermissionGroup;

import javax.inject.Inject;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;


@RestApiController(GenericProject.class)
@UseDefaultRestApiConfiguration
public class RestProjectController extends BaseRestController {

    @Inject
    private RestProjectService service;

    @Inject
    private RestPartyService partyService;

    @Inject
    private ResourceLinksHelper linksHelper;

    @Inject
    private GenericProjectPostValidator genericProjectPostValidator;

    @RequestMapping("/projects/{id}")
    @EntityGetter({Project.class, ProjectTemplate.class})
    @DynamicFilterExpression("*")
    public ResponseEntity<Resource<GenericProject>> findProject(@PathVariable("id") long projectId) {

        GenericProject project = service.getOne(projectId);

        Resource<GenericProject> res = toResource(project);

        res.add(linkService.fromBasePath(linkTo(methodOn(RestProjectController.class).findRequirementLibraryContent(projectId, null, null))).withRel("requirements"));
        res.add(linkService.fromBasePath(linkTo(methodOn(RestProjectController.class).findTestCaseLibraryContent(projectId, null, null))).withRel("test-cases"));
        res.add(linkService.fromBasePath(linkTo(methodOn(RestProjectController.class).findCampaignLibraryContent(projectId, null, null))).withRel("campaigns"));
        res.add(linkService.fromBasePath(linkTo(methodOn(RestProjectController.class).findPartyPermissionsByProject(projectId))).withRel("permissions"));
        res.add(createRelationTo("attachments"));

        return ResponseEntity.ok(res);
    }

    @RequestMapping(value = "/projects", params = "projectName")
    @DynamicFilterExpression("*")
    public ResponseEntity<Resource<GenericProject>> findProjectByName(@RequestParam("projectName") String projectName) {
        GenericProject project = service.getOneByName(projectName);

        Resource<GenericProject> res = toResource(project);

        long projectId = project.getId();
        res.add(linkService.fromBasePath(linkTo(methodOn(RestProjectController.class).findRequirementLibraryContent(projectId, null, null))).withRel("requirements"));
        res.add(linkService.fromBasePath(linkTo(methodOn(RestProjectController.class).findTestCaseLibraryContent(projectId, null, null))).withRel("test-cases"));
        res.add(linkService.fromBasePath(linkTo(methodOn(RestProjectController.class).findCampaignLibraryContent(projectId, null, null))).withRel("campaigns"));
        res.add(linkService.fromBasePath(linkTo(methodOn(RestProjectController.class).findPartyPermissionsByProject(projectId))).withRel("permissions"));
        res.add(createRelationTo("attachments"));

        return ResponseEntity.ok(res);
    }

    @RequestMapping("/projects")
    @ResponseBody
    @DynamicFilterExpression("name")
    public ResponseEntity<PagedResources<Resource>> findAllReadableProjects(Pageable pageable){

        Page<GenericProject> projects = service.findAllReadable(pageable);

        PagedResources<Resource> res = toPagedResources(projects);

        return ResponseEntity.ok(res);

    }

    @RequestMapping(value = "/projects", method = RequestMethod.POST)
    @ResponseBody
    @DynamicFilterExpression("*")
    public ResponseEntity<Resource<GenericProject>> createGenericProject(@RequestBody GenericProjectDto genericProjectDto) throws BindException, InvocationTargetException, IllegalAccessException {

        validatePostGenericProject(genericProjectDto);
        GenericProject genericProject = service.createGenericProject(genericProjectDto);
        Resource<GenericProject> res = toResource(genericProject);

        res.add(linkService.fromBasePath(linkTo(methodOn(RestProjectController.class).findRequirementLibraryContent(genericProject.getId(), null, null))).withRel("requirements"));
        res.add(linkService.fromBasePath(linkTo(methodOn(RestProjectController.class).findTestCaseLibraryContent(genericProject.getId(), null, null))).withRel("test-cases"));
        res.add(linkService.fromBasePath(linkTo(methodOn(RestProjectController.class).findCampaignLibraryContent(genericProject.getId(), null, null))).withRel("campaigns"));

        return ResponseEntity.status(HttpStatus.CREATED).body(res);

    }

    // *********************** requirements **************************

    @RequestMapping(value = "/projects/{id}/requirements-library/content")
    @DynamicFilterExpression("name, reference")
    public ResponseEntity<PagedResources<Resource>> findRequirementLibraryContent(@PathVariable("id") long projectId,
                                                                                  Pageable paging,
                                                                                  ContentInclusion include) {
        Page<RequirementLibraryNode> nodes = null;
        switch (include) {
            case NESTED:
                nodes = service.findRequirementLibraryAllContent(projectId, paging);
                break;
            default:
                nodes = service.findRequirementLibraryRootContent(projectId, paging);
                break;
        }

        PagedResources<Resource> res = toPagedResourcesWithRel(nodes, "requirement-library-content");

        return ResponseEntity.ok(res);
    }


    @RequestMapping(value = "/projects/{id}/requirements")
    @DynamicFilterExpression("name, reference")
    public ResponseEntity<PagedResources<Resource>> findRequirementsByProject(@PathVariable("id") long projectId,
                                                                          Pageable paging) {

        Page<Requirement> reqs = service.findRequirementsByProject(projectId, paging);

        PagedResources<Resource> res = toPagedResources(reqs);

        return ResponseEntity.ok(res);
    }

    // *********************** test cases **************************


    /**
     * Returns the content of the test case library.
     *
     * @param projectId
     * @param paging
     * @param include   value is either {@link UriComponents#CONTENT_INCLUDE_ROOT} or {@link UriComponents#CONTENT_INCLUDE_NESTED}, if null defaults to CONTENT_INCLUDE_ROOT.
     * @return
     */
    @RequestMapping(value = "/projects/{id}/test-cases-library/content")
    @DynamicFilterExpression("name, reference")
    public ResponseEntity<PagedResources<Resource>> findTestCaseLibraryContent(@PathVariable("id") long projectId,
                                                                               Pageable paging,
                                                                               ContentInclusion include) {

        Page<TestCaseLibraryNode> nodes = null;
        switch (include) {
            case NESTED:
                nodes = service.findTestCaseLibraryAllContent(projectId, paging);
                break;
            default:
                nodes = service.findTestCaseLibraryRootContent(projectId, paging);
                break;
        }

        PagedResources<Resource> res = toPagedResourcesWithRel(nodes, "test-case-library-content");

        return ResponseEntity.ok(res);
    }

    @RequestMapping(value = "/projects/{id}/test-cases")
    @DynamicFilterExpression("name, reference")
    public ResponseEntity<PagedResources<Resource>> findTestCasesByProject(@PathVariable("id") long projectId,
                                                                               Pageable paging) {

        Page<TestCase> testCases = service.findTestCasesByProject(projectId, paging);

        PagedResources<Resource> res = toPagedResourcesWithRel(testCases, "test-cases");

        return ResponseEntity.ok(res);
    }


    //************************* campaigns ************************

    /**
     * Returns the content of the campaign library.
     *
     * @param projectId
     * @param paging
     * @param include   value is either {@link UriComponents#CONTENT_INCLUDE_ROOT} or {@link UriComponents#CONTENT_INCLUDE_NESTED}, if null defaults to CONTENT_INCLUDE_ROOT.
     * @return
     */
    @RequestMapping(value = "/projects/{id}/campaigns-library/content")
    @DynamicFilterExpression("name, reference")
    public ResponseEntity<PagedResources<Resource>> findCampaignLibraryContent(@PathVariable("id") long projectId,
                                                                               Pageable paging,
                                                                               ContentInclusion include) {

        Page<CampaignLibraryNode> nodes = null;
        switch (include) {
            case NESTED:
                nodes = service.findCampaignLibraryAllContent(projectId, paging);
                break;
            default:
                nodes = service.findCampaignLibraryRootContent(projectId, paging);
                break;
        }

        PagedResources<Resource> res = toPagedResourcesWithRel(nodes, "campaign-library-content");

        return ResponseEntity.ok(res);
    }

    @RequestMapping(value = "/projects/{id}/campaigns")
    @DynamicFilterExpression("name, reference")
    public ResponseEntity<PagedResources<Resource>> findCampaignsByProject(@PathVariable("id") long projectId,
                                                                           Pageable paging) {

        Page<Campaign> testCases = service.findCampaignsByProject(projectId, paging);

        PagedResources<Resource> res = toPagedResources(testCases);

        return ResponseEntity.ok(res);
    }


    //************************* permissions ************************

    @RequestMapping(value = "/projects/{id}/permissions")
    @DynamicFilterExpression("login, name")
    public ResponseEntity<Resource<RestPartyPermission>> findPartyPermissionsByProject(@PathVariable("id") long projectId) {

        RestPartyPermission partyPermissions = service.findAllPermissionsByProjectId(projectId);

        Resource<RestPartyPermission> res = new Resource<>(partyPermissions);
        linksHelper.addAllLinksForPartyPermission(projectId, res);

        return ResponseEntity.ok(res);
    }

    @RequestMapping(value = "/projects/{projectId}/permissions/{permissionGroup}", method = RequestMethod.POST)
    @DynamicFilterExpression("login, name")
    public ResponseEntity<Resource<RestPartyPermission>> postNewPermissionToProjectWithPartyIds(@PathVariable("projectId") long projectId,
                                                                                     @PathVariable("permissionGroup") String permissionGroup,
                                                                                     @RequestParam(value = "ids") List<Long> partyIds)
            throws BindException {

        PostProjectPermissionModel bean = new PostProjectPermissionModel(partyIds, permissionGroup);
        validatePostNewPermissionsToProject(bean);
        for (Long partyId : bean.partyIds) {
            service.addNewPermissionToProject(partyId, projectId, bean.permissionGroup);
        }

        RestPartyPermission partyPermissions = service.buildPartyPermissionDataModel(permissionGroup, partyIds);

        Resource<RestPartyPermission> res = new Resource<>(partyPermissions);
        linksHelper.addAllLinksForPartyPermission(projectId, res);

        return ResponseEntity.ok(res);
    }

    @DeleteMapping("/projects/{projectId}/users/{partyIds}")
    public ResponseEntity<Void> removePartyFromProject(@PathVariable("projectId") long projectId, @PathVariable("partyIds") List<Long> partyIds) throws BindException {

        // 1 - validation
        RemoveProjectPartyModel bean = new RemoveProjectPartyModel(partyIds);
        validateRemovePartyFromProject(bean);

        // 2 - action
        for (Long partyId : bean.partyIds) {
            service.deletePartyFromProject(partyId, projectId);
        }

        return ResponseEntity.noContent().build();
    }

    private void validateRemovePartyFromProject(RemoveProjectPartyModel bean) throws BindException {
        List<Errors> errors = new ArrayList<>();
        List<Long> partyIds = bean.partyIds;

        for (int i = 0; i < partyIds.size(); ++i) {
            Long partyId = partyIds.get(i);
            Party party = partyService.findById(partyId);
            if (party == null) {
                String fieldName = "partyIds["+i+"]";
                String message = "No id of user or team known for "+ partyId;
                BindingResult validation = new BeanPropertyBindingResult(bean, "remove-project-party");
                validation.rejectValue(fieldName, "invalid ids for user or team", message);
                errors.add(validation);
            }
        }

        ErrorHandlerHelper.throwIfError(bean, errors, "remove-project-party");
    }

    private void validatePostGenericProject(GenericProjectDto genericProjectDto) throws BindException {
        List<Errors> errors = new ArrayList<>();

        BindingResult validation = new BeanPropertyBindingResult(genericProjectDto, "post-generic-project");
        genericProjectPostValidator.validate(genericProjectDto, validation);

        if (validation.hasErrors()) {
            errors.add(validation);
        }

        ErrorHandlerHelper.throwIfError(genericProjectDto, errors, "post-generic-project");
    }

    private void validatePostNewPermissionsToProject(PostProjectPermissionModel bean) throws BindException {

        List<Errors> errors = new ArrayList<>();
        BindingResult validation = new BeanPropertyBindingResult(bean, "post-project-permission");
        int index = 0;
        for (Long partyId : bean.partyIds) {
            Party party = partyService.getOne(partyId);
            if (party == null) {
                String message = String.format("No id of user or team known for %d", partyId);
                String fieldName = String.format("partyIds[%d]", index);
                validation.rejectValue(fieldName, "invalid ids for user or team", message);
                errors.add(validation);
                validation = new BeanPropertyBindingResult(bean, "post-project-permission");
            }
            index += 1;
        }

        String rawData = bean.permissionGroup;

        // manual translation for advanced tester |*_*|
        if (bean.permissionGroup.equals("advanced_tester")){
            bean.permissionGroup = "advance_tester";
        }

        bean.permissionGroup = CaseFormat.LOWER_UNDERSCORE.to(CaseFormat.UPPER_CAMEL, bean.permissionGroup);

        if (!getAllPermissionGroups().contains(bean.permissionGroup)) {
            String message = String.format("No permission group known for %s", rawData);
            validation.rejectValue("permissionGroup", "invalid permission group", message);
            errors.add(validation);
        }

        ErrorHandlerHelper.throwIfError(bean, errors, "post-project-permission");

    }

    private List<String> getAllPermissionGroups(){
        List<PermissionGroup> groups = service.findAllPossiblePermission();
        List<String> groupNames = new ArrayList<>();
        for (PermissionGroup group : groups) {
            groupNames.add(group.getSimpleName());
        }

        return groupNames;
    }

    /*a private bean for validation propose*/
    private class PostProjectPermissionModel {
        private List<Long> partyIds;
        private String permissionGroup;

        private PostProjectPermissionModel(List<Long> partyIds, String permissionGroup) {
            this.partyIds = partyIds;
            this.permissionGroup = permissionGroup;
        }

        public List<Long> getPartyIds() {
            return partyIds;
        }

        public String getPermissionGroup() {
            return permissionGroup;
        }
    }

    private class RemoveProjectPartyModel {
        private List<Long> partyIds;

        public RemoveProjectPartyModel(List<Long> partyIds) {
            this.partyIds = partyIds;
        }

        public List<Long> getPartyIds() {return partyIds;}

    }
}
