/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.controller;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.PagedResources;
import org.springframework.hateoas.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.squashtest.tm.domain.requirement.Requirement;
import org.squashtest.tm.domain.requirement.RequirementVersion;
import org.squashtest.tm.plugin.rest.controller.helper.ResourceLinksHelper;
import org.squashtest.tm.plugin.rest.core.jackson.DynamicFilterExpression;
import org.squashtest.tm.plugin.rest.core.web.BaseRestController;
import org.squashtest.tm.plugin.rest.core.web.ContentInclusion;
import org.squashtest.tm.plugin.rest.core.web.EntityGetter;
import org.squashtest.tm.plugin.rest.core.web.RestApiController;
import org.squashtest.tm.plugin.rest.core.web.UseDefaultRestApiConfiguration;
import org.squashtest.tm.plugin.rest.jackson.model.RequirementDto;
import org.squashtest.tm.plugin.rest.service.RestRequirementVersionService;
import org.squashtest.tm.plugin.rest.service.RestVerifyingTestCaseManagerService;
import org.squashtest.tm.plugin.rest.validators.RequirementValidator;
import org.squashtest.tm.plugin.rest.validators.RequirementVersionValidator;

import javax.inject.Inject;
import java.util.List;

@RestApiController(Requirement.class)
@UseDefaultRestApiConfiguration
public class RestRequirementController extends BaseRestController {

    @Inject
    private RestRequirementVersionService service;

    @Inject
    private RestVerifyingTestCaseManagerService verifyingTestCaseManager;

    @Inject
    private RequirementValidator requirementValidator;

    @Inject
    private RequirementVersionValidator requirementVersionValidator;

    @Inject
    private ResourceLinksHelper linksHelper;

    @RequestMapping(value = "/requirements", method = RequestMethod.GET, params = {"!remote_key","!server_name"} )
    @ResponseBody
    @DynamicFilterExpression("name, current_version[reference]")
    public ResponseEntity<PagedResources<Resource>> findAllReadable(Pageable pageable) {

        Page<Requirement> reqs = service.findAllReadable(pageable);

        PagedResources<Resource> res = toPagedResources(reqs);

        return ResponseEntity.ok(res);
    }

    /*
     * This is a secret method created for jira plugin development, thus it will not documented. It exists 
     * because there are no search service that would fulfill such use case. 
     */
    @RequestMapping(value = "/requirements", method = RequestMethod.GET, params = {"remote_key","server_name"} )
    @ResponseBody
    @DynamicFilterExpression("*, project[name], parent[name], current_version[*, -requirement, verifying_test_cases[name]], versions[name,version_number]")
    public ResponseEntity<PagedResources<Resource>> findSynchronizedRequirement(@RequestParam(value = "remote_key") String remoteKey, @RequestParam(value = "server_name") String serverName, Pageable pageable) {

        List<Requirement> reqs = service.findSynchronizedRequirementsBy(remoteKey, serverName);
        /*[Issue 7435] Upgrading Springframework via Springboot upgrade make failed toPagedResources if the page in argument doesn't have a pageable.
        Therefore, we can't use PageImpl constructor with only a List of items as arguments.
         */
        Page pagedReqs = new PageImpl(reqs, pageable, reqs.size());

        PagedResources<Resource> res = toPagedResources(pagedReqs);

        /*
        res.add(linkService.createLinkTo(req.getProject()));
        res.add(linkService.createLinkTo(req.getCurrentVersion(), "current_version"));
        */

        return ResponseEntity.ok(res);

    }


    @RequestMapping("/requirements/{id}")
    @ResponseBody
    @DynamicFilterExpression("*, project[name], parent[name], current_version[*, -requirement, verifying_test_cases[name]], versions[name,version_number]")
    @EntityGetter
    public ResponseEntity<Resource<Requirement>> findRequirement(@PathVariable("id") long id) {

        Requirement req = service.findRequirement(id);

        Resource<Requirement> res = toResource(req);

        res.add(linkService.createLinkTo(req.getProject()));
        res.add(linkService.createLinkTo(req.getCurrentVersion(), "current_version"));

        return ResponseEntity.ok(res);
    }

    @RequestMapping("/requirements/{id}/children")
    @ResponseBody
    @DynamicFilterExpression("name, current_version[reference]")
    public ResponseEntity<PagedResources<Resource>> findAllReadable(@PathVariable("id") long requirementId,
                                                                    Pageable pageable,
                                                                    ContentInclusion include) {
        Page<Requirement> content = null;
        switch (include) {
            case NESTED:
                content = service.findRequirementAllChildren(requirementId, pageable);
                break;
            default:
                content = service.findRequirementChildren(requirementId, pageable);
                break;
        }

        PagedResources<Resource> res = toPagedResourcesWithRel(content, "children");

        return ResponseEntity.ok(res);

    }

    @RequestMapping(value = "/requirements", method = RequestMethod.POST)
    @ResponseBody
    @DynamicFilterExpression("*, project[name], parent[name], current_version[*, -requirement, verifying_test_cases[name]], versions[name,version_number]")
    public ResponseEntity<Resource<Requirement>> createRequirement(@RequestBody RequirementDto requirementDto) throws BindException {

        requirementValidator.validatePostRequirement(requirementDto);

        requirementVersionValidator.validateRequirementVersion(requirementDto.getCurrentVersion());

        Requirement req = service.createRequirement(requirementDto);

        Resource<Requirement> res = toResource(req);

        res.add(linkService.createLinkTo(req.getProject()));
        res.add(linkService.createLinkTo(req.getCurrentVersion(), "current_version"));

        return ResponseEntity.status(HttpStatus.CREATED).body(res);
    }

    /*AMK: modify requirement*/
    @RequestMapping(value = "/requirements/{id}", method = RequestMethod.PATCH)
    @ResponseBody
    @DynamicFilterExpression("*, project[name], parent[name], current_version[*, -requirement, verifying_test_cases[name]], versions[name,version_number]")
    public ResponseEntity<Resource<Requirement>> ModifyRequirement(@RequestBody RequirementDto requirementDto ,
                                                                        @PathVariable("id") Long requirementId)throws BindException{

        requirementValidator.validatePatchRequirement(requirementDto,requirementId);
        //validate current version
        requirementVersionValidator.validatePatchRequirementVersion(requirementDto.getCurrentVersion(), requirementId);


        Requirement requirement = service.modifyRequirement(requirementDto, requirementId);

        Resource<Requirement> res = toResource(requirement);

       linksHelper.addAllLinksForRequirement(res);

        return ResponseEntity.ok(res);
    }


    @RequestMapping(value = "/requirements/{ids}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> deleteRequirements(@PathVariable("ids") List<Long> reqIds) {
        service.deleteRequirements(reqIds);
        return ResponseEntity.noContent().build();
    }

    @ResponseBody
    @RequestMapping(value = "/requirements/{id}/coverages", method = RequestMethod.POST)
    @DynamicFilterExpression("*, project[name], parent[name], current_version[*, -requirement, verifying_test_cases[name]], versions[name, version__number]")
    public ResponseEntity<Resource<Requirement>> associateTestCases(
            @PathVariable("id") Long requirementId, @RequestParam("testCaseIds") List<Long> testCaseIds)
            throws BindException {
        requirementValidator.validateAssociateTestCases(requirementId, testCaseIds);
        Requirement requirement = service.findRequirement(requirementId);
        RequirementVersion requirementVersion = requirement.getCurrentVersion();
        verifyingTestCaseManager.addVerifyingTestCasesToRequirementVersion(testCaseIds, requirementVersion.getId());
        Resource<Requirement> res = toResource(requirement);
        res.add(linkService.createLinkTo(requirement.getProject()));
        res.add(linkService.createLinkTo(requirement.getCurrentVersion(), "current_version"));
        return ResponseEntity.ok(res);
    }

    @RequestMapping(value = "/requirements/{id}/coverages/{testCaseIds}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> removeVerifyingTestCasesFromRequirement(@PathVariable("id") Long requirementId, @PathVariable("testCaseIds") List<Long> testCaseIds) throws BindException {
        requirementValidator.validateDisassociateTestCases(requirementId, testCaseIds);
        Long requirementVersionId = service.findCurrentVersionIdByRequirementId(requirementId);
        verifyingTestCaseManager.removeVerifyingTestCasesToRequirementVersion(testCaseIds, requirementVersionId);
        return ResponseEntity.noContent().build();
    }

}
