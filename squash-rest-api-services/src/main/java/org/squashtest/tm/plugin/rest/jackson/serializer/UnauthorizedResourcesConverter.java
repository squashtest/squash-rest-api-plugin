/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.jackson.serializer;

import com.fasterxml.jackson.databind.util.StdConverter;
import org.springframework.hateoas.RelProvider;
import org.springframework.hateoas.Resource;
import org.springframework.stereotype.Component;
import org.squashtest.tm.domain.Identified;
import org.squashtest.tm.plugin.rest.jackson.model.UnauthorizedResource;
import org.squashtest.tm.service.security.PermissionEvaluationService;

import javax.inject.Inject;

/**
 * That converter will test if the client can read the resource and will either let it pass if ok or replace the resource with 
 * a a dummy.
 * 
 * @author bsiri
 *
 */
@Component
public class UnauthorizedResourcesConverter extends StdConverter<Resource<? extends Identified>, Resource<?>> {

	@Inject
	private PermissionEvaluationService permService;
	
	@Inject
	private RelProvider relProvider;
	
	@Override
	public Resource<?> convert(Resource<? extends Identified> value) {
		if (permService.canRead(value.getContent())){
			return value;
		}
		else{
			return createUnauthorized(value);
		}
	}
	
	
	private <T extends Identified> Resource<UnauthorizedResource> createUnauthorized(Resource<T> resource){
		
		T entity = resource.getContent();
		
		String entityType = relProvider.getItemResourceRelFor(entity.getClass());
		UnauthorizedResource unauthRes = new UnauthorizedResource(entityType, entity.getId());
		
		return new Resource<UnauthorizedResource>(unauthRes, resource.getLinks());
	}

}
