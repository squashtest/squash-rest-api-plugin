/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.validators;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.squashtest.tm.domain.customfield.DenormalizedCustomFieldOption;
import org.squashtest.tm.domain.customfield.InputType;
import org.squashtest.tm.domain.customfield.RawValue;
import org.squashtest.tm.domain.denormalizedfield.DenormalizedFieldHolder;
import org.squashtest.tm.domain.denormalizedfield.DenormalizedFieldValue;
import org.squashtest.tm.domain.denormalizedfield.DenormalizedFieldVisitor;
import org.squashtest.tm.domain.denormalizedfield.DenormalizedMultiSelectField;
import org.squashtest.tm.domain.denormalizedfield.DenormalizedNumericValue;
import org.squashtest.tm.domain.denormalizedfield.DenormalizedSingleSelectField;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.plugin.rest.core.exception.ProgrammingError;
import org.squashtest.tm.plugin.rest.core.jackson.DeserializationHints;
import org.squashtest.tm.plugin.rest.core.jackson.WrappedDTO;
import org.squashtest.tm.plugin.rest.core.validation.HintedValidator;
import org.squashtest.tm.plugin.rest.jackson.model.CustomFieldValueDto;
import org.squashtest.tm.plugin.rest.jackson.model.WrappedDtoWithDenormalizedFields;
import org.squashtest.tm.service.denormalizedfield.DenormalizedFieldValueManager;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * DOES NOT WORK FOR CREATION (POST) because it cannot know what the would-be values are because the reference to their source
 * entity is missing.
 *
 * Validates the custom fields for instances of {@link WrappedDtoWithDenormalizedFields} & {@link org.squashtest.tm.plugin.rest.core.jackson.WrappedDTO}
 *
 *
 */
@Component
public class DenormalizedFieldValueHintedValidator implements HintedValidator {


    private static final DateFormat DATE_TESTER;

    public static final String DENORMALIZED_FIELDS = "denormalizedFields";
    public static final String INVALID_VALUE = "invalid value";
    public static final String TRUE = "true";
    public static final String FALSE = "false";

    static {
        DATE_TESTER = new SimpleDateFormat("yyyy-MM-dd");
        DATE_TESTER.setLenient(false);
    }

    @Inject
    private DenormalizedFieldValueManager denoFinder;

    @Override
    public boolean supports(Class<?> clazz) {
        return (WrappedDtoWithDenormalizedFields.class.isAssignableFrom(clazz) &&
                WrappedDTO.class.isAssignableFrom(clazz));

    }


    @Override
    public void validate(Object target, Errors errors) {
        // NO-OP
    }

    @Override
    public void validateWithHints(Object target, Errors errors, DeserializationHints hints) {

        WrappedDtoWithDenormalizedFields wrapped = (WrappedDtoWithDenormalizedFields) target;

        Project project = hints.getProject();

        Object holder = ((WrappedDTO)wrapped).getWrapped();

        if (! DenormalizedFieldHolder.class.isAssignableFrom(holder.getClass())){
            throw new ProgrammingError("Attempted to process denormalized field values for an entity that cannot have one : '"
                    +holder.getClass()+"'");
        }

        DenormalizedFieldHolder entity = (DenormalizedFieldHolder)holder;
        List<CustomFieldValueDto> dtos = wrapped.getDenormalizedFields();
        List<DenormalizedFieldValue> currentValues = denoFinder.findAllForEntity(entity);

        DenormalizedValueValidationHelper helper =new DenormalizedValueValidationHelper(errors, dtos, currentValues);
        helper.validate();

    }


    private static final class DenormalizedValueValidationHelper{

        private Errors errors;
        private List<CustomFieldValueDto> dtos;
        private List<DenormalizedFieldValue> currentValues;

        DenormalizedValueValidationHelper(Errors errors, List<CustomFieldValueDto> dtos, List<DenormalizedFieldValue> currentValues) {
            this.dtos = dtos;
            this.currentValues = currentValues;
            this.errors = errors;
        }

        void validate(){
            checkCodes();
            checkValues();
        }

        private void checkCodes(){
            for (CustomFieldValueDto dto : dtos){

                DenormalizedFieldValue denoValue = locateByCode(dto.getCode());

                if (denoValue == null){
                    errors.rejectValue(DENORMALIZED_FIELDS,"invalid code","there is no field with code '"+dto.getCode()+"'");
                }

                RawValue value = dto.getValue();

                /* A DenormalizedFieldValue is never mandatory. //
                if (value!= null && value.isEmpty()){
                    errors.rejectValue(DENORMALIZED_FIELDS, INVALID_VALUE,"values cannot be empty. If you do not wish to modify a value, " +
                            "just omit it from the json payload");
                }
                */
            }
        }

        private void checkValues(){
            for (CustomFieldValueDto dto : dtos){

                RawValue value = dto.getValue();

                String code = dto.getCode();
                DenormalizedFieldValue denoValue = locateByCode(code);

                if (denoValue != null){
                    DenormalizedFieldValidationVisitor visitor = new DenormalizedFieldValidationVisitor(errors, dto, value);
                    denoValue.accept(visitor);
                }

            }
        }

        private DenormalizedFieldValue locateByCode(String code){
            for (DenormalizedFieldValue value : currentValues){
                if (value.getCode().equals(code)){
                    return value;
                }
            }
            return null;
        }

    }

    private static final class DenormalizedFieldValidationVisitor implements DenormalizedFieldVisitor{

        private Errors errors;
        private CustomFieldValueDto dto;
        private RawValue value;

        public DenormalizedFieldValidationVisitor(Errors errors, CustomFieldValueDto dto, RawValue value) {
            this.errors = errors;
            this.dto = dto;
            this.value = value;
        }

        // well, the definition of that visitor interface is not quite helpful.
        // it seems we'll have to explicitly test the classes anyway.
        @Override
        public void visit(DenormalizedFieldValue denoValue) {

            checkIsScalarValue(denoValue);

            Class<?> clazz = denoValue.getClass();
            if (DenormalizedSingleSelectField.class.isAssignableFrom(clazz)){
                checkOptions((DenormalizedSingleSelectField)denoValue);
            }
            else if (DenormalizedNumericValue.class.isAssignableFrom(clazz)){
                checkNumeric((DenormalizedNumericValue)denoValue);
            }

            else if (denoValue.getInputType() == InputType.DATE_PICKER){
                checkDate(denoValue);
            }

            else if (denoValue.getInputType() == InputType.CHECKBOX){
                checkBoolean(denoValue);
            }

            // else regular field, and we just checked that the
            // value is a scalar already


        }

        @Override
        public void visit(DenormalizedMultiSelectField multiselect) {
            checkIsMultiValue(multiselect);
        }


        private void checkDate(DenormalizedFieldValue denoValue){
            if(value == null || value.getValue().isEmpty()) {
                return;
            }
            String strDate=value.getValue();
            try{
                DATE_TESTER.parse(strDate);
            } catch (ParseException e) {
                errors.rejectValue(DENORMALIZED_FIELDS, INVALID_VALUE,
                        "The field '"+denoValue.getLabel()+"' identified by label '"+denoValue.getLabel()+"' " +
                                "accepts dates using format 'yyyy-mm-dd' only");

            }
        }
        
        private void checkBoolean(DenormalizedFieldValue denoValue){
            if(value == null) {
                return;
            }
            String strBool = value.getValue().toLowerCase();
            boolean check = strBool.equals(TRUE) || strBool.equals(FALSE);
            if (! check){
                errors.rejectValue(DENORMALIZED_FIELDS, INVALID_VALUE,
                        "The field '"+denoValue.getLabel()+"' identified by label '"+denoValue.getLabel()+"' " +
                                        "accepts boolean 'true' or 'false'");
            }
        }

        private void checkNumeric(DenormalizedNumericValue denoValue){
            if(value == null || value.getValue().isEmpty()) {
                return;
            }
            String numberAsString = value.getValue();
            try {
                new BigDecimal(numberAsString);
            } catch (NumberFormatException nfe) {
                String message = String.format("The field %s identified by code %s accept numeric value. The value provided is not convertible to number", denoValue.getLabel(), denoValue.getCode(), value.getValue());
                errors.rejectValue(DENORMALIZED_FIELDS, INVALID_VALUE, message);
            }
        }

        private void checkOptions(DenormalizedSingleSelectField listField){
            List<DenormalizedCustomFieldOption> options = listField.getOptions();
            boolean valid = false;
            for (DenormalizedCustomFieldOption option : options) {
                if (option.getLabel().equals(dto.getValue().getValue())) {
                    valid = true;
                }
            }
            if (!valid) {
                errors.rejectValue(DENORMALIZED_FIELDS, INVALID_VALUE, "Unknown value for list : " + listField.getCode() + " . Options are identified by label.");
            }
        }

        private void checkIsScalarValue(DenormalizedFieldValue denoValue){
            if (value == null || value.getValues() == null || value.getValues().isEmpty()) {
                return;
            }
            String message = String.format("The field %s identified by code %s is a single-valued field. You provided a json array with values : %s. Please provide a standard attribute not an array.", denoValue.getLabel(), denoValue.getCode(), value.getValues());
            errors.rejectValue(DENORMALIZED_FIELDS, INVALID_VALUE, message);
        }

        private void checkIsMultiValue(DenormalizedMultiSelectField denoValue){
            if (value != null && (value.getValues()== null || value.getValues().isEmpty())) {
                String message = String.format("The cuf %s identified by code %s is a multi-valued field. You provided standard attribute with value : %s. Please provide json array.", denoValue.getLabel(), denoValue.getCode(), value.getValue());
                errors.rejectValue(DENORMALIZED_FIELDS, INVALID_VALUE, message);
            }
        }

    }



}
