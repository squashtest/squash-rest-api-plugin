/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.jackson.serializer;

import com.fasterxml.jackson.databind.util.StdConverter;
import org.springframework.hateoas.Resource;
import org.springframework.stereotype.Component;
import org.squashtest.tm.domain.Identified;
import org.squashtest.tm.plugin.rest.core.web.BasicResourceAssembler;

import javax.inject.Inject;

/**
 * A Converter that convert an entity into a resource. Note : BasicResourceAssembler could have itself extended StdConverter<T, Resource<T>>, but I prefer not to mix the roles of 
 * ResourceAssembler and Converter
 * 
 * @author bsiri
 *
 * @param <T>
 */
@Component
public class HateoasWrapperConverter<T extends Identified> extends StdConverter<T, Resource<T>>{


	@Inject
	private BasicResourceAssembler resAssembler;
	
	@Override
	@SuppressWarnings("unchecked")
	public Resource<T> convert(T value) {
		return (Resource<T>)resAssembler.toResource(value);
	}

	
	
}
