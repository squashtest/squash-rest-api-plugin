/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.jackson.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.fasterxml.jackson.databind.type.TypeFactory;
import org.springframework.hateoas.Resource;
import org.springframework.stereotype.Component;
import org.squashtest.tm.domain.Identified;
import org.squashtest.tm.plugin.rest.core.web.BasicResourceAssembler;

import javax.inject.Inject;
import java.io.IOException;
import java.util.Collection;

/**
 * Same as {@link HateoasWrapperConverter}, but accepts a collection instead
 * 
 * @author bsiri
 *
 * @param <T>
 */
@Component
public class CollectionHateoasWrapperSerializer<T extends Identified> extends StdSerializer<Collection<T>>{

	@Inject
	private BasicResourceAssembler resAssembler;

    public CollectionHateoasWrapperSerializer() {
        super(TypeFactory.defaultInstance().constructType(Collection.class));
    }

    @Override
    public void serialize(Collection<T> value, JsonGenerator gen, SerializerProvider provider) throws IOException {
        gen.writeStartArray();
        JsonSerializer valSerializer = provider.findValueSerializer(Resource.class);
        for(T v : value){
            Resource res = resAssembler.toResource(v);
            valSerializer.serialize(res, gen, provider);
        }
        gen.writeEndArray();
    }
	
}
