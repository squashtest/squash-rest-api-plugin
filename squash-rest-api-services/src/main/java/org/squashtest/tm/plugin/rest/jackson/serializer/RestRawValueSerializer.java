/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.jackson.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import org.squashtest.tm.domain.customfield.RawValue;

import java.io.IOException;
import java.util.List;

@SuppressWarnings("serial")
public class RestRawValueSerializer extends StdSerializer<RawValue>{

	public RestRawValueSerializer(){
		super(RawValue.class);
	}
	
	@Override
	public void serialize(RawValue value, JsonGenerator gen, SerializerProvider provider) throws IOException {
		// is it empty ?
		if (value.isEmpty()){
			gen.writeNull();
		}
		// has it multiple values ? 
		else if (value.getValues() != null){
			List<String> values = value.getValues();
			
			gen.writeStartArray(values.size());
			for (String str : values){
				gen.writeString(str);
			}
			gen.writeEndArray();
		}
		// must have a single value then
		else{
			gen.writeString(value.getValue());
		}
	}

}
