/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.jackson.mixin;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;
import com.fasterxml.jackson.annotation.JsonUnwrapped;
import com.fasterxml.jackson.databind.annotation.JsonAppend;
import com.fasterxml.jackson.databind.annotation.JsonAppend.Prop;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.squashtest.tm.domain.audit.AuditableSupport;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.plugin.rest.jackson.serializer.AttachmentHolderPropertyWriter;
import org.squashtest.tm.plugin.rest.jackson.serializer.HateoasWrapperConverter;
import org.squashtest.tm.plugin.rest.jackson.serializer.ParentNodePropertyWriter;
import org.squashtest.tm.plugin.rest.jackson.serializer.PathPropertyWriter;

@JsonAutoDetect(fieldVisibility=Visibility.NONE, getterVisibility=Visibility.NONE, isGetterVisibility=Visibility.NONE)
@JsonAppend(props={@Prop(name="path", value=PathPropertyWriter.class),
		@Prop(name="parent", value=ParentNodePropertyWriter.class),
		@Prop(name="attachments", value= AttachmentHolderPropertyWriter.class)})
public abstract class RestGenericLibraryNodeMixin extends RestIdentifiedMixin {

	@JsonProperty
	String name;
	
	@JsonProperty
	String description;
	
	@JsonProperty
	@JsonSerialize(converter=HateoasWrapperConverter.class)
	@JsonTypeInfo(use=Id.NONE)
	Project project;

	@JsonUnwrapped
	abstract AuditableSupport getAudit();
	
}
