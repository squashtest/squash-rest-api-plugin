/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.jackson.model;

import org.squashtest.tm.service.attachment.RawAttachment;

import java.io.InputStream;

/**
 * Duplicated class of UploadedData in SquashTM web package
 */
public class RestUploadedData implements RawAttachment {

    private InputStream stream;
    private String name;
    private long sizeInBytes;

    public RestUploadedData(InputStream stream, String name, long sizeInBytes) {
        super();
        this.stream = stream;
        this.name = name;
        this.sizeInBytes = sizeInBytes;
    }

    @Override
    public InputStream getStream() {
        return stream;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public long getSizeInBytes() {
        return sizeInBytes;
    }
}
