/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.controller;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.PagedResources;
import org.springframework.hateoas.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.squashtest.tm.domain.testautomation.TestAutomationServer;
import org.squashtest.tm.plugin.rest.core.jackson.DynamicFilterExpression;
import org.squashtest.tm.plugin.rest.core.web.BaseRestController;
import org.squashtest.tm.plugin.rest.core.web.EntityGetter;
import org.squashtest.tm.plugin.rest.core.web.RestApiController;
import org.squashtest.tm.plugin.rest.core.web.UseDefaultRestApiConfiguration;
import org.squashtest.tm.plugin.rest.service.RestTestAutomationServerService;

import javax.inject.Inject;

@RestApiController(TestAutomationServer.class)
@UseDefaultRestApiConfiguration
public class RestTestAutomationServerController extends BaseRestController {

    @Inject
    private RestTestAutomationServerService serverService;

    @RequestMapping(value = "/test-automation-servers/{id}", method = RequestMethod.GET)
    @EntityGetter
    @ResponseBody
    @DynamicFilterExpression("*")
    public ResponseEntity<Resource<TestAutomationServer>> findTestAutomationServer(@PathVariable("id") long serverId) {
        TestAutomationServer server = serverService.getOne(serverId);
        Resource<TestAutomationServer> res = toResource(server);

        return ResponseEntity.ok(res);
    }

    @RequestMapping(value = "/test-automation-servers", method = RequestMethod.GET)
    @ResponseBody
    @DynamicFilterExpression("*")
    public ResponseEntity<PagedResources<Resource>> findAllReadableTestAutomationServer(Pageable pageable) {

        Page<TestAutomationServer> tcs = serverService.findAllReadable(pageable);

        PagedResources<Resource> res = toPagedResources(tcs);

        return ResponseEntity.ok(res);
    }

}
