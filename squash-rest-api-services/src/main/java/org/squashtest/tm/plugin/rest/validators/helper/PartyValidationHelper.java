/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.validators.helper;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.squashtest.tm.domain.users.UsersGroup;
import org.squashtest.tm.plugin.rest.jackson.model.UserDto;
import org.squashtest.tm.service.internal.repository.UsersGroupDao;

import javax.inject.Inject;

@Component
public class PartyValidationHelper {

    @Inject
    private UsersGroupDao groupDao;

    public void checkAndAssignValues(Errors errors, UserDto userDto) {
        String groupName = userDto.getGroup().getSimpleName();
        if (groupName.equalsIgnoreCase("admin")) {
            UsersGroup group = groupDao.findByQualifiedName(UsersGroup.ADMIN);
            userDto.setGroupId(group.getId());
        } else if (groupName.equalsIgnoreCase("user")) {
            UsersGroup group = groupDao.findByQualifiedName(UsersGroup.USER);
            userDto.setGroupId(group.getId());
        } else if (groupName.equalsIgnoreCase("testAutomationServer")) {
            UsersGroup group = groupDao.findByQualifiedName(UsersGroup.TEST_AUTOMATION_SERVER);
            userDto.setGroupId(group.getId());
        } else {
            errors.rejectValue("group", "invalid group", "Invalid group value for this user. ");
        }
    }

}
