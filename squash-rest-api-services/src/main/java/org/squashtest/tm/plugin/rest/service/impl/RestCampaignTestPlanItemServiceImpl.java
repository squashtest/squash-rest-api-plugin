/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.service.impl;

import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.core.foundation.lang.Couple;
import org.squashtest.tm.domain.campaign.Campaign;
import org.squashtest.tm.domain.campaign.CampaignTestPlanItem;
import org.squashtest.tm.domain.testcase.Dataset;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.domain.users.User;
import org.squashtest.tm.plugin.rest.jackson.model.CampaignTestPlanItemDto;
import org.squashtest.tm.plugin.rest.jackson.model.DatasetDto;
import org.squashtest.tm.plugin.rest.jackson.model.TestCaseDto;
import org.squashtest.tm.plugin.rest.repository.RestCampaignTestPlanItemRepository;
import org.squashtest.tm.plugin.rest.service.RestCampaignTestPlanItemService;
import org.squashtest.tm.service.campaign.CampaignTestPlanManagerService;
import org.squashtest.tm.service.internal.repository.CampaignDao;
import org.squashtest.tm.service.internal.repository.CampaignTestPlanItemDao;
import org.squashtest.tm.service.internal.repository.DatasetDao;
import org.squashtest.tm.service.internal.repository.TestCaseDao;
import org.squashtest.tm.service.internal.repository.UserDao;

import javax.inject.Inject;
import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.squashtest.tm.service.security.Authorizations.OR_HAS_ROLE_ADMIN;

/**
 * Created by jthebault on 22/06/2017.
 */
@Service
@Transactional
public class RestCampaignTestPlanItemServiceImpl implements RestCampaignTestPlanItemService {

    private static final String LINK_CAMPAIGN_OR_ROLE_ADMIN =
            "hasPermission(#campaignId, 'org.squashtest.tm.domain.campaign.Campaign', 'LINK')" + OR_HAS_ROLE_ADMIN;
    private static final String CAMPAIGN_ID = "campaignId";
    @Inject
    private RestCampaignTestPlanItemRepository ctpiRepository;

    @Inject
    private CampaignDao campaignDao;
    @Inject
    private TestCaseDao testCaseDao;
    @Inject
    private DatasetDao datasetDao;
    @Inject
    private UserDao userDao;
    @Inject
    private CampaignTestPlanItemDao campaignTestPlanItemDao;
    @Inject
    private CampaignTestPlanManagerService campaignTestPlanManagerService;

    @Override
    @PostAuthorize("hasPermission(returnObject , 'READ') or hasRole('ROLE_ADMIN')")
    @Transactional(readOnly=true)
    public CampaignTestPlanItem getOne(long id) {
        CampaignTestPlanItem ctpi = ctpiRepository.getOne(id);
        if(ctpi == null){
            throw new EntityNotFoundException();
        }
        return ctpi;
    }

    @Override
    @PreAuthorize("@apiSecurity.hasPermission(#campaignId,'org.squashtest.tm.domain.campaign.Campaign' , 'LINK')")
    public CampaignTestPlanItem addTestCaseToCampaign(CampaignTestPlanItemDto ctpiDto, Long campaignId) {

        TestCaseDto referencedTcDTo = ctpiDto.getReferencedTestCase();

        DatasetDto referencedDatasetDto = ctpiDto.getReferencedDataset();

        Campaign campaign = campaignDao.findById(campaignId);

        TestCase testCase = testCaseDao.findById(referencedTcDTo.getId());

        User assignedTo =  ctpiDto.getUser() != null ? userDao.findUserByLogin(ctpiDto.getUser()) : null;

        Dataset ds = referencedDatasetDto != null ? datasetDao.getOne(referencedDatasetDto.getId()) : null;

        CampaignTestPlanItem ctpi = new CampaignTestPlanItem(testCase, ds);

        ctpi.setUser(assignedTo);

        campaign.addToTestPlan(ctpi);

        campaignTestPlanItemDao.persist(ctpi);

        return ctpi;
    }

    @Override
    @PreAuthorize("@apiSecurity.hasPermission(#testPlanId,'org.squashtest.tm.domain.campaign.CampaignTestPlanItem' , 'WRITE')")
    public CampaignTestPlanItem modifyCampaignTestPlan(CampaignTestPlanItemDto ctpiDto, Long testPlanId) {

        CampaignTestPlanItem ctpi = campaignTestPlanItemDao.findById(testPlanId);

        Dataset ds = ctpiDto.getReferencedDataset()!=null ? datasetDao.getOne(ctpiDto.getReferencedDataset().getId()) : null;
        User assignedTo = ctpiDto.getUser()!=null ?  userDao.findUserByLogin(ctpiDto.getUser()): null;

        if(ctpiDto.isHasSetDataset()){
            ctpi.setReferencedDataset(ds);
        }
        if(ctpiDto.isAssignedTo()) {
            ctpi.setUser(assignedTo);
        }
        return ctpi;
    }

    @Override
    public void deleteCampaignTestPlan(List<Long> testPlanIds) {
        //Couple(idItem, idCampaign)
        List<Couple<Long,Long>> listIds=ctpiRepository.findCampaignByIdItem(testPlanIds);
        //Map k= id campaign et v = liste des items à supprimer
        Map<Long, List<Long>> mapItpi =
                listIds.stream().collect(Collectors.groupingBy(Couple::getA1,
                        Collectors.mapping(
                                Couple::getA2,
                                Collectors.toList()
                        )));
        mapItpi.forEach((k,v)-> {
            campaignTestPlanManagerService.removeTestPlanItems(k, v);
        });
    }

}
