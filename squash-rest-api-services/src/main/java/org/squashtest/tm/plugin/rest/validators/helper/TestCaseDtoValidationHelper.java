/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.validators.helper;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.squashtest.tm.domain.infolist.InfoListItem;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.domain.testautomation.AutomatedTestTechnology;
import org.squashtest.tm.plugin.rest.jackson.model.KeywordTestCaseDto;
import org.squashtest.tm.plugin.rest.jackson.model.RestType;
import org.squashtest.tm.plugin.rest.jackson.model.ScriptedTestCaseDto;
import org.squashtest.tm.plugin.rest.jackson.model.TestCaseDto;
import org.squashtest.tm.plugin.rest.jackson.model.TestCaseDtoVisitor;
import org.squashtest.tm.service.infolist.InfoListItemFinderService;
import org.squashtest.tm.service.testautomation.AutomatedTestTechnologyFinderService;

import javax.inject.Inject;
import java.util.stream.Collectors;

/**
 * Created by jthebault on 13/06/2017.
 */
@Component
public class TestCaseDtoValidationHelper extends RestNodeValidationHelper {

    @Inject
    private InfoListItemFinderService infoListItemFinderService;
    @Inject
    private AutomatedTestTechnologyFinderService automatedTestTechnologyFinderService;

    private final static String NONE_VALUE = "None";

    public void checkInvalidAttributeForEachTestCaseType(Errors errors, TestCaseDto iTestCase) {
        TestCaseDtoVisitor testCaseDtoVisitor = new TestCaseDtoVisitor() {
            @Override
            public void visit(TestCaseDto testCaseDto) {
            }

            @Override
            public void visit(ScriptedTestCaseDto scriptedTestCaseDto) {
                if (iTestCase.getSteps() != null) {
                    errors.rejectValue("steps", "invalid steps", "Script test case should not have steps");
                }
                if (iTestCase.getParameters() != null) {
                    errors.rejectValue("parameters", "invalid parameters", "Script test case should not have parameters");
                }
                if (iTestCase.getDatasets() != null) {
                    errors.rejectValue("datasets", "invalid datasets", "Script test case should not have datasets");
                }
            }

            @Override
            public void visit(KeywordTestCaseDto keywordTestCaseDto) {
            }
        };

        iTestCase.accept(testCaseDtoVisitor);
    }

    public void assignInfoList(Errors errors, TestCaseDto iTestCase) {
        checkAndAssignType(errors, iTestCase);
        checkAndAssignNature(errors, iTestCase);
    }

    private void checkAndAssignNature(Errors errors, TestCaseDto iTestCase) {
        InfoListItem nature = iTestCase.getNature();
        if (nature != null) {
            String code = nature.getCode();
            Project project = iTestCase.getProject();
            if (project == null) {
                throw new IllegalArgumentException("Programmatic error : You must give a project to a Rest Node to be able to check infolist.");
            }
            if (infoListItemFinderService.isNatureConsistent(project.getId(), code)) {
                iTestCase.setNature(infoListItemFinderService.findByCode(code));
            } else {
                errors.rejectValue("nature", "invalid nature", "Invalid test case nature for this project");
            }
        }
    }

    private void checkAndAssignType(Errors errors, TestCaseDto iTestCase) {
        InfoListItem type = iTestCase.getType();
        if (type != null) {
            String code = type.getCode();
            if (infoListItemFinderService.isTypeConsistent(iTestCase.getProject().getId(), code)) {
                iTestCase.setType(infoListItemFinderService.findByCode(code));
            } else {
                errors.rejectValue("type", "invalid type", "Invalid test case type for this project");
            }
        }
    }

    public void checkAutomationAttributes(Errors errors, TestCaseDto patch) {
        if (patch.getAutomatedTestTechnology() != null) {
            checkAutomatedTestTechnologyAttribute(errors, patch);
        }
        if (patch.getScmRepositoryId() != null) {
            checkScmRepositoryAttribute(errors, patch);
        } else if (patch.getScmRepositoryUrl() != null) {
            errors.rejectValue("scmRepositoryUrl", "invalid attribute", "Scm Repository Url can not be modified directly. Use instead scm_repository_id to modify the associated scm repository.");
        }
    }

    private void checkAutomatedTestTechnologyAttribute(Errors errors, TestCaseDto patch) {
        AutomatedTestTechnology techno = automatedTestTechnologyFinderService.findByNameIgnoreCase(patch.getAutomatedTestTechnology());
        if (techno == null && !NONE_VALUE.equalsIgnoreCase(patch.getAutomatedTestTechnology())) {
            String allAutomatedTestTechnologies =
                    automatedTestTechnologyFinderService.getAllAvailableAutomatedTestTechnology()
                            .stream()
                            .map(AutomatedTestTechnology::getName)
                            .collect(Collectors.joining(", "));
            String message = String.format("No entity known for %s and name %s. Available values are : %s, %s",
                    AutomatedTestTechnology.class,
                    patch.getAutomatedTestTechnology(),
                    NONE_VALUE,
                    allAutomatedTestTechnologies);
            errors.rejectValue("automatedTestTechnology", "invalid automated test technology name", message);
        }
    }

    private void checkScmRepositoryAttribute(Errors errors, TestCaseDto patch) {
        checkEntityExist(errors, RestType.SCM_REPOSITORY, patch.getScmRepositoryId());
    }

}
