/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.jackson.serializer;

import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import org.springframework.hateoas.Resource;

import java.io.IOException;

/**
 * Serialize a {@link Resource} just like if no view was applied, and forwards the {@link JsonView} to the content serialization only. 
 * Otherwise the use of a view would prevent the normal serialization, as the mixins for Resource, ResourceSupport etc have no JsonView annotation.
 * 
 * @author bsiri
 *
 */

/*
 * 
 * TODOOOOO
 * 
 */
@SuppressWarnings("serial")
public class ResourceSerializer extends StdSerializer<Resource<?>>{

	
	
	public ResourceSerializer(Class<?> t, boolean dummy) {
		super(t, dummy);
		// TODO Auto-generated constructor stub
	}

	public ResourceSerializer(Class<Resource<?>> t) {
		super(t);
		// TODO Auto-generated constructor stub
	}

	public ResourceSerializer(JavaType type) {
		super(type);
		// TODO Auto-generated constructor stub
	}

	public ResourceSerializer(StdSerializer<?> src) {
		super(src);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void serialize(Resource<?> value, JsonGenerator gen, SerializerProvider provider) throws IOException {
		// TODO Auto-generated method stub
		
	}

}
