/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.service.impl;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.domain.campaign.Campaign;
import org.squashtest.tm.domain.campaign.CampaignLibraryNode;
import org.squashtest.tm.domain.project.GenericProject;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.domain.project.ProjectTemplate;
import org.squashtest.tm.domain.requirement.Requirement;
import org.squashtest.tm.domain.requirement.RequirementLibraryNode;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.domain.testcase.TestCaseLibraryNode;
import org.squashtest.tm.domain.users.Party;
import org.squashtest.tm.domain.users.PartyProjectPermissionsBean;
import org.squashtest.tm.plugin.rest.jackson.model.GenericProjectCopyParameterDto;
import org.squashtest.tm.plugin.rest.jackson.model.GenericProjectDto;
import org.squashtest.tm.plugin.rest.jackson.model.GenericProjectDtoVisitor;
import org.squashtest.tm.plugin.rest.jackson.model.ProjectDto;
import org.squashtest.tm.plugin.rest.jackson.model.ProjectTemplateDto;
import org.squashtest.tm.plugin.rest.jackson.model.RestPartyPermission;
import org.squashtest.tm.plugin.rest.repository.RestGenericProjectRepository;
import org.squashtest.tm.plugin.rest.service.RestPartyService;
import org.squashtest.tm.plugin.rest.service.RestProjectService;
import org.squashtest.tm.security.acls.PermissionGroup;
import org.squashtest.tm.service.project.GenericProjectManagerService;
import org.squashtest.tm.service.project.ProjectManagerService;
import org.squashtest.tm.service.project.ProjectTemplateManagerService;
import org.squashtest.tm.service.security.PermissionEvaluationService;

import javax.inject.Inject;
import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;

import static java.util.Objects.isNull;
import static org.squashtest.tm.service.security.Authorizations.OR_HAS_ROLE_ADMIN;

@Service
@Transactional
public class RestProjectServiceImpl implements RestProjectService {

    private static final String NAMESPACE = "squashtest.acl.group.tm.";

    @Inject
    private RestGenericProjectRepository dao;

    @Inject
    private PermissionEvaluationService permService;

    @Inject
    private GenericProjectManagerService genericProjectManager;

    @Inject
    private ProjectManagerService projectManager;

    @Inject
    private ProjectTemplateManagerService projectTemplateManagerService;

    @Inject
    private RestPartyService restPartyService;

    @Override
    @PreAuthorize("hasPermission(#id, 'org.squashtest.tm.domain.project.Project' , 'MANAGEMENT')"
            + " or hasPermission(#id, 'org.squashtest.tm.domain.project.ProjectTemplate' , 'MANAGEMENT')"
            + OR_HAS_ROLE_ADMIN)
    public GenericProject getOne(long id) {
        return dao.getOne(id);
    }

    public GenericProject getOneByName(String projectName) {
        GenericProject project = dao.getOneByName(projectName);
        if (isNull(project)) {
            throw new EntityNotFoundException("Unable to find org.squashtest.tm.domain.project.GenericProject with name " + projectName);
        }
        if (!permService.hasPermissionOnObject("MANAGEMENT", project) && !permService.hasRole("ROLE_ADMIN")) {
            throw new AccessDeniedException("access is denied");
        }
        return project;
    }

    @Override
    @Transactional(readOnly=true)
    public Page<GenericProject> findAllReadable(Pageable pageable) {
        return dao.findAll(pageable);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<RequirementLibraryNode> findRequirementLibraryAllContent(long id, Pageable paging) {
        checkReadOnRequirements(id);
        return dao.findRequirementLibraryAllContent(id, paging);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<RequirementLibraryNode> findRequirementLibraryRootContent(long id, Pageable paging) {
        checkReadOnRequirements(id);
        return dao.findRequirementLibraryRootContent(id, paging);
    }

    @Override
    @Transactional(readOnly = true)
    // secured by direct call to permService
    public Page<TestCaseLibraryNode> findTestCaseLibraryAllContent(long id, Pageable paging) {
        checkReadOnTestCases(id);
        return dao.findTestCaseLibraryAllContent(id, paging);
    }

    @Override
    @Transactional(readOnly = true)
    // secured by direct call to permService
    public Page<TestCaseLibraryNode> findTestCaseLibraryRootContent(long id, Pageable paging) {
        checkReadOnTestCases(id);
        return dao.findTestCaseLibraryRootContent(id, paging);
    }

    @Override
    @Transactional(readOnly = true)
    // secured by direct call to permService
    public Page<CampaignLibraryNode> findCampaignLibraryAllContent(long id, Pageable paging) {
        checkReadOnCampaigns(id);
        return dao.findCampaignLibraryAllContent(id, paging);
    }

    @Override
    @Transactional(readOnly = true)
    // secured by direct call to permService
    public Page<CampaignLibraryNode> findCampaignLibraryRootContent(long id, Pageable paging) {
        checkReadOnCampaigns(id);
        return dao.findCampaignLibraryRootContent(id, paging);
    }

    @Override
    public GenericProject createGenericProject(GenericProjectDto genericProjectDto) {
        final GenericProject genericProject = GenericProjectDto.convertDto(genericProjectDto);
        GenericProjectDtoVisitor visitor = new GenericProjectDtoVisitor() {
            @Override
            public void visit(ProjectDto projectDto) {
                if (projectDto.getTemplateId() != null) {
                    projectManager.addProjectFromTemplate((Project) genericProject,
                            projectDto.getTemplateId(), GenericProjectCopyParameterDto.convertDto(projectDto.getParams()));
                } else {
                    genericProjectManager.persist(genericProject);
                }
            }

            @Override
            public void visit(ProjectTemplateDto projectTemplateDto) {
                if (projectTemplateDto.getProjectId() != null) {
                    projectTemplateManagerService.addTemplateFromProject((ProjectTemplate) genericProject,
                            projectTemplateDto.getProjectId(), GenericProjectCopyParameterDto.convertDto(projectTemplateDto.getParams()));
                } else {
                    genericProjectManager.persist(genericProject);
                }
            }
        };
        genericProjectDto.accept(visitor);
        return genericProject;
    }

    @Override
    public RestPartyPermission findAllPermissionsByProjectId(long projectId) {
        List<PartyProjectPermissionsBean> beanList = genericProjectManager.findPartyPermissionsBeansByProject(projectId);
        RestPartyPermission permissions = new RestPartyPermission();
        for (PartyProjectPermissionsBean item : beanList) {
            String groupName = item.getPermissionGroup().getSimpleName();

            // why do we name advanceTester instead of advancedTester?? Then this is what you got for translation
            if(groupName.matches("advanceTester")){
                groupName = "advancedTester";
            }

            Party target = restPartyService.findById(item.getParty().getId());

            List<Party> targetList = permissions.get(groupName);
            if (targetList == null) {
                targetList = new ArrayList<>();
                targetList.add(target);
                permissions.put(groupName, targetList);
            } else {
                targetList.add(target);
            }
        }
        return permissions;

    }

    @Override
    public RestPartyPermission buildPartyPermissionDataModel(String permissionGroup, List<Long> partyIds) {
        List<Party> parties = new ArrayList<>();
        for (Long partyId : partyIds) {
            parties.add(restPartyService.findById(partyId));
        }

        RestPartyPermission permissions = new RestPartyPermission();
        permissions.put(permissionGroup, parties);

        return permissions;
    }

    @Override
    // secured by the native service
    public void addNewPermissionToProject(long userId, long projectId, String permissionGroup) {
        genericProjectManager.addNewPermissionToProject(userId, projectId, NAMESPACE + permissionGroup);
    }

    @Override
    public List<PermissionGroup> findAllPossiblePermission() {
        return genericProjectManager.findAllPossiblePermission();
    }

    @Override
    public Page<Requirement> findRequirementsByProject(long projectId, Pageable paging) {
        return dao.findAllRequirementByProjectId(projectId, paging);
    }

    @Override
    public Page<TestCase> findTestCasesByProject(long projectId, Pageable paging) {
        return dao.findAllTestCaseByProjectId(projectId, paging);
    }

    @Override
    public Page<Campaign> findCampaignsByProject(long projectId, Pageable paging) {
        return dao.findAllCampaignByProjectId(projectId, paging);
    }

    @Override
    public void deletePartyFromProject(Long partyId, long projectId) {
        genericProjectManager.removeProjectPermission(partyId, projectId);
    }

    // ***************** private **************************

    // see those methods canReadX here ? That how the world looked like before java 8

    private void checkReadOnRequirements(long projectId) {

        GenericProject p = dao.getOne(projectId);
        if (p == null) {
            throw new EntityNotFoundException();
        }

        if (!permService.canRead(p.getRequirementLibrary())) {
            throw new EntityNotFoundException("access is denied");
        }
    }

    private void checkReadOnTestCases(long projectId) {
        GenericProject p = dao.getOne(projectId);
        if (p == null) {
            throw new EntityNotFoundException();
        }

        if (!permService.canRead(p.getTestCaseLibrary())) {
            throw new AccessDeniedException("access is denied");
        }
    }

    private void checkReadOnCampaigns(long projectId) {
        GenericProject p = dao.getOne(projectId);
        if (p == null) {
            throw new EntityNotFoundException();
        }

        if (!permService.canRead(p.getCampaignLibrary())) {
            throw new AccessDeniedException("access is denied");
        }
    }


}
