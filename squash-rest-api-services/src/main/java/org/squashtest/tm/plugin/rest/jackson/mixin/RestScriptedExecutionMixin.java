/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.jackson.mixin;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.databind.annotation.JsonAppend;
import org.squashtest.tm.plugin.rest.jackson.model.RestCustomFieldMembers;
import org.squashtest.tm.plugin.rest.jackson.serializer.AttachmentHolderPropertyWriter;
import org.squashtest.tm.plugin.rest.jackson.serializer.CustomFieldValuesPropertyWriter;
import org.squashtest.tm.plugin.rest.jackson.serializer.DenormalizedCustomFieldValuesPropertyWriter;
import org.squashtest.tm.plugin.rest.jackson.serializer.ScriptedExecutionScriptNamePropertyWriter;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.NONE, getterVisibility = JsonAutoDetect.Visibility.NONE, isGetterVisibility = JsonAutoDetect.Visibility.NONE)
@JsonPropertyOrder({"_type", "id","name","execution_order","execution_status","last_executed_by","last_executed_on","execution_mode",
        "reference","dataset_label","execution_steps","comment","prerequisite","description",
        "importance","nature","type","test_case_status","test_plan_item", "script_name"})
@JsonAppend(props = {
        @JsonAppend.Prop(name = "custom_fields", type = RestCustomFieldMembers.class, value = CustomFieldValuesPropertyWriter.class),
        @JsonAppend.Prop(name = "test_case_custom_fields", type = RestCustomFieldMembers.class, value = DenormalizedCustomFieldValuesPropertyWriter.class),
        @JsonAppend.Prop(name = "script_name", value = ScriptedExecutionScriptNamePropertyWriter.class),
        @JsonAppend.Prop(name = "attachments", value = AttachmentHolderPropertyWriter.class)
})
@JsonTypeName("scripted-execution")
public abstract class RestScriptedExecutionMixin extends RestExecutionMixin {

    @JsonProperty("script_name")
    String scriptName;
}
