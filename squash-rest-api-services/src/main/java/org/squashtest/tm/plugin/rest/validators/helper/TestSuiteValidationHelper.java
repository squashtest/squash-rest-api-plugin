/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.validators.helper;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.squashtest.tm.domain.IdCollector;
import org.squashtest.tm.domain.campaign.Iteration;
import org.squashtest.tm.domain.campaign.IterationTestPlanItem;
import org.squashtest.tm.domain.customfield.BindableEntity;
import org.squashtest.tm.plugin.rest.jackson.model.IterationTestPlanItemDto;
import org.squashtest.tm.plugin.rest.jackson.model.RestType;
import org.squashtest.tm.plugin.rest.jackson.model.TestSuiteDto;
import org.squashtest.tm.plugin.rest.service.helper.CustomFieldValueHelper;
import org.squashtest.tm.service.campaign.TestSuiteTestPlanManagerService;
import org.squashtest.tm.service.customfield.CustomFieldBindingFinderService;
import org.squashtest.tm.service.internal.repository.IterationDao;

import javax.inject.Inject;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;


@Component
public class TestSuiteValidationHelper extends
        RestNodeValidationHelper {
    @Inject
    private IterationDao iterationDao;
    @Inject
    private CustomFieldBindingFinderService bindingService;
    @Inject
    private CustomFieldValueHelper customFieldValueHelper;
    @Inject
    private TestSuiteTestPlanManagerService testSuiteTestPlanManagerService;

    //validate iteration if exist, and his item
    public void validateIterationAndItem(TestSuiteDto testSuiteDto, final Errors errors) {
        String msgErrors = "";
        boolean isExist=true ;
        //validate iteration
        Long iterationId = testSuiteDto.getParent().getId();
        Iteration iteration = iterationDao.findById(iterationId);
        if (iteration == null) {
            String message = String.format("No entity known for type %s and id %d", RestType.ITERATION, iterationId);
            errors.rejectValue("id", "invalid id", message);
        }
        if(!errors.hasErrors()){
            //we need IdProjet for check cufs
            testSuiteDto.setProject(iteration.getCampaign().getProject());
        }

        //if requirement doesn't exist do not need to validate item
        if(!errors.hasErrors() && testSuiteDto.isHasListItpi()){

            List<IterationTestPlanItem> listTestSuiteByIdIter = iteration.getTestPlans();
            Collection<Long> listId = CollectionUtils.collect(listTestSuiteByIdIter,new IdCollector());

            List<IterationTestPlanItemDto> listItpiDto = testSuiteDto.getListItpi();
            Collection<Long> listIdDto = listItpiDto.stream().map(IterationTestPlanItemDto::getId).collect(Collectors.toList());

            List<Long> listIdDifferent =(List<Long>) CollectionUtils.subtract(listIdDto, listId);
            StringBuilder msgErrorsBuilder =new StringBuilder();
            for(Long id: listIdDifferent){
                msgErrorsBuilder.append("Iteration ").append(iterationId).append(" has no test plan item  ").append(id).append("\n");
            }
            msgErrors= msgErrorsBuilder.toString();

            if (!msgErrors.isEmpty()){
                 errors.rejectValue("listItpi", "invalid value", msgErrors);
            }
        }
    }

    public void checkCufs(final Errors errors, TestSuiteDto testSuiteDto){
        customFieldValueHelper.checkCufs(errors,BindableEntity.TEST_SUITE,testSuiteDto.getProject().getId(),testSuiteDto.getCustomFields());
    }

}
