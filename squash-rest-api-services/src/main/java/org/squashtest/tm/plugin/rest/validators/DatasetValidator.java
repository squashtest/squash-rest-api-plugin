/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.validators;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import org.squashtest.tm.core.foundation.lang.Couple;
import org.squashtest.tm.domain.testcase.Dataset;
import org.squashtest.tm.domain.testcase.DatasetParamValue;
import org.squashtest.tm.domain.testcase.Parameter;
import org.squashtest.tm.plugin.rest.controller.helper.ErrorHandlerHelper;
import org.squashtest.tm.plugin.rest.jackson.model.DatasetDto;
import org.squashtest.tm.plugin.rest.jackson.model.DatasetParamValueDto;
import org.squashtest.tm.plugin.rest.jackson.model.RestType;
import org.squashtest.tm.plugin.rest.jackson.model.TestCaseDto;
import org.squashtest.tm.plugin.rest.repository.RestDatasetRepository;
import org.squashtest.tm.plugin.rest.validators.helper.DatasetDtoValidationHelper;
import org.squashtest.tm.service.internal.repository.DatasetDao;
import org.squashtest.tm.service.internal.repository.TestCaseDao;
import org.squashtest.tm.service.testcase.ParameterFinder;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.squashtest.tm.plugin.rest.jackson.model.RestType.TEST_CASE;

@Component
public class DatasetValidator implements Validator {

    private static final String POST_DATASET = "post-dataset";
    private static final String PATCH_DATASET = "patch-dataset";

    @Inject
    private DatasetDtoValidationHelper datasetDtoValidationHelper;
    @Inject
    private ParameterFinder parameterFinder;
    @Inject
    private RestDatasetRepository restDatasetRepository;
    @Inject
    private TestCaseDao testCaseDao;
    @Inject
    private DatasetDao datasetDao;


   @Override
    public boolean supports(Class<?> clazz) {
        return DatasetDto.class.equals(clazz);
    }

    public void validationPostDataset(DatasetDto datasetDto, List<Parameter> listAllParamByIdTc) throws BindException {

        List<Errors> errors = new ArrayList<>();
        BindingResult validation = new BeanPropertyBindingResult(datasetDto, POST_DATASET);

        //validate id et name of test case and of dataset
        validate(datasetDto, validation);

        //check test case called by referenced test case and parameter deleguate
        validateDatasetDto(datasetDto,listAllParamByIdTc, validation);

        if (validation.hasErrors()) {
            errors.add(validation);
        }
        ErrorHandlerHelper.throwIfError(datasetDto, errors, POST_DATASET);
    }

    public void validationPatchDataset(DatasetDto datasetDto, Long datasetId) throws BindException {
        List<Errors> errors = new ArrayList<>();
        BindingResult validation = new BeanPropertyBindingResult(datasetDto, PATCH_DATASET);

        //valider dataset
        datasetDtoValidationHelper.checkEntityExist(validation, RestType.DATASET, datasetId);

        //valider name si envoyer
        if(datasetDto.isHasName()== true) {
            ValidationUtils.rejectIfEmptyOrWhitespace(validation, "name", "required", "This attribute can't be empty");
            //valider l'unicité  du name
            Dataset dataset = restDatasetRepository.getOne(datasetId);
            if (datasetDto.getName() != null && dataset.getName().equals(datasetDto.getName())) {
                validation.rejectValue("name", "invalid value", "A Dataset with the same name already exists in the Test Case");
            }
        }
        //valider idParamValue envoyer ds json
        if(datasetDto.isHasParamValue()==true) {
            validateParamValue(datasetDto, datasetId, validation);
        }


        if (validation.hasErrors()) {
            errors.add(validation);
        }
        ErrorHandlerHelper.throwIfError(datasetDto, errors, PATCH_DATASET);
    }

    @Override
    public void validate(Object target, final Errors errors) {

        DatasetDto datasetDto = (DatasetDto) target;
        TestCaseDto referencedTcDTo = datasetDto.getReferencedTestCase();

        //Valider le test case à qui on ajoute dataset
        if (!referencedTcDTo.getRestType().equals(TEST_CASE)) {
            errors.rejectValue("_type", "invalid type", "Type test-case expected");
        } else if (referencedTcDTo.getId() == null) {
            errors.rejectValue("id", "generated value", "The test case id must not be null ");
        }
        datasetDtoValidationHelper.checkEntityExist(errors, RestType.TEST_CASE, referencedTcDTo.getId());

        //valider id null dataset et name non null
        if (datasetDto.getId() != null) {
            errors.rejectValue("id", "generated value", "This attribute is generated by database and should not be provided. If you want to update an existing dataset, please do a patch request to the dataset id. ");
        }
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "required", "This attribute can't be empty");

        //unicité name
        Dataset sameName = datasetDao.findByTestCaseIdAndName(referencedTcDTo.getId(), datasetDto.getName());
        if(sameName != null ){
            errors.rejectValue("name", "invalid value", "A Dataset with the same name already exists in the Test Case");
        }
    }

    public void validateDatasetDto(DatasetDto datasetDto, List<Parameter> listAllParamByIdTc, final Errors errors) {

        String msgErrors = "";

        Set<DatasetParamValueDto> listParamValueDto = datasetDto.getValueDtos();

        // listAllParamByIdTc :la liste des parametres delegués pour ce testCase
        //on compare les couples (idParm, et idParamTC) des deux listValueDto et listParamByIdTc

        List<Couple<Long, Long>> listIdParamIdTcDto = listParamValueDto.stream().map(paramValueDto->new Couple<Long, Long>(paramValueDto.getParameterId(), paramValueDto.getTestCaseId())).collect(Collectors.toList());
        List<Couple<Long, Long>>  listIdParamIdTcBD =listAllParamByIdTc.stream().map(paramByIdTc->new Couple<Long, Long>(paramByIdTc.getId(), paramByIdTc.getTestCase().getId())).collect(Collectors.toList());

        List<Couple<Long, Long>> listDifference = (List<Couple<Long, Long>>) CollectionUtils.subtract(listIdParamIdTcDto,listIdParamIdTcBD);
        for(Couple p : listDifference){
            msgErrors = msgErrors + "Test Case " + p.getA2() + " is not called by Test Case " + datasetDto.getReferencedTestCase().getId()
                                    + ", or Parameters "+ p.getA1() +  " of Test Case " + p.getA2() + " are not delegated" + "\n";
        }
        if (!msgErrors.isEmpty()){
            if (!msgErrors.isEmpty()) errors.rejectValue("valueDtos", "invalid value", msgErrors);
        }
    }

    
    public  void validateParamValue(DatasetDto datasetDto,Long datasetId,  final Errors errors){
        String msgErrors = "";
        boolean isExist=true ;

        //si paramValue verifier que le paramId est existe bien pour datasetId
        Set<DatasetParamValueDto> DatasetParamValueDto = datasetDto.getValueDtos();

        List<DatasetParamValue> listParamValueById = restDatasetRepository.findParamValueByIdDataset(datasetId);
        for (DatasetParamValueDto pvDto: DatasetParamValueDto){
            isExist= listParamValueById.stream()
                    .anyMatch(paramValueById->paramValueById.getParameter().getId().equals(pvDto.getParameterId()));
            if (isExist == false) {
                msgErrors = msgErrors + "Dataset "+datasetId+" has no parameter "+pvDto.getParameterId()+"\n";
            }
        }
        if (!msgErrors.isEmpty()){
            if (!msgErrors.isEmpty()) errors.rejectValue("valueDtos", "invalid value", msgErrors);
        }
    }
}

