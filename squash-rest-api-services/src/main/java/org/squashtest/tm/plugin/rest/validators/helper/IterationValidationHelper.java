/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.validators.helper;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.squashtest.tm.domain.campaign.IterationStatus;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.plugin.rest.jackson.model.IterationDto;

import java.util.Arrays;

// Even iteration is not actually a rest node, we still want to benefit the cuf services in RestNodeValidationHelper
@Component
public class IterationValidationHelper extends RestNodeValidationHelper{

    public void checkProject(IterationDto dto) {
        if (dto.getProjectId() == null) {
            throw new IllegalArgumentException("Programmatic error, you must provide a not null project id to add an iteration");
        } else {
            Project project = entityManager.find(Project.class, dto.getProjectId());
            if (project == null) {
                throw new IllegalArgumentException("Programmatic error, you must provide a valid project id to add an iteration");
            }
        }
    }

    public void checkAndAssignStatus(Errors errors, IterationDto iterationDto) {
        IterationStatus status = iterationDto.getStatus();
        if (status != null) {
            if(!Arrays.asList(IterationStatus.values()).contains(status)) {
                errors.rejectValue("status", "invalid type", "Invalid iteration status for this project");
            }
        } else {
            iterationDto.setStatus(IterationStatus.defaultValue());
        }
    }
}
