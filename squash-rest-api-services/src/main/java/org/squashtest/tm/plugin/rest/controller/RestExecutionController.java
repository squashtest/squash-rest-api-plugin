/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.controller;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.PagedResources;
import org.springframework.hateoas.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.squashtest.tm.domain.execution.Execution;
import org.squashtest.tm.domain.execution.ExecutionStep;
import org.squashtest.tm.domain.execution.KeywordExecution;
import org.squashtest.tm.domain.execution.ScriptedExecution;
import org.squashtest.tm.plugin.rest.controller.helper.ResourceLinksHelper;
import org.squashtest.tm.plugin.rest.core.jackson.DeserializationFilterExpression;
import org.squashtest.tm.plugin.rest.core.jackson.DynamicFilterExpression;
import org.squashtest.tm.plugin.rest.core.web.BaseRestController;
import org.squashtest.tm.plugin.rest.core.web.EntityGetter;
import org.squashtest.tm.plugin.rest.core.web.PersistentEntity;
import org.squashtest.tm.plugin.rest.core.web.RestApiController;
import org.squashtest.tm.plugin.rest.core.web.UseDefaultRestApiConfiguration;
import org.squashtest.tm.plugin.rest.jackson.model.ExecutionAndCustomFields;
import org.squashtest.tm.plugin.rest.service.RestExecutionService;
import org.squashtest.tm.plugin.rest.validators.CustomFieldValueHintedValidator;
import org.squashtest.tm.plugin.rest.validators.DenormalizedFieldValueHintedValidator;
import org.squashtest.tm.plugin.rest.validators.ExecutionHintedValidator;

import javax.inject.Inject;

/**
 * Created by jthebault on 16/06/2017.
 */
@RestApiController(Execution.class)
@UseDefaultRestApiConfiguration
public class RestExecutionController extends BaseRestController{

    public static final String EXECUTION_DYNAMIC_FILTER = "*,execution_steps[execution_status,action,expected_result], nature[code], type[code], automated_execution_extender[result_url, result_status]";
    public static final String PATCH_DYNAMIC_FILTER = "comment, execution_status, custom_fields, test_case_custom_fields";

    @Inject
    private ResourceLinksHelper linksHelper;

    @Inject
    private RestExecutionService restExecutionService;

    @Inject
    private CustomFieldValueHintedValidator cufValidator;

    @Inject
    private DenormalizedFieldValueHintedValidator denoValidator;

    @Inject
    private ExecutionHintedValidator execValidator;

    public void initBinder(WebDataBinder binder){
        binder.addValidators(execValidator);
        binder.addValidators(cufValidator);
        binder.addValidators(denoValidator);
    }


    @RequestMapping(value = "/executions/{id}", method = RequestMethod.GET)
    @EntityGetter({Execution.class, ScriptedExecution.class, KeywordExecution.class})
    @ResponseBody
    @DynamicFilterExpression(EXECUTION_DYNAMIC_FILTER)
    public ResponseEntity<Resource<Execution>> findExecution(@PathVariable("id") long id){

        Execution execution = restExecutionService.getOne(id);

        Resource<Execution> res = toResource(execution);

        linksHelper.addAllLinksForExecution(res);

        return ResponseEntity.ok(res);
    }

    @RequestMapping(value = "/executions/{id}", method = RequestMethod.PATCH)
    @ResponseBody
    @DynamicFilterExpression(EXECUTION_DYNAMIC_FILTER)
    @DeserializationFilterExpression(PATCH_DYNAMIC_FILTER)
    public ResponseEntity<Resource<Execution>> patchExecution(@Validated @PersistentEntity ExecutionAndCustomFields exec){
    	restExecutionService.updateExecution(exec);

    	Resource<Execution> res = toResource(exec.getWrapped());

    	linksHelper.addAllLinksForExecution(res);

    	return ResponseEntity.ok(res);
    }


    @RequestMapping("/executions/{id}/execution-steps")
    @ResponseBody
    @DynamicFilterExpression("*, execution[execution_status]")
    public ResponseEntity<PagedResources<Resource>> findExecutionSteps(@PathVariable("id") long executionId, Pageable pageable){

        Page<ExecutionStep> steps = restExecutionService.findExecutionSteps(executionId, pageable);

        PagedResources<Resource> res = toPagedResourcesWithRel(steps, "execution-steps");

        return ResponseEntity.ok(res);

    }

    /*AMK Delete execution*/
    @RequestMapping(value = "/executions/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    @DynamicFilterExpression("*")
    public ResponseEntity<Void> deleteExecution(@PathVariable("id") Long executionId) {

        restExecutionService.deleteExecution(executionId);
        return ResponseEntity.noContent().build();
    }

}
