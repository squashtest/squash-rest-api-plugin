/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.jackson.mixin;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.squashtest.tm.domain.campaign.Iteration;
import org.squashtest.tm.domain.execution.Execution;
import org.squashtest.tm.domain.execution.ExecutionStatus;
import org.squashtest.tm.domain.testcase.Dataset;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.domain.users.User;
import org.squashtest.tm.plugin.rest.jackson.serializer.HateoasWrapperConverter;
import org.squashtest.tm.plugin.rest.jackson.serializer.UserLoginConverter;

import java.util.Date;
import java.util.List;

/**
 * Created by jthebault on 20/06/2017.
 */
@JsonAutoDetect(fieldVisibility= JsonAutoDetect.Visibility.NONE, getterVisibility= JsonAutoDetect.Visibility.NONE, isGetterVisibility= JsonAutoDetect.Visibility.NONE)
@JsonPropertyOrder({"_type", "id","execution_status","referenced_test_case","referenced_dataset","last_executed_by","last_executed_on","assigned_to","executions","iteration"})
@JsonTypeName("iteration-test-plan-item")
public abstract class RestIterationTestPlanItemMixin extends RestIdentifiedMixin {

    @JsonProperty("execution_status")
    private ExecutionStatus executionStatus;

    @JsonProperty("assigned_to")
    @JsonSerialize(converter = UserLoginConverter.class)
    private User user;

    @JsonProperty("last_executed_by")
    private String lastExecutedBy;

    @JsonProperty("last_executed_on")
    private Date lastExecutedOn;

    @JsonProperty("referenced_test_case")
    @JsonSerialize(converter=HateoasWrapperConverter.class)
    @JsonTypeInfo(use= JsonTypeInfo.Id.NONE)
    private TestCase referencedTestCase;

    @JsonProperty("referenced_dataset")
    @JsonSerialize(converter=HateoasWrapperConverter.class)
    @JsonTypeInfo(use= JsonTypeInfo.Id.NONE)
    private Dataset referencedDataset;

    @JsonProperty()
    @JsonSerialize(contentConverter=HateoasWrapperConverter.class)
    @JsonTypeInfo(use= JsonTypeInfo.Id.NONE)
    private List<Execution> executions;

    @JsonProperty
    @JsonSerialize(converter=HateoasWrapperConverter.class)
    @JsonTypeInfo(use= JsonTypeInfo.Id.NONE)
    private Iteration iteration;


}
