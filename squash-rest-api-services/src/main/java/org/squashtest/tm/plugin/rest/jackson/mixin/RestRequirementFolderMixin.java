/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.jackson.mixin;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.annotation.JsonUnwrapped;
import com.fasterxml.jackson.databind.annotation.JsonAppend;
import org.squashtest.tm.domain.requirement.RequirementFolder;
import org.squashtest.tm.domain.resource.SimpleResource;
import org.squashtest.tm.plugin.rest.jackson.model.RestCustomFieldMembers;
import org.squashtest.tm.plugin.rest.jackson.serializer.AttachmentHolderPropertyWriter;
import org.squashtest.tm.plugin.rest.jackson.serializer.CustomFieldValuesPropertyWriter;


/**
 * Mixin for {@link RequirementFolder}s. Note that the class hierarchy here differs a bit : it directly extends from {@link RestIdentifiedMixin} (not from {@link RestGenericLibraryNodeMixin}. It 
 * is so because the domain object is not a regular node either.
 * 
 * @author bsiri
 *
 */
@JsonAutoDetect(fieldVisibility=Visibility.NONE, getterVisibility=Visibility.NONE, isGetterVisibility=Visibility.NONE)
@JsonPropertyOrder({"_type", "id", "name", "project", "path", "parent", "audit", "description","custom_fields"})
@JsonAppend(props= {
					@JsonAppend.Prop(name = "attachments", value = AttachmentHolderPropertyWriter.class),
					@JsonAppend.Prop(name = "custom_fields", type = RestCustomFieldMembers.class, value = CustomFieldValuesPropertyWriter.class)
})

@JsonTypeName("requirement-folder")
public abstract class RestRequirementFolderMixin extends RestRequirementLibraryNodeMixin {

	@JsonUnwrapped
	SimpleResource resource;

}
