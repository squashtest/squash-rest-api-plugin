/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.service.impl;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.core.foundation.lang.Couple;
import org.squashtest.tm.domain.campaign.Iteration;
import org.squashtest.tm.domain.campaign.IterationTestPlanItem;
import org.squashtest.tm.domain.execution.Execution;
import org.squashtest.tm.domain.testcase.Dataset;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.domain.users.User;
import org.squashtest.tm.plugin.rest.jackson.model.DatasetDto;
import org.squashtest.tm.plugin.rest.jackson.model.IterationTestPlanItemDto;
import org.squashtest.tm.plugin.rest.jackson.model.TestCaseDto;
import org.squashtest.tm.plugin.rest.repository.RestExecutionRepository;
import org.squashtest.tm.plugin.rest.repository.RestIterationTestPlanItemRepository;
import org.squashtest.tm.plugin.rest.service.RestIterationTestPlanItemService;
import org.squashtest.tm.plugin.rest.validators.IterationTestPlanItemPostValidator;
import org.squashtest.tm.service.campaign.CustomIterationModificationService;
import org.squashtest.tm.service.campaign.IterationTestPlanManagerService;
import org.squashtest.tm.service.internal.repository.DatasetDao;
import org.squashtest.tm.service.internal.repository.IterationDao;
import org.squashtest.tm.service.internal.repository.IterationTestPlanDao;
import org.squashtest.tm.service.internal.repository.TestCaseDao;
import org.squashtest.tm.service.internal.repository.UserDao;

import javax.inject.Inject;
import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.squashtest.tm.service.security.Authorizations.OR_HAS_ROLE_ADMIN;

/**
 * Created by jthebault on 20/06/2017.
 */
@Service
@Transactional
public class RestIterationTestPlanItemServiceImpl implements RestIterationTestPlanItemService {

    private static final String LINK_ITERATION_OR_ROLE_ADMIN =
            "hasPermission(#iterationId, 'org.squashtest.tm.domain.campaign.Iteration', 'LINK')" + OR_HAS_ROLE_ADMIN;
    private static final String ITERATION_ID = "iterationId";

    @Inject
    private RestIterationTestPlanItemRepository itpiRepository;

    @Inject
    private RestExecutionRepository executionRepository;

    @Inject
    private CustomIterationModificationService nativeService;

    @Inject
    private IterationTestPlanManagerService iterationTestPlanManagerService;

    @Inject
    private IterationTestPlanItemPostValidator iterationTestPlanItemPostValidator;
    @Inject
    private TestCaseDao testCaseDao;
    @Inject
    private DatasetDao datasetDao;
    @Inject
    private UserDao userDao;
    @Inject
    private IterationTestPlanDao iterationTestPlanDao;
    @Inject
    private IterationDao iterationDao;
    private Object object;
    private Object object1;
    private Object elt;

    @Override
    /*
     * XXX
     * I suppose the use of @PostAuthorize here is meant to personally handle the case where the dao returns null,
     * can someone confirm this ? If there is a legit reason why use @PreAuthorize on #findExcecutions then ?  
    */
    @PostAuthorize("hasPermission(returnObject , 'READ') or hasRole('ROLE_ADMIN')")
    @Transactional(readOnly=true)
    public IterationTestPlanItem getOne(long id) {
        IterationTestPlanItem itpi = itpiRepository.getOne(id);
        if(itpi == null){
            throw new EntityNotFoundException();
        }
        return itpi;
    }

    @Override
    @PreAuthorize("@apiSecurity.hasPermission(#itpiId, 'org.squashtest.tm.domain.campaign.IterationTestPlanItem', 'READ')")
    @Transactional(readOnly=true)
    public Page<Execution> findExecutions(long itpiId, Pageable pageable) {
        return executionRepository.findAllByTestPlan_Id(itpiId,pageable);
    }

    @Override
    // secured by the nativeService
    public Execution createExecution(long itpiId) {
        return nativeService.addExecution(itpiId);
    }


    /*
     * Private feature for the jira plugin
     */
	@Override
	@Transactional(readOnly=true)
	// TODO : secure this
	// Unfortunately we cannot apply @PostFilter on Page objects 
	// The proper way needs to first retrieve all items that match the query parameters, filter on those he can actually read, 
	// then re-run the query on those using pagination  
	public Page<IterationTestPlanItem> findItemsByCoveredRemoteRequirement(Pageable pageable, String remoteKey,
			String serverName) {
		return itpiRepository.findItemsByCoveredRemoteRequirement(remoteKey, serverName, pageable);
	}

    @Override
    @PreAuthorize("@apiSecurity.hasPermission(#testPlanId, 'org.squashtest.tm.domain.campaign.IterationTestPlanItem', 'WRITE')")
    public IterationTestPlanItem modifyIterationTestPlan(IterationTestPlanItemDto itpiDto, Long testPlanId) {

        IterationTestPlanItem itpi = itpiRepository.getOne(testPlanId);

        Dataset ds = itpiDto.getReferencedDataset()!=null ? datasetDao.getOne(itpiDto.getReferencedDataset().getId()) : null;
        User assignedTo = itpiDto.getUser()!=null ?  userDao.findUserByLogin(itpiDto.getUser()): null;

        if(itpiDto.isHasSetDataset()){
            itpi.setReferencedDataset(ds);
        }
        if(itpiDto.isAssignedTo()) {
            itpi.setUser(assignedTo);
        }
        return itpi;
    }

    @Override
    public void deleteIterationTestPlan(List<Long> testPlanIds) {
        //Couple(idItem, idIteration)
	    List<Couple<Long,Long>> listIds = itpiRepository.findIterationByIdItem(testPlanIds);
        //Map k= id iteration et v = liste des items à supprimer
        Map<Long, List<Long>> mapItpi =
             listIds.stream().collect(Collectors.groupingBy(Couple::getA1,
                                        Collectors.mapping(
                                                Couple::getA2,
                                                Collectors.toList()
                                        )));
        mapItpi.forEach((k,v)-> {
            iterationTestPlanManagerService.removeTestPlansFromIteration(v, k);
        });
	}

    @Override
    @PreAuthorize("@apiSecurity.hasPermission(#iterationId, 'org.squashtest.tm.domain.campaign.Iteration', 'WRITE')")
    public IterationTestPlanItem addIterationTestPlanItem(IterationTestPlanItemDto testPlanItemDto , long iterationId) {

        TestCaseDto referencedTcDTo = testPlanItemDto.getTestCaseDto();

        DatasetDto referencedDatasetDto = testPlanItemDto.getReferencedDataset();

        Iteration iteration = iterationDao.findById(iterationId);

        TestCase testCase = testCaseDao.findById(referencedTcDTo.getId());

        String assignedTo = testPlanItemDto.getUser();

        User aTo = assignedTo!= null ? userDao.findUserByLogin(assignedTo): null;

        Dataset ds = referencedDatasetDto != null ? datasetDao.getOne(referencedDatasetDto.getId()) : null;

        IterationTestPlanItem itp = new IterationTestPlanItem(testCase, ds, aTo);
        iterationTestPlanDao.save(itp);
        iteration.addTestPlan(itp);
        return itp;
    }


}
