/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.service.impl;

import org.springframework.stereotype.Component;
import org.squashtest.tm.domain.requirement.RequirementVersion;
import org.squashtest.tm.plugin.rest.jackson.model.CustomFieldValueDto;
import org.squashtest.tm.plugin.rest.jackson.model.RequirementVersionDto;
import org.squashtest.tm.plugin.rest.service.helper.CustomFieldValueHelper;
import org.squashtest.tm.service.customfield.CustomFieldValueFinderService;

import javax.inject.Inject;
import java.util.EnumSet;
import java.util.List;

@Component
public class RequirementVersionPatcher extends AbstractPatcher{

    @Inject
    private CustomFieldValueHelper customFieldValueHelper;
    @Inject
    private CustomFieldValueFinderService customFieldValueFinderService;


    private final EnumSet<RequirementVersionPatchableAttributes> patchableProperties = EnumSet.allOf(RequirementVersionPatchableAttributes.class);

    public RequirementVersionPatcher() {
    }

    @Override
    EnumSet<? extends PatchableAttributes> getPatchableAttributes() {
        return patchableProperties;
    }

    public void patchCustomFieldValue(RequirementVersionDto reqVersionDto, RequirementVersion reqVersion) {
        // sent via json
        List<CustomFieldValueDto> cufValuesDto = reqVersionDto.getCustomFields();
        //patch value cufs
        customFieldValueHelper.patchCustomFieldValue(reqVersion,cufValuesDto);
    }

}
