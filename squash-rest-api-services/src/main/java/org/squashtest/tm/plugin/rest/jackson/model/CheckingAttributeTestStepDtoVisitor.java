/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.jackson.model;

import org.springframework.validation.Errors;
import org.squashtest.tm.domain.customfield.BindableEntity;
import org.squashtest.tm.plugin.rest.validators.helper.TestStepValidationHelper;

import java.util.List;

public class CheckingAttributeTestStepDtoVisitor implements TestStepDtoVisitor {

    private TestStepValidationHelper testStepValidationHelper;

    private Errors errors;

    public CheckingAttributeTestStepDtoVisitor(TestStepValidationHelper testStepValidationHelper, Errors errors) {
        this.testStepValidationHelper = testStepValidationHelper;
        this.errors = errors;
    }

    @Override
    public void visit(ActionTestStepDto actionTestStepDto) {
        testStepValidationHelper.checkProject(actionTestStepDto.getProjectId());
        List<CustomFieldValueDto> customFieldValueDtos = actionTestStepDto.getCustomFields();
        testStepValidationHelper.checkCufs(errors, customFieldValueDtos, actionTestStepDto.getProjectId(), BindableEntity.TEST_STEP);
    }

    @Override
    public void visit(CalledTestStepDto calledTestStepDto) {
        //NOOP
    }

    @Override
    public void visit(KeywordTestStepDto keywordTestStepDto) {
        testStepValidationHelper.checkProject(keywordTestStepDto.getProjectId());
    }
}
