/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.jackson.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.cfg.MapperConfig;
import com.fasterxml.jackson.databind.introspect.AnnotatedClass;
import com.fasterxml.jackson.databind.introspect.BeanPropertyDefinition;
import com.fasterxml.jackson.databind.ser.VirtualBeanPropertyWriter;
import com.fasterxml.jackson.databind.util.Annotations;
import org.springframework.stereotype.Component;
import org.squashtest.tm.domain.EntityType;
import org.squashtest.tm.domain.campaign.CampaignLibraryNode;
import org.squashtest.tm.domain.library.TreeNode;
import org.squashtest.tm.domain.library.WhichNodeVisitor;
import org.squashtest.tm.domain.requirement.RequirementLibraryNode;
import org.squashtest.tm.domain.testcase.TestCaseLibraryNode;
import org.squashtest.tm.service.internal.library.PathService;

import javax.inject.Inject;

@SuppressWarnings("serial")
@Component
public class PathPropertyWriter extends VirtualBeanPropertyWriter {

    @Inject
    private PathService pathService;

    public PathPropertyWriter() {
        super();
    }

    public PathPropertyWriter(BeanPropertyDefinition propDef,
                              Annotations contextAnnotations,
                              JavaType declaredType,
                              PathService service) {
        super(propDef, contextAnnotations, declaredType);
        this.pathService = service;

    }


    /**
     * Accepts only instances of {@link TreeNode}
     *
     * @param bean
     * @param gen
     * @param prov
     * @return
     * @throws Exception
     */
    @Override
    protected Object value(Object bean, JsonGenerator gen, SerializerProvider prov) throws Exception {

        String path;

        if (!TreeNode.class.isAssignableFrom(bean.getClass())) {
            throw new IllegalArgumentException("attempted to serializer the path of a '" +
                    bean.getClass().getName() + "', but only implementors of TreeNode are accepted");
        }

        EntityType beanType = new WhichNodeVisitor().getTypeOf((TreeNode) bean);

        switch (beanType) {
            case TEST_CASE_FOLDER:
            case TEST_CASE:
                path = pathService.buildTestCasePath(((TestCaseLibraryNode) bean).getId());
                break;

            case REQUIREMENT_FOLDER:
            case REQUIREMENT:
                path = pathService.buildRequirementPath(((RequirementLibraryNode) bean).getId());
                break;

            case CAMPAIGN:
            case CAMPAIGN_FOLDER:
                path = pathService.buildCampaignPath(((CampaignLibraryNode) bean).getId());
                break;
            default:
                path = "not-implemented";
        }


        return path;
    }

    @Override
    public VirtualBeanPropertyWriter withConfig(MapperConfig<?> config, AnnotatedClass declaringClass,
                                                BeanPropertyDefinition propDef, JavaType type) {
        return new PathPropertyWriter(propDef, declaringClass.getAnnotations(), type, pathService);
    }


}
