/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.controller;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.LinkBuilder;
import org.springframework.hateoas.PagedResources;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.web.util.UriComponentsBuilder;
import org.squashtest.tm.domain.testautomation.AutomatedExecutionExtender;
import org.squashtest.tm.domain.testautomation.AutomatedSuite;
import org.squashtest.tm.plugin.rest.core.jackson.DynamicFilterExpression;
import org.squashtest.tm.plugin.rest.core.web.BaseRestController;
import org.squashtest.tm.plugin.rest.core.web.BasicResourceAssembler;
import org.squashtest.tm.plugin.rest.core.web.RestApiController;
import org.squashtest.tm.plugin.rest.core.web.UseDefaultRestApiConfiguration;
import org.squashtest.tm.plugin.rest.jackson.model.AutomatedSuiteDto;
import org.squashtest.tm.plugin.rest.service.RestAutomatedExecutionExtenderService;
import org.squashtest.tm.plugin.rest.service.RestAutomatedSuiteService;

import javax.inject.Inject;
import java.util.List;


/**
 * Because AutomatedSuite is not an identified class, so everything became complicated,
 * we can only use a rest dto {@link AutomatedSuiteDto} for jackson serialization,
 * and all toResource methods were customized.
 */
@RestApiController
@UseDefaultRestApiConfiguration
public class RestAutomatedSuiteController extends BaseRestController {

    @Inject
    private RestAutomatedSuiteService automatedSuiteService;

    @Inject
    private RestAutomatedExecutionExtenderService automatedExecutionExtenderService;

    private AutomatedSuiteResourceAssembler suiteResourceAssembler = new AutomatedSuiteResourceAssembler();


    @RequestMapping(value = "/automated-suites", method = RequestMethod.GET)
    @ResponseBody
    @DynamicFilterExpression("*")
    public ResponseEntity<PagedResources<Resource>> findAllReadableAutomatedSuites(Pageable pageable) {

        Page<AutomatedSuite> suites = automatedSuiteService.findAllReadable(pageable);
        Page<AutomatedSuiteDto> suiteDtos = suites.map(suite -> new AutomatedSuiteDto(suite.getId(), suite.getExecutionExtenders()));
        PagedResources<Resource> res = pageAssembler.toResource(suiteDtos, suiteResourceAssembler);

        return ResponseEntity.ok(res);
    }

    @RequestMapping(value = "/automated-suites/{id}", method = RequestMethod.GET)
    @ResponseBody
    @DynamicFilterExpression("*")
    public ResponseEntity<Resource<AutomatedSuiteDto>> findAutomatedSuite(@PathVariable("id") String id) {
        AutomatedSuite suite = automatedSuiteService.findById(id);
        Resource<AutomatedSuiteDto> res = toResource(suite);

        UriComponentsBuilder builder = ServletUriComponentsBuilder.fromCurrentRequestUri();
        res.add(new Link(builder.toUriString(), "self"));
        builder.path("/" + "executions");
        res.add(new Link(builder.toUriString(), "executions"));

        return ResponseEntity.ok(res);
    }

    @RequestMapping(value = "/automated-suites/{id}/executions", method = RequestMethod.GET)
    @ResponseBody
    @DynamicFilterExpression("*")
    public ResponseEntity<PagedResources<Resource>> findAutomatedSuiteExecutions(@PathVariable("id") String id, Pageable pageable) {

        Page<AutomatedExecutionExtender> executions = automatedExecutionExtenderService.findAutomatedExecutionExtenderByAutomatedSuiteId(id, pageable);
        PagedResources<Resource> res = toPagedResources(executions);
        return ResponseEntity.ok(res);
    }

    @RequestMapping(value = "/automated-suite-utils/from-iteration-test-plan-items", method = RequestMethod.POST)
    @ResponseBody
    @DynamicFilterExpression("*")
    public ResponseEntity<Resource<AutomatedSuiteDto>> createAutomatedSuiteFromIterationTestPlanItems(@RequestParam(value = "itemIds") List<Long> itemIds) {

        AutomatedSuite suite = automatedSuiteService.createAutomatedSuiteFromIterationTestPlanItems(itemIds);
        Resource<AutomatedSuiteDto> res = toResource(suite);

        addAllLinksForAutomatedSuite(suite.getId(), res);
        return ResponseEntity.status(HttpStatus.CREATED).body(res);
    }

    @RequestMapping(value = "/automated-suite-utils/from-test-suite", method = RequestMethod.POST)
    @ResponseBody
    @DynamicFilterExpression("*")
    public ResponseEntity<Resource<AutomatedSuiteDto>> createAutomatedSuiteFromTestSuite(@RequestParam(value = "testSuiteId") long testSuiteId) {

        AutomatedSuite suite = automatedSuiteService.createAutomatedSuiteFromTestSuite(testSuiteId);
        Resource<AutomatedSuiteDto> res = toResource(suite);

        addAllLinksForAutomatedSuite(suite.getId(), res);
        return ResponseEntity.status(HttpStatus.CREATED).body(res);
    }

      @RequestMapping(value = "/automated-suite-utils/from-iteration", method = RequestMethod.POST)
    @ResponseBody
    @DynamicFilterExpression("*")
    public ResponseEntity<Resource<AutomatedSuiteDto>> createAutomatedSuiteFromIteration(@RequestParam(value = "iterationId") long iterationId) {

        AutomatedSuite suite = automatedSuiteService.createAutomatedSuiteFromIteration(iterationId);
        Resource<AutomatedSuiteDto> res = toResource(suite);

        addAllLinksForAutomatedSuite(suite.getId(), res);
        return ResponseEntity.status(HttpStatus.CREATED).body(res);
    }

    @RequestMapping(value = "/automated-suite-utils/{suiteId}/executor", method = RequestMethod.POST)
    @ResponseBody
    @DynamicFilterExpression("*")
    public ResponseEntity<Resource<AutomatedSuiteDto>> executeAutomatedSuite(@PathVariable("suiteId") String suiteId) {

        AutomatedSuite suite = automatedSuiteService.findById(suiteId);
        automatedSuiteService.start(suite);

        Resource<AutomatedSuiteDto> res = toResource(suite);

        addAllLinksForAutomatedSuite(suite.getId(), res);
        return ResponseEntity.ok(res);
    }

    /**
     * customized toResource method
     */
    private Resource<AutomatedSuiteDto> toResource(AutomatedSuite suite) {
        AutomatedSuiteDto suiteDto = new AutomatedSuiteDto(suite.getId(), suite.getExecutionExtenders());
        return new Resource<>(suiteDto);
    }

    /**
     * customized add links method
     */
    private void addAllLinksForAutomatedSuite(String suiteId, Resource<AutomatedSuiteDto> res) {
        LinkBuilder builder = linkService.fromBasePath(ControllerLinkBuilder.linkTo(ControllerLinkBuilder.methodOn(RestAutomatedSuiteController.class).findAutomatedSuite(suiteId)));
        res.add(new Link(builder.toString(), "self"));
        res.add(new Link(builder.slash("/executions").toString(), "executions"));
    }

    /**
     * customized assembler to replace {@link BasicResourceAssembler}, for now, only used for AutomatedSuite
     */
    private class AutomatedSuiteResourceAssembler implements ResourceAssembler<AutomatedSuiteDto, Resource<AutomatedSuiteDto>>{

        @Override
        public Resource<AutomatedSuiteDto> toResource(AutomatedSuiteDto entity) {
            Resource<AutomatedSuiteDto> res = new Resource<AutomatedSuiteDto>(entity);
            addAllLinksForAutomatedSuite(entity.getId(), res);
            return res;
        }


    }

}
