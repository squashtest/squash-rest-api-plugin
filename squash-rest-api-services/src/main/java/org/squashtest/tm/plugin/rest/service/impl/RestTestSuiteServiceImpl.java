/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.service.impl;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.domain.campaign.IterationTestPlanItem;
import org.squashtest.tm.domain.campaign.TestSuite;
import org.squashtest.tm.domain.execution.ExecutionStatus;
import org.squashtest.tm.plugin.rest.jackson.model.CustomFieldValueDto;
import org.squashtest.tm.plugin.rest.jackson.model.IterationTestPlanItemDto;
import org.squashtest.tm.plugin.rest.jackson.model.TestSuiteDto;
import org.squashtest.tm.plugin.rest.repository.RestIterationTestPlanItemRepository;
import org.squashtest.tm.plugin.rest.repository.RestTestSuiteRepository;
import org.squashtest.tm.plugin.rest.service.RestTestSuiteService;
import org.squashtest.tm.plugin.rest.service.helper.CustomFieldValueHelper;
import org.squashtest.tm.service.attachment.AttachmentManagerService;
import org.squashtest.tm.service.campaign.CustomIterationModificationService;
import org.squashtest.tm.service.campaign.CustomTestSuiteModificationService;
import org.squashtest.tm.service.campaign.TestSuiteModificationService;
import org.squashtest.tm.service.campaign.TestSuiteTestPlanManagerService;
import org.squashtest.tm.service.internal.campaign.CampaignNodeDeletionHandler;
import org.squashtest.tm.service.internal.customfield.PrivateCustomFieldValueService;
import org.squashtest.tm.service.internal.repository.IterationTestPlanDao;
import org.squashtest.tm.service.internal.repository.TestSuiteDao;
import org.squashtest.tm.service.security.PermissionEvaluationService;
import org.squashtest.tm.service.security.PermissionsUtils;

import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by jthebault on 21/06/2017.
 */
@Service
@Transactional
public class RestTestSuiteServiceImpl implements RestTestSuiteService {

    @Inject
    private RestTestSuiteRepository testSuiteRepository;
    @Inject
    private RestIterationTestPlanItemRepository itpiRepository;
    @Inject
    private CustomIterationModificationService customIterationModificationService;
    @Inject
    private TestSuiteTestPlanManagerService testSuiteTestPlanManagerService;
    @Inject
    private TestSuiteModificationService testSuiteModificationService;
    @Inject
    private PrivateCustomFieldValueService customValueService;
    @Inject
    private AttachmentManagerService attachmentManagerService;
    @Inject
    private CustomFieldValueHelper customFieldValueHelper;
    @Inject
    private TestSuiteDao testSuiteDao;
    @Inject
    private RestTestSuiteRepository restTestSuiteRepository;
    @Inject
    private IterationTestPlanDao itemTestPlanDao;
    @Inject
    private CustomTestSuiteModificationService customTestSuiteModificationService;
    @Inject
    private CampaignNodeDeletionHandler campaignNodeDeletionHandler;

    @Inject
    private PermissionEvaluationService permissionService;

    @Override
    @Transactional(readOnly=true)
    @PreAuthorize("@apiSecurity.hasPermission(#id,'org.squashtest.tm.domain.campaign.TestSuite','READ')")
    public TestSuite getOne(long id) {
        return testSuiteRepository.getOne(id);
    }

    @Override
    @Transactional(readOnly=true)
    @PreAuthorize("@apiSecurity.hasPermission(#testSuiteId,'org.squashtest.tm.domain.campaign.TestSuite','READ')")
    public Page<IterationTestPlanItem> findTestPlan(long testSuiteId, Pageable pageable) {
        return itpiRepository.findAllByTestSuiteId(testSuiteId, pageable);
    }

    @Override
    @PreAuthorize("@apiSecurity.hasPermission(#iterationId,'org.squashtest.tm.domain.campaign.Iteration','WRITE')")
    public TestSuite addTestSuite(Long iterationId ,TestSuiteDto testSuiteDto) {

        TestSuite testSuite = new TestSuite();
        testSuite.setName(testSuiteDto.getName());
        if(testSuiteDto.isHasDescription()){
            testSuite.setDescription(testSuiteDto.getDescription());
        }
        //name et generated id
        customIterationModificationService.addTestSuite(iterationId,testSuite);
        //list id item for bind it   to  test suite
        if(testSuiteDto.isHasListItpi()) {
            List<Long> listIdsItem = testSuiteDto.getListItpi().stream().map(IterationTestPlanItemDto::getId).collect(Collectors.toList());
            testSuiteTestPlanManagerService.bindTestPlan(testSuite.getId(), listIdsItem);
        }
        if(testSuiteDto.isHasCufs()){
            List<CustomFieldValueDto> cufValuesDto = testSuiteDto.getCustomFields();
            //patch value cufs
            customFieldValueHelper.patchCustomFieldValue(testSuite,cufValuesDto);
        }
        return testSuite;
    }

    @Override
    @PreAuthorize("@apiSecurity.hasPermission(#testSuiteId,'org.squashtest.tm.domain.campaign.TestSuite','WRITE')")
    public TestSuite modifyTestSuite(TestSuiteDto testSuiteDto, Long testSuiteId) {

        TestSuite testSuite = testSuiteTestPlanManagerService.findTestSuite(testSuiteId);
        if(testSuiteDto.isHasDescription()){
            testSuite.setDescription(testSuiteDto.getDescription());
        }
        if(testSuiteDto.isHasStatus()){
            ExecutionStatus executionStatus = ExecutionStatus.valueOf(testSuiteDto.getStatus());
            testSuite.setExecutionStatus(executionStatus);
        }
        if(testSuiteDto.isHasCufs()){
            List<CustomFieldValueDto> cufValuesDto = testSuiteDto.getCustomFields();
            //patch value cufs
            customFieldValueHelper.patchCustomFieldValue(testSuite,cufValuesDto);
        }
        return testSuite;
    }

    @Override
    // security handled by code
    public void deleteTestSuite(List<Long> testSuiteIds) {
        PermissionsUtils.checkPermission(permissionService, testSuiteIds, "DELETE", TestSuite.class.getName());
        campaignNodeDeletionHandler.deleteSuites(testSuiteIds, false);

    }

    @Override
    @PreAuthorize("@apiSecurity.hasPermission(#idTestSuite,'org.squashtest.tm.domain.campaign.TestSuite','LINK')")
    public TestSuite detachTestPlanFromTestSuite(Long idTestSuite, List<Long> ids ) {
        TestSuite testSuite = testSuiteDao.getOne(idTestSuite);
        testSuiteTestPlanManagerService.detachTestPlanFromTestSuite(ids, idTestSuite);
        return testSuite;
    }

    @Override
    @PreAuthorize("@apiSecurity.hasPermission(#idTestSuite,'org.squashtest.tm.domain.campaign.TestSuite','LINK')")
    public TestSuite attachTestPlanToTestSuite(Long idTestSuite, List<Long> ids) {
        TestSuite testSuite = testSuiteDao.getOne(idTestSuite);
        testSuiteTestPlanManagerService.bindTestPlan(testSuite.getId(), ids);
        return testSuite;
    }

}
