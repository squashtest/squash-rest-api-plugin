/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.jackson.mixin;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.annotation.JsonUnwrapped;
import com.fasterxml.jackson.databind.annotation.JsonAppend;
import com.fasterxml.jackson.databind.annotation.JsonAppend.Prop;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.squashtest.tm.domain.audit.AuditableSupport;
import org.squashtest.tm.domain.infolist.InfoListItem;
import org.squashtest.tm.domain.requirement.Requirement;
import org.squashtest.tm.domain.requirement.RequirementCriticality;
import org.squashtest.tm.domain.requirement.RequirementStatus;
import org.squashtest.tm.domain.testcase.RequirementVersionCoverage;
import org.squashtest.tm.plugin.rest.core.jackson.SerializeChainConverter;
import org.squashtest.tm.plugin.rest.jackson.model.RestCustomFieldMembers;
import org.squashtest.tm.plugin.rest.jackson.serializer.AttachmentHolderPropertyWriter;
import org.squashtest.tm.plugin.rest.jackson.serializer.CustomFieldValuesPropertyWriter;
import org.squashtest.tm.plugin.rest.jackson.serializer.HateoasWrapperConverter;
import org.squashtest.tm.plugin.rest.jackson.serializer.RestNatureTypeCategorySerializer;
import org.squashtest.tm.plugin.rest.jackson.serializer.UnauthorizedResourcesConverter;
import org.squashtest.tm.plugin.rest.jackson.serializer.VerifyingTestCaseConverter;

import java.util.Set;


@JsonAutoDetect(fieldVisibility=Visibility.NONE, getterVisibility=Visibility.NONE, isGetterVisibility=Visibility.NONE)
@JsonTypeName("requirement-version")
@JsonAppend(props={@Prop(name="custom_fields", type=RestCustomFieldMembers.class, value=CustomFieldValuesPropertyWriter.class),
					@Prop(name="attachments", value= AttachmentHolderPropertyWriter.class)})
@JsonPropertyOrder({"_type","id","name", "reference", "version_number", "requirement", "audit", "criticality", "category", "status",
					"description", "custom_fields", "verifying_test_cases"})
public abstract class RestRequirementVersionMixin extends RestIdentifiedMixin{

	
	@JsonProperty
	String name;
	
	@JsonProperty
	String reference;
	
	@JsonProperty
	String description;
	
	@JsonUnwrapped
	abstract AuditableSupport getAudit();
	
	@JsonProperty("version_number")
	int versionNumber;
	
	@JsonProperty
	@JsonSerialize(converter=HateoasWrapperConverter.class)
	@JsonTypeInfo(use=Id.NONE)
	Requirement requirement;
	
	@JsonProperty
	RequirementCriticality criticality;
	
	@JsonProperty
	@JsonSerialize(using=RestNatureTypeCategorySerializer.class)
	InfoListItem category;
	
	@JsonProperty
	RequirementStatus status;
	
	@JsonProperty("verifying_test_cases")
	@SerializeChainConverter(contentConverters={
		VerifyingTestCaseConverter.class,
		HateoasWrapperConverter.class,
		UnauthorizedResourcesConverter.class
	})
	@JsonTypeInfo(use=Id.NONE)
	Set<RequirementVersionCoverage> requirementVersionCoverages;
	
}
