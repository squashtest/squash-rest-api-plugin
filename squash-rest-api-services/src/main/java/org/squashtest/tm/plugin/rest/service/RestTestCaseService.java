/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.domain.testcase.TestStep;
import org.squashtest.tm.plugin.rest.jackson.model.TestCaseDto;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Locale;

public interface RestTestCaseService {

    TestCase getOne(Long id);

    Page<TestCase> getAllReadableTestCases(Pageable pageable);

    Page<TestStep> getTestCaseSteps(long tcid, Pageable pageable);

    TestCase createTestCase(TestCaseDto testCaseDto) throws InvocationTargetException, IllegalAccessException;

    TestCase patchTestCase(TestCaseDto testCasePatch, long id);

    List<String>  deleteTestCase(List<Long> testCaseIds, Boolean dryRun, Locale locale);

    Page<TestCase> getAllStandardTestCases(Pageable paging);

    Page<TestCase> getAllScriptedTestCases(Pageable paging);

    Page<TestCase> getAllKeywordTestCases(Pageable paging);

}
