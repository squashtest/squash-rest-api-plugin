/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.controller;

import org.springframework.hateoas.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.squashtest.tm.domain.execution.ExecutionStatus;
import org.squashtest.tm.domain.execution.ExecutionStep;
import org.squashtest.tm.plugin.rest.core.jackson.DeserializationFilterExpression;
import org.squashtest.tm.plugin.rest.core.jackson.DynamicFilterExpression;
import org.squashtest.tm.plugin.rest.core.web.BaseRestController;
import org.squashtest.tm.plugin.rest.core.web.EntityGetter;
import org.squashtest.tm.plugin.rest.core.web.PersistentEntity;
import org.squashtest.tm.plugin.rest.core.web.RestApiController;
import org.squashtest.tm.plugin.rest.core.web.UseDefaultRestApiConfiguration;
import org.squashtest.tm.plugin.rest.jackson.model.ExecutionStepAndCustomFields;
import org.squashtest.tm.plugin.rest.service.RestExecutionStepService;
import org.squashtest.tm.plugin.rest.validators.CustomFieldValueHintedValidator;
import org.squashtest.tm.plugin.rest.validators.DenormalizedFieldValueHintedValidator;

import javax.inject.Inject;

/**
 * Created by jthebault on 16/06/2017.
 */
@RestApiController(ExecutionStep.class)
@UseDefaultRestApiConfiguration
public class RestExecutionStepController extends BaseRestController{
    public static final String PATCH_DYNAMIC_FILTER = "*, execution[execution_status]";

    @Inject
    private RestExecutionStepService restExecutionStepService;


    @Inject
    private CustomFieldValueHintedValidator cufValidator;

    @Inject
    private DenormalizedFieldValueHintedValidator denoValidator;

    @InitBinder
    public void initBinder(WebDataBinder binder){
        binder.addValidators(cufValidator);
        binder.addValidators(denoValidator);
    }

    @RequestMapping(value = "/execution-steps/{id}", method = RequestMethod.GET)
    @EntityGetter
    @ResponseBody
    @DynamicFilterExpression("*, execution[execution_status]")
    public ResponseEntity<Resource<ExecutionStep>> findExecutionStep(@PathVariable("id") long id){

        ExecutionStep executionStep = restExecutionStepService.getOne(id);

        Resource<ExecutionStep> res = toResource(executionStep);

        res.add(linkService.createLinkTo(executionStep.getProject()));
        res.add(linkService.createLinkTo(executionStep.getExecution()));
        res.add(linkService.createRelationTo(executionStep, "attachments"));


        return ResponseEntity.ok(res);
    }

    @RequestMapping(value = "/execution-steps/{id}/execution-status/{status}", method = RequestMethod.PATCH)
    @ResponseBody
    @DynamicFilterExpression("*, execution[execution_status]")
    @PreAuthorize("@apiSecurity.hasPermission(#id,'org.squashtest.tm.domain.execution.ExecutionStep' , 'WRITE')")
    public ResponseEntity<Resource<ExecutionStep>> modifyExecutionStatus(@PathVariable("id") long id, @PathVariable("status") String status){

        try {
            ExecutionStatus validatedStatus = ExecutionStatus.valueOf(status.toUpperCase());
            restExecutionStepService.modifyExecutionStatus(id, validatedStatus);
        } catch (IllegalArgumentException e){
            throw new EnumConstantNotPresentException(ExecutionStatus.class, "Execution status "+status+" does not exist");
        }

        ExecutionStep executionStep = restExecutionStepService.getOne(id);

        Resource<ExecutionStep> res = toResource(executionStep);

        res.add(linkService.createLinkTo(executionStep.getProject()));
        res.add(linkService.createLinkTo(executionStep.getExecution()));

        return ResponseEntity.ok(res);
    }

    @RequestMapping(value = "/execution-steps/{id}", method = RequestMethod.PATCH)
    @ResponseBody
    @DynamicFilterExpression("*, execution[execution_status]")
    @DeserializationFilterExpression("comment")
    public ResponseEntity<Resource<ExecutionStep>> patchExecutionStep(@Validated @PersistentEntity ExecutionStepAndCustomFields step){
        restExecutionStepService.updateStep(step);

        ExecutionStep unwrapped = step.getWrapped();
        Resource<ExecutionStep> res = toResource(unwrapped);

        res.add(linkService.createLinkTo(unwrapped.getProject()));
        res.add(linkService.createLinkTo(unwrapped.getExecution()));

        return ResponseEntity.ok(res);
    }

}
