/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.jackson.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import org.squashtest.tm.domain.testcase.ActionTestStep;
import org.squashtest.tm.domain.testcase.CallTestStep;
import org.squashtest.tm.domain.testcase.KeywordTestStep;
import org.squashtest.tm.domain.testcase.TestStep;

/**
 * Created by jthebault on 14/06/2017.
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "_type")
@JsonSubTypes({ //here are the _type -> java class for DESERIALIZATION only
        @JsonSubTypes.Type(value = ActionTestStepDto.class, name = "action-step"),
        @JsonSubTypes.Type(value = CalledTestStepDto.class, name = "call-step"),
        @JsonSubTypes.Type(value = KeywordTestStepDto.class, name = "keyword-step")})
public abstract class TestStepDto {

    private Long id;

    @JsonProperty("test_case")
    private TestCaseDto testCaseDto;
    //convenient attribute used for validation as we need the test case dto to have the project and check cufs
    @JsonIgnore
    private Long projectId;

    @JsonProperty("index")
    private Integer index;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TestCaseDto getTestCaseDto() {
        return testCaseDto;
    }

    public void setTestCaseDto(TestCaseDto testCaseDto) {
        this.testCaseDto = testCaseDto;
    }

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public abstract void accept(TestStepDtoVisitor testStepDtoVisitor);

    public static TestStep convertDto(TestStepDto dto) {
        final TestStep[] testSteps = new TestStep[1];

        TestStepDtoVisitor visitor = new TestStepDtoVisitor() {

            @Override
            public void visit(ActionTestStepDto actionTestStepDto) {
                ActionTestStep actionTestStep = new ActionTestStep();
                actionTestStep.setAction(actionTestStepDto.getAction());
                actionTestStep.setExpectedResult(actionTestStepDto.getExpectedResult());
                testSteps[0] = actionTestStep;
            }

            @Override
            public void visit(CalledTestStepDto calledTestStepDto) {
                CallTestStep callTestStep = new CallTestStep();
                callTestStep.setCalledTestCase(calledTestStepDto.getCalledTestCase());
                callTestStep.setCalledDataset(calledTestStepDto.getCalledDataset());
                callTestStep.setDelegateParameterValues(calledTestStepDto.isDelegateParameterValues());
                testSteps[0] = callTestStep;
            }

            @Override
            public void visit(KeywordTestStepDto keywordTestStepDto) {
                KeywordTestStep keywordTestStep = new KeywordTestStep();
                keywordTestStep.setKeyword(keywordTestStepDto.getKeyword());
                testSteps[0] = keywordTestStep;
            }
        };
        dto.accept(visitor);
        return testSteps[0];
    }
}
