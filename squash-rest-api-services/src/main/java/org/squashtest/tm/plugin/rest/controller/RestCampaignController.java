/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.controller;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.PagedResources;
import org.springframework.hateoas.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.squashtest.tm.domain.campaign.Campaign;
import org.squashtest.tm.domain.campaign.CampaignTestPlanItem;
import org.squashtest.tm.domain.campaign.Iteration;
import org.squashtest.tm.plugin.rest.controller.helper.ErrorHandlerHelper;
import org.squashtest.tm.plugin.rest.core.jackson.DynamicFilterExpression;
import org.squashtest.tm.plugin.rest.core.web.BaseRestController;
import org.squashtest.tm.plugin.rest.core.web.EntityGetter;
import org.squashtest.tm.plugin.rest.core.web.RestApiController;
import org.squashtest.tm.plugin.rest.core.web.UseDefaultRestApiConfiguration;
import org.squashtest.tm.plugin.rest.jackson.model.CampaignDto;
import org.squashtest.tm.plugin.rest.service.RestCampaignService;
import org.squashtest.tm.plugin.rest.validators.CampaignPatchValidator;
import org.squashtest.tm.plugin.rest.validators.CampaignPostValidator;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

import static org.squashtest.tm.plugin.rest.controller.RestCampaignTestPlanItemController.CTPI_DYNAMIC_FILTER;
import static org.squashtest.tm.plugin.rest.controller.RestIterationController.ITERATION_DYNAMIC_FILTER;

/**
 * Created by jthebault on 16/06/2017.
 */
@RestApiController(Campaign.class)
@UseDefaultRestApiConfiguration
public class RestCampaignController extends BaseRestController{

    public static final String ITERATION_EMBEDDED_FILTER = ITERATION_DYNAMIC_FILTER + ",-campaign";

    public static final String CTPI_EMBEDDED_FILTER = CTPI_DYNAMIC_FILTER + ",-campaign";

    @Inject
    private RestCampaignService restCampaignService;

    @Inject
    private CampaignPostValidator campaignPostValidator;

    @Inject
    private CampaignPatchValidator campaignPatchValidator;

    @GetMapping("/campaigns/{id}")
    @EntityGetter
    @ResponseBody
    @DynamicFilterExpression("*, project[name], parent[name], iterations[name], test_plan[referenced_test_case[name]]")
    public ResponseEntity<Resource<Campaign>> findCampaign(@PathVariable("id") long id){

        Campaign campaign = restCampaignService.getOne(id);

        Resource<Campaign> res = toResource(campaign);

        res.add(linkService.createLinkTo(campaign.getProject()));
        res.add(createRelationTo("iterations"));
        res.add(createRelationTo("test-plan"));
        res.add(createRelationTo("attachments"));

        return ResponseEntity.ok(res);
    }

    @RequestMapping(value = "/campaignsByName/{name}")
    @ResponseBody
    @DynamicFilterExpression("name,reference")
    @SuppressWarnings("rawtypes")
    public ResponseEntity<PagedResources<Resource>> findCampaignByName(@PathVariable("name") String campaignName, Pageable pageable) {
        Page<Campaign> campaigns = restCampaignService.findAllByName(campaignName, pageable);
        PagedResources<Resource> res = toPagedResources(campaigns);
        return ResponseEntity.ok(res);
    }

    @RequestMapping("/campaigns")
    @ResponseBody
    @DynamicFilterExpression("name,reference")
    @SuppressWarnings("rawtypes")
    public ResponseEntity<PagedResources<Resource>> findAllReadableCampaigns(Pageable pageable){

        Page<Campaign> tcs = restCampaignService.findAllReadable(pageable);

        PagedResources<Resource> res = toPagedResources(tcs);

        return ResponseEntity.ok(res);

    }

    @PostMapping("/campaigns")
    @ResponseBody
    @DynamicFilterExpression("*")
    public ResponseEntity<Resource<Campaign>> createCampaign(@RequestBody CampaignDto campaignDto) throws BindException {

        validatePostCampaign(campaignDto);
        Campaign campaign = restCampaignService.createCampaign(campaignDto);
        Resource<Campaign> res = toResource(campaign);

        return ResponseEntity.status(HttpStatus.CREATED).body(res);

    }

    @PatchMapping("/campaigns/{id}")
    @ResponseBody
    @DynamicFilterExpression("*")
    public ResponseEntity<Resource<Campaign>> patchCampaign(@RequestBody CampaignDto campaignDto, @PathVariable("id") long id) throws BindException {

        campaignDto.setId(id);
        validatePatchCampaign(campaignDto);
        Campaign campaign = restCampaignService.patchCampaign(campaignDto, id);
        Resource<Campaign> res = toResource(campaign);

        return ResponseEntity.ok(res);

    }

    @DeleteMapping("/campaigns/{ids}")
    @ResponseBody
    @DynamicFilterExpression("*")
    public ResponseEntity<Void> deleteCampaign(@PathVariable("ids")  List<Long> ids) throws BindException {

        restCampaignService.deleteCampaignsByIds(ids);

        return ResponseEntity.noContent().build();

    }

    @GetMapping("/campaigns/{id}/iterations")
    @ResponseBody
    @DynamicFilterExpression(ITERATION_EMBEDDED_FILTER)
    @SuppressWarnings("rawtypes")
    public ResponseEntity<PagedResources<Resource>> findAllCampaignIterations(@PathVariable("id") long id, Pageable pageable){

        Page<Iteration> tcs = restCampaignService.findIterations(id,pageable);

        PagedResources<Resource> res = toPagedResources(tcs);

        return ResponseEntity.ok(res);

    }

    @GetMapping("/campaigns/{id}/test-plan")
    @ResponseBody
    @SuppressWarnings("rawtypes")
    @DynamicFilterExpression(CTPI_EMBEDDED_FILTER)
    public ResponseEntity<PagedResources<Resource>> findAllCampaignTestPlan(@PathVariable("id") long id, Pageable pageable){

        Page<CampaignTestPlanItem> tcs = restCampaignService.findTestPlan(id,pageable);

        PagedResources<Resource> res = toPagedResources(tcs);

        return ResponseEntity.ok(res);

    }

    private void validatePostCampaign(CampaignDto dto) throws BindException {
        List<Errors> errors = new ArrayList<>();

        BindingResult validation = new BeanPropertyBindingResult(dto, "post-campaign");
        campaignPostValidator.validate(dto, validation);

        if (validation.hasErrors()) {
            errors.add(validation);
        }

        ErrorHandlerHelper.throwIfError(dto, errors, "post-campaign");
    }

    private void validatePatchCampaign(CampaignDto patch) throws BindException {
        List<Errors> errors = new ArrayList<>();

        BindingResult validation = new BeanPropertyBindingResult(patch, "patch-campaign");
        campaignPatchValidator.validate(patch, validation);

        if (validation.hasErrors()) {
            errors.add(validation);
        }

        ErrorHandlerHelper.throwIfError(patch, errors, "patch-campaign");
    }

}
