/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.service.impl;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.core.foundation.lang.Wrapped;
import org.squashtest.tm.domain.IdCollector;
import org.squashtest.tm.domain.customfield.RawValue;
import org.squashtest.tm.domain.infolist.InfoListItem;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.domain.scm.ScmRepository;
import org.squashtest.tm.domain.testautomation.AutomatedTestTechnology;
import org.squashtest.tm.domain.testcase.ActionTestStep;
import org.squashtest.tm.domain.testcase.CallTestStep;
import org.squashtest.tm.domain.testcase.Dataset;
import org.squashtest.tm.domain.testcase.DatasetParamValue;
import org.squashtest.tm.domain.testcase.KeywordTestCase;
import org.squashtest.tm.domain.testcase.KeywordTestStep;
import org.squashtest.tm.domain.testcase.Parameter;
import org.squashtest.tm.domain.testcase.ScriptedTestCase;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.domain.testcase.TestCaseFolder;
import org.squashtest.tm.domain.testcase.TestCaseVisitor;
import org.squashtest.tm.domain.testcase.TestStep;
import org.squashtest.tm.plugin.rest.jackson.model.ActionTestStepDto;
import org.squashtest.tm.plugin.rest.jackson.model.CalledTestStepDto;
import org.squashtest.tm.plugin.rest.jackson.model.CustomFieldValueDto;
import org.squashtest.tm.plugin.rest.jackson.model.DatasetDto;
import org.squashtest.tm.plugin.rest.jackson.model.DatasetParamValueDto;
import org.squashtest.tm.plugin.rest.jackson.model.KeywordTestCaseDto;
import org.squashtest.tm.plugin.rest.jackson.model.KeywordTestStepDto;
import org.squashtest.tm.plugin.rest.jackson.model.ParentEntity;
import org.squashtest.tm.plugin.rest.jackson.model.RequirementVersionDto;
import org.squashtest.tm.plugin.rest.jackson.model.ScriptedTestCaseDto;
import org.squashtest.tm.plugin.rest.jackson.model.TestCaseDto;
import org.squashtest.tm.plugin.rest.jackson.model.TestCaseDtoVisitor;
import org.squashtest.tm.plugin.rest.jackson.model.TestStepDto;
import org.squashtest.tm.plugin.rest.jackson.model.TestStepDtoVisitor;
import org.squashtest.tm.plugin.rest.repository.RestTestCaseRepository;
import org.squashtest.tm.plugin.rest.service.RestInternalCustomFieldValueUpdaterService;
import org.squashtest.tm.plugin.rest.service.RestRequirementVersionService;
import org.squashtest.tm.plugin.rest.service.RestTestCaseService;
import org.squashtest.tm.plugin.rest.service.helper.CustomFieldValueHelper;
import org.squashtest.tm.plugin.rest.validators.TestCasePatchValidator;
import org.squashtest.tm.service.deletion.SuppressionPreviewReport;
import org.squashtest.tm.service.internal.repository.InfoListItemDao;
import org.squashtest.tm.service.project.ProjectFinder;
import org.squashtest.tm.service.requirement.VerifiedRequirementsManagerService;
import org.squashtest.tm.service.security.PermissionEvaluationService;
import org.squashtest.tm.service.security.PermissionsUtils;
import org.squashtest.tm.service.testautomation.AutomatedTestTechnologyFinderService;
import org.squashtest.tm.service.testcase.ParameterModificationService;
import org.squashtest.tm.service.testcase.TestCaseLibraryNavigationService;
import org.squashtest.tm.service.testcase.TestCaseModificationService;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Transactional
public class RestTestCaseServiceImpl implements RestTestCaseService {

    @Inject
    private TestCasePatcher testCasePatcher;
    @Inject
    private KeywordTestStepPatcher keywordTestStepPatcher;
    @Inject
    private InfoListItemDao infoListItemDao;
    @Inject
    private TestCaseModificationService testCaseModificationService;
    @Inject
    private VerifiedRequirementsManagerService verifiedRequirementsManagerService;
    @Inject
    private RestRequirementVersionService requirementVersionService;
    @Inject
    private ProjectFinder projectFinder;
    @Inject
    private RestTestCaseRepository dao;
    @Inject
    private ParameterModificationService parameterModificationService;
    @Inject
    private TestCaseLibraryNavigationService testCaseLibraryNavigationService;
    @Inject
    private RestInternalCustomFieldValueUpdaterService internalCufService;
    @Inject
    private CustomFieldValueHelper customFieldValueConverter;
    @Inject
    private PermissionEvaluationService permissionService;
    @Inject
    private TestCasePatchValidator testCasePatchValidator;
    @Inject
    private AutomatedTestTechnologyFinderService automatedTestTechnologyFinderService;
    @Inject
    private MessageSource messageSource;



    @PersistenceContext
    private EntityManager entityManager;


    @Override
    public TestCase getOne(Long id) {
        return testCaseModificationService.findById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<TestCase> getAllReadableTestCases(Pageable pageable) {
        Collection<Long> ids = getAllProjectIds();
        if (ids.isEmpty()) {
            return new PageImpl<>(Collections.<TestCase>emptyList(), pageable, 0);
        } else {
            return dao.findAllInProjects(ids, pageable);
        }
    }

    @Override
    @Transactional(readOnly = true)
    @PreAuthorize("@apiSecurity.hasPermission(#tcid,'org.squashtest.tm.domain.testcase.TestCase' , 'READ')")
    public Page<TestStep> getTestCaseSteps(long tcid, Pageable pageable) {
        return dao.findTestCaseSteps(tcid, pageable);
    }

    @Override
    // Note : security is handled by the native service, called in the flow of #addToParent
    // It's the easiest way to achieve ACL checks, and works as expected.
    public TestCase createTestCase(TestCaseDto testCaseDto) throws InvocationTargetException, IllegalAccessException {
        String testCaseName = testCaseDto.getName();
        final TestCase testCase = TestCaseDto.convertDto(testCaseDto);
        testCase.setName(testCaseName);

        //we fill some default values in testCaseDto, as patcher will take care of patching all properties from testCaseDto to testCase automatically
        fillDefaultValues(testCaseDto);
        testCasePatcher.patch(testCase, testCaseDto);

        // SQUASH-4543 - add specific automation attributes
        patchAutomationAttributes(testCaseDto, testCase);

        addToParent(testCaseDto, testCase);

        entityManager.persist(testCase);
        entityManager.flush(); //flush for setting ids as we need it for parameter research

        //now we have an id we can create dependant object and use some nice squash service witch will ease our work
        createSteps(testCaseDto, testCase);
        createParameters(testCaseDto, testCase);
        createDatasets(testCaseDto, testCase);

        // [Issue 7201]
        addOrMergeVerifiedRequirements(testCaseDto, testCase);

        return testCase;
    }

    @Override
    @PreAuthorize("@apiSecurity.hasPermission(#id,'org.squashtest.tm.domain.testcase.TestCase' , 'WRITE')")
    public TestCase patchTestCase(TestCaseDto testCasePatch, long id) {
        final TestCase testCase = dao.retrieveById(id);
        //name is a special attribute, witch can be treated by a generic attribute patcher
        if (StringUtils.isNotBlank(testCasePatch.getName())) {
            testCaseModificationService.rename(testCase.getId(), testCasePatch.getName());
        }
        testCasePatcher.patch(testCase, testCasePatch);

        //[SQUASH-425: refactoring plugin de squash]
        patchScriptIfExisted(testCasePatch, testCase);

        // SQUASH-4543 - special automation attributes
        patchAutomationAttributes(testCasePatch, testCase);

        // [Issue 6897]
        internalCufService.mergeCustomFields(testCase, testCasePatch.getCustomFields());

        return testCase;
    }

    private void patchAutomationAttributes(TestCaseDto testCasePatch, TestCase testCase) {
        if (testCasePatch.getAutomatedTestTechnology() != null) {
            AutomatedTestTechnology techno = automatedTestTechnologyFinderService.findByNameIgnoreCase(testCasePatch.getAutomatedTestTechnology());
            testCase.setAutomatedTestTechnology(techno);
        }
        if (testCasePatch.getScmRepositoryId() != null) {
            ScmRepository repo = entityManager.find(ScmRepository.class, testCasePatch.getScmRepositoryId());
            testCase.setScmRepository(repo);
        }
    }

    private void patchScriptIfExisted(TestCaseDto testCasePatch, TestCase testCase) {
        TestCaseVisitor testCaseVisitor = new TestCaseVisitor() {
            @Override
            public void visit(TestCase testCase) {
            }

            @Override
            public void visit(KeywordTestCase keywordTestCase) {
            }

            @Override
            public void visit(ScriptedTestCase scriptedTestCase) {
                Wrapped<String> scriptWrapped = new Wrapped<>(null);
                TestCaseDtoVisitor dtoVisitor = new TestCaseDtoVisitor() {
                    @Override
                    public void visit(TestCaseDto testCaseDto) {
                    }

                    @Override
                    public void visit(ScriptedTestCaseDto scriptedTestCaseDto) {
                        scriptWrapped.setValue(scriptedTestCaseDto.getScript());
                    }

                    @Override
                    public void visit(KeywordTestCaseDto keywordTestCaseDto) {
                    }
                };

                testCasePatch.accept(dtoVisitor);
                String newScript = scriptWrapped.getValue();
                if(newScript != null) {
                    scriptedTestCase.setScript(newScript);
                }
            }
        };

        testCase.accept(testCaseVisitor);
    }

    @Override
    public List<String> deleteTestCase(List<Long> testCaseIds, Boolean dryRun, Locale locale) {
        PermissionsUtils.checkPermission(permissionService, testCaseIds, "DELETE", TestCase.class.getName());
        //simulation deletion
        List<SuppressionPreviewReport> reports = testCasePatchValidator.simulationDelete(testCaseIds);
        if (dryRun == null || dryRun == false) {
            testCaseLibraryNavigationService.deleteNodes(testCaseIds);
        }

        List<String> reportMessages = new ArrayList<>(reports.size());
        for (SuppressionPreviewReport report : reports) {
            reportMessages.add(report.toString(messageSource, locale));
        }
        return reportMessages;
    }

    @Override
    @Transactional(readOnly = true)
    public Page<TestCase> getAllStandardTestCases(Pageable paging) {
        Collection<Long> ids = getAllProjectIds();
        if (ids.isEmpty()) {
            return new PageImpl<>(Collections.<TestCase>emptyList(), paging, 0);
        } else {
            return dao.findAllStandardTestCasesInProject(ids,paging);
        }
    }

    private Collection<Long> getAllProjectIds() {
        Collection<Project> projects = projectFinder.findAllReadable();
        return (Collection<Long>) CollectionUtils.collect(projects, new IdCollector());
    }

    @Override
    @Transactional(readOnly = true)
    public Page<TestCase> getAllScriptedTestCases(Pageable paging) {
        Collection<Long> ids = getAllProjectIds();
        if (ids.isEmpty()) {
            return new PageImpl<>(Collections.<TestCase>emptyList(), paging, 0);
        } else {
            return dao.findAllScriptedTestCasesInProject(ids,paging);
        }
    }

    @Override
    @Transactional(readOnly = true)
    public Page<TestCase> getAllKeywordTestCases(Pageable paging) {
        Collection<Long> ids = getAllProjectIds();
        if (ids.isEmpty()) {
            return new PageImpl<>(Collections.<TestCase>emptyList(), paging, 0);
        } else {
            return dao.findAllKeywordTestCasesInProject(ids,paging);
        }

    }

    private void fillDefaultValues(TestCaseDto testCaseDto) {
        Project project = testCaseDto.getProject();
        if (testCaseDto.getNature() == null) {
            InfoListItem nature = infoListItemDao.findDefaultTestCaseNature(project.getId());
            testCaseDto.setNature(nature);
        }

        if (testCaseDto.getType() == null) {
            InfoListItem nature = infoListItemDao.findDefaultTestCaseType(project.getId());
            testCaseDto.setType(nature);
        }
    }

    private void addToParent(TestCaseDto testCaseDto, TestCase testCase) {
        ParentEntity parent = testCaseDto.getParent();
        switch (parent.getRestType()) {
            case PROJECT:
                addTestCaseToLibrary(testCase, testCaseDto, parent);
                break;
            case TEST_CASE_FOLDER:
                addTestCaseToFolder(testCase, testCaseDto, parent);
                break;
            default:
                throw new IllegalArgumentException("Programmatic error : Rest type " + parent.getRestType() + "is not a valid parent. You should validate this before.");
        }
    }

    private void addTestCaseToFolder(TestCase testCase, TestCaseDto testCaseDto, ParentEntity parent) {
        TestCaseFolder testCaseFolder = entityManager.find(TestCaseFolder.class, parent.getId());
        if (testCaseFolder != null) {
            Map<Long, RawValue> customFieldMap = customFieldValueConverter.convertCustomFieldDtoToMap(testCaseDto.getCustomFields());
            testCaseLibraryNavigationService.addTestCaseToFolder(testCaseFolder.getId(), testCase, customFieldMap, null, new ArrayList<Long>());
        } else {
            throw new IllegalArgumentException("Programmatic error : test case folder with id " + parent.getId() + "is unknown. You should validate this before.");
        }
    }

    private void addTestCaseToLibrary(TestCase testCase, TestCaseDto testCaseDto, ParentEntity parent) {
        Project project = entityManager.find(Project.class, parent.getId());
        if (project != null) {
            Map<Long, RawValue> customFieldMap = customFieldValueConverter.convertCustomFieldDtoToMap(testCaseDto.getCustomFields());
            testCaseLibraryNavigationService.addTestCaseToLibrary(project.getTestCaseLibrary().getId(), testCase, customFieldMap, null, new ArrayList<Long>());
        } else {
            throw new IllegalArgumentException("Programmatic error : project with id " + parent.getId() + "is unknown. You should validate this before.");
        }
    }

    private void createDatasetParamValues(TestCase testCase, DatasetDto datasetDto, Dataset newDataset) {

        Set<DatasetParamValueDto> datasetParamValues = datasetDto.getValueDtos();
        // Getting all parameters : own and inherited.
        List<Parameter> parameters = parameterModificationService.findAllParameters(testCase.getId());
        // Creating a DatasetParamValue for each parameter.
        for (Parameter param : parameters) {
            String newValue = "";
            for (DatasetParamValueDto valueDto : datasetParamValues) {
                if (valueDto.getParameterName().equals(param.getName())) {
                    newValue = valueDto.getValue();
                    // Removing the parameter from the Posted set of DatasetParamValues
                    datasetParamValues.remove(valueDto);
                    break;
                }
            }
            new DatasetParamValue(param, newDataset, newValue);
        }
        // if something remains in the Posted set of DatasetParamValues, it is either a wrong or a duplicate name.
        if (!datasetParamValues.isEmpty()) {
            throw new IllegalArgumentException("Error in parameter_values : parameter name '"
                    + datasetParamValues.iterator().next().getParameterName()
                    + "' must exist and be posted only once.");
        }
    }

    private void createParameters(TestCaseDto testCaseDto, TestCase testCase) {
        if (testCaseDto.getParameters() == null) {
            return;
        }
        for (Parameter parameter : testCaseDto.getParameters()) {
            Parameter detachedCopy = parameter.detachedCopy();
            detachedCopy.setTestCase(testCase);
        }
    }

    private void createSteps(TestCaseDto testCaseDto, final TestCase testCase) {
        if (testCaseDto.getSteps() == null) {
            return;
        }
        List<TestStepDto> stepDtos = testCaseDto.getSteps();
        for (TestStepDto stepDto : stepDtos) {
            createStep(testCase, stepDto);
        }
    }

    private void createStep(final TestCase testCase, TestStepDto stepDto) {
        final TestStep testStep = TestStepDto.convertDto(stepDto);
        testStep.setTestCase(testCase);

        TestStepDtoVisitor visitor = new TestStepDtoVisitor() {
            @Override
            public void visit(ActionTestStepDto actionTestStepDto) {
                List<CustomFieldValueDto> customFieldValueDtos = actionTestStepDto.getCustomFields();
                Map<Long, RawValue> customFieldRawValues = customFieldValueConverter.convertCustomFieldDtoToMap(customFieldValueDtos);
                testCaseModificationService.addActionTestStep(testCase.getId(), (ActionTestStep) testStep, customFieldRawValues);
            }

            @Override
            public void visit(CalledTestStepDto calledTestStepDto) {
                CallTestStep callTestStep = (CallTestStep) testStep;
                testCase.addStep(callTestStep);
            }

            @Override
            public void visit(KeywordTestStepDto keywordTestStepDto) {
                KeywordTestStep createdKeywordTestStep = testCaseModificationService.addKeywordTestStep(
                    testCase.getId(),
                    keywordTestStepDto.getKeyword().name(),
                    keywordTestStepDto.getAction()
                );
                keywordTestStepPatcher.patch(createdKeywordTestStep, keywordTestStepDto);
            }
        };

        stepDto.accept(visitor);
    }

    private void createDatasets(TestCaseDto testCaseDto, TestCase testCase) {
        if (testCaseDto.getDatasets() == null) {
            return;
        }
        for (DatasetDto dataset : testCaseDto.getDatasets()) {
            if (DatasetDto.class.isAssignableFrom(dataset.getClass())) {
                DatasetDto datasetDto = (DatasetDto) dataset;
                Dataset newDataset = new Dataset(datasetDto.getName(), testCase);
                createDatasetParamValues(testCase, datasetDto, newDataset);
            }
        }

    }

    private void addOrMergeVerifiedRequirements(TestCaseDto testCaseDto, TestCase testCase) {
        if (testCaseDto.getVerifiedRequirements() == null) {
            return;
        }

        // adding verified requirements to test case
        // in case of invalid req ids, service in SquashTM will handle it.
        // RequirementAlreadyVerifiedException is also been taken care of in Squash service
        List<Long> reqVerIds = testCaseDto.getVerifiedRequirements().stream().map(RequirementVersionDto::getId).collect(Collectors.toList());
        List<Long> verifReqIds = requirementVersionService.findReqIdsByVersionIds(reqVerIds);
        verifiedRequirementsManagerService.addVerifiedRequirementsToTestCase(verifReqIds, testCase.getId());
    }


}
