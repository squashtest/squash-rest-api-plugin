/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.controller;

import org.springframework.hateoas.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.squashtest.tm.domain.testcase.Dataset;
import org.squashtest.tm.domain.testcase.Parameter;
import org.squashtest.tm.plugin.rest.controller.helper.ResourceLinksHelper;
import org.squashtest.tm.plugin.rest.core.jackson.DynamicFilterExpression;
import org.squashtest.tm.plugin.rest.core.web.BaseRestController;
import org.squashtest.tm.plugin.rest.core.web.EntityGetter;
import org.squashtest.tm.plugin.rest.core.web.RestApiController;
import org.squashtest.tm.plugin.rest.core.web.UseDefaultRestApiConfiguration;
import org.squashtest.tm.plugin.rest.jackson.model.DatasetDto;
import org.squashtest.tm.plugin.rest.service.RestDatasetService;
import org.squashtest.tm.plugin.rest.validators.DatasetValidator;

import javax.inject.Inject;
import java.util.List;


@RestApiController(Dataset.class)
@UseDefaultRestApiConfiguration
public class RestDatasetController extends BaseRestController {

	@Inject
	private RestDatasetService restDatasetService;
	@Inject
	private DatasetValidator datasetValidator;
	@Inject
	private ResourceLinksHelper linksHelper;

	
	@RequestMapping("/datasets/{id}")
	@EntityGetter
	@DynamicFilterExpression("*,parameters[name],test_case[name]")
	public ResponseEntity<Resource<Dataset>> findParameter(@PathVariable("id") long id){
		Dataset dataset = restDatasetService.getOne(id);

		Resource<Dataset> res = new Resource<Dataset>(dataset);
		
		res.add(createSelfLink(dataset));
		
		return ResponseEntity.ok(res);
	}

	/*AMk: add dataset*/

	@RequestMapping(value = "/datasets", method = RequestMethod.POST)
	@ResponseBody
	@DynamicFilterExpression("*")
	public ResponseEntity<Resource<Dataset>> addDataset(@RequestBody DatasetDto datasetDto)throws BindException {
		// get the list of parameters of the referenced test case
		List<Parameter> listAllParamByIdTc = restDatasetService.findAllParametersByTc(datasetDto.getReferencedTestCase().getId());
		//validation json dataset
		datasetValidator.validationPostDataset(datasetDto, listAllParamByIdTc);

		Dataset dataset = restDatasetService.addDataset(datasetDto, listAllParamByIdTc);

		Resource<Dataset> res = toResource(dataset);

		linksHelper.addAllLinksForDataset(res);

		return ResponseEntity.status(HttpStatus.CREATED).body(res);

	}

	/*AMk: modify dataset*/

	@RequestMapping(value = "/datasets/{id}", method = RequestMethod.PATCH)
	@ResponseBody
	@DynamicFilterExpression("*")
	public ResponseEntity<Resource<Dataset>> ModifyDataset(@RequestBody DatasetDto datasetDto,
														   @PathVariable("id") long datasetId)throws BindException{
		//validation Dto
		datasetValidator.validationPatchDataset(datasetDto, datasetId);
		//modification dataset
		Dataset dataset = restDatasetService.modifyDataset(datasetDto, datasetId);

		Resource<Dataset> res = toResource(dataset);

		linksHelper.addAllLinksForDataset(res);

		return ResponseEntity.ok(res);
	}

	/*AMK Delete dataset*/
	@RequestMapping(value = "/datasets/{id}", method = RequestMethod.DELETE)
	@ResponseBody
	@DynamicFilterExpression("*")
	public ResponseEntity<Void> removeDataset(@PathVariable("id") Long datasetId) {
		restDatasetService.deleteDataset(datasetId);
		return ResponseEntity.noContent().build();
	}
}
