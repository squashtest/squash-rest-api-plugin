/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.validators;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.squashtest.tm.domain.execution.Execution;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.plugin.rest.core.jackson.DeserializationHints;
import org.squashtest.tm.plugin.rest.core.jackson.WrappedDTO;
import org.squashtest.tm.plugin.rest.core.validation.HintedValidator;
import org.squashtest.tm.plugin.rest.validators.helper.ExecutionValidationHelper;

/**
 * Created by jlor on 28/07/2017.
 */
@Component
public class ExecutionHintedValidator implements HintedValidator {

    @Override
    public void validateWithHints(Object target, Errors errors, DeserializationHints hints) {

        WrappedDTO wrapped = (WrappedDTO) target;
        Project project = hints.getProject();

        Execution execution = (Execution) wrapped.getWrapped();

        ExecutionValidationHelper helper = new ExecutionValidationHelper(execution, project, errors);

        helper.validate();
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return WrappedDTO.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        // NO-OP
    }
}
