/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.controller;

import org.springframework.hateoas.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.squashtest.tm.domain.requirement.RequirementVersion;
import org.squashtest.tm.plugin.rest.core.jackson.DynamicFilterExpression;
import org.squashtest.tm.plugin.rest.core.web.BaseRestController;
import org.squashtest.tm.plugin.rest.core.web.EntityGetter;
import org.squashtest.tm.plugin.rest.core.web.RestApiController;
import org.squashtest.tm.plugin.rest.core.web.UseDefaultRestApiConfiguration;
import org.squashtest.tm.plugin.rest.service.RestRequirementVersionService;

import javax.inject.Inject;

@RestApiController(RequirementVersion.class)
@UseDefaultRestApiConfiguration
public class RestRequirementVersionController extends BaseRestController{

	@Inject
	private RestRequirementVersionService service;
	
	@RequestMapping("/requirement-versions/{id}")
	@ResponseBody
	@DynamicFilterExpression("*, requirement[name], verifying_test_cases[name]")
	@EntityGetter
	public ResponseEntity<Resource<RequirementVersion>> findRequirementVersion(@PathVariable("id") long id){
		
		RequirementVersion ver = service.findRequirementVersion(id);
		
		Resource<RequirementVersion> res = toResource(ver);

		res.add(linkService.createLinkTo(ver.getProject()));
		res.add(linkService.createLinkTo(ver.getRequirement()));
		res.add(linkService.createRelationTo(ver, "attachments"));

		return ResponseEntity.ok(res);
	}


	
}
