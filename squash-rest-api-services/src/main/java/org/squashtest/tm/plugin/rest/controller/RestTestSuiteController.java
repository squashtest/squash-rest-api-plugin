/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.controller;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.PagedResources;
import org.springframework.hateoas.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.squashtest.tm.domain.campaign.IterationTestPlanItem;
import org.squashtest.tm.domain.campaign.TestSuite;
import org.squashtest.tm.plugin.rest.controller.helper.ResourceLinksHelper;
import org.squashtest.tm.plugin.rest.core.jackson.DynamicFilterExpression;
import org.squashtest.tm.plugin.rest.core.web.BaseRestController;
import org.squashtest.tm.plugin.rest.core.web.EntityGetter;
import org.squashtest.tm.plugin.rest.core.web.RestApiController;
import org.squashtest.tm.plugin.rest.core.web.UseDefaultRestApiConfiguration;
import org.squashtest.tm.plugin.rest.jackson.model.TestSuiteDto;
import org.squashtest.tm.plugin.rest.service.RestTestSuiteService;
import org.squashtest.tm.plugin.rest.validators.TestSuiteValidator;

import javax.inject.Inject;
import java.util.List;

import static org.squashtest.tm.plugin.rest.controller.RestIterationController.ITPI_EMBEDDED_FILTER;

/**
 * Created by jthebault on 16/06/2017.
 */
@RestApiController(TestSuite.class)
@UseDefaultRestApiConfiguration
public class RestTestSuiteController extends BaseRestController{

    public static final String TEST_SUITE_DYNAMIC_FILTER = "*, parent[name], -path,test_plan[referenced_test_case[name],referenced_dataset[name], execution_status]";

    @Inject
    private RestTestSuiteService restTestSuiteService;
    @Inject
    private TestSuiteValidator testSuiteValidator;

    @Inject
    private ResourceLinksHelper linksHelper;

    @RequestMapping(value = "/test-suites/{id}", method = RequestMethod.GET)
    @EntityGetter
    @ResponseBody
    @DynamicFilterExpression(TEST_SUITE_DYNAMIC_FILTER)
    public ResponseEntity<Resource<TestSuite>> findTestSuite(@PathVariable("id") long id){

        TestSuite testSuite = restTestSuiteService.getOne(id);

        Resource<TestSuite> res = toResource(testSuite);

        linksHelper.addAllLinksForTestSuite(res);

        return ResponseEntity.ok(res);
    }

    @RequestMapping("/test-suites/{id}/test-plan")
    @ResponseBody
    @DynamicFilterExpression(ITPI_EMBEDDED_FILTER)
    public ResponseEntity<PagedResources<Resource>> findIterationTestPlan(@PathVariable("id") long testSuiteId, Pageable pageable){

        Page<IterationTestPlanItem> steps = restTestSuiteService.findTestPlan(testSuiteId, pageable);

        PagedResources<Resource> res = toPagedResourcesWithRel(steps, "test-plan");

        return ResponseEntity.ok(res);

    }

    /*AMK add Test suite*/

    @RequestMapping(value="/test-suites" , method = RequestMethod.POST)
    @ResponseBody
    @DynamicFilterExpression(TEST_SUITE_DYNAMIC_FILTER)
    public ResponseEntity<Resource<TestSuite>> addTestSuite(@RequestBody TestSuiteDto testSuiteDto) throws BindException {

        testSuiteValidator.validatePostTestSuite(testSuiteDto);

        TestSuite testSuite =  restTestSuiteService.addTestSuite(testSuiteDto.getParent().getId(), testSuiteDto);

        Resource<TestSuite> res = toResource(testSuite);

        linksHelper.addAllLinksForTestSuite(res);

        return ResponseEntity.status(HttpStatus.CREATED).body(res);
    }

    /*AMK modify Test Suite*/

    @RequestMapping(value = "/test-suites/{id}", method = RequestMethod.PATCH)
    @ResponseBody
    @DynamicFilterExpression(TEST_SUITE_DYNAMIC_FILTER)
    public ResponseEntity<Resource<TestSuite>> ModifyTestSuite(@RequestBody TestSuiteDto testSuiteDto ,
                                                                   @PathVariable("id") Long testSuiteId)throws BindException{

        testSuiteValidator.validatePatchTestSuite(testSuiteDto,testSuiteId);

        TestSuite testSuite = restTestSuiteService.modifyTestSuite(testSuiteDto,testSuiteId);

        Resource<TestSuite> res = toResource(testSuite);

        linksHelper.addAllLinksForTestSuite(res);

        return ResponseEntity.ok(res);
    }

    /*AMK delete just TestSuite without his items*/
    @RequestMapping(value = "/test-suites/{ids}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> deleteTestSuite(@PathVariable("ids") List<Long> testSuiteIds) {
        restTestSuiteService.deleteTestSuite(testSuiteIds);
        return ResponseEntity.noContent().build();
    }

    /*AMK draw back item without delete it */
    @RequestMapping(value = "/test-suites/{idTestSuite}/test-plan/{ids}", method = RequestMethod.DELETE)
    @ResponseBody
    @DynamicFilterExpression(TEST_SUITE_DYNAMIC_FILTER)
    public ResponseEntity<Resource<TestSuite>> detachItemFromTestSuite(@PathVariable("idTestSuite") Long idTestSuite,
                                                            @PathVariable("ids") List<Long> ids){


        TestSuite testSuite = restTestSuiteService.detachTestPlanFromTestSuite(idTestSuite,ids);

        Resource<TestSuite> res = toResource(testSuite);

        linksHelper.addAllLinksForTestSuite(res);

        return ResponseEntity.ok(res);
    }

    /*AMK attach item to test suite */
    @RequestMapping(value = "/test-suites/{idTestSuite}/test-plan/{ids}", method = RequestMethod.POST)
    @ResponseBody
    @DynamicFilterExpression(TEST_SUITE_DYNAMIC_FILTER)
    public ResponseEntity<Resource<TestSuite>> attachTestPlanToTestSuite(@PathVariable("idTestSuite") Long idTestSuite,
                                                                       @PathVariable("ids") List<Long> ids){

        TestSuite testSuite = restTestSuiteService.attachTestPlanToTestSuite(idTestSuite,ids);

        Resource<TestSuite> res = toResource(testSuite);

        linksHelper.addAllLinksForTestSuite(res);

        return ResponseEntity.ok(res);
    }

}
