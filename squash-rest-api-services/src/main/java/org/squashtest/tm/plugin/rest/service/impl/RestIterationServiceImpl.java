/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.service.impl;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.domain.campaign.Iteration;
import org.squashtest.tm.domain.campaign.IterationTestPlanItem;
import org.squashtest.tm.domain.campaign.TestSuite;
import org.squashtest.tm.domain.customfield.RawValue;
import org.squashtest.tm.plugin.rest.jackson.model.IterationDto;
import org.squashtest.tm.plugin.rest.repository.RestIterationRepository;
import org.squashtest.tm.plugin.rest.repository.RestIterationTestPlanItemRepository;
import org.squashtest.tm.plugin.rest.repository.RestTestSuiteRepository;
import org.squashtest.tm.plugin.rest.service.RestInternalCustomFieldValueUpdaterService;
import org.squashtest.tm.plugin.rest.service.RestIterationService;
import org.squashtest.tm.plugin.rest.service.helper.CustomFieldValueHelper;
import org.squashtest.tm.service.campaign.CampaignLibraryNavigationService;
import org.squashtest.tm.service.campaign.IterationModificationService;
import org.squashtest.tm.service.internal.repository.DatasetDao;
import org.squashtest.tm.service.internal.repository.IterationDao;
import org.squashtest.tm.service.internal.repository.IterationTestPlanDao;
import org.squashtest.tm.service.internal.repository.TestCaseDao;
import org.squashtest.tm.service.internal.repository.UserDao;

import javax.inject.Inject;
import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Map;

import static java.util.Objects.isNull;

/**
 * Created by jthebault on 19/06/2017.
 */
@Service
@Transactional
public class RestIterationServiceImpl implements RestIterationService {

    @Inject
    private RestIterationRepository iterationRepository;

    @Inject
    private RestIterationTestPlanItemRepository itpiRepository;

    @Inject
    private RestTestSuiteRepository testSuiteRepository;

    @Inject
    private IterationPatcher iterationPatcher;

    @Inject
    private CustomFieldValueHelper customFieldValueConverter;

    @Inject
    private CampaignLibraryNavigationService campaignLibraryNavigationService;

    @Inject
    private IterationModificationService iterationModificationService;

    @Inject
    private RestInternalCustomFieldValueUpdaterService internalCufService;
    @Inject
    private IterationDao iterationDao;
    @Inject
    private TestCaseDao testCaseDao;
    @Inject
    private DatasetDao datasetDao;
    @Inject
    private UserDao userDao;
    @Inject
    private IterationTestPlanDao iterationTestPlanDao;

    @Override
    @PreAuthorize("@apiSecurity.hasPermission(#id, 'org.squashtest.tm.domain.campaign.Iteration', 'READ')")
    @Transactional(readOnly=true)
    public Iteration getOne(long id) {
        return iterationRepository.getOne(id);
    }

    @Override
    @PreAuthorize("@apiSecurity.hasPermission(#campaignId, 'org.squashtest.tm.domain.campaign.Campaign', 'WRITE')")
    public Iteration createIteration(IterationDto iterationDto, Long campaignId) {
        Iteration iteration = new Iteration();
        iteration.setName(iterationDto.getName());

        iterationPatcher.patch(iteration, iterationDto);

        iteration.setActualStartAuto(iterationDto.isActualStartAuto());
        iteration.setActualEndAuto(iterationDto.isActualEndAuto());

        Map<Long, RawValue> customFieldRawValues =
                customFieldValueConverter.convertCustomFieldDtoToMap(iterationDto.getCustomFields());

        campaignLibraryNavigationService.addIterationToCampaign(iteration, campaignId,
                iterationDto.isCopyCampaignExecutionPlan(), customFieldRawValues);

        // after persist iteration
        fillTimePeriod(iterationDto, iteration.getId());

        return iteration;
    }

    @Override
    @PreAuthorize("@apiSecurity.hasPermission(#iterationId, 'org.squashtest.tm.domain.campaign.Iteration', 'WRITE')")
    public Iteration patchIteration(IterationDto patch, long iterationId) {
        Iteration iteration = getOne(iterationId);
        if (StringUtils.isNotBlank(patch.getName())) {
            iterationModificationService.rename(patch.getId(), patch.getName());
        }
        iterationPatcher.patch(iteration, patch);
        fillTimePeriod(patch, iteration.getId());

        internalCufService.mergeCustomFields(iteration, patch.getCustomFields());
        return iteration;
    }

    @Override
    public void deleteIterationsByIds(List<Long> iterationIds) {
        campaignLibraryNavigationService.deleteIterations(iterationIds);
    }

    @Override
    @PreAuthorize("@apiSecurity.hasPermission(#iterationId, 'org.squashtest.tm.domain.campaign.Iteration', 'READ')")
    @Transactional(readOnly=true)
    public Page<IterationTestPlanItem> findIterationTestPlan(long iterationId, Pageable pageable) {
        return itpiRepository.findAllByIteration_Id(iterationId,pageable);
    }

    @Override
    @PreAuthorize("@apiSecurity.hasPermission(#iterationId, 'org.squashtest.tm.domain.campaign.Iteration', 'READ')")
    @Transactional(readOnly=true)
    public Page<TestSuite> findIterationTestSuite(long iterationId, Pageable pageable) {
        return testSuiteRepository.findAllByIteration_Id(iterationId,pageable);
    }

    @Override
    @Transactional(readOnly=true)
    public Iteration getOneByName(String iterationName) {
        Iteration iteration = iterationRepository.getOneByName(iterationName);
        if (isNull(iteration)) {
            throw new EntityNotFoundException("Unable to find org.squashtest.tm.domain.campaign.Iteration with name " + iterationName);
        }
        return iteration;
    }

    private void fillTimePeriod(IterationDto iterationDto, long iterationId) {
        if (iterationDto.getScheduledStartDate() != null) {
            iterationModificationService.changeScheduledStartDate(iterationId, iterationDto.getScheduledStartDate());
        }

        if (iterationDto.getScheduledEndDate() != null) {
            iterationModificationService.changeScheduledEndDate(iterationId, iterationDto.getScheduledEndDate());
        }

        if (iterationDto.getActualStartDate() != null) {
            iterationModificationService.changeActualStartDate(iterationId, iterationDto.getActualStartDate());
        }

        if(iterationDto.getActualEndDate() != null) {
            iterationModificationService.changeActualEndDate(iterationId, iterationDto.getActualEndDate());
        }


    }
}
