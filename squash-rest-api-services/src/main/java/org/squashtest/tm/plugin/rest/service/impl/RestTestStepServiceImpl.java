/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.service.impl;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.core.foundation.lang.Wrapped;
import org.squashtest.tm.domain.customfield.RawValue;
import org.squashtest.tm.domain.testcase.ActionTestStep;
import org.squashtest.tm.domain.testcase.CallTestStep;
import org.squashtest.tm.domain.testcase.KeywordTestStep;
import org.squashtest.tm.domain.testcase.ParameterAssignationMode;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.domain.testcase.TestStep;
import org.squashtest.tm.plugin.rest.jackson.model.ActionTestStepDto;
import org.squashtest.tm.plugin.rest.jackson.model.CalledTestStepDto;
import org.squashtest.tm.plugin.rest.jackson.model.CustomFieldValueDto;
import org.squashtest.tm.plugin.rest.jackson.model.KeywordTestStepDto;
import org.squashtest.tm.plugin.rest.jackson.model.TestStepDto;
import org.squashtest.tm.plugin.rest.jackson.model.TestStepDtoVisitor;
import org.squashtest.tm.plugin.rest.service.RestInternalCustomFieldValueUpdaterService;
import org.squashtest.tm.plugin.rest.service.RestTestStepService;
import org.squashtest.tm.plugin.rest.service.helper.CustomFieldValueHelper;
import org.squashtest.tm.service.testcase.CallStepManagerService;
import org.squashtest.tm.service.testcase.TestCaseModificationService;
import org.squashtest.tm.service.testcase.TestStepModificationService;

import javax.inject.Inject;
import java.lang.reflect.InvocationTargetException;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class RestTestStepServiceImpl implements RestTestStepService {

    @Inject
    private TestCaseModificationService testCaseModificationService;

    @Inject
    private TestStepModificationService testStepModificationService;

    @Inject
    private CallStepManagerService callStepManagerService;

    @Inject
    private CustomFieldValueHelper customFieldValueConverter;

    @Inject
    private ActionTestStepPatcher actionTestStepPatcher;

    @Inject
    private KeywordTestStepPatcher keywordTestStepPatcher;

    @Inject
    private RestInternalCustomFieldValueUpdaterService internalCufService;


    @Override
    @Transactional(readOnly = true)
    @PreAuthorize("@apiSecurity.hasPermission(#id,'org.squashtest.tm.domain.testcase.TestStep' , 'READ')")
    public TestStep getOne(long id) {
        return testStepModificationService.findById(id);
    }

    @Override
    // Note : security is handled by the native service, called in the flow of #addActionTestStep
    public TestStep createTestStep(TestStepDto testStepDto, TestCase testCase) throws InvocationTargetException, IllegalAccessException {
        return createStep(testCase, testStepDto);
    }

    @Override
    @PreAuthorize("@apiSecurity.hasPermission(#stepId,'org.squashtest.tm.domain.testcase.TestStep', 'WRITE')")
    public TestStep patchTestStep(final TestStepDto patch, long stepId) {
        final TestStep testStep = testStepModificationService.findById(stepId);

        if (patch.getIndex() != null) {
            testCaseModificationService.changeTestStepsPosition(testStep.getTestCase().getId(), patch.getIndex(), Collections.singletonList(patch.getId()));
        }

        TestStepDtoVisitor visitor = new TestStepDtoVisitor() {
            @Override
            public void visit(ActionTestStepDto actionTestStepDto) {
                actionTestStepPatcher.patch(testStep, patch);
                internalCufService.mergeCustomFields((ActionTestStep) testStep, actionTestStepDto.getCustomFields());
            }

            @Override
            public void visit(CalledTestStepDto calledTestStepDto) {
                ParameterAssignationMode mode;
                Long datasetId = null;
                if (calledTestStepDto.isDelegateParameterValues()) {
                    mode = ParameterAssignationMode.DELEGATE;
                } else {
                    if (calledTestStepDto.getCalledDataset() != null) {
                        mode = ParameterAssignationMode.CALLED_DATASET;
                        datasetId = calledTestStepDto.getCalledDataset().getId();
                    } else {
                        mode = ParameterAssignationMode.NOTHING;
                    }
                }
                callStepManagerService.setParameterAssignationMode(calledTestStepDto.getId(), mode, datasetId);
            }

            @Override
            public void visit(KeywordTestStepDto keywordTestStepDto) {
                if(keywordTestStepDto.getKeyword() != null) {
                    testCaseModificationService.updateKeywordTestStep(stepId, keywordTestStepDto.getKeyword());
                }
                if(keywordTestStepDto.getAction() != null) {
                    testCaseModificationService.updateKeywordTestStep(stepId, keywordTestStepDto.getAction());
                }
                keywordTestStepPatcher.patch(testStep, patch);
            }
        };

        patch.accept(visitor);

        return testStep;
    }

    @Override
    public void deleteTestStepsByIds(List<Long> stepIds) {
        // Since we can not sure all the steps come from the same test case, this loop is the only way to make this deletion go well
        for (Long stepId : stepIds) {
            TestStep testStep = testStepModificationService.findById(stepId);
            testCaseModificationService.removeStepFromTestCase(testStep.getTestCase().getId(), stepId);
        }
    }

    private TestStep createStep(final TestCase testCase, TestStepDto stepDto) {
        final Integer index = stepDto.getIndex();
        final TestStep testStep = TestStepDto.convertDto(stepDto);
        testStep.setTestCase(testCase);

        TestStepDtoWithWrapperVisitor visitor = new TestStepDtoWithWrapperVisitor(index, testCase, testStep);

        stepDto.accept(visitor);
        return visitor.getTestStep();
    }

    private class TestStepDtoWithWrapperVisitor implements TestStepDtoVisitor {
        /*
         * Unfortunately, CustomTestCaseModificationServiceImpl#addKeywordTestStep(long, KeywordTestStep)}
         * does not persist the TestStep which is given as input, but a new one created during the process.
         * As a consequence, the input TestStep is not updated with the new created id.
         * The solution is to get the TestStep which is returned by addKeywordTestStep() method,
         * and this is done with a Wrapped<TestStep> in the visitor.
         * */
        private Wrapped<TestStep> wrappedTestStep = new Wrapped<>();
        private Integer index;
        private TestCase testCase;
        private TestStep testStep;

        public TestStepDtoWithWrapperVisitor(Integer index, TestCase testCase, TestStep testStep) {
            this.index = index;
            this.testCase = testCase;
            this.testStep = testStep;
        }

        @Override
        public void visit(ActionTestStepDto actionTestStepDto) {
            List<CustomFieldValueDto> customFieldValueDtos = actionTestStepDto.getCustomFields();
            Map<Long, RawValue> customFieldRawValues = customFieldValueConverter.convertCustomFieldDtoToMap(customFieldValueDtos);
            if (index != null) {
                wrappedTestStep.setValue(
                        testCaseModificationService.addActionTestStep(
                                testCase.getId(), (ActionTestStep) testStep, customFieldRawValues, index));
            } else {
                wrappedTestStep.setValue(
                        testCaseModificationService.addActionTestStep(
                                testCase.getId(), (ActionTestStep) testStep, customFieldRawValues));
            }
        }

        @Override
        public void visit(CalledTestStepDto calledTestStepDto) {
            CallTestStep callTestStep = (CallTestStep) testStep;
            callStepManagerService.checkForCyclicStepCallBeforePaste(
                    testCase.getId(), callTestStep.getCalledTestCase().getId());
            if (index != null) {
                testCase.addStep(index, callTestStep);
            } else {
                testCase.addStep(callTestStep);
            }
            wrappedTestStep.setValue(testStep);
        }

        @Override
        public void visit(KeywordTestStepDto keywordTestStepDto) {
            KeywordTestStep keywordTestStep = (KeywordTestStep) testStep;
            KeywordTestStep createdKeywordTestStep;
            if (index != null) {
                createdKeywordTestStep =
                        testCaseModificationService.addKeywordTestStep(
                                testCase.getId(),
                                keywordTestStepDto.getKeyword().name(),
                                keywordTestStepDto.getAction(),
                                index);
            } else {
                createdKeywordTestStep =
                        testCaseModificationService.addKeywordTestStep(
                                testCase.getId(),
                                keywordTestStep.getKeyword().name(),
                                keywordTestStepDto.getAction());
            }
            keywordTestStepPatcher.patch(createdKeywordTestStep, keywordTestStepDto);
            wrappedTestStep.setValue(createdKeywordTestStep);
        }

        public TestStep getTestStep() {
            return wrappedTestStep.getValue();
        }
    }
}
