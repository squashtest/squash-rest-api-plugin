/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.controller;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.PagedResources;
import org.springframework.hateoas.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.squashtest.tm.domain.users.Team;
import org.squashtest.tm.domain.users.User;
import org.squashtest.tm.plugin.rest.controller.helper.ErrorHandlerHelper;
import org.squashtest.tm.plugin.rest.core.jackson.DynamicFilterExpression;
import org.squashtest.tm.plugin.rest.core.web.BaseRestController;
import org.squashtest.tm.plugin.rest.core.web.EntityGetter;
import org.squashtest.tm.plugin.rest.core.web.RestApiController;
import org.squashtest.tm.plugin.rest.core.web.UseDefaultRestApiConfiguration;
import org.squashtest.tm.plugin.rest.jackson.model.TeamDto;
import org.squashtest.tm.plugin.rest.service.RestPartyService;
import org.squashtest.tm.plugin.rest.validators.PartyPatchValidator;
import org.squashtest.tm.plugin.rest.validators.PartyPostValidator;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

@RestApiController(Team.class)
@UseDefaultRestApiConfiguration
public class RestTeamController extends BaseRestController {

    @Inject
    private RestPartyService restPartyService;

    @Inject
    private PartyPostValidator partyPostValidator;

    @Inject
    private PartyPatchValidator partyPatchValidator;

    /*
    * Team
    * */
    @RequestMapping(value = "/teams/{id}", method = RequestMethod.GET)
    @EntityGetter
    @DynamicFilterExpression("*, members[*, -teams]")
    public ResponseEntity<Resource<Team>> findTeam(@PathVariable("id") long teamId) {
        Team team = (Team) restPartyService.findById(teamId);
        Resource<Team> res = toResource(team);

        return ResponseEntity.ok(res);
    }

    @RequestMapping(value = "/teams", method = RequestMethod.GET)
    @ResponseBody
    @DynamicFilterExpression("name")
    public ResponseEntity<PagedResources<Resource>> findAllTeams(Pageable pageable) {
        Page<Team> pagedTeams = restPartyService.findAllTeams(pageable);

        PagedResources<Resource> res = toPagedResources(pagedTeams);

        return ResponseEntity.ok(res);
    }

    @RequestMapping(value = "/teams", method = RequestMethod.POST)
    @ResponseBody
    @DynamicFilterExpression("*")
    public ResponseEntity<Resource<Team>> createTeam(@RequestBody TeamDto teamDto) throws BindException {
        validatePostTeam(teamDto);
        Team team = (Team) restPartyService.createParty(teamDto);
        Resource<Team> res = toResource(team);

        return ResponseEntity.status(HttpStatus.CREATED).body(res);
    }

    @RequestMapping(value = "/teams/{id}", method = RequestMethod.PATCH)
    @ResponseBody
    @DynamicFilterExpression("*, members[*, -teams]")
    public ResponseEntity<Resource<Team>> patchTeam(@RequestBody TeamDto patch, @PathVariable("id") long id) throws BindException {

        patch.setId(id);
        validatePatchTeam(patch);
        Team team = (Team) restPartyService.patchParty(patch, id);
        Resource<Team> res = toResource(team);

        return ResponseEntity.ok(res);
    }

    @RequestMapping(value = "/teams/{ids}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> deleteTeams(@PathVariable("ids") List<Long> teamIds) {
        restPartyService.deleteTeams(teamIds);

        return ResponseEntity.noContent().build();
    }


    /*
     * Team members
     * */
    @RequestMapping(value = "/teams/{teamId}/members", method = RequestMethod.GET)
    @ResponseBody
    @DynamicFilterExpression("*, -teams")
    public ResponseEntity<PagedResources<Resource>> findTeamMembers(@PathVariable("teamId") long teamId, Pageable pageable) {

        Page<User> users = restPartyService.findAllTeamMembers(teamId, pageable);
        PagedResources<Resource> res = toPagedResourcesWithRel(users, "members");
        return ResponseEntity.ok(res);

    }

    @RequestMapping(value = "/teams/{teamId}/members", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<Void> addMembersToTeam(@PathVariable("teamId") long teamId, @RequestParam(value = "userIds") List<Long> userIds){

        restPartyService.addMembersToTeam(teamId, userIds);
        return ResponseEntity.noContent().build();
    }

    @RequestMapping(value = "/teams/{teamId}/members", method = RequestMethod.DELETE)
    @ResponseBody
    public ResponseEntity<Void> removeMembersFromTeam(@PathVariable("teamId") long teamId, @RequestParam(value = "userIds") List<Long> userIds) {

        restPartyService.removeMembersFromTeam(teamId, userIds);
        return ResponseEntity.noContent().build();

    }

    private void validatePostTeam(TeamDto teamDto) throws BindException {
        List<Errors> errors = new ArrayList<>();

        BindingResult validation = new BeanPropertyBindingResult(teamDto, "post-team");
        partyPostValidator.validate(teamDto, validation);

        if (validation.hasErrors()) {
            errors.add(validation);
        }

        ErrorHandlerHelper.throwIfError(teamDto, errors, "post-team");
    }

    private void validatePatchTeam(TeamDto patch) throws BindException {
        List<Errors> errors = new ArrayList<>();

        BindingResult validation = new BeanPropertyBindingResult(patch, "patch-team");
        partyPatchValidator.validate(patch, validation);

        if (validation.hasErrors()) {
            errors.add(validation);
        }

        ErrorHandlerHelper.throwIfError(patch, errors, "patch-team");
    }
}
