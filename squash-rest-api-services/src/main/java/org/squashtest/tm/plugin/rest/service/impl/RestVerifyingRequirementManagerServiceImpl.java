/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.plugin.rest.service.RestVerifyingRequirementManagerService;
import org.squashtest.tm.service.requirement.VerifiedRequirementsManagerService;

import javax.inject.Inject;
import java.util.List;

@Service
@Transactional
public class RestVerifyingRequirementManagerServiceImpl implements RestVerifyingRequirementManagerService {

    @Inject
    private VerifiedRequirementsManagerService verifiedRequirementsManagerService;

    @Override
    public void linkRequirementsToTestCase(List<Long> requirementIds, Long TestCaseId) {
        verifiedRequirementsManagerService.addVerifiedRequirementsToTestCase(requirementIds, TestCaseId);
    }

    @Override
    public void unlinkRequirementsFromTestCase(List<Long> requirementIds, Long TestCaseId) {
        verifiedRequirementsManagerService.removeVerifiedRequirementFromTestCase(requirementIds,TestCaseId);
    }
}
