/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.jackson.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.squashtest.tm.domain.testcase.Dataset;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.plugin.rest.jackson.deserializer.CallTestStepDeserializer;

/**
 * Created by jthebault on 14/06/2017.
 */
@JsonTypeName("call-step")
@JsonDeserialize(using = CallTestStepDeserializer.class)
public class CalledTestStepDto extends TestStepDto {

    @JsonProperty("delegate_parameter_values")
    private boolean delegateParameterValues;

    @JsonProperty("called_test_case")
    private TestCase calledTestCase;

    @JsonProperty("called_dataset")
    private Dataset calledDataset;

    public boolean isDelegateParameterValues() {
        return delegateParameterValues;
    }

    public void setDelegateParameterValues(boolean delegateParameterValues) {
        this.delegateParameterValues = delegateParameterValues;
    }

    public TestCase getCalledTestCase() {
        return calledTestCase;
    }

    public void setCalledTestCase(TestCase calledTestCase) {
        this.calledTestCase = calledTestCase;
    }

    public Dataset getCalledDataset() {
        return calledDataset;
    }

    public void setCalledDataset(Dataset calledDataset) {
        this.calledDataset = calledDataset;
    }

    @Override
    public void accept(TestStepDtoVisitor testStepDtoVisitor) {
        testStepDtoVisitor.visit(this);
    }
}
