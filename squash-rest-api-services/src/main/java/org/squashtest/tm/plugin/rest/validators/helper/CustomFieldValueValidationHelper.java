/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.validators.helper;

import org.apache.commons.lang3.StringUtils;
import org.springframework.validation.Errors;
import org.squashtest.tm.domain.customfield.BindableEntity;
import org.squashtest.tm.domain.customfield.CustomField;
import org.squashtest.tm.domain.customfield.CustomFieldOption;
import org.squashtest.tm.domain.customfield.CustomFieldValue;
import org.squashtest.tm.domain.customfield.CustomFieldVisitor;
import org.squashtest.tm.domain.customfield.InputType;
import org.squashtest.tm.domain.customfield.MultiSelectField;
import org.squashtest.tm.domain.customfield.NumericField;
import org.squashtest.tm.domain.customfield.RawValue;
import org.squashtest.tm.domain.customfield.RichTextField;
import org.squashtest.tm.domain.customfield.SingleSelectField;
import org.squashtest.tm.plugin.rest.jackson.model.CustomFieldValueDto;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class CustomFieldValueValidationHelper {

    private static final DateFormat DATE_TESTER;

    public static final String CUSTOM_FIELDS = "customFields";
    public static final String INVALID_VALUE = "invalid value";
    public static final String TRUE = "true";
    public static final String FALSE = "false";

    static {
        DATE_TESTER = new SimpleDateFormat("yyyy-MM-dd");
        DATE_TESTER.setLenient(false);
    }


    private BindableEntity bindableEntity;

    private Long projectId;

    private List<CustomField> customFields;

    private List<CustomFieldValueDto> dtos;

    private Errors errors;

    public CustomFieldValueValidationHelper() {
        super();
    }


    public CustomFieldValueValidationHelper(BindableEntity bindableEntity, Long projectId, List<CustomField> customFields, List<CustomFieldValueDto> dtos, Errors errors) {
        this.bindableEntity = bindableEntity;
        this.projectId = projectId;
        this.customFields = customFields;
        this.dtos = dtos;
        this.errors = errors;
    }

    public void setBindableEntity(BindableEntity bindableEntity) {
        this.bindableEntity = bindableEntity;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    public void setCustomFields(List<CustomField> customFields) {
        this.customFields = customFields;
    }

    public void setDtos(List<CustomFieldValueDto> dtos) {
        this.dtos = dtos;
    }

    public void setErrors(Errors errors) {
        this.errors = errors;
    }


    public static List<CustomField> collectCustomFields(List<CustomFieldValue> values) {
        List<CustomField> customFields = new ArrayList<>();
        for (CustomFieldValue cfv : values) {
            customFields.add(cfv.getCustomField());
        }
        return customFields;
    }

    public void validate() {
        checkCufCode();
        checkCufValue();
    }

    private void checkCufCode() {

        for (CustomFieldValueDto dto : dtos) {
            String code = dto.getCode();

            if (StringUtils.isBlank(code)) {
                errors.rejectValue(CUSTOM_FIELDS, "Mandatory custom field code", "The code is mandatory to post custom field values.");
            }

            CustomField customField = locateByCode(customFields, code);

            if (customField == null) {
                String message = String.format("The custom field with code : %s is not bound for entities of type %s in project %d", code, bindableEntity, projectId);
                errors.rejectValue(CUSTOM_FIELDS, "Not bound custom field", message);
            }
        }

    }

    private void checkCufValue() {

        for (CustomFieldValueDto dto : dtos) {

            String code = dto.getCode();
            RawValue value = dto.getValue();

            CustomField customField = locateByCode(customFields, code);

            if (customField == null) {
                // this case was covered already by #checkCufCode
                continue;
            }

            if (!customField.isOptional() && value != null && value.isEmpty()) {
                errors.rejectValue(CUSTOM_FIELDS, "Mandatory custom field value", "The value is mandatory to post custom field values. If you want to leave the default value for this field, just omit the whole custom field object from json");
            }

            if (customField != null) {
                CustomFieldVisitor visitor = new CustomFieldValidationVisitor(errors, dto);

                customField.accept(visitor);
            }

        }


    }

    private CustomField locateByCode(List<CustomField> customFields, String code) {
        for (CustomField cf : customFields) {
            if (cf.getCode().equals(code)) {
                return cf;
            }
        }
        return null;
    }


    private static final class CustomFieldValidationVisitor implements CustomFieldVisitor {

        private Errors errors;
        private CustomFieldValueDto dto;
        private RawValue value;

        public CustomFieldValidationVisitor(Errors errors, CustomFieldValueDto dto) {
            this.errors = errors;
            this.dto = dto;
            this.value = dto.getValue();
        }

        @Override
        public void visit(SingleSelectField singleSelectField) {
            checkOption(singleSelectField);
            checkArityForSingleValueCuf(singleSelectField);
        }

        @Override
        public void visit(CustomField customField) {
            checkArityForSingleValueCuf(customField);
            // bonus point for weak domain conception : extra checks
            // for boolean and dates
            if (customField.getInputType() == InputType.DATE_PICKER) {
                checkDate(customField);
            }
            if (customField.getInputType() == InputType.CHECKBOX) {
                checkBoolean(customField);
            }

            // Issue 6883, there is no accept method in NumericField class in SquashTM 1.16, so the
            // visit(NumericField numericField) method will not be called
            if(customField.getInputType() == InputType.NUMERIC) {
                visit((NumericField) customField);
            }

        }

        @Override
        public void visit(RichTextField richTextField) {
            checkArityForSingleValueCuf(richTextField);

        }

        @Override
        public void visit(MultiSelectField multiSelectField) {
            checkArityForMultiValueCuf(multiSelectField);
        }


        @Override
        public void visit(NumericField numericField) {
            checkArityForSingleValueCuf(numericField);
            String numberAsString = value.getValue();
            try {
                new BigDecimal(numberAsString);
            } catch (NumberFormatException nfe) {
                String message = String.format("The cuf %s identified by code %s is a numeric value cuf. The value provided is not convertible to number", numericField.getLabel(), numericField.getCode(), value.getValue());
                errors.rejectValue(CUSTOM_FIELDS, "custom field numeric value", message);
            }

        }

        private void checkArityForMultiValueCuf(MultiSelectField customField) {
            if (value != null && (value.getValues() == null || value.getValues().isEmpty())) {
                String message = String.format("The cuf %s identified by code %s is a multi value cuf. You provided standard attribute with value : %s. Please provide json array.", customField.getLabel(), customField.getCode(), value.getValue());
                errors.rejectValue(CUSTOM_FIELDS, "custom field multi value", message);
            }
        }

        private void checkArityForSingleValueCuf(CustomField customField) {
            if (value == null || value.getValues() == null || value.getValues().isEmpty()) {
                return;
            }
            String message = String.format("The cuf %s identified by code %s is a single value cuf. You provided a json array with values : %s. Please provide a standard attribute not an array.", customField.getLabel(), customField.getCode(), value.getValues());
            errors.rejectValue(CUSTOM_FIELDS, "custom field single value", message);
        }

        private void checkOption(SingleSelectField singleSelectField) {
            List<CustomFieldOption> options = singleSelectField.getOptions();
            boolean valid = false;
            for (CustomFieldOption option : options) {
                if (option.getLabel().equals(dto.getValue().getValue())) {
                    valid = true;
                }
            }
            if (!valid) {
                errors.rejectValue(CUSTOM_FIELDS, "Mandatory custom field value", "Unknown value for list : " + singleSelectField.getCode() + " . Custom field option are identified by label.");
            }
        }


        private void checkDate(CustomField cuf) {
            if (value == null || value.getValue().isEmpty()) {
                return;
            }
            String strDate = value.getValue();
            try {
                DATE_TESTER.parse(strDate);
            } catch (ParseException e) {
                errors.rejectValue(CUSTOM_FIELDS, INVALID_VALUE,
                        "The field '" + cuf.getLabel() + "' identified by label '" + cuf.getLabel() + "' " +
                                "accepts dates using format 'yyyy-mm-dd' only");
            }
        }

        private void checkBoolean(CustomField cuf) {
            if (value == null) {
                return;
            }
            String strBool = value.getValue().toLowerCase();
            boolean check = strBool.equals(TRUE) || strBool.equals(FALSE);
            if (!check) {
                errors.rejectValue(CUSTOM_FIELDS, INVALID_VALUE,
                        "The field '" + cuf.getLabel() + "' identified by label '" + cuf.getLabel() + "' " +
                                "accepts boolean 'true' or 'false'");
            }
        }
    }

    ;


}
