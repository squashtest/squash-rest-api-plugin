/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.validators;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import org.squashtest.tm.domain.project.GenericProject;
import org.squashtest.tm.plugin.rest.jackson.model.GenericProjectCopyParameterDto;
import org.squashtest.tm.plugin.rest.jackson.model.GenericProjectDto;
import org.squashtest.tm.plugin.rest.jackson.model.GenericProjectDtoVisitor;
import org.squashtest.tm.plugin.rest.jackson.model.ProjectDto;
import org.squashtest.tm.plugin.rest.jackson.model.ProjectTemplateDto;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Component
public class GenericProjectPostValidator implements Validator {

    @PersistenceContext
    protected EntityManager entityManager;

    @Override
    public boolean supports(Class<?> clazz) {
        return GenericProjectDto.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, final Errors errors) {

        GenericProjectDto genericProjectDto = (GenericProjectDto) target;

        if (genericProjectDto.getId() != null) {
            errors.rejectValue("id", "generated value", "This attribute is generated by database and should not be provided. If you want to update an existing generic project, please do a patch request to the generic project id. ");
        }

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "required", "This attribute can't be empty");

        GenericProjectDtoVisitor visitor = new GenericProjectDtoVisitor() {
            @Override
            public void visit(ProjectDto projectDto) {
                if (projectDto.getTemplateId() != null) {
                    checkAndAssignValues(projectDto);
                    checkIfEntityExist(errors, projectDto.getTemplateId());
                }
            }

            @Override
            public void visit(ProjectTemplateDto projectTemplateDto) {
                if (projectTemplateDto.getProjectId() != null) {
                    checkAndAssignValues(projectTemplateDto);
                    checkIfEntityExist(errors, projectTemplateDto.getProjectId());
                }
            }
        };

        genericProjectDto.accept(visitor);
    }

    private void checkAndAssignValues(GenericProjectDto genericProjectDto) {
        if (genericProjectDto.getParams() == null) {
            genericProjectDto.setParams(new GenericProjectCopyParameterDto());
        }
    }

    private void checkIfEntityExist(Errors errors, Long id) {
        GenericProject genericProject = entityManager.find(GenericProject.class, id);
        if (genericProject == null) {
            String message = String.format("No project of project template known for id %d", id);
            errors.rejectValue("id", "invalid id", message);
        }
    }
}
