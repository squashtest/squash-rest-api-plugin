/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.configuration;

import com.fasterxml.jackson.databind.Module.SetupContext;
import org.squashtest.csp.core.bugtracker.domain.BugTracker;
import org.squashtest.tm.domain.Identified;
import org.squashtest.tm.domain.attachment.Attachment;
import org.squashtest.tm.domain.audit.AuditableSupport;
import org.squashtest.tm.domain.bugtracker.Issue;
import org.squashtest.tm.domain.campaign.ActualTimePeriod;
import org.squashtest.tm.domain.campaign.Campaign;
import org.squashtest.tm.domain.campaign.CampaignFolder;
import org.squashtest.tm.domain.campaign.CampaignTestPlanItem;
import org.squashtest.tm.domain.campaign.Iteration;
import org.squashtest.tm.domain.campaign.IterationTestPlanItem;
import org.squashtest.tm.domain.campaign.ScheduledTimePeriod;
import org.squashtest.tm.domain.campaign.TestSuite;
import org.squashtest.tm.domain.execution.Execution;
import org.squashtest.tm.domain.execution.ExecutionStep;
import org.squashtest.tm.domain.execution.KeywordExecution;
import org.squashtest.tm.domain.execution.ScriptedExecution;
import org.squashtest.tm.domain.infolist.DenormalizedNature;
import org.squashtest.tm.domain.infolist.DenormalizedType;
import org.squashtest.tm.domain.library.GenericLibraryNode;
import org.squashtest.tm.domain.project.GenericProject;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.domain.project.ProjectTemplate;
import org.squashtest.tm.domain.requirement.Requirement;
import org.squashtest.tm.domain.requirement.RequirementFolder;
import org.squashtest.tm.domain.requirement.RequirementVersion;
import org.squashtest.tm.domain.resource.SimpleResource;
import org.squashtest.tm.domain.testautomation.AutomatedExecutionExtender;
import org.squashtest.tm.domain.testautomation.AutomatedTest;
import org.squashtest.tm.domain.testautomation.TestAutomationServer;
import org.squashtest.tm.domain.testcase.ActionTestStep;
import org.squashtest.tm.domain.testcase.CallTestStep;
import org.squashtest.tm.domain.testcase.Dataset;
import org.squashtest.tm.domain.testcase.KeywordTestCase;
import org.squashtest.tm.domain.testcase.KeywordTestStep;
import org.squashtest.tm.domain.testcase.Parameter;
import org.squashtest.tm.domain.testcase.ScriptedTestCase;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.domain.testcase.TestCaseFolder;
import org.squashtest.tm.domain.testcase.TestStep;
import org.squashtest.tm.domain.users.Party;
import org.squashtest.tm.domain.users.Team;
import org.squashtest.tm.domain.users.User;
import org.squashtest.tm.plugin.rest.core.configuration.SquashRestApiJacksonModuleConfigurer;
import org.squashtest.tm.plugin.rest.jackson.mixin.RestActionStepMixin;
import org.squashtest.tm.plugin.rest.jackson.mixin.RestActualPeriodTimeMixin;
import org.squashtest.tm.plugin.rest.jackson.mixin.RestAttachmentMixin;
import org.squashtest.tm.plugin.rest.jackson.mixin.RestAuditableSupportMixin;
import org.squashtest.tm.plugin.rest.jackson.mixin.RestAutomatedExecutionExtenderMixin;
import org.squashtest.tm.plugin.rest.jackson.mixin.RestAutomatedTestMixin;
import org.squashtest.tm.plugin.rest.jackson.mixin.RestBugTrackerMixin;
import org.squashtest.tm.plugin.rest.jackson.mixin.RestCallStepMixin;
import org.squashtest.tm.plugin.rest.jackson.mixin.RestCampaignFolderMixin;
import org.squashtest.tm.plugin.rest.jackson.mixin.RestCampaignMixin;
import org.squashtest.tm.plugin.rest.jackson.mixin.RestCampaignTestPlanItemMixin;
import org.squashtest.tm.plugin.rest.jackson.mixin.RestDatasetMixin;
import org.squashtest.tm.plugin.rest.jackson.mixin.RestDenormalizedInfoListItemMixin;
import org.squashtest.tm.plugin.rest.jackson.mixin.RestExecutionMixin;
import org.squashtest.tm.plugin.rest.jackson.mixin.RestExecutionStepMixin;
import org.squashtest.tm.plugin.rest.jackson.mixin.RestGenericLibraryNodeMixin;
import org.squashtest.tm.plugin.rest.jackson.mixin.RestGenericProjectMixin;
import org.squashtest.tm.plugin.rest.jackson.mixin.RestIdentifiedMixin;
import org.squashtest.tm.plugin.rest.jackson.mixin.RestIssueMixin;
import org.squashtest.tm.plugin.rest.jackson.mixin.RestIterationMixin;
import org.squashtest.tm.plugin.rest.jackson.mixin.RestIterationTestPlanItemMixin;
import org.squashtest.tm.plugin.rest.jackson.mixin.RestKeywordExecutionMixin;
import org.squashtest.tm.plugin.rest.jackson.mixin.RestKeywordStepMixin;
import org.squashtest.tm.plugin.rest.jackson.mixin.RestKeywordTestCaseMixin;
import org.squashtest.tm.plugin.rest.jackson.mixin.RestParameterMixin;
import org.squashtest.tm.plugin.rest.jackson.mixin.RestPartyMixin;
import org.squashtest.tm.plugin.rest.jackson.mixin.RestProjectMixin;
import org.squashtest.tm.plugin.rest.jackson.mixin.RestProjectTemplateMixin;
import org.squashtest.tm.plugin.rest.jackson.mixin.RestRequirementFolderMixin;
import org.squashtest.tm.plugin.rest.jackson.mixin.RestRequirementMixin;
import org.squashtest.tm.plugin.rest.jackson.mixin.RestRequirementVersionMixin;
import org.squashtest.tm.plugin.rest.jackson.mixin.RestScheduledTimePeriodMixin;
import org.squashtest.tm.plugin.rest.jackson.mixin.RestScriptedExecutionMixin;
import org.squashtest.tm.plugin.rest.jackson.mixin.RestScriptedTestCaseMixin;
import org.squashtest.tm.plugin.rest.jackson.mixin.RestSimpleResourceMixin;
import org.squashtest.tm.plugin.rest.jackson.mixin.RestTeamMixin;
import org.squashtest.tm.plugin.rest.jackson.mixin.RestTestAutomationServerMixin;
import org.squashtest.tm.plugin.rest.jackson.mixin.RestTestCaseFolderMixin;
import org.squashtest.tm.plugin.rest.jackson.mixin.RestTestCaseMixin;
import org.squashtest.tm.plugin.rest.jackson.mixin.RestTestStepMixin;
import org.squashtest.tm.plugin.rest.jackson.mixin.RestTestSuiteMixin;
import org.squashtest.tm.plugin.rest.jackson.mixin.RestUserMixin;
import org.squashtest.tm.plugin.rest.jackson.mixin.dto.RestParentMixin;
import org.squashtest.tm.plugin.rest.jackson.model.ParentEntity;

public class SquashRestApiJacksonModuleConfigurerImpl implements SquashRestApiJacksonModuleConfigurer {

    @Override
    public void setupModule(SetupContext context) {

		/*
         * mixins
		 */
        context.setMixInAnnotations(Identified.class, RestIdentifiedMixin.class);
        context.setMixInAnnotations(AuditableSupport.class, RestAuditableSupportMixin.class);

        context.setMixInAnnotations(GenericProject.class, RestGenericProjectMixin.class);
        context.setMixInAnnotations(Project.class, RestProjectMixin.class);
        context.setMixInAnnotations(ProjectTemplate.class, RestProjectTemplateMixin.class);

        context.setMixInAnnotations(Party.class, RestPartyMixin.class);
        context.setMixInAnnotations(User.class, RestUserMixin.class);
        context.setMixInAnnotations(Team.class, RestTeamMixin.class);

        context.setMixInAnnotations(GenericLibraryNode.class, RestGenericLibraryNodeMixin.class);
        context.setMixInAnnotations(TestCaseFolder.class, RestTestCaseFolderMixin.class);
        context.setMixInAnnotations(TestCase.class, RestTestCaseMixin.class);
        context.setMixInAnnotations(ScriptedTestCase.class, RestScriptedTestCaseMixin.class);
        context.setMixInAnnotations(KeywordTestCase.class, RestKeywordTestCaseMixin.class);

        context.setMixInAnnotations(TestStep.class, RestTestStepMixin.class);
        context.setMixInAnnotations(ActionTestStep.class, RestActionStepMixin.class);
        context.setMixInAnnotations(CallTestStep.class, RestCallStepMixin.class);
        context.setMixInAnnotations(KeywordTestStep.class, RestKeywordStepMixin.class);

        context.setMixInAnnotations(Parameter.class, RestParameterMixin.class);
        context.setMixInAnnotations(Dataset.class, RestDatasetMixin.class);

        context.setMixInAnnotations(RequirementVersion.class, RestRequirementVersionMixin.class);
        context.setMixInAnnotations(Requirement.class, RestRequirementMixin.class);
        context.setMixInAnnotations(RequirementFolder.class, RestRequirementFolderMixin.class);
        context.setMixInAnnotations(SimpleResource.class, RestSimpleResourceMixin.class);

        context.setMixInAnnotations(Campaign.class, RestCampaignMixin.class);
        context.setMixInAnnotations(CampaignFolder.class, RestCampaignFolderMixin.class);
        context.setMixInAnnotations(Iteration.class, RestIterationMixin.class);
        context.setMixInAnnotations(TestSuite.class, RestTestSuiteMixin.class);
        context.setMixInAnnotations(IterationTestPlanItem.class, RestIterationTestPlanItemMixin.class);
        context.setMixInAnnotations(CampaignTestPlanItem.class, RestCampaignTestPlanItemMixin.class);
        context.setMixInAnnotations(ScheduledTimePeriod.class, RestScheduledTimePeriodMixin.class);
        context.setMixInAnnotations(ActualTimePeriod.class, RestActualPeriodTimeMixin.class);
        context.setMixInAnnotations(Execution.class, RestExecutionMixin.class);
        context.setMixInAnnotations(ScriptedExecution.class, RestScriptedExecutionMixin.class);
        context.setMixInAnnotations(KeywordExecution.class, RestKeywordExecutionMixin.class);
        context.setMixInAnnotations(ExecutionStep.class, RestExecutionStepMixin.class);
        context.setMixInAnnotations(Issue.class, RestIssueMixin.class);
        context.setMixInAnnotations(BugTracker.class, RestBugTrackerMixin.class);
        context.setMixInAnnotations(DenormalizedNature.class, RestDenormalizedInfoListItemMixin.class);
        context.setMixInAnnotations(DenormalizedType.class, RestDenormalizedInfoListItemMixin.class);

        context.setMixInAnnotations(AutomatedTest.class, RestAutomatedTestMixin.class);
        context.setMixInAnnotations(AutomatedExecutionExtender.class, RestAutomatedExecutionExtenderMixin.class);
        context.setMixInAnnotations(TestAutomationServer.class, RestTestAutomationServerMixin.class);

        context.setMixInAnnotations(Attachment.class, RestAttachmentMixin.class);


        /*
        * DTO mixin
		* */
        // TODO : no need for a mixin there, ParentEntity is already a DTO
        // we can annotate it already
        context.setMixInAnnotations(ParentEntity.class, RestParentMixin.class);

    }

}
