/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.controller;

import org.apache.commons.io.IOUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.PagedResources;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.squashtest.tm.domain.attachment.Attachment;
import org.squashtest.tm.domain.attachment.AttachmentHolder;
import org.squashtest.tm.plugin.rest.core.jackson.DynamicFilterExpression;
import org.squashtest.tm.plugin.rest.core.web.BaseRestController;
import org.squashtest.tm.plugin.rest.core.web.EntityGetter;
import org.squashtest.tm.plugin.rest.core.web.RestApiController;
import org.squashtest.tm.plugin.rest.core.web.UseDefaultRestApiConfiguration;
import org.squashtest.tm.plugin.rest.service.RestAttachmentService;

import javax.inject.Inject;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestApiController(Attachment.class)
@UseDefaultRestApiConfiguration
public class RestAttachmentController extends BaseRestController {

	@Inject
	private RestAttachmentService restAttachmentService;

	@RequestMapping("/attachments/{id}")
	@EntityGetter
	@ResponseBody
	@DynamicFilterExpression("*")
	public ResponseEntity<Resource<Attachment>> findAttachment(@PathVariable("id") long id) throws IOException{

		Attachment attachment = restAttachmentService.findById(id);
		Resource<Attachment> res = new Resource<>(attachment);
		res.add(createSelfLink(attachment));
		res.add(linkService.fromBasePath(linkTo(methodOn(RestAttachmentController.class).downloadAttachment(id))).withRel("content"));

		return ResponseEntity.ok(res);
	}

	@RequestMapping(value = "/attachments/{id}", method = RequestMethod.PATCH)
	@EntityGetter
	@ResponseBody
	@DynamicFilterExpression("*")
	public ResponseEntity<Resource<Attachment>> renameAttachment(@PathVariable("id") long id, @RequestParam("name") String name) throws IOException{

		Attachment attachment = restAttachmentService.renameAttachment(id, name);
		Resource<Attachment> res = new Resource<>(attachment);
		res.add(createSelfLink(attachment));
		res.add(linkService.fromBasePath(linkTo(methodOn(RestAttachmentController.class).downloadAttachment(id))).withRel("content"));

		return ResponseEntity.ok(res);
	}

	@RequestMapping(value = "/attachments/{id}/content", produces = "application/octet-stream")
	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public ResponseEntity downloadAttachment(@PathVariable("id") long id) throws IOException{

		Attachment attachment = restAttachmentService.findById(id);
		InputStream in = attachment.getContent().getStream();

		byte[] out = IOUtils.toByteArray(in);
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Disposition", "attachment; filename=" + attachment.getName().replace(" ", "_"));
		responseHeaders.add("Content-Type", "application/octet-stream");

		return new ResponseEntity(out, responseHeaders, HttpStatus.OK);

}

	@RequestMapping(value = "/{owner}/{ownerId}/attachments", method = RequestMethod.GET)
	@ResponseBody
	@DynamicFilterExpression("*, -type,-size, -added_on")
	public ResponseEntity<PagedResources<Resource>> findAttachmentsByOwner(@PathVariable("owner") String owner, @PathVariable("ownerId") long ownerId, Pageable pageable) {

		AttachmentHolder holder = restAttachmentService.findHolder(owner, ownerId);
		Page<Attachment> attachments = restAttachmentService.findPagedAttachments(holder, pageable);
		PagedResources<Resource> res = toPagedResources(attachments);

		return ResponseEntity.ok(res);

	}

	@RequestMapping(value = "/{owner}/{ownerId}/attachments", method = RequestMethod.POST)
	@ResponseBody
	@DynamicFilterExpression("*, -type,-size, -added_on")
	@SuppressWarnings("unchecked")
	public ResponseEntity<Resources> uploadAttachments(@PathVariable("owner") String owner,
																	  @PathVariable("ownerId") long ownerId,
																	  @RequestParam("files") List<MultipartFile> files) {

		AttachmentHolder holder = restAttachmentService.findHolderWithPermission(owner, ownerId, "CREATE");
		List<Attachment> attachments = restAttachmentService.addAttachments(holder, files);

		List<Resource> resources = attachments.stream().map(attachment -> {
			Resource<Attachment> res = new Resource<>(attachment);
			res.add(createSelfLink(attachment));
			return res;
		}).collect(Collectors.toList());

		Resources res = new Resources<>(resources);

		return ResponseEntity.status(HttpStatus.CREATED).body(res);
	}

	@RequestMapping(value = "/{owner}/{ownerId}/attachments", method = RequestMethod.DELETE)
	@ResponseBody
	@DynamicFilterExpression("*, -type,-size, -added_on")
	@SuppressWarnings("unchecked")
	public ResponseEntity<Void> deleteAttachments(@PathVariable("owner") String owner,
												  @PathVariable("ownerId") long ownerId,
												  @RequestParam(value = "attachmentIds") List<Long> attachmentIds) throws IOException{

		AttachmentHolder holder = restAttachmentService.findHolderWithPermission(owner, ownerId, "DELETE");
		restAttachmentService.deleteAttachments(holder, attachmentIds);
		return ResponseEntity.noContent().build();
	}

}
