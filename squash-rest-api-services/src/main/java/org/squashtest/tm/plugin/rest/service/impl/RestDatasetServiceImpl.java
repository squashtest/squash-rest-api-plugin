/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.service.impl;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.domain.testcase.Dataset;
import org.squashtest.tm.domain.testcase.DatasetParamValue;
import org.squashtest.tm.domain.testcase.Parameter;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.plugin.rest.jackson.model.DatasetDto;
import org.squashtest.tm.plugin.rest.jackson.model.DatasetParamValueDto;
import org.squashtest.tm.plugin.rest.repository.RestDatasetRepository;
import org.squashtest.tm.plugin.rest.service.RestDatasetService;
import org.squashtest.tm.service.internal.repository.DatasetDao;
import org.squashtest.tm.service.internal.repository.hibernate.ParameterDaoImpl;
import org.squashtest.tm.service.security.PermissionEvaluationService;
import org.squashtest.tm.service.security.PermissionsUtils;
import org.squashtest.tm.service.testcase.DatasetModificationService;
import org.squashtest.tm.service.testcase.ParameterFinder;

import javax.inject.Inject;
import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * Created by jthebault on 30/05/2017.
 */
@Service
@Transactional
public class RestDatasetServiceImpl implements RestDatasetService {

    @Inject
    private RestDatasetRepository restDatasetRepository;

    @Inject
    private PermissionEvaluationService permissionEvaluationService;

    @Inject
    private DatasetModificationService datasetModificationService;

    @Inject
    private ParameterFinder parameterFinder;

    @Inject
    private DatasetDao datasetDao;
    //@inject CustomParameterdao mais erreur "available: expected single matching bean but found 2: parameterDaoImpl,parameterDao"
    @Inject
    private ParameterDaoImpl parameterDao;

    @Override
    @Transactional(readOnly=true)
    public Dataset getOne(long id) throws AccessDeniedException {
        Dataset dataset = restDatasetRepository.retrieveById(id);
        if(dataset == null){
            throw new EntityNotFoundException("The dataset with id : " + id + " do not exist.");
        }
        //checking permission on the test case not the param directly
        TestCase testCase = dataset.getTestCase();
        if (permissionEvaluationService.canRead(testCase)){
            return dataset;
        } else {
            throw new AccessDeniedException("Access denied");
        }
    }

    @Override
    @Transactional(readOnly=true)
    @PreAuthorize("@apiSecurity.hasPermission(#testCaseId,'org.squashtest.tm.domain.testcase.TestCase' , 'READ')")
    public Page<Dataset> findAllByTestCaseId(long testCaseId, Pageable pageable) {
            return restDatasetRepository.findByTestCase_Id(testCaseId,pageable);
    }

    @Override
    @PreAuthorize("@apiSecurity.hasPermission(#datasetDto.getReferencedTestCase().getId(),'org.squashtest.tm.domain.testcase.TestCase' , 'WRITE')")
    public Dataset addDataset(DatasetDto datasetDto, List<Parameter> listAllParamByIdTc) {

        // listAllParamByIdTc: list of all delegated parameters from referenced test case
        Dataset dataset = new Dataset();
        List<DatasetParamValueDto> listParamValueDto =  new ArrayList<>(datasetDto.getValueDtos());
        // Dataset and DatasetParamValue
        String newParamValue;
        dataset.setName(datasetDto.getName());
        Iterator<Parameter> it = listAllParamByIdTc.iterator();
        while(it.hasNext()) {
            newParamValue = "";
            Parameter paramByIdTc = it.next();
            // initialize paramValue with DTO's value or null if no value
            for(DatasetParamValueDto paramValueDto:listParamValueDto) {
                if(paramValueDto.getParameterId().equals(paramByIdTc.getId())) {
                    newParamValue = paramValueDto.getValue();
                }
            }
            DatasetParamValue dpv = new DatasetParamValue(paramByIdTc, dataset, newParamValue);
            dataset.addParameterValue(dpv);
        }

        datasetModificationService.persist(dataset, datasetDto.getReferencedTestCase().getId());

        return dataset;
    }

    @Override
    public Dataset modifyDataset(DatasetDto datasetDto, Long datasetId) {

        Dataset dataset = datasetDao.getOne(datasetId);
        PermissionsUtils.checkPermission(permissionEvaluationService, Collections.singletonList(dataset.getTestCase().getId()), "WRITE", TestCase.class.getName());
        if (datasetDto.isHasName()) {
            dataset.setName(datasetDto.getName());
        }
        List<DatasetParamValueDto> listParamValueDto =  new ArrayList<DatasetParamValueDto>(datasetDto.getValueDtos());
        if(datasetDto.isHasParamValue()){
            List<DatasetParamValue> listParamValueById = restDatasetRepository.findParamValueByIdDataset(datasetId);
            Iterator<DatasetParamValue> it = listParamValueById.iterator();
            while(it.hasNext()){
                DatasetParamValue paramValueById = it.next();
                for (DatasetParamValueDto elt:listParamValueDto) {
                    if (elt.getParameterId().equals(paramValueById.getParameter().getId())) {
                        paramValueById.setParamValue(elt.getValue());
                    }
                }

            }
        }

    return dataset;
    }

    @Override
    public void deleteDataset(Long datasetId) {
        Dataset dataset = datasetDao.getOne(datasetId);
        PermissionsUtils.checkPermission(permissionEvaluationService, Collections.singletonList(dataset.getTestCase().getId()), "WRITE", TestCase.class.getName());
        datasetModificationService.removeById(datasetId);
    }

    @Override
    public List<Parameter> findAllParametersByTc(Long id) {
        return   parameterDao.findAllParametersByTestCase(id);
    }

}
