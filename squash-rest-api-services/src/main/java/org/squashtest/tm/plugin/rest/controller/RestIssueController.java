/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.controller;

import org.springframework.hateoas.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.squashtest.tm.domain.bugtracker.Issue;
import org.squashtest.tm.plugin.rest.core.jackson.DynamicFilterExpression;
import org.squashtest.tm.plugin.rest.core.web.BaseRestController;
import org.squashtest.tm.plugin.rest.core.web.EntityGetter;
import org.squashtest.tm.plugin.rest.core.web.RestApiController;
import org.squashtest.tm.plugin.rest.core.web.UseDefaultRestApiConfiguration;
import org.squashtest.tm.plugin.rest.jackson.model.IssueDto;
import org.squashtest.tm.plugin.rest.service.RestIssueService;

import javax.inject.Inject;

/**
 * Created by jthebault on 16/06/2017.
 */
@RestApiController(Issue.class)
@UseDefaultRestApiConfiguration
public class RestIssueController extends BaseRestController{


    @Inject
    private RestIssueService  restIssueService;


    @RequestMapping(value = "/issues/{id}", method = RequestMethod.GET)
    @EntityGetter
    @ResponseBody
    @DynamicFilterExpression("*")
    public ResponseEntity<Resource<Issue>> findCampaign(@PathVariable("id") long id){

        throw new UnsupportedOperationException("This service is not available yet.");

        /* == This service is not available in the version 1.0.0 ==

        Issue issue = restIssueService.getOne(id);

        Resource<Issue> res = toResource(issue);

        return ResponseEntity.ok(res);

        */
    }


    /*AMK: Attach an issue to execution*/
    @RequestMapping(value ="/executions/{idExecution}/issues", method = RequestMethod.POST)
    @ResponseBody
    @DynamicFilterExpression("*")
    public  ResponseEntity<Resource<Issue>> attachIssueToExecution(@PathVariable("idExecution") long idExecution, @RequestBody IssueDto issueDto) throws UnsupportedOperationException  {

        //On ne verifie pas si remoteIssue existe dans le projet du bugtracker,

        Issue issue = restIssueService.attachIssue(issueDto,idExecution);

        Resource<Issue> res = toResource(issue);

        return ResponseEntity.status(HttpStatus.CREATED).body(res);
    }


}
























