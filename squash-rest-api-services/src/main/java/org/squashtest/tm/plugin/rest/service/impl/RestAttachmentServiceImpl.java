/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.service.impl;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import org.squashtest.tm.api.security.acls.Roles;
import org.squashtest.tm.domain.EntityReference;
import org.squashtest.tm.domain.attachment.Attachment;
import org.squashtest.tm.domain.attachment.AttachmentHolder;
import org.squashtest.tm.domain.campaign.Campaign;
import org.squashtest.tm.domain.campaign.CampaignFolder;
import org.squashtest.tm.domain.campaign.Iteration;
import org.squashtest.tm.domain.campaign.TestSuite;
import org.squashtest.tm.domain.execution.Execution;
import org.squashtest.tm.domain.execution.ExecutionStep;
import org.squashtest.tm.domain.project.GenericProject;
import org.squashtest.tm.domain.requirement.RequirementFolder;
import org.squashtest.tm.domain.requirement.RequirementVersion;
import org.squashtest.tm.domain.testcase.ActionTestStep;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.domain.testcase.TestCaseFolder;
import org.squashtest.tm.domain.testcase.TestStep;
import org.squashtest.tm.plugin.rest.core.exception.ProgrammingError;
import org.squashtest.tm.plugin.rest.jackson.model.RestUploadedData;
import org.squashtest.tm.plugin.rest.service.RestAttachmentService;
import org.squashtest.tm.plugin.rest.service.helper.AttachmentContentFilterHelper;
import org.squashtest.tm.service.attachment.AttachmentManagerService;
import org.squashtest.tm.service.attachment.RawAttachment;
import org.squashtest.tm.service.internal.display.dto.AttachmentDto;
import org.squashtest.tm.service.security.PermissionEvaluationService;
import org.squashtest.tm.service.security.PermissionsUtils;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class RestAttachmentServiceImpl implements RestAttachmentService {

    @Inject
    private AttachmentManagerService attachmentService;

    @Inject
    private AttachmentContentFilterHelper filterHelper;

    @Inject
    private PermissionEvaluationService permissionService;

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Attachment findById(long id) {
        Attachment attachment = attachmentService.findAttachment(id);
        if (attachment == null) {
            throw new EntityNotFoundException("The attachment with id: " + id + " does not exist.");
        }
        return attachment;
    }

    @Override
    public Page<Attachment> findPagedAttachments(AttachmentHolder attached, Pageable pageable) {
        return attachmentService.findPagedAttachments(attached, pageable);
    }

    @Override
    public Attachment addAttachment(long attachmentListId, RawAttachment upload, EntityReference entityReference) throws IOException {
        if (!filterHelper.isTypeAllowed(upload)) {
            throw new IllegalArgumentException(upload.getName() + " : file type not supported");
        }
        AttachmentDto dto = attachmentService.addAttachment(attachmentListId, upload, entityReference);
        return findById(dto.getId());
    }

    @Override
    public Attachment renameAttachment(long attachmentId, String newName) {

        if (newName != null && !newName.isEmpty()) {
            attachmentService.renameAttachment(attachmentId, newName);
            return findById(attachmentId);
        } else {
            throw new IllegalArgumentException("name can not be empty or null");
        }

    }

    @Override
    public List<Attachment> addAttachments(AttachmentHolder holder, List<MultipartFile> files) {
        long listId = holder.getAttachmentList().getId();
        return files.stream().map(file -> {
            try {
                RestUploadedData data = new RestUploadedData(file.getInputStream(), file.getOriginalFilename(), file.getSize());
                return addAttachment(listId, data, holder.toEntityReference());
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }).collect(Collectors.toList());
    }

    @Override
    public void deleteAttachments(AttachmentHolder holder, List<Long> attachmentIds) throws IOException {
        attachmentService.removeListOfAttachments(holder.getAttachmentList().getId(), attachmentIds, holder.toEntityReference());
    }

    @Override
    public AttachmentHolder findHolder(String owner, long id) {
        return findHolderWithPermission(owner, id, "READ");
    }

    @Override
    public AttachmentHolder findHolderWithPermission(String owner, long id, String permission) {
        switch (owner) {
            case "test-steps":
                PermissionsUtils.checkPermission(permissionService, Collections.singletonList(id), permission, TestStep.class.getName());
                return entityManager.find(ActionTestStep.class, id);
            case "campaigns":
                PermissionsUtils.checkPermission(permissionService, Collections.singletonList(id), permission, Campaign.class.getName());
                return entityManager.find(Campaign.class, id);
            case "campaign-folders":
                PermissionsUtils.checkPermission(permissionService, Collections.singletonList(id), permission, CampaignFolder.class.getName());
                return entityManager.find(CampaignFolder.class, id);
            case "executions":
                PermissionsUtils.checkPermission(permissionService, Collections.singletonList(id), permission, Execution.class.getName());
                return entityManager.find(Execution.class, id);
            case "execution-steps":
                PermissionsUtils.checkPermission(permissionService, Collections.singletonList(id), permission, ExecutionStep.class.getName());
                return entityManager.find(ExecutionStep.class, id);
            case "projects":
                if (!permissionService.hasRole(Roles.ROLE_ADMIN) && !permissionService.hasRole(Roles.ROLE_TM_PROJECT_MANAGER) && !permission.equals("READ")) {
                    throw new AccessDeniedException("Access denied");
                }
                return entityManager.find(GenericProject.class, id);
            case "iterations":
                PermissionsUtils.checkPermission(permissionService, Collections.singletonList(id), permission, Iteration.class.getName());
                return entityManager.find(Iteration.class, id);
            case "requirement-folders":
                PermissionsUtils.checkPermission(permissionService, Collections.singletonList(id), permission, RequirementFolder.class.getName());
                return entityManager.find(RequirementFolder.class, id);
            case "requirement-versions":
                PermissionsUtils.checkPermission(permissionService, Collections.singletonList(id), permission, RequirementVersion.class.getName());
                return entityManager.find(RequirementVersion.class, id);
            case "test-cases":
                PermissionsUtils.checkPermission(permissionService, Collections.singletonList(id), permission, TestCase.class.getName());
                return entityManager.find(TestCase.class, id);
            case "test-case-folders":
                PermissionsUtils.checkPermission(permissionService, Collections.singletonList(id), permission, TestCaseFolder.class.getName());
                return entityManager.find(TestCaseFolder.class, id);
            case "test-suites":
                PermissionsUtils.checkPermission(permissionService, Collections.singletonList(id), permission, TestSuite.class.getName());
                return entityManager.find(TestSuite.class, id);
            default:
                throw new ProgrammingError("the url : " + owner + "/{id}/attachments does not exist, please double check it");
        }
    }
}
