/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.jackson.mixin;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.squashtest.tm.domain.users.Team;
import org.squashtest.tm.domain.users.UsersGroup;
import org.squashtest.tm.plugin.rest.jackson.serializer.HateoasWrapperConverter;
import org.squashtest.tm.plugin.rest.jackson.serializer.UsersGroupConverter;

import java.util.Set;

@JsonAutoDetect(fieldVisibility = Visibility.NONE, getterVisibility = Visibility.NONE, isGetterVisibility = Visibility.NONE)
@JsonTypeName("user")
@JsonPropertyOrder({"_type", "id", "first_name", "last_name", "login", "email", "active", "group", "teams", "preferences", "lastConnectedOn"})
public abstract class RestUserMixin extends RestPartyMixin {

    @JsonProperty(value = "first_name")
    String firstName;

    @JsonProperty(value = "last_name")
    String lastName;

    @JsonProperty
    String login;

    @JsonProperty
    String email;

    @JsonProperty
    Boolean active;

    @JsonProperty(value = "last_connected_on")
    String lastConnectedOn;

    @JsonProperty
    @JsonSerialize(contentConverter=HateoasWrapperConverter.class)
    @JsonTypeInfo(use=JsonTypeInfo.Id.NONE)
    Set<Team> teams;

    @JsonProperty()
    @JsonSerialize(converter = UsersGroupConverter.class)
    private UsersGroup group;

}
