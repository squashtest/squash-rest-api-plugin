/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.validators;

import org.springframework.stereotype.Component;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.squashtest.tm.domain.customfield.BindableEntity;
import org.squashtest.tm.plugin.rest.controller.helper.ErrorHandlerHelper;
import org.squashtest.tm.plugin.rest.jackson.model.RequirementFolderDto;
import org.squashtest.tm.plugin.rest.jackson.model.RestType;
import org.squashtest.tm.plugin.rest.validators.helper.RequirementFolderDtoValidationHelper;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

@Component
public class RequirementFolderPatchValidator implements Validator {

    private static final String PATCH_REQUIREMENT_FOLDER = "patch-requirement-folder";

    @Inject
    private RequirementFolderDtoValidationHelper requirementFolderDtoValidationHelper;

    @Override
    public boolean supports(Class<?> clazz) {
        return RequirementFolderDto.class.equals(clazz);
    }

    @Override
    public void validate(Object target, final Errors errors) {
        RequirementFolderDto folderPatch = (RequirementFolderDto) target;
        requirementFolderDtoValidationHelper.checkEntityExist(errors, RestType.REQUIREMENT_FOLDER, folderPatch.getId());
        requirementFolderDtoValidationHelper.loadProject(folderPatch);
        requirementFolderDtoValidationHelper.checkParent(errors, folderPatch, RestType.REQUIREMENT_FOLDER);
        requirementFolderDtoValidationHelper.checkCufs(errors, folderPatch, BindableEntity.REQUIREMENT_FOLDER);
    }

    public void validatePatchRequirementFolder(RequirementFolderDto patch) throws BindException {
        List<Errors> errors = new ArrayList<>();

        BindingResult validation = new BeanPropertyBindingResult(patch, PATCH_REQUIREMENT_FOLDER);
        validate(patch, validation);

        if (validation.hasErrors()) {
            errors.add(validation);
        }
        ErrorHandlerHelper.throwIfError(patch, errors, PATCH_REQUIREMENT_FOLDER);
    }

}
