/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.squashtest.tm.core.foundation.lang.Couple;
import org.squashtest.tm.domain.campaign.IterationTestPlanItem;

import java.util.List;

/**
 * Created by jthebault on 20/06/2017.
 */
public interface RestIterationTestPlanItemRepository extends JpaRepository<IterationTestPlanItem, Long> {
    @Query("select testPlans from Iteration it join it.testPlans testPlans where it.id = :iterationId order by index(testPlans)")
    Page<IterationTestPlanItem> findAllByIteration_Id(@Param("iterationId") Long iterationId, Pageable pageable);

    @Query("select distinct itpi from IterationTestPlanItem itpi inner join itpi.testSuites ts where ts.id = :testSuiteId")
    Page<IterationTestPlanItem> findAllByTestSuiteId(@Param("testSuiteId") Long testSuiteId, Pageable pageable);
    
    @Query("select distinct item from IterationTestPlanItem item "
    		+ "inner join item.referencedTestCase tc "
    		+ "inner join tc.requirementVersionCoverages cov "
    		+ "inner join cov.verifiedRequirementVersion rv "
    		+ "inner join rv.requirement req "
    		+ "inner join req.syncExtender ext "
    		+ "inner join ext.server server "
    		+ "where ext.remoteReqId = :remoteKey "
    		+ "and server.name = :serverName")
    Page<IterationTestPlanItem> findItemsByCoveredRemoteRequirement(@Param("remoteKey") String remoteKey, @Param("serverName") String serverName, Pageable pageable);

    @Query("Select new org.squashtest.tm.core.foundation.lang.Couple(it.id , itpi.id ) from IterationTestPlanItem itpi  "
			+ " inner join itpi.iteration it "
			+ " where itpi.id in (:testPlanIds) ")
	List<Couple<Long,Long>> findIterationByIdItem(@Param("testPlanIds") List<Long> testPlanIds);
}

