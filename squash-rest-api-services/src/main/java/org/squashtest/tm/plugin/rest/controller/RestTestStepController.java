/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.controller;

import org.springframework.hateoas.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.squashtest.tm.domain.testcase.ActionTestStep;
import org.squashtest.tm.domain.testcase.CallTestStep;
import org.squashtest.tm.domain.testcase.KeywordTestStep;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.domain.testcase.TestStep;
import org.squashtest.tm.plugin.rest.controller.helper.ErrorHandlerHelper;
import org.squashtest.tm.plugin.rest.controller.helper.ResourceLinksHelper;
import org.squashtest.tm.plugin.rest.core.jackson.DynamicFilterExpression;
import org.squashtest.tm.plugin.rest.core.web.BaseRestController;
import org.squashtest.tm.plugin.rest.core.web.EntityGetter;
import org.squashtest.tm.plugin.rest.core.web.RestApiController;
import org.squashtest.tm.plugin.rest.core.web.UseDefaultRestApiConfiguration;
import org.squashtest.tm.plugin.rest.jackson.model.TestStepDto;
import org.squashtest.tm.plugin.rest.service.RestTestCaseService;
import org.squashtest.tm.plugin.rest.service.RestTestStepService;
import org.squashtest.tm.plugin.rest.validators.TestStepPatchValidator;
import org.squashtest.tm.plugin.rest.validators.TestStepPostValidator;

import javax.inject.Inject;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

@RestApiController(TestStep.class)
@UseDefaultRestApiConfiguration
public class RestTestStepController extends BaseRestController {

    @Inject
    private RestTestStepService restTestStepService;

    @Inject
    private RestTestCaseService restTestCaseService;

    @Inject
    private TestStepPostValidator testStepPostValidator;

    @Inject
    private TestStepPatchValidator testStepPatchValidator;

    @Inject
    private ResourceLinksHelper linksHelper;

    @RequestMapping("/test-steps/{id}")
    @EntityGetter({ActionTestStep.class, CallTestStep.class, KeywordTestStep.class})
    @DynamicFilterExpression("*, test_case[name] ,called_test_case[name], called_dataset[name]")
    public ResponseEntity<Resource<TestStep>> findTestStep(@PathVariable("id") long id) {

        TestStep step = restTestStepService.getOne(id);

        Resource<TestStep> res = toResource(step);

        return ResponseEntity.ok(res);
    }

    @RequestMapping(value = "/test-cases/{testCaseId}/steps", method = RequestMethod.POST)
    @ResponseBody
    @DynamicFilterExpression("*, test_case[name], called_test_case[name], called_dataset[name]")
    public ResponseEntity<Resource<TestStep>> createTestStep(@RequestBody TestStepDto testStepDto, @PathVariable("testCaseId") long testCaseId)
            throws BindException, InvocationTargetException, IllegalAccessException {

        TestCase testCase = restTestCaseService.getOne(testCaseId);
        testStepDto.setProjectId(testCase.getProject().getId());

        validatePostTestStep(testStepDto);

        TestStep ats = restTestStepService.createTestStep(testStepDto, testCase);

        Resource<TestStep> res = toResource(ats);

        linksHelper.populateLinks(res);

        return ResponseEntity.status(HttpStatus.CREATED).body(res);
    }

    @RequestMapping(value = "/test-steps/{id}", method = RequestMethod.PATCH)
    @ResponseBody
    @DynamicFilterExpression("*, test_case[name], called_test_case[name], called_dataset[name]")
    public ResponseEntity<Resource<TestStep>> patchTestStep(@RequestBody TestStepDto patch, @PathVariable("id") long id)
            throws BindException, InvocationTargetException, IllegalAccessException {

        TestStep testStep = restTestStepService.getOne(id);
        patch.setId(id);
        patch.setProjectId(testStep.getTestCase().getProject().getId());

        validatePatchTestStep(patch);

        testStep = restTestStepService.patchTestStep(patch, id);

        Resource<TestStep> res = toResource(testStep);

        linksHelper.populateLinks(res);

        return ResponseEntity.ok(res);
    }

    @RequestMapping(value = "/test-steps/{ids}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> deleteTestStep(@PathVariable("ids") List<Long> ids) {
        restTestStepService.deleteTestStepsByIds(ids);
        return ResponseEntity.noContent().build();
    }

    private void validatePatchTestStep(TestStepDto patch) throws BindException {
        List<Errors> errors = new ArrayList<>();

        BindingResult validation = new BeanPropertyBindingResult(patch, "patch-test-step");
        testStepPatchValidator.validate(patch, validation);

        if (validation.hasErrors()) {
            errors.add(validation);
        }

        ErrorHandlerHelper.throwIfError(patch, errors, "patch-test-step");
    }

    private void validatePostTestStep(TestStepDto testStepDto) throws BindException {

        List<Errors> errors = new ArrayList<>();

        BindingResult validation = new BeanPropertyBindingResult(testStepDto, "post-test-step");
        testStepPostValidator.validate(testStepDto, validation);

        if (validation.hasErrors()) {
            errors.add(validation);
        }

        ErrorHandlerHelper.throwIfError(testStepDto, errors, "post-test-step");
    }
}
