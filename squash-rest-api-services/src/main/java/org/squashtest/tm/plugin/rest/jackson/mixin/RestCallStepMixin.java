/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.jackson.mixin;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.databind.annotation.JsonAppend;
import com.fasterxml.jackson.databind.annotation.JsonAppend.Prop;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.squashtest.tm.domain.testcase.Dataset;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.plugin.rest.core.jackson.SerializeChainConverter;
import org.squashtest.tm.plugin.rest.jackson.serializer.HateoasWrapperConverter;
import org.squashtest.tm.plugin.rest.jackson.serializer.TestStepIndexPropertyWriter;
import org.squashtest.tm.plugin.rest.jackson.serializer.UnauthorizedResourcesConverter;

@JsonAutoDetect(fieldVisibility = Visibility.NONE, getterVisibility = Visibility.NONE, isGetterVisibility = Visibility.NONE)
@JsonTypeName("call-step")
@JsonAppend(props = @Prop(name = "index", value = TestStepIndexPropertyWriter.class))
@JsonPropertyOrder({"_type", "id", "test_case", "delegate_parameter_values", "called_test_case", "called_dataset"})

public class RestCallStepMixin extends RestTestStepMixin {

    @JsonProperty("delegate_parameter_values")
    boolean delegateParameterValues;

    @JsonProperty("called_test_case")
    @SerializeChainConverter(converters = {
            HateoasWrapperConverter.class,
            UnauthorizedResourcesConverter.class
    })
    @JsonTypeInfo(use = Id.NONE)
    TestCase calledTestCase;

    @JsonProperty("called_dataset")
    @JsonSerialize(converter = HateoasWrapperConverter.class)
    @JsonTypeInfo(use = Id.NONE)
    Dataset calledDataset;

}
