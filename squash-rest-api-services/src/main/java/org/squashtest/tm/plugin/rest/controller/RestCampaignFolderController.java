/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.controller;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.PagedResources;
import org.springframework.hateoas.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.squashtest.tm.domain.campaign.CampaignFolder;
import org.squashtest.tm.domain.campaign.CampaignLibraryNode;
import org.squashtest.tm.plugin.rest.controller.helper.ResourceLinksHelper;
import org.squashtest.tm.plugin.rest.core.jackson.DynamicFilterExpression;
import org.squashtest.tm.plugin.rest.core.web.BaseRestController;
import org.squashtest.tm.plugin.rest.core.web.ContentInclusion;
import org.squashtest.tm.plugin.rest.core.web.EntityGetter;
import org.squashtest.tm.plugin.rest.core.web.RestApiController;
import org.squashtest.tm.plugin.rest.core.web.UseDefaultRestApiConfiguration;
import org.squashtest.tm.plugin.rest.jackson.model.CampaignFolderDto;
import org.squashtest.tm.plugin.rest.service.RestCampaignFolderService;
import org.squashtest.tm.plugin.rest.validators.CampaignFolderPatchValidator;
import org.squashtest.tm.plugin.rest.validators.CampaignFolderPostValidator;

import javax.inject.Inject;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

/**
 * Created by jthebault on 16/06/2017.
 */
@RestApiController(CampaignFolder.class)
@UseDefaultRestApiConfiguration
public class RestCampaignFolderController extends BaseRestController{



    @Inject
    private RestCampaignFolderService service;
    @Inject
    private CampaignFolderPostValidator campaignFolderPostValidator;
    @Inject
    private CampaignFolderPatchValidator campaignFolderPatchValidator;
    @Inject
    private ResourceLinksHelper linksHelper;
 /*   @Inject
    private TmServiceConfig tm;*/


	@RequestMapping("/campaign-folders")
	@ResponseBody
	@DynamicFilterExpression("name")
	public ResponseEntity<PagedResources<Resource>> findAllReadableTestCaseFolders( Pageable pageable){
		
		Page<CampaignFolder> folders = service.findAllReadable(pageable);
		
		PagedResources<Resource> res = toPagedResources(folders);
		
		return ResponseEntity.ok(res);
		
	}
    

    @RequestMapping(value = "/campaign-folders/{id}", method = RequestMethod.GET)
    @EntityGetter
    @ResponseBody
    @DynamicFilterExpression("*, parent[name], project[name]")
    public ResponseEntity<Resource<CampaignFolder>> findCampaignFolder(@PathVariable("id") long id){

        CampaignFolder campaignFolder = service.getOne(id);

        Resource<CampaignFolder> res = toResource(campaignFolder);

        res.add(createLinkTo(campaignFolder.getProject()));
        res.add(createRelationTo("content"));
        res.add(createRelationTo("attachments"));


        return ResponseEntity.ok(res);
    }


    @RequestMapping("/campaign-folders/{id}/content")
    @ResponseBody
    @DynamicFilterExpression("name,reference")
    public ResponseEntity<PagedResources<Resource>> findCampaignFolderContent(@PathVariable("id") long folderId, 
    																			Pageable pageable,
    																			ContentInclusion include){

        Page<CampaignLibraryNode> content;
        switch(include){
        case NESTED :
          content = service.findFolderAllContent(folderId, pageable); break;
        default :
          content = service.findFolderContent(folderId, pageable); break;
        }
        PagedResources<Resource> res = toPagedResourcesWithRel(content, "content");

        return ResponseEntity.ok(res);
    }

    /*AMK : Add a new folder */

    @RequestMapping(value = "/campaign-folders", method = RequestMethod.POST)
    @ResponseBody
    @DynamicFilterExpression("*, parent[name], project[name]")
    public ResponseEntity<Resource<CampaignFolder>> createCampaignFolder(@RequestBody CampaignFolderDto folderDto) throws BindException, InvocationTargetException, IllegalAccessException {

        campaignFolderPostValidator.validatePostCampaignFolder(folderDto);

        CampaignFolder folder = service.addCampaignFolder(folderDto);

        Resource<CampaignFolder> res = toResource(folder);

        linksHelper.addAllLinksForCampaignFolder(res);

        return ResponseEntity.status(HttpStatus.CREATED).body(res);
    }

    /*AMK : modify folder */
    @RequestMapping(value = "/campaign-folders/{id}", method = RequestMethod.PATCH)
    @ResponseBody
    @DynamicFilterExpression("*, parent[name], project[name]")
   public ResponseEntity<Resource<CampaignFolder>> patchCampaignFolder(@RequestBody CampaignFolderDto folderPatch, @PathVariable("id") long id) throws BindException {
        folderPatch.setId(id);

        campaignFolderPatchValidator.validatePatchCampaignFolder(folderPatch);

        CampaignFolder folder = service.patchCampaignFolder(folderPatch, id);

        Resource<CampaignFolder> res = toResource(folder);

        linksHelper.addAllLinksForCampaignFolder(res);

        return ResponseEntity.ok(res);
    }

    /*AMK: delete test case folder*/

    @RequestMapping(value = "/campaign-folders/{ids}", method = RequestMethod.DELETE)
    @ResponseBody
    public ResponseEntity<Void> deleteCampaignFolder(@PathVariable("ids")  List<Long>  folderIds){

        service.deleteFolder(folderIds);

        return ResponseEntity.noContent().build();
    }
}
