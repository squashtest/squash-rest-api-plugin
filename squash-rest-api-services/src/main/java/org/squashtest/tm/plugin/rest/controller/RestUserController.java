/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.controller;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.PagedResources;
import org.springframework.hateoas.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.squashtest.tm.domain.users.Team;
import org.squashtest.tm.domain.users.User;
import org.squashtest.tm.plugin.rest.controller.helper.ErrorHandlerHelper;
import org.squashtest.tm.plugin.rest.core.jackson.DynamicFilterExpression;
import org.squashtest.tm.plugin.rest.core.web.BaseRestController;
import org.squashtest.tm.plugin.rest.core.web.EntityGetter;
import org.squashtest.tm.plugin.rest.core.web.RestApiController;
import org.squashtest.tm.plugin.rest.core.web.UseDefaultRestApiConfiguration;
import org.squashtest.tm.plugin.rest.jackson.model.UserDto;
import org.squashtest.tm.plugin.rest.service.RestPartyService;
import org.squashtest.tm.plugin.rest.validators.PartyPatchValidator;
import org.squashtest.tm.plugin.rest.validators.PartyPostValidator;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

@RestApiController(User.class)
@UseDefaultRestApiConfiguration
public class RestUserController extends BaseRestController {

    @Inject
    private RestPartyService restPartyService;

    @Inject
    private PartyPostValidator partyPostValidator;

    @Inject
    private PartyPatchValidator partyPatchValidator;

    /*
    * User
    * */
    @RequestMapping(value = "/users/{id}", method = RequestMethod.GET)
    @EntityGetter
    @DynamicFilterExpression("*, teams[*, -members]")
    public ResponseEntity<Resource<User>> findUser(@PathVariable("id") long userId) {

        User user = (User) restPartyService.findById(userId);

        Resource<User> res = toResource(user);

        return ResponseEntity.ok(res);
    }

    @RequestMapping(value = "/users", method = RequestMethod.GET)
    @ResponseBody
    @DynamicFilterExpression("login, active, group")
    public ResponseEntity<PagedResources<Resource>> findAllUsers(Pageable pageable) {
        Page<User> pagedUsers = restPartyService.findAllUsers(pageable);

        PagedResources<Resource> res = toPagedResources(pagedUsers);

        return ResponseEntity.ok(res);
    }

    @RequestMapping(value = "/users", method = RequestMethod.POST)
    @ResponseBody
    @DynamicFilterExpression("*")
    public ResponseEntity<Resource<User>> createUser(@RequestBody UserDto userDto) throws BindException {
        validatePostUser(userDto);
        User user = (User) restPartyService.createParty(userDto);
        Resource<User> res = toResource(user);

        return ResponseEntity.status(HttpStatus.CREATED).body(res);
    }

    @RequestMapping(value = "/users/{ids}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> deleteUsers(@PathVariable("ids") List<Long> userIds) {
        restPartyService.deleteUsers(userIds);

        return ResponseEntity.noContent().build();
    }

    @RequestMapping(value = "/users/{id}", method = RequestMethod.PATCH)
    @ResponseBody
    @DynamicFilterExpression("*, teams[*, -members]")
    public ResponseEntity<Resource<User>> patchUser(@RequestBody UserDto patch, @PathVariable("id") long id) throws BindException {

        patch.setId(id);

        validatePatchUser(patch);

        User user = (User) restPartyService.patchParty(patch, id);

        Resource<User> res = toResource(user);

        return ResponseEntity.ok(res);
    }

    @RequestMapping(value = "/users/{userId}/teams", method = RequestMethod.GET)
    @ResponseBody
    @DynamicFilterExpression("*, -members")
    public ResponseEntity<PagedResources<Resource>> findTeams(@PathVariable("userId") long userId, Pageable pageable) {

        Page<Team> teams = restPartyService.findTeamsByUser(userId, pageable);
        PagedResources<Resource> res = toPagedResources(teams);

        return ResponseEntity.ok(res);
    }

    @RequestMapping(value = "/users/{userId}/teams", method = RequestMethod.POST)
    public ResponseEntity<Void> addTeamsToUser(@PathVariable("userId") long userId, @RequestParam(value = "teamIds") List<Long> teamIds) {

        restPartyService.addTeamsToUser(userId, teamIds);

        return ResponseEntity.noContent().build();
    }

    @RequestMapping(value = "/users/{userId}/teams", method = RequestMethod.DELETE)
    public ResponseEntity<Void> disassociateUserFromTeams(@PathVariable("userId") long userId, @RequestParam(value = "teamIds") List<Long> teamIds) {

        restPartyService.disassociateUserFromTeams(userId, teamIds);
        return ResponseEntity.noContent().build();

    }

    private void validatePatchUser(UserDto patch) throws BindException {
        List<Errors> errors = new ArrayList<>();

        BindingResult validation = new BeanPropertyBindingResult(patch, "patch-user");
        partyPatchValidator.validate(patch, validation);

        if (validation.hasErrors()) {
            errors.add(validation);
        }

        ErrorHandlerHelper.throwIfError(patch, errors, "patch-user");
    }


    private void validatePostUser(UserDto userDto) throws BindException {
        List<Errors> errors = new ArrayList<>();

        BindingResult validation = new BeanPropertyBindingResult(userDto, "post-user");
        partyPostValidator.validate(userDto, validation);

        if (validation.hasErrors()) {
            errors.add(validation);
        }

        ErrorHandlerHelper.throwIfError(userDto, errors, "post-user");
    }
}
