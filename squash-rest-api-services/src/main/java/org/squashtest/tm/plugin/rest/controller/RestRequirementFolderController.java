/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.controller;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.PagedResources;
import org.springframework.hateoas.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.squashtest.tm.domain.requirement.RequirementFolder;
import org.squashtest.tm.domain.requirement.RequirementLibraryNode;
import org.squashtest.tm.plugin.rest.controller.helper.ResourceLinksHelper;
import org.squashtest.tm.plugin.rest.core.jackson.DynamicFilterExpression;
import org.squashtest.tm.plugin.rest.core.web.BaseRestController;
import org.squashtest.tm.plugin.rest.core.web.ContentInclusion;
import org.squashtest.tm.plugin.rest.core.web.EntityGetter;
import org.squashtest.tm.plugin.rest.core.web.RestApiController;
import org.squashtest.tm.plugin.rest.core.web.UseDefaultRestApiConfiguration;
import org.squashtest.tm.plugin.rest.jackson.model.RequirementFolderDto;
import org.squashtest.tm.plugin.rest.service.RestRequirementFolderService;
import org.squashtest.tm.plugin.rest.validators.RequirementFolderPatchValidator;
import org.squashtest.tm.plugin.rest.validators.RequirementFolderPostValidator;

import javax.inject.Inject;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

@RestApiController(RequirementFolder.class)
@UseDefaultRestApiConfiguration
public class RestRequirementFolderController extends BaseRestController{


	@Inject
	private RestRequirementFolderService service;

	@Inject
	private RequirementFolderPostValidator requirementFolderPostValidator;

	@Inject
	private RequirementFolderPatchValidator  requirementFolderPatchValidator;

	@Inject
	private ResourceLinksHelper linksHelper;

	@RequestMapping("/requirement-folders")
	@ResponseBody
	@DynamicFilterExpression("name")
	public ResponseEntity<PagedResources<Resource>> findAllReadableRequirementFolders( Pageable pageable){
		
		Page<RequirementFolder> folders = service.findAllReadable(pageable);
		
		PagedResources<Resource> res = toPagedResources(folders);
		
		return ResponseEntity.ok(res);
		
	}
	
	
	@RequestMapping("/requirement-folders/{id}")
	@ResponseBody
	@DynamicFilterExpression("*, parent[name], project[name]")
	@EntityGetter
	public ResponseEntity<Resource<RequirementFolder>> findRequirementFolder(@PathVariable("id") long id){
		
		RequirementFolder folder = service.getOne(id);
		
		Resource<RequirementFolder> res = toResource(folder);
		
		res.add(linkService.createLinkTo(folder.getProject()));
		res.add(createRelationTo("content"));
		res.add(createRelationTo("attachments"));


		return ResponseEntity.ok(res);
	}
	
	
	
	@RequestMapping("/requirement-folders/{id}/content")
	@ResponseBody
	@DynamicFilterExpression("name")
	public ResponseEntity<PagedResources<Resource>> findTestCaseFolderContent(@PathVariable("id") long folderId, 
																			Pageable pageable,
																			ContentInclusion include){
		
		Page<RequirementLibraryNode<?>> content = null;
		switch(include){
		case NESTED :
			content = service.findFolderAllContent(folderId, pageable); break;
		default :
			content = service.findFolderContent(folderId, pageable); break;
		
		}
		
		PagedResources<Resource> res = toPagedResourcesWithRel(content, "content");
		
		return ResponseEntity.ok(res);
	}
	/*AMK: add a new folder */

	@RequestMapping(value = "/requirement-folders", method = RequestMethod.POST)
	@ResponseBody
	@DynamicFilterExpression("*, parent[name], project[name]")
	public ResponseEntity<Resource<RequirementFolder>> createRequirementFolder(@RequestBody RequirementFolderDto folderDto) throws BindException, InvocationTargetException, IllegalAccessException {

		requirementFolderPostValidator.validatePostRequirementFolder(folderDto);

		RequirementFolder folder = service.addRequirementFolder(folderDto);

		Resource<RequirementFolder> res = toResource(folder);

		linksHelper.addAllLinksForRequirementFolder(res);

		return ResponseEntity.status(HttpStatus.CREATED).body(res);
	}

	@RequestMapping(value = "/requirement-folders/{id}", method = RequestMethod.PATCH)
	@ResponseBody
	@DynamicFilterExpression("*, parent[name], project[name]")
	public ResponseEntity<Resource<RequirementFolder>> patchRequirementFolder(@RequestBody RequirementFolderDto folderPatch, @PathVariable("id") long id) throws BindException {
		folderPatch.setId(id);

		requirementFolderPatchValidator.validatePatchRequirementFolder(folderPatch);

		RequirementFolder folder = service.patchRequirementFolder(folderPatch, id);

		Resource<RequirementFolder> res = toResource(folder);

		linksHelper.addAllLinksForRequirementFolder(res);

		return ResponseEntity.ok(res);
	}
	/*AMK: delete requirement folder*/

	@RequestMapping(value = "/requirement-folders/{ids}", method = RequestMethod.DELETE)
	@ResponseBody
	public ResponseEntity<Void> deleteTestCaseFolder(@PathVariable("ids")  List<Long>  folderIds){

		service.deleteFolder(folderIds);

		return ResponseEntity.noContent().build();
	}

}
