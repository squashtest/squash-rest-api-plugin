/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.validators;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.squashtest.tm.domain.customfield.BindableEntity;
import org.squashtest.tm.domain.scm.ScmRepository;
import org.squashtest.tm.domain.testautomation.AutomatedTestTechnology;
import org.squashtest.tm.domain.testcase.KeywordTestCase;
import org.squashtest.tm.domain.testcase.ScriptedTestCase;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.domain.testcase.TestCaseVisitor;
import org.squashtest.tm.plugin.rest.jackson.model.KeywordTestCaseDto;
import org.squashtest.tm.plugin.rest.jackson.model.RestType;
import org.squashtest.tm.plugin.rest.jackson.model.ScriptedTestCaseDto;
import org.squashtest.tm.plugin.rest.jackson.model.TestCaseDto;
import org.squashtest.tm.plugin.rest.jackson.model.TestCaseDtoVisitor;
import org.squashtest.tm.plugin.rest.service.RestTestCaseService;
import org.squashtest.tm.plugin.rest.validators.helper.TestCaseDtoValidationHelper;
import org.squashtest.tm.service.deletion.SuppressionPreviewReport;
import org.squashtest.tm.service.testautomation.AutomatedTestTechnologyFinderService;
import org.squashtest.tm.service.testcase.TestCaseLibraryNavigationService;

import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by jthebault on 13/06/2017.
 */
@Component
public class TestCasePatchValidator implements Validator {

    private static final String NON_PATCHABLE_ATTRIBUTE = "non patchable attribute";

    @Inject
    private TestCaseDtoValidationHelper testCaseDtoValidationHelper;

    @Inject
    private RestTestCaseService restTestCaseService;

    @Inject
    private TestCaseLibraryNavigationService testCaseLibraryNavigationService;


    @Override
    public boolean supports(Class<?> clazz) {
        return TestCaseDto.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        final TestCaseDto patch = (TestCaseDto) target;
        testCaseDtoValidationHelper.checkEntityExist(errors, RestType.TEST_CASE, patch.getId());
        testCaseDtoValidationHelper.loadProject(patch);
        testCaseDtoValidationHelper.checkParent(errors, patch, RestType.TEST_CASE);
        testCaseDtoValidationHelper.assignInfoList(errors, patch);
        testCaseDtoValidationHelper.checkCufs(errors, patch, BindableEntity.TEST_CASE);
        testCaseDtoValidationHelper.checkInvalidAttributeForEachTestCaseType(errors, patch);
        testCaseDtoValidationHelper.checkAutomationAttributes(errors, patch);
        checkForbiddenPatchAttributes(errors, patch);
    }

    private void checkForbiddenPatchAttributes(Errors errors, TestCaseDto patch) {
        if (patch.getSteps() != null) {
            errors.rejectValue("steps", NON_PATCHABLE_ATTRIBUTE, "Only attributes belonging to the test case itself can be modified. The attribute steps cannot be patched. Use direct url to the steps entity instead");
        }
        if (patch.getParameters() != null) {
            errors.rejectValue("parameters", NON_PATCHABLE_ATTRIBUTE, "Only attributes belonging to the test case itself can be modified. The attribute parameters cannot be patched. Use direct url to the parameters entity instead");
        }
        if (patch.getDatasets() != null) {
            errors.rejectValue("datasets", NON_PATCHABLE_ATTRIBUTE, "Only attributes belonging to the test case itself can be modified. The attribute datasets cannot be patched. Use direct url to the datasets entity instead");
        }
        if (patch.getRequirementVersionCoverages() != null) {
            errors.rejectValue("requirementVersionCoverages", NON_PATCHABLE_ATTRIBUTE, "Only attributes belonging to the test case itself can be modified. The attribute requirementVersionCoverages cannot be patched. Use /test-cases/{id}/coverages instead");
        }
        if (patch.getVerifiedRequirements() != null) {
            errors.rejectValue("verifiedRequirements", NON_PATCHABLE_ATTRIBUTE, "Only attributes belonging to the test case itself can be modified. The attribute verifiedRequirements cannot be patched. Use /test-cases/{id}/coverages instead");
        }
        if (patch.getMilestones() != null) {
            errors.rejectValue("milestones", NON_PATCHABLE_ATTRIBUTE, "Only attributes belonging to the test case itself can be modified. The attribute milestones cannot be patched. Use direct url to the milestones entity instead");
        }

        TestCase testCase = restTestCaseService.getOne(patch.getId());

        checkTestCaseTypeWithGivenDtoType(patch, testCase);

    }

    public List<SuppressionPreviewReport> simulationDelete( List<Long> testCaseIds) {
        return testCaseLibraryNavigationService.simulateDeletion(testCaseIds);
    }

    private void checkTestCaseTypeWithGivenDtoType(TestCaseDto testCasePatch, TestCase testCase) {
        TestCaseVisitor testCaseVisitor = new TestCaseVisitor() {
            @Override
            public void visit(TestCase testCase) {
                TestCaseDtoVisitor dtoVisitor = new TestCaseDtoVisitor() {
                    @Override
                    public void visit(TestCaseDto testCaseDto) {
                    }

                    @Override
                    public void visit(ScriptedTestCaseDto scriptedTestCaseDto) {
                        throw new IllegalArgumentException("Invalid type for Standard Test Case");
                    }

                    @Override
                    public void visit(KeywordTestCaseDto keywordTestCaseDto) {
                        throw new IllegalArgumentException("Invalid type for Standard Test Case");
                    }
                };
                testCasePatch.accept(dtoVisitor);
            }

            @Override
            public void visit(KeywordTestCase keywordTestCase) {
                TestCaseDtoVisitor dtoVisitor = new TestCaseDtoVisitor() {
                    @Override
                    public void visit(TestCaseDto testCaseDto) {
                        throw new IllegalArgumentException("Invalid type for Keyword Test Case");
                    }

                    @Override
                    public void visit(ScriptedTestCaseDto scriptedTestCaseDto) {
                        throw new IllegalArgumentException("Invalid type for Keyword Test Case");
                    }

                    @Override
                    public void visit(KeywordTestCaseDto keywordTestCaseDto) {
                    }
                };
                testCasePatch.accept(dtoVisitor);
            }

            @Override
            public void visit(ScriptedTestCase scriptedTestCase) {
                TestCaseDtoVisitor dtoVisitor = new TestCaseDtoVisitor() {
                    @Override
                    public void visit(TestCaseDto testCaseDto) {
                        throw new IllegalArgumentException("Invalid type for Scripted Test Case");
                    }

                    @Override
                    public void visit(ScriptedTestCaseDto scriptedTestCaseDto) {
                    }

                    @Override
                    public void visit(KeywordTestCaseDto keywordTestCaseDto) {
                        throw new IllegalArgumentException("Invalid type for Scripted Test Case");
                    }
                };
                testCasePatch.accept(dtoVisitor);
            }
        };
        testCase.accept(testCaseVisitor);
    }
}
