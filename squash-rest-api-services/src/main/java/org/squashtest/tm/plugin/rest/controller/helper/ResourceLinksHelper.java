/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.controller.helper;

import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resource;
import org.springframework.stereotype.Component;
import org.squashtest.tm.domain.campaign.CampaignFolder;
import org.squashtest.tm.domain.campaign.CampaignTestPlanItem;
import org.squashtest.tm.domain.campaign.Iteration;
import org.squashtest.tm.domain.campaign.IterationTestPlanItem;
import org.squashtest.tm.domain.campaign.TestSuite;
import org.squashtest.tm.domain.execution.Execution;
import org.squashtest.tm.domain.project.GenericProject;
import org.squashtest.tm.domain.requirement.Requirement;
import org.squashtest.tm.domain.requirement.RequirementFolder;
import org.squashtest.tm.domain.testcase.Dataset;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.domain.testcase.TestCaseFolder;
import org.squashtest.tm.domain.testcase.TestStep;
import org.squashtest.tm.plugin.rest.core.web.BasePathAwareLinkBuildingService;
import org.squashtest.tm.plugin.rest.jackson.model.RestPartyPermission;

import javax.inject.Inject;


/**
 * This class defines the set of links for certain entities, in order to share among different controllers
 * these definitions. It is helpful when one handler must return an entity which is normally handled in a different
 * controller.
 *
 * Created by bsiri on 04/07/2017.
 */
@Component
public class ResourceLinksHelper {

    @Inject
    private BasePathAwareLinkBuildingService linkService;

    public void addAllLinksForPartyPermission(Long projectId, Resource<RestPartyPermission> res) {

        res.add(linkService.createRelationTo(GenericProject.class, projectId, "self","/permissions"));

    }
    public void addAllLinksForDataset(Resource<Dataset> res ){
        Dataset ds = res.getContent();
        res.add(linkService.createLinkTo(ds.getTestCase()));

    }

    public void addAllLinksForTestCaseFolder(Resource<TestCaseFolder> res) {
        TestCaseFolder cf = res.getContent();

        res.add(linkService.createLinkTo(cf.getProject()));
        res.add(linkService.createRelationTo(cf, "content"));
        res.add(linkService.createRelationTo(cf, "attachments"));
    }
    public void addAllLinksForCampaignFolder(Resource<CampaignFolder> res) {
        CampaignFolder cf = res.getContent();

        res.add(linkService.createLinkTo(cf.getProject()));
        res.add(linkService.createRelationTo(cf, "content"));
        res.add(linkService.createRelationTo(cf, "attachments"));
    }
    public void addAllLinksForRequirementFolder(Resource<RequirementFolder> res) {
        RequirementFolder cf = res.getContent();

        res.add(linkService.createLinkTo(cf.getProject()));
        res.add(linkService.createRelationTo(cf, "content"));
        res.add(linkService.createRelationTo(cf, "attachments"));
    }



    public void addAllLinksForExecution(Resource<Execution> res) {
        Execution exec = res.getContent();

        if (!hasSelfLink(res)) {
            res.add(linkService.createSelfLink(exec));
        }

        res.add(linkService.createLinkTo(exec.getProject()));
        res.add(linkService.createLinkTo(exec.getTestPlan(), "test_plan_item"));
        res.add(linkService.createRelationTo(exec, "execution-steps"));
        res.add(linkService.createRelationTo(exec, "attachments"));


    }

    public void addAllLinksForTestCase(Resource<TestCase> res) {
        TestCase tc = res.getContent();

        res.add(linkService.createLinkTo(tc.getProject()));
        res.add(linkService.createRelationTo(tc, "steps"));
        res.add(linkService.createRelationTo(tc, "parameters"));
        res.add(linkService.createRelationTo(tc, "datasets"));
        res.add(linkService.createRelationTo(tc, "attachments"));

    }
    public void addAllLinksForTestSuite(Resource<TestSuite> res){
        TestSuite ts= res.getContent();

        res.add(linkService.createLinkTo(ts.getProject()));
        res.add(linkService.createLinkTo(ts.getIteration()));
        res.add(linkService.createRelationTo(ts,"test-plan"));
        res.add(linkService.createRelationTo(ts,"attachments"));
    }



    public void addAllLinksForRequirement(Resource<Requirement> res){
        Requirement requirement = res.getContent();

        res.add(linkService.createLinkTo(requirement.getCurrentVersion().getProject()));
        res.add(linkService.createRelationTo(requirement.getCurrentVersion(), "current_version"));
    }
    public void populateLinks(Resource<TestStep> res) {
        TestStep step = res.getContent();
        res.add(linkService.createLinkTo(step.getTestCase()));
    }

    public void addAllLinksForIteration(Resource<Iteration> res) {
        Iteration iteration = res.getContent();

        res.add(linkService.createLinkTo(iteration.getProject()));
        res.add(linkService.createLinkTo(iteration.getCampaign()));
        res.add(linkService.createRelationTo(iteration, "test-suites"));
        res.add(linkService.createRelationTo(iteration, "test-plan"));
        res.add(linkService.createRelationTo(iteration, "attachments"));
    }
    public void addAllLinksForIterationTestPlanItem(Resource<IterationTestPlanItem> res) {


        IterationTestPlanItem  itp = res.getContent();

        res.add(linkService.createLinkTo(itp.getProject()));

        if (itp.getReferencedTestCase() != null) {
            res.add(linkService.createLinkTo(itp.getReferencedTestCase()));
        }

        if (itp.getReferencedDataset() != null) {
            res.add(linkService.createLinkTo(itp.getReferencedDataset()));
        }
        res.add(linkService.createLinkTo(itp.getIteration()));
        res.add(linkService.createRelationTo(itp, "executions"));
    }

    public void addAllLinksForCampaignTestPlanItem(Resource<CampaignTestPlanItem> res) {


        CampaignTestPlanItem ctpi = res.getContent();

        res.add(linkService.createLinkTo(ctpi.getProject()));

        if (ctpi.getReferencedTestCase() != null) {
            res.add(linkService.createLinkTo(ctpi.getReferencedTestCase()));
        }

        if (ctpi.getReferencedDataset() != null) {
            res.add(linkService.createLinkTo(ctpi.getReferencedDataset()));
        }
        res.add(linkService.createLinkTo(ctpi.getCampaign()));
    }


    private boolean hasSelfLink(Resource<?> res) {
        return res.getLink(Link.REL_SELF) != null;
    }

}
