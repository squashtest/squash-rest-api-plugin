/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.service.impl;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PostFilter;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.domain.IdCollector;
import org.squashtest.tm.domain.customfield.RawValue;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.domain.requirement.NewRequirementVersionDto;
import org.squashtest.tm.domain.requirement.Requirement;
import org.squashtest.tm.domain.requirement.RequirementFolder;
import org.squashtest.tm.domain.requirement.RequirementSyncExtender;
import org.squashtest.tm.domain.requirement.RequirementVersion;
import org.squashtest.tm.plugin.rest.jackson.model.ParentEntity;
import org.squashtest.tm.plugin.rest.jackson.model.RequirementDto;
import org.squashtest.tm.plugin.rest.repository.RestRequirementRepository;
import org.squashtest.tm.plugin.rest.repository.RestRequirementSyncExtenderRepository;
import org.squashtest.tm.plugin.rest.repository.RestRequirementVersionRepository;
import org.squashtest.tm.plugin.rest.service.RestRequirementVersionService;
import org.squashtest.tm.plugin.rest.service.helper.CustomFieldValueHelper;
import org.squashtest.tm.service.project.ProjectFinder;
import org.squashtest.tm.service.requirement.RequirementLibraryNavigationService;
import org.squashtest.tm.service.requirement.RequirementVersionManagerService;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.squashtest.tm.service.security.Authorizations.OR_HAS_ROLE_ADMIN;

@Service
@Transactional
public class RestRequirementVersionServiceImpl implements RestRequirementVersionService {

    @Inject
    private ProjectFinder projectFinder;

    @Inject
    private RequirementVersionManagerService service;

    @Inject
    private RestRequirementRepository reqDao;

    @Inject
    private RestRequirementVersionRepository reqVerRepository;

    @Inject
    private RestRequirementSyncExtenderRepository syncExtenderDao;

    @Inject
    private RequirementLibraryNavigationService requirementLibraryNavigationService;

    @Inject
    private CustomFieldValueHelper customFieldValueConverter;

    @Inject
    private RestRequirementRepository restRequirementRepository;

    @Inject
    private RequirementVersionPatcher requirementVersionPatcher;

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    @Transactional(readOnly = true)
    @SuppressWarnings("unchecked")
    public Page<Requirement> findAllReadable(Pageable paging) {
        Collection<Project> projects = projectFinder.findAllReadable();
        Collection<Long> ids = CollectionUtils.collect(projects, new IdCollector());
        if (ids.isEmpty()) {
            return new PageImpl<Requirement>(Collections.<Requirement>emptyList(), paging, 0);

        } else {
            return reqDao.findAllInProjects(ids, paging);
        }
    }

    @Override
    @Transactional(readOnly = true)
    public Requirement findRequirement(long requirementId) {
        return service.findRequirementById(requirementId);
    }

    @Override
    @Transactional(readOnly = true)
    @PreAuthorize("@apiSecurity.hasPermission(#requirementId,'org.squashtest.tm.domain.requirement.Requirement' , 'READ')")
    public Page<Requirement> findRequirementChildren(long requirementId, Pageable paging) {
        return reqDao.findRequirementChildren(requirementId, paging);
    }

    @Override
    @Transactional(readOnly = true)
    @PreAuthorize("@apiSecurity.hasPermission(#requirementId,'org.squashtest.tm.domain.requirement.Requirement' , 'READ')")
    public Page<Requirement> findRequirementAllChildren(long requirementId, Pageable paging) {
        return reqDao.findRequirementAllChildren(requirementId, paging);
    }


    @Override
    @Transactional(readOnly = true)
    public RequirementVersion findRequirementVersion(long versionId) {
        return service.findById(versionId);
    }

    @Override
    public Requirement createRequirement(RequirementDto requirementDto) {

        NewRequirementVersionDto newRequirementVersionDto = convertRequirementDtoToNewRequirementVersionDto(requirementDto);

        return addToParent(requirementDto, newRequirementVersionDto);
    }

    @Override
    public void deleteRequirements(List<Long> reqIds) {
        requirementLibraryNavigationService.deleteNodes(reqIds);
    }

    @Override
    @Transactional(readOnly=true)
    @PostFilter("hasPermission(filterObject, 'READ')" + OR_HAS_ROLE_ADMIN)
    public List<Requirement> findSynchronizedRequirementsBy(String remoteKey, String serverName) {
        Collection<RequirementSyncExtender> extenders = syncExtenderDao.findAllByRemoteReqIdAndServerName(remoteKey, serverName);
        return extenders.stream().map(ex -> ex.getRequirement()).collect(Collectors.toList());
    }

    @Override
    public List<Long> findReqIdsByVersionIds(List<Long> versionIds) {
        return reqVerRepository.findRequirementIdsByRequirementVersionIds(versionIds);
    }

    @Override
    public Long findCurrentVersionIdByRequirementId(Long requirementId) {
        Requirement requirement = findRequirement(requirementId);
        if (requirement != null) {
            return requirement.getCurrentVersion().getId();
        } else {
            throw new EntityNotFoundException("Try to retrieve Requirement with given id : " + requirementId + ", but could not find it");
        }
    }

    @Override
    @PreAuthorize("@apiSecurity.hasPermission(#requirementId,'org.squashtest.tm.domain.requirement.Requirement' , 'WRITE')")
    public Requirement modifyRequirement(RequirementDto requirementDto, Long requirementId) {
        Requirement requirement = restRequirementRepository.getOne(requirementId);
        RequirementVersion reqVersion = requirement.getCurrentVersion();
        //Status traitement particulier
        requirementVersionPatcher.patch(reqVersion, requirementDto.getCurrentVersion());
        if(requirementDto.getCurrentVersion().isHasCufs()){
            requirementVersionPatcher.patchCustomFieldValue(requirementDto.getCurrentVersion(),reqVersion);
        }


        return requirement;
    }

    private Requirement addToParent(RequirementDto requirementDto, NewRequirementVersionDto newRequirementVersionDto) {
        ParentEntity parent = requirementDto.getParent();
        Requirement requirement;
        switch (parent.getRestType()) {
            case PROJECT:
                requirement = addRequirementToLibrary(newRequirementVersionDto, parent);
                break;
            case REQUIREMENT_FOLDER:
                requirement = addRequirementToFolder(newRequirementVersionDto, parent);
                break;
            case REQUIREMENT:
                requirement = addRequirementToRequirement(newRequirementVersionDto, parent);
                break;
            default:
                throw new IllegalArgumentException("Programmatic error : Rest type " + parent.getRestType() + "is not a valid parent. You should validate this before.");
        }
        return requirement;
    }

    private Requirement addRequirementToLibrary(NewRequirementVersionDto newRequirementVersionDto, ParentEntity parent) {
        Project project = entityManager.find(Project.class, parent.getId());
        if (project != null) {
            return requirementLibraryNavigationService.addRequirementToRequirementLibrary(project.getRequirementLibrary().getId(), newRequirementVersionDto, new ArrayList<Long>());
        } else {
            throw new IllegalArgumentException("Programmatic error : project with id " + parent.getId() + "is unknown. You should validate this before.");
        }
    }

    private Requirement addRequirementToFolder(NewRequirementVersionDto newRequirementVersionDto, ParentEntity parent) {
        RequirementFolder requirementFolder = entityManager.find(RequirementFolder.class, parent.getId());
        if (requirementFolder != null) {
            return requirementLibraryNavigationService.addRequirementToRequirementFolder(requirementFolder.getId(), newRequirementVersionDto, new ArrayList<Long>());
        } else {
            throw new IllegalArgumentException("Programmatic error : requirement folder with id " + parent.getId() + "is unknown. You should validate this before.");
        }
    }

    private Requirement addRequirementToRequirement(NewRequirementVersionDto newRequirementVersionDto, ParentEntity parent) {
        Requirement req = entityManager.find(Requirement.class, parent.getId());
        if (req != null) {
            return requirementLibraryNavigationService.addRequirementToRequirement(req.getId(), newRequirementVersionDto, new ArrayList<Long>());
        } else {
            throw new IllegalArgumentException("Programmatic error : requirement folder with id " + parent.getId() + "is unknown. You should validate this before.");
        }
    }

    private NewRequirementVersionDto convertRequirementDtoToNewRequirementVersionDto(RequirementDto requirementDto) {
        NewRequirementVersionDto newRequirementVersionDto = new NewRequirementVersionDto();

        Map<Long, RawValue> customFieldMap = customFieldValueConverter.convertCustomFieldDtoToMap(requirementDto.getCustomFields());

        newRequirementVersionDto.setName(requirementDto.getName());
        newRequirementVersionDto.setReference(requirementDto.getCurrentVersion().getReference());
        newRequirementVersionDto.setCategory(requirementDto.getCurrentVersion().getCategory().getCode());
        newRequirementVersionDto.setCriticality(requirementDto.getCurrentVersion().getCriticality());
        newRequirementVersionDto.setDescription(requirementDto.getDescription());
        newRequirementVersionDto.setCustomFields(customFieldMap);

        return newRequirementVersionDto;

    }
}
