/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.validators;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.squashtest.tm.domain.testcase.TestStep;
import org.squashtest.tm.plugin.rest.jackson.model.CheckingAttributeTestStepDtoVisitor;
import org.squashtest.tm.plugin.rest.jackson.model.TestStepDto;
import org.squashtest.tm.plugin.rest.jackson.model.TestStepDtoVisitor;
import org.squashtest.tm.plugin.rest.service.RestTestStepService;
import org.squashtest.tm.plugin.rest.validators.helper.TestStepValidationHelper;

import javax.inject.Inject;

@Component
public class TestStepPatchValidator implements Validator {
    @Inject
    private RestTestStepService restTestStepService;

    @Inject
    private TestStepValidationHelper testStepValidationHelper;

    @Override
    public boolean supports(Class<?> clazz) {
        return TestStepDto.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, final Errors errors) {
        TestStepDto patch = (TestStepDto) target;
        checkIfExist(errors, patch.getId());

        TestStepDtoVisitor visitor = new CheckingAttributeTestStepDtoVisitor(testStepValidationHelper, errors);
        patch.accept(visitor);

        checkForbiddenPatchAttributes(errors, patch);

    }

    private void checkIfExist(Errors errors, long id) {
        TestStep testStep = restTestStepService.getOne(id);
        if (testStep == null) {
            String message = String.format("No test step known for id %d", id);
            errors.rejectValue("id", "invalid id", message);
        }
    }

    private void checkForbiddenPatchAttributes(final Errors errors, TestStepDto patch) {
        if (patch.getTestCaseDto() != null) {
            errors.rejectValue("testCase", "non patchable attribute", "Only attributes belonging to the test step itself can be modified. The attribute test case cannot be patched. Use direct url to the test case entity instead");
        }
    }
}
