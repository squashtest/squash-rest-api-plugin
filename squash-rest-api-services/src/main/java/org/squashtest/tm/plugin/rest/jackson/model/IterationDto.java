/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.jackson.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.squashtest.tm.domain.campaign.IterationStatus;
import org.squashtest.tm.domain.campaign.IterationTestPlanItem;
import org.squashtest.tm.domain.campaign.TestSuite;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class IterationDto {

    private Long id;

    private Long projectId;

    private String name;

    private String description;

    private IterationStatus status;

    @JsonProperty("scheduled_start_date")
    private Date scheduledStartDate;

    @JsonProperty("scheduled_end_date")
    private Date scheduledEndDate;

    @JsonProperty("actual_start_date")
    private Date actualStartDate;

    @JsonProperty("actual_end_date")
    private Date actualEndDate;

    @JsonProperty("actual_start_auto")
    private boolean actualStartAuto;

    @JsonProperty("actual_end_auto")
    private boolean actualEndAuto;

    private String reference;

    @JsonProperty("test_plan")
    private List<IterationTestPlanItem> testPlan;

    @JsonProperty("test_suites")
    private List<TestSuite> testSuites;

    @JsonProperty("custom_fields")
    private List<CustomFieldValueDto> customFields = new ArrayList<>();

    @JsonProperty("copy_campaign_execution_plan")
    private boolean copyCampaignExecutionPlan;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public IterationStatus getStatus() {
        return status;
    }

    public void setStatus(IterationStatus status) {
        this.status = status;
    }

    public Date getScheduledStartDate() {
        return scheduledStartDate;
    }

    public void setScheduledStartDate(Date scheduledStartDate) {
        this.scheduledStartDate = scheduledStartDate;
    }

    public Date getScheduledEndDate() {
        return scheduledEndDate;
    }

    public void setScheduledEndDate(Date scheduledEndDate) {
        this.scheduledEndDate = scheduledEndDate;
    }

    public Date getActualStartDate() {
        return actualStartDate;
    }

    public void setActualStartDate(Date actualStartDate) {
        this.actualStartDate = actualStartDate;
    }

    public Date getActualEndDate() {
        return actualEndDate;
    }

    public void setActualEndDate(Date actualEndDate) {
        this.actualEndDate = actualEndDate;
    }

    public boolean isActualStartAuto() {
        return actualStartAuto;
    }

    public void setActualStartAuto(boolean actualStartAuto) {
        this.actualStartAuto = actualStartAuto;
    }

    public boolean isActualEndAuto() {
        return actualEndAuto;
    }

    public void setActualEndAuto(boolean actualEndAuto) {
        this.actualEndAuto = actualEndAuto;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public List<IterationTestPlanItem> getTestPlan() {
        return testPlan;
    }

    public void setTestPlan(List<IterationTestPlanItem> testPlan) {
        this.testPlan = testPlan;
    }

    public List<TestSuite> getTestSuites() {
        return testSuites;
    }

    public void setTestSuites(List<TestSuite> testSuites) {
        this.testSuites = testSuites;
    }

    public List<CustomFieldValueDto> getCustomFields() {
        return customFields;
    }

    public void setCustomFields(List<CustomFieldValueDto> customFields) {
        this.customFields = customFields;
    }

    public boolean isCopyCampaignExecutionPlan() {
        return copyCampaignExecutionPlan;
    }

    public void setCopyCampaignExecutionPlan(boolean copyCampaignExecutionPlan) {
        this.copyCampaignExecutionPlan = copyCampaignExecutionPlan;
    }
}
