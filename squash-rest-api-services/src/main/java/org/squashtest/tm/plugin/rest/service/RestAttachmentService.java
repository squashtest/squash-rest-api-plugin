/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;
import org.squashtest.tm.domain.EntityReference;
import org.squashtest.tm.domain.attachment.Attachment;
import org.squashtest.tm.domain.attachment.AttachmentHolder;
import org.squashtest.tm.service.attachment.RawAttachment;

import java.io.IOException;
import java.util.List;

public interface RestAttachmentService {

    Attachment findById(long id);

    Page<Attachment> findPagedAttachments(AttachmentHolder attached, Pageable pageable);

    Attachment addAttachment(long attachmentListId, RawAttachment upload, EntityReference entityReference) throws IOException;

    Attachment renameAttachment(long attachmentId, String newName);

    List<Attachment> addAttachments(AttachmentHolder holder, List<MultipartFile> files);

    void deleteAttachments(AttachmentHolder holder, List<Long> attachmentIds) throws IOException;

    AttachmentHolder findHolder(String owner, long id);

    AttachmentHolder findHolderWithPermission(String owner, long id, String permission);

}
