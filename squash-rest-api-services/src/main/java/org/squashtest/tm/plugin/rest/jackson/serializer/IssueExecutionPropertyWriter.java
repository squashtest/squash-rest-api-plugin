/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.jackson.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.cfg.MapperConfig;
import com.fasterxml.jackson.databind.introspect.AnnotatedClass;
import com.fasterxml.jackson.databind.introspect.BeanPropertyDefinition;
import com.fasterxml.jackson.databind.ser.VirtualBeanPropertyWriter;
import com.fasterxml.jackson.databind.util.Annotations;
import org.springframework.beans.factory.annotation.Configurable;
import org.squashtest.tm.domain.bugtracker.Issue;
import org.squashtest.tm.domain.execution.Execution;
import org.squashtest.tm.service.internal.repository.IssueDao;

import javax.inject.Inject;

/**
 * Created by jthebault on 23/06/2017.
 */
@SuppressWarnings("serial")
@Configurable
public class IssueExecutionPropertyWriter extends VirtualBeanPropertyWriter {

    @Inject
    private IssueDao issueDao;

    @Inject
    private HateoasWrapperConverter<Execution> hateoasWrapperConverter;

    public IssueExecutionPropertyWriter() {
        super();
    }

    public IssueExecutionPropertyWriter(BeanPropertyDefinition propDef, Annotations contextAnnotations, JavaType declaredType, IssueDao issueDao, HateoasWrapperConverter<Execution> hateoasWrapperConverter) {
        super(propDef, contextAnnotations, declaredType);
        this.issueDao = issueDao;
        this.hateoasWrapperConverter = hateoasWrapperConverter;
    }

    @Override
    protected Object value(Object bean, JsonGenerator gen, SerializerProvider prov) throws Exception {
        Issue issue = (Issue) bean;
        Execution execution = issueDao.findExecutionRelatedToIssue(issue.getId());
        return hateoasWrapperConverter.convert(execution);
    }

    @Override
    public VirtualBeanPropertyWriter withConfig(MapperConfig<?> config, AnnotatedClass declaringClass, BeanPropertyDefinition propDef, JavaType type) {
        return new IssueExecutionPropertyWriter(propDef,declaringClass.getAnnotations(), type, issueDao, hateoasWrapperConverter);
    }

}
