/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.jackson.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.squashtest.tm.domain.testautomation.AutomatedExecutionExtender;
import org.squashtest.tm.plugin.rest.core.jackson.RestDtoName;

import java.util.List;

// this dto is present only because the AutomatedSuite is not Identified, so we can not use our mixin for jackson serialization
@RestDtoName(value = "automated-suite")
public class AutomatedSuiteDto{

    private String id;

    @JsonIgnore
    private List<AutomatedExecutionExtender> executionExtenders;

    private String _type = "automated-suite";

    public AutomatedSuiteDto(String id, List<AutomatedExecutionExtender> executionExtenders) {
        this.id = id;
        this.executionExtenders = executionExtenders;
    }

    public AutomatedSuiteDto() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<AutomatedExecutionExtender> getExecutionExtenders() {
        return executionExtenders;
    }

    public void setExecutionExtenders(List<AutomatedExecutionExtender> executionExtenders) {
        this.executionExtenders = executionExtenders;
    }

    public String get_type() {
        return _type;
    }

    public void set_type(String _type) {
        this._type = _type;
    }
}
