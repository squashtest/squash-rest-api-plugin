/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.validators;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.squashtest.tm.domain.users.Party;
import org.squashtest.tm.plugin.rest.jackson.model.PartyDto;
import org.squashtest.tm.plugin.rest.jackson.model.PartyDtoVisitor;
import org.squashtest.tm.plugin.rest.jackson.model.TeamDto;
import org.squashtest.tm.plugin.rest.jackson.model.UserDto;
import org.squashtest.tm.plugin.rest.service.RestPartyService;
import org.squashtest.tm.plugin.rest.validators.helper.PartyValidationHelper;

import javax.inject.Inject;

@Component
public class PartyPatchValidator implements Validator {

    @Inject
    private RestPartyService restPartyService;

    @Inject
    private PartyValidationHelper partyValidationHelper;

    @Override
    public boolean supports(Class<?> clazz) {
        return PartyDto.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, final Errors errors) {
        PartyDto patch = (PartyDto) target;
        checkIfExist(errors, patch.getId());

        PartyDtoVisitor visitor = new PartyDtoVisitor() {
            @Override
            public void visit(UserDto userDto) {

                if(userDto.getGroup() != null){
                    partyValidationHelper.checkAndAssignValues(errors, userDto);
                }

            }

            @Override
            public void visit(TeamDto teamDto) {
                //NOOP
            }
        };

        patch.accept(visitor);

        checkForbiddenPatchAttributes(errors, patch);

    }

    private void checkIfExist(Errors errors, long id) {
        Party party = restPartyService.getOne(id);
        if (party == null) {
            String message = String.format("No party known for id %d", id);
            errors.rejectValue("id", "invalid id", message);
        }
    }

    private void checkForbiddenPatchAttributes(final Errors errors, PartyDto patch) {

        PartyDtoVisitor visitor = new PartyDtoVisitor() {
            @Override
            public void visit(UserDto userDto) {
                if(userDto.getTeams() != null){
                    errors.rejectValue("teams", "non patchable attribute", "Only attributes belonging to the user itself can be modified. The attribute teams cannot be patched. Use direct url to the team entity instead");
                }
            }

            @Override
            public void visit(TeamDto teamDto) {
                if(teamDto.getMembers() != null) {
                    errors.rejectValue("members", "non patchable attribute", "Only attributes belonging to the team itself can be modified. The attribute members cannot be patched. Use direct url to the team entity instead");
                }
            }
        };
        patch.accept(visitor);
    }
}
