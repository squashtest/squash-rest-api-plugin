/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.jackson.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.springframework.context.i18n.LocaleContextHolder;
import org.squashtest.tm.core.foundation.lang.Wrapped;
import org.squashtest.tm.domain.attachment.AttachmentList;
import org.squashtest.tm.domain.infolist.InfoListItem;
import org.squashtest.tm.domain.library.Copiable;
import org.squashtest.tm.domain.library.Library;
import org.squashtest.tm.domain.library.NodeVisitor;
import org.squashtest.tm.domain.milestone.Milestone;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.domain.testautomation.AutomatedTest;
import org.squashtest.tm.domain.testcase.KeywordTestCase;
import org.squashtest.tm.domain.testcase.Parameter;
import org.squashtest.tm.domain.testcase.RequirementVersionCoverage;
import org.squashtest.tm.domain.testcase.ScriptedTestCase;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.domain.testcase.TestCaseAutomatable;
import org.squashtest.tm.domain.testcase.TestCaseExecutionMode;
import org.squashtest.tm.domain.testcase.TestCaseImportance;
import org.squashtest.tm.domain.testcase.TestCaseStatus;
import org.squashtest.tm.domain.tf.automationrequest.AutomationRequestStatus;
import org.squashtest.tm.plugin.rest.jackson.deserializer.RestNatureTypeCategoryDeserializer;

import java.util.List;
import java.util.Set;

/**
 * [JTH 2017-06-08]
 * Convenient pure POJO class that aim to make incomplete post or patch request.
 * We cannot use Squash model as this is a rich domain with a lot of data integrity constraints (@NotNull and other stuff) that undermine our effort to implement patch requests.
 * Do not use this class for any other needs that posting a new test case or patching an existing test case, it's a dumb object with a lot of null inside ;)
 * Created by jthebault on 08/06/2017.
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "_type")
@JsonTypeName("test-case")
@JsonSubTypes({
        @JsonSubTypes.Type(value = ScriptedTestCaseDto.class, name = "scripted-test-case"),
        @JsonSubTypes.Type(value = KeywordTestCaseDto.class, name = "keyword-test-case")
})
public class TestCaseDto implements RestNode{

    private Long id;

    private Project project;

    private String name;

    private String description;

    private String reference;

    private String prerequisite;

    private List<TestStepDto> steps;

    private Set<RequirementVersionCoverage> requirementVersionCoverages;

    // this property is only used for adding verified requirements when POSTing a test case
    // Though I'm not sure if I should keep or delete the requirementVersionCoverages attribute above.
    @JsonProperty(access = Access.WRITE_ONLY, value = "verified_requirements")
    private Set<RequirementVersionDto> verifiedRequirements;

    private Set<Parameter> parameters;

    private Set<DatasetDto> datasets;

    private TestCaseImportance importance;

    @JsonDeserialize(using=RestNatureTypeCategoryDeserializer.class)
    private InfoListItem nature;

    @JsonDeserialize(using=RestNatureTypeCategoryDeserializer.class)
    private InfoListItem type;

    private TestCaseStatus status;

    private TestCaseExecutionMode executionMode;

    private Boolean importanceAuto;

    private TestCaseAutomatable automatable;

    @JsonProperty("automated_test")
    private AutomatedTest automatedTest;

    // SQUASH-1443
    @JsonProperty("automation_status")
    private AutomationRequestStatus automationStatus;

    // SQUASH-1679
    @JsonProperty("automation_priority")
    private Integer automationPriority;

    private Set<Milestone> milestones;

    @JsonProperty("custom_fields")
    private List<CustomFieldValueDto> customFields;

    private ParentEntity parent;

    private String path;

    @JsonProperty("automated_test_technology")
    private String automatedTestTechnology;

    @JsonProperty("automated_test_reference")
    private String automatedTestReference;

    @JsonProperty("scm_repository_id")
    private Long scmRepositoryId;

    @JsonProperty("scm_repository_url")
    private String scmRepositoryUrl;

    public static TestCase convertDto(TestCaseDto testCaseDto) {
        Wrapped<TestCase>testCaseWrapped = new Wrapped<>();
        TestCaseDtoVisitor dtoVisitor = new TestCaseDtoVisitor() {
            @Override
            public void visit(TestCaseDto testCaseDto) {
                testCaseWrapped.setValue(new TestCase());
            }

            @Override
            public void visit(ScriptedTestCaseDto scriptedTestCaseDto) {
                ScriptedTestCase scriptedTestCase = new ScriptedTestCase();
                String dtoScript = scriptedTestCaseDto.getScript();
                if(dtoScript != null) {
                    scriptedTestCase.setScript(dtoScript);
                } else {
                    scriptedTestCase.populateInitialScript(LocaleContextHolder.getLocale().getLanguage());
                }
                testCaseWrapped.setValue(scriptedTestCase);
            }

            @Override
            public void visit(KeywordTestCaseDto keywordTestCaseDto) {
                testCaseWrapped.setValue(new KeywordTestCase());
            }
        };

        testCaseDto.accept(dtoVisitor);
        return testCaseWrapped.getValue();
    }

    public Set<RequirementVersionDto> getVerifiedRequirements() {
        return verifiedRequirements;
    }

    public void setVerifiedRequirements(Set<RequirementVersionDto> dtos) {
        this.verifiedRequirements = dtos;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPrerequisite() {
        return prerequisite;
    }

    public void setPrerequisite(String prerequisite) {
        this.prerequisite = prerequisite;
    }

    public List<TestStepDto> getSteps() {
        return steps;
    }

    public void setSteps(List<TestStepDto> steps) {
        this.steps = steps;
    }

    public Set<RequirementVersionCoverage> getRequirementVersionCoverages() {
        return requirementVersionCoverages;
    }

    public void setRequirementVersionCoverages(Set<RequirementVersionCoverage> requirementVersionCoverages) {
        this.requirementVersionCoverages = requirementVersionCoverages;
    }

    public Set<Parameter> getParameters() {
        return parameters;
    }

    public void setParameters(Set<Parameter> parameters) {
        this.parameters = parameters;
    }

    public Set<DatasetDto> getDatasets() {
        return datasets;
    }

    public void setDatasets(Set<DatasetDto> datasets) {
        this.datasets = datasets;
    }

    public TestCaseImportance getImportance() {
        return importance;
    }

    public void setImportance(TestCaseImportance importance) {
        this.importance = importance;
    }

    public InfoListItem getNature() {
        return nature;
    }

    public void setNature(InfoListItem nature) {
        this.nature = nature;
    }

    public InfoListItem getType() {
        return type;
    }

    public void setType(InfoListItem type) {
        this.type = type;
    }

    public TestCaseStatus getStatus() {
        return status;
    }

    public void setStatus(TestCaseStatus status) {
        this.status = status;
    }

    public TestCaseExecutionMode getExecutionMode() {
        return executionMode;
    }

    public void setExecutionMode(TestCaseExecutionMode executionMode) {
        this.executionMode = executionMode;
    }

    public Boolean getImportanceAuto() {
        return importanceAuto;
    }

    public void setImportanceAuto(Boolean importanceAuto) {
        this.importanceAuto = importanceAuto;
    }

    public AutomatedTest getAutomatedTest() {
        return automatedTest;
    }

    public void setAutomatedTest(AutomatedTest automatedTest) {
        this.automatedTest = automatedTest;
    }

    public Set<Milestone> getMilestones() {
        return milestones;
    }

    public void setMilestones(Set<Milestone> milestones) {
        this.milestones = milestones;
    }

    public List<CustomFieldValueDto> getCustomFields() {
        return customFields;
    }

    @Override
    public RestType getRestType() {
        return RestType.TEST_CASE;
    }

    public void setCustomFields(List<CustomFieldValueDto> customFields) {
        this.customFields = customFields;
    }

    public ParentEntity getParent() {
        return parent;
    }

    public void setParent(ParentEntity parent) {
        this.parent = parent;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public void notifyAssociatedWithProject(Project project) {
        this.project = project;
    }

    @Override
    public AttachmentList getAttachmentList() {
        throw new UnsupportedOperationException();
    }

    @Override
    public Copiable createCopy() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void accept(NodeVisitor nodeVisitor) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Library<?> getLibrary() {
        return getProject().getTestCaseLibrary();
    }

    public void accept(TestCaseDtoVisitor testCaseDtoVisitor) {
        testCaseDtoVisitor.visit(this);
    }

    public AutomationRequestStatus getAutomationStatus() {
        return automationStatus;
    }

    public void setAutomationStatus(AutomationRequestStatus automationStatus) {
        this.automationStatus = automationStatus;
    }

    public Integer getAutomationPriority() {
        return automationPriority;
    }

    public void setAutomationPriority(Integer automationPriority) {
        this.automationPriority = automationPriority;
    }

    public TestCaseAutomatable getAutomatable() {
        return automatable;
    }

    public void setAutomatable(TestCaseAutomatable automatable) {
        this.automatable = automatable;
    }

    public String getAutomatedTestTechnology() {
        return automatedTestTechnology;
    }

    public void setAutomatedTestTechnology(String automatedTestTechnology) {
        this.automatedTestTechnology = automatedTestTechnology;
    }

    public String getAutomatedTestReference() {
        return automatedTestReference;
    }

    public void setAutomatedTestReference(String automatedTestReference) {
        this.automatedTestReference = automatedTestReference;
    }

    public Long getScmRepositoryId() {
        return scmRepositoryId;
    }

    public void setScmRepositoryId(Long scmRepositoryId) {
        this.scmRepositoryId = scmRepositoryId;
    }

    public String getScmRepositoryUrl() {
        return scmRepositoryUrl;
    }

    public void setScmRepositoryUrl(String scmRepositoryUrl) {
        this.scmRepositoryUrl = scmRepositoryUrl;
    }
}
