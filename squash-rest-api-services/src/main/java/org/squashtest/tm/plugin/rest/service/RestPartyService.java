/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.squashtest.tm.domain.users.Party;
import org.squashtest.tm.domain.users.Team;
import org.squashtest.tm.domain.users.User;
import org.squashtest.tm.plugin.rest.jackson.model.PartyDto;

import java.util.List;

public interface RestPartyService {

    Party findById(long id);

    Party getOne(long id);

    User findUserByLogin(String login);

    Page<User> findAllUsers(Pageable pageable);

    Page<Team> findAllTeams(Pageable pageable);

    Page<Team> findTeamsByUser(long userId, Pageable pageable);

    Party createParty(PartyDto partyDto);

    Party patchParty(PartyDto partyDto, long id);

    void deleteUsers(List<Long> ids);

    void deleteTeams(List<Long> ids);

    Page<User> findAllTeamMembers(long teamId, Pageable pageable);

    void addMembersToTeam(long teamId, List<Long> userIds);

    void removeMembersFromTeam(long teamId, List<Long> userIds);

    void addTeamsToUser(long userId, List<Long> teamIds);

    void disassociateUserFromTeams(long userId, List<Long> teamIds);

}
