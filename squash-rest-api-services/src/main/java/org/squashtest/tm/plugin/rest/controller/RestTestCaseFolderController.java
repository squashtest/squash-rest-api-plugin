/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.PagedResources;
import org.springframework.hateoas.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.squashtest.tm.domain.testcase.TestCaseFolder;
import org.squashtest.tm.domain.testcase.TestCaseLibraryNode;
import org.squashtest.tm.plugin.rest.controller.helper.ResourceLinksHelper;
import org.squashtest.tm.plugin.rest.core.jackson.DynamicFilterExpression;
import org.squashtest.tm.plugin.rest.core.web.BaseRestController;
import org.squashtest.tm.plugin.rest.core.web.ContentInclusion;
import org.squashtest.tm.plugin.rest.core.web.EntityGetter;
import org.squashtest.tm.plugin.rest.core.web.RestApiController;
import org.squashtest.tm.plugin.rest.core.web.UseDefaultRestApiConfiguration;
import org.squashtest.tm.plugin.rest.jackson.model.TestCaseFolderDto;
import org.squashtest.tm.plugin.rest.service.RestTestCaseFolderService;
import org.squashtest.tm.plugin.rest.validators.TestCaseFolderPatchValidator;
import org.squashtest.tm.plugin.rest.validators.TestCaseFolderPostValidator;
import org.squashtest.tm.service.testcase.TestCaseLibraryNavigationService;

import javax.inject.Inject;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

@RestApiController(TestCaseFolder.class)
@UseDefaultRestApiConfiguration
@SuppressWarnings("rawtypes")
public class RestTestCaseFolderController extends BaseRestController{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(RestTestCaseFolderController.class);

	
	@Inject
	private RestTestCaseFolderService service;

	@Inject
	private TestCaseFolderPostValidator testCaseFolderPostValidator;

	@Inject
	private ResourceLinksHelper linksHelper;

	@Inject
	private TestCaseFolderPatchValidator testCaseFolderPatchValidator;

	private TestCaseLibraryNavigationService testCaseLibraryNavigationService;
	
	@RequestMapping("/test-case-folders")
	@ResponseBody
	@DynamicFilterExpression("name")
	public ResponseEntity<PagedResources<Resource>> findAllReadableTestCaseFolders( Pageable pageable){
		
		Page<TestCaseFolder> folders = service.findAllReadable(pageable);
		
		PagedResources<Resource> res = toPagedResources(folders);
		
		return ResponseEntity.ok(res);
		
	}
	
	@RequestMapping("/test-case-folders/{id}")
	@EntityGetter
	@ResponseBody
	@DynamicFilterExpression("*, parent[name], project[name]")
	public ResponseEntity<Resource<TestCaseFolder>> findTestCaseFolder(@PathVariable("id") long id){
		
		TestCaseFolder folder = service.getOne(id);
		
		Resource<TestCaseFolder> res = toResource(folder);
		
		res.add(createLinkTo(folder.getProject()));
		res.add(createRelationTo("content"));
		res.add(createRelationTo("attachments"));

		return ResponseEntity.ok(res);
	}
	
	@RequestMapping("/test-case-folders/{id}/content")
	@ResponseBody
	@DynamicFilterExpression("name,reference")
	public ResponseEntity<PagedResources<Resource>> findTestCaseFolderContent(@PathVariable("id") long folderId, 
																				Pageable pageable, 
																				ContentInclusion include){
		
		Page<TestCaseLibraryNode> content = null;
		switch(include){
		case NESTED :
			content = service.findFolderAllContent(folderId, pageable); break;
		default :
			content = service.findFolderContent(folderId, pageable); break;
		}
		
		PagedResources<Resource> res = toPagedResourcesWithRel(content, "content");
		
		return ResponseEntity.ok(res);
	}

	/*AMK: add  new folder*/
	@RequestMapping(value = "/test-case-folders", method = RequestMethod.POST)
	@ResponseBody
	@DynamicFilterExpression("*, parent[name], project[name]")
	public ResponseEntity<Resource<TestCaseFolder>> createCampaignFolder(@RequestBody TestCaseFolderDto folderDto) throws BindException, InvocationTargetException, IllegalAccessException {

		testCaseFolderPostValidator.validatePostTestCaseFolder(folderDto);

		TestCaseFolder folder = service.addTestCaseFolder(folderDto);

		Resource<TestCaseFolder> res = toResource(folder);

		linksHelper.addAllLinksForTestCaseFolder(res);


		return ResponseEntity.status(HttpStatus.CREATED).body(res);
	}
	/*AMK : modify folder*/
	@RequestMapping(value = "/test-case-folders/{id}", method = RequestMethod.PATCH)
	@ResponseBody
	@DynamicFilterExpression("*, parent[name], project[name]")
	public ResponseEntity<Resource<TestCaseFolder>> patchTestCaseFolder(@RequestBody TestCaseFolderDto folderPatch, @PathVariable("id") long id) throws BindException {
		folderPatch.setId(id);

		testCaseFolderPatchValidator.validatePatchTestCaseFolder(folderPatch);

		TestCaseFolder folder = service.patchTestCaseFolder(folderPatch, id);

		Resource<TestCaseFolder> res = toResource(folder);

		linksHelper.addAllLinksForTestCaseFolder(res);

		return ResponseEntity.ok(res);

	}

	 /*AMK: delete test case folder*/

    @RequestMapping(value = "/test-case-folders/{ids}", method = RequestMethod.DELETE)
    @ResponseBody
    public ResponseEntity<Void> deleteTestCaseFolder(@PathVariable("ids")  List<Long>  folderIds){

		service.deleteFolder(folderIds);

        return ResponseEntity.noContent().build();
    }
}
