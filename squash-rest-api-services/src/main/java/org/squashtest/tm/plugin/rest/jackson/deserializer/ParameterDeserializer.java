/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.jackson.deserializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.jsontype.TypeDeserializer;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Configurable;
import org.squashtest.tm.domain.testcase.Parameter;

import javax.inject.Inject;
import java.io.IOException;

/**
 * Created by jthebault on 08/06/2017.
 */
@Configurable
public class ParameterDeserializer extends JsonDeserializer<Parameter> {

    @Inject
    private JsonCrawler jsonCrawler;

    @Override
    public Parameter deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        return doDeserialize(p);
    }

    @Override
    public Object deserializeWithType(JsonParser p, DeserializationContext ctxt, TypeDeserializer typeDeserializer) throws IOException {
        return doDeserialize(p);
    }

    private Parameter doDeserialize(JsonParser p) throws IOException {
        JsonNode node = p.getCodec().readTree(p);

        Parameter parameter = new Parameter();

        JsonNode nameNode = node.get("name");
        if(nameNode!= null && StringUtils.isNotBlank(nameNode.textValue())){
            parameter.setName(nameNode.textValue());
        }

        JsonNode descriptionNode = node.get("description");
        if(descriptionNode!= null && StringUtils.isNotBlank(descriptionNode.textValue())){
            parameter.setDescription(descriptionNode.textValue());
        }

        return parameter;
    }


}
