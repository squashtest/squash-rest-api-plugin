/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.jackson.mixin;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.databind.annotation.JsonAppend;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.squashtest.tm.domain.execution.Execution;
import org.squashtest.tm.domain.execution.ExecutionStatus;
import org.squashtest.tm.domain.testcase.TestStep;
import org.squashtest.tm.plugin.rest.core.jackson.SerializeChainConverter;
import org.squashtest.tm.plugin.rest.jackson.model.RestCustomFieldMembers;
import org.squashtest.tm.plugin.rest.jackson.serializer.AttachmentHolderPropertyWriter;
import org.squashtest.tm.plugin.rest.jackson.serializer.CustomFieldValuesPropertyWriter;
import org.squashtest.tm.plugin.rest.jackson.serializer.DenormalizedCustomFieldValuesPropertyWriter;
import org.squashtest.tm.plugin.rest.jackson.serializer.HateoasWrapperConverter;
import org.squashtest.tm.plugin.rest.jackson.serializer.UnauthorizedResourcesConverter;

import java.util.Date;

/**
 * Created by jthebault on 20/06/2017.
 */
@JsonAutoDetect(fieldVisibility= JsonAutoDetect.Visibility.NONE, getterVisibility= JsonAutoDetect.Visibility.NONE, isGetterVisibility= JsonAutoDetect.Visibility.NONE)
@JsonPropertyOrder({"_type", "id","execution_status","action", "expected_result","comment","last_executed_by","last_executed_on","execution_step_order"})
@JsonAppend(props={
        @JsonAppend.Prop(name="custom_fields",type = RestCustomFieldMembers.class, value=CustomFieldValuesPropertyWriter.class),
        @JsonAppend.Prop(name="test_step_custom_fields",type = RestCustomFieldMembers.class, value= DenormalizedCustomFieldValuesPropertyWriter.class),
        @JsonAppend.Prop(name = "attachments", value = AttachmentHolderPropertyWriter.class)
}
)@JsonTypeName("execution-step")
public abstract class RestExecutionStepMixin extends RestIdentifiedMixin{

    @JsonProperty
    private String action;

    @JsonProperty("expected_result")
    private String expectedResult;

    @JsonProperty("execution_status")
    private ExecutionStatus executionStatus = ExecutionStatus.READY;

    @JsonProperty
    private String comment;

    @JsonProperty("last_executed_by")
    private String lastExecutedBy;

    @JsonProperty("last_executed_on")
    private Date lastExecutedOn;

    @JsonProperty("referenced_test_step")
    @SerializeChainConverter(converters={
            HateoasWrapperConverter.class,
            UnauthorizedResourcesConverter.class
    })
    @JsonTypeInfo(use= JsonTypeInfo.Id.NONE)
    private TestStep referencedTestStep;

    @JsonProperty("execution")
    @JsonSerialize(converter=HateoasWrapperConverter.class)
    @JsonTypeInfo(use= JsonTypeInfo.Id.NONE)
    private Execution execution;

    @JsonProperty("execution_step_order")
    private Integer executionStepOrder;

}

