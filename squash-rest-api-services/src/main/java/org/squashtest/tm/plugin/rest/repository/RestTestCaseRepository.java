/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.repository;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.domain.testcase.TestStep;

import java.util.Collection;
@Repository
public interface RestTestCaseRepository extends JpaRepository<TestCase, Long>{

	
	@Query("from TestCase tc where tc.project.id in (:projectIds)")
	Page<TestCase> findAllInProjects(@Param("projectIds") Collection<Long> projectIds, Pageable pageable);
	
	@Query("select steps from TestCase tc join tc.steps steps where tc.id = :tcid order by index(steps)")
	Page<TestStep> findTestCaseSteps(@Param("tcid") long tcid, Pageable pageable);

	@Query("select tc from TestCase tc " +
			"left join fetch tc.parameters " +
			"left join fetch tc.datasets " +
			"left join fetch tc.requirementVersionCoverages " +
			"left join fetch tc.steps " +
			"left join fetch tc.attachmentList list " +
			"left join fetch list.attachments " +
			"where tc.id = :id")
	TestCase retrieveById(@Param("id") Long id);

	@Query("select distinct tc from TestCase tc left join ScriptedTestCase stc on stc.id = tc.id " +
												"left join KeywordTestCase ktc on ktc.id = tc.id " +
												"where tc.project.id in (:projectIds) " +
												"and stc.id is null " +
												"and ktc.id is null")
	Page<TestCase> findAllStandardTestCasesInProject(@Param("projectIds") Collection<Long> projectIds, Pageable paging);

	@Query("select distinct tc from TestCase tc inner join ScriptedTestCase stc on stc.id = tc.id where tc.project.id in (:projectIds)")
	Page<TestCase> findAllScriptedTestCasesInProject(@Param("projectIds") Collection<Long> projectIds, Pageable pageable);

	@Query("select distinct tc from TestCase tc inner join KeywordTestCase ktc on ktc.id = tc.id where tc.project.id in (:projectIds)")
	Page<TestCase> findAllKeywordTestCasesInProject(@Param("projectIds") Collection<Long> projectIds, Pageable pageable);

}
