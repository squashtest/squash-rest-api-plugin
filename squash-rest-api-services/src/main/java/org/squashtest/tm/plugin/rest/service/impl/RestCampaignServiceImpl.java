/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.service.impl;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.domain.IdCollector;
import org.squashtest.tm.domain.campaign.Campaign;
import org.squashtest.tm.domain.campaign.CampaignTestPlanItem;
import org.squashtest.tm.domain.campaign.Iteration;
import org.squashtest.tm.domain.customfield.RawValue;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.plugin.rest.jackson.model.CampaignDto;
import org.squashtest.tm.plugin.rest.jackson.model.ParentEntity;
import org.squashtest.tm.plugin.rest.repository.RestCampaignRepository;
import org.squashtest.tm.plugin.rest.repository.RestCampaignTestPlanItemRepository;
import org.squashtest.tm.plugin.rest.repository.RestIterationRepository;
import org.squashtest.tm.plugin.rest.service.RestCampaignService;
import org.squashtest.tm.plugin.rest.service.RestInternalCustomFieldValueUpdaterService;
import org.squashtest.tm.plugin.rest.service.helper.CustomFieldValueHelper;
import org.squashtest.tm.service.campaign.CampaignLibraryNavigationService;
import org.squashtest.tm.service.campaign.CampaignModificationService;
import org.squashtest.tm.service.project.ProjectFinder;

import javax.inject.Inject;
import javax.persistence.EntityNotFoundException;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static java.util.Objects.isNull;

/**
 * Created by jthebault on 16/06/2017.
 */
@Service
@Transactional
public class RestCampaignServiceImpl implements RestCampaignService {

    @Inject
    private RestCampaignRepository campaignRepository;

    @Inject
    private RestIterationRepository iterationRepository;

    @Inject
    private RestCampaignTestPlanItemRepository ctpiRepository;

    @Inject
    private ProjectFinder projectFinder;

    @Inject
    private CampaignLibraryNavigationService campaignLibraryNavigationService;

    @Inject
    private CampaignModificationService campaignModificationService;

    @Inject
    private CustomFieldValueHelper customFieldValueConverter;

    @Inject
    private CampaignPatcher campaignPatcher;

    @Inject
    private RestInternalCustomFieldValueUpdaterService internalCufService;

    @Override
    @Transactional(readOnly=true)
    @PreAuthorize("@apiSecurity.hasPermission(#campaignId, 'org.squashtest.tm.domain.campaign.Campaign', 'READ')")
    public Campaign getOne(Long campaignId) {
        return campaignRepository.getOne(campaignId);
    }

    @Override
    public Campaign createCampaign(CampaignDto campaignDto) {
        Campaign campaign = new Campaign();
        campaign.setName(campaignDto.getName());

        campaignPatcher.patch(campaign, campaignDto);

        campaign.setActualStartAuto(campaignDto.isActualStartAuto());
        campaign.setActualEndAuto(campaignDto.isActualEndAuto());

        Map<Long, RawValue> customFieldRawValues = customFieldValueConverter.convertCustomFieldDtoToMap(campaignDto.getCustomFields());

        ParentEntity parent = campaignDto.getParent();

        switch (parent.getRestType()) {
            case PROJECT:
                campaignLibraryNavigationService.addCampaignToCampaignLibrary(parent.getId(), campaign, customFieldRawValues);
                break;

            case CAMPAIGN_FOLDER:
                campaignLibraryNavigationService.addCampaignToCampaignFolder(parent.getId(), campaign, customFieldRawValues);
                break;

            default:
                throw new IllegalArgumentException("Programmatic error : Rest type " + parent.getRestType() + "is not a valid parent. You should validate this before.");
        }

        // after persist campaign
        fillTimePeriod(campaignDto, campaign.getId());

        return campaign;
    }

    @Override
    @PreAuthorize("@apiSecurity.hasPermission(#id,'org.squashtest.tm.domain.campaign.Campaign' , 'WRITE')")
    public Campaign patchCampaign(CampaignDto campaignDto, long id) {
        Campaign campaign = getOne(id);
        if (StringUtils.isNotBlank(campaignDto.getName())) {
            campaignModificationService.rename(campaign.getId(), campaignDto.getName());
        }
        campaignPatcher.patch(campaign, campaignDto);
        fillTimePeriod(campaignDto, campaign.getId());

        internalCufService.mergeCustomFields(campaign, campaignDto.getCustomFields());

        return campaign;
    }

    @Override
    public void deleteCampaignsByIds(List<Long> campaignIds) {
        campaignLibraryNavigationService.deleteNodes(campaignIds);
    }

    @Override
    @Transactional(readOnly=true)
    public Page<Campaign> findAllReadable(Pageable pageable) {
        Collection<Project> projects = projectFinder.findAllReadable();
        Collection<Long> ids = CollectionUtils.collect(projects, new IdCollector());
        if (ids.isEmpty()) {
            return new PageImpl<>(Collections.<Campaign>emptyList(), pageable, 0);
        } else {
            return campaignRepository.findAllInProjects(ids, pageable);
        }
    }

    @Override
    @Transactional(readOnly=true)
    @PreAuthorize("@apiSecurity.hasPermission(#campaignId, 'org.squashtest.tm.domain.campaign.Campaign', 'READ')")
    public Page<Iteration> findIterations(long campaignId, Pageable pageable) {
        return iterationRepository.findAllByCampaign_Id(campaignId,pageable);
    }

    @Override
    @Transactional(readOnly=true)
    @PreAuthorize("@apiSecurity.hasPermission(#campaignId, 'org.squashtest.tm.domain.campaign.Campaign', 'READ')")
    public Page<CampaignTestPlanItem> findTestPlan(long campaignId, Pageable pageable) {
        return ctpiRepository.findAllByCampaign_Id(campaignId,pageable);
    }

    @Override
    @Transactional(readOnly=true)
    public Page<Campaign> findAllByName(String campaignName, Pageable pageable) {
        Page<Campaign> campaign = campaignRepository.findAllByName(campaignName, pageable);
        if (isNull(campaign)) {
            throw new EntityNotFoundException("Unable to find org.squashtest.tm.domain.campaign.Campaign with name " + campaignName);
        }
        return campaign;
    }

    private void fillTimePeriod(CampaignDto campaignDto, long campaignId) {
        if (campaignDto.getScheduledStartDate() != null) {
            campaignModificationService.changeScheduledStartDate(campaignId, campaignDto.getScheduledStartDate());
        }

        if (campaignDto.getScheduledEndDate() != null) {
            campaignModificationService.changeScheduledEndDate(campaignId, campaignDto.getScheduledEndDate());
        }

        if (campaignDto.getActualStartDate() != null) {
            campaignModificationService.changeActualStartDate(campaignId, campaignDto.getActualStartDate());
        }

        if(campaignDto.getActualEndDate() != null) {
            campaignModificationService.changeActualEndDate(campaignId, campaignDto.getActualEndDate());
        }


    }
}
