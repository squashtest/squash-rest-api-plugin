/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.validators;

import org.springframework.stereotype.Component;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.squashtest.tm.domain.customfield.BindableEntity;
import org.squashtest.tm.plugin.rest.controller.helper.ErrorHandlerHelper;
import org.squashtest.tm.plugin.rest.jackson.model.RestType;
import org.squashtest.tm.plugin.rest.jackson.model.TestCaseFolderDto;
import org.squashtest.tm.plugin.rest.validators.helper.TestCaseFolderDtoValidationHelper;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

@Component
public class TestCaseFolderPatchValidator implements Validator {


    private static final String PATCH_TESTCASE_FOLDER = "patch-test-Case-folder";

    @Inject
    private TestCaseFolderDtoValidationHelper testCaseFolderDtoValidationHelper;

    @Override
    public boolean supports(Class<?> clazz) {
        return TestCaseFolderDto.class.equals(clazz);
    }

    @Override
    public void validate(Object target, final Errors errors) {
        TestCaseFolderDto folderPatch = (TestCaseFolderDto) target;
        testCaseFolderDtoValidationHelper.checkEntityExist(errors, RestType.TEST_CASE_FOLDER, folderPatch.getId());
        testCaseFolderDtoValidationHelper.loadProject(folderPatch);
        testCaseFolderDtoValidationHelper.checkParent(errors, folderPatch, RestType.TEST_CASE_FOLDER);
        testCaseFolderDtoValidationHelper.checkCufs(errors, folderPatch, BindableEntity.TESTCASE_FOLDER);
    }

    public void validatePatchTestCaseFolder(TestCaseFolderDto patch) throws BindException {
        List<Errors> errors = new ArrayList<>();

        BindingResult validation = new BeanPropertyBindingResult(patch, PATCH_TESTCASE_FOLDER);
        validate(patch, validation);

        if (validation.hasErrors()) {
            errors.add(validation);
        }
        ErrorHandlerHelper.throwIfError(patch, errors, PATCH_TESTCASE_FOLDER);
    }

}
