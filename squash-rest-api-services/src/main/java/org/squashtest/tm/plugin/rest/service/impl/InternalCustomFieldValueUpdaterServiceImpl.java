/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.domain.customfield.BoundEntity;
import org.squashtest.tm.domain.customfield.CustomFieldValue;
import org.squashtest.tm.domain.customfield.RawValue;
import org.squashtest.tm.domain.denormalizedfield.DenormalizedFieldHolder;
import org.squashtest.tm.domain.denormalizedfield.DenormalizedFieldValue;
import org.squashtest.tm.plugin.rest.jackson.model.CustomFieldValueDto;
import org.squashtest.tm.plugin.rest.jackson.model.WrappedDtoWithCustomFields;
import org.squashtest.tm.plugin.rest.jackson.model.WrappedDtoWithDenormalizedFields;
import org.squashtest.tm.plugin.rest.service.RestInternalCustomFieldValueUpdaterService;
import org.squashtest.tm.service.customfield.CustomFieldValueFinderService;
import org.squashtest.tm.service.denormalizedfield.DenormalizedFieldValueManager;

import javax.inject.Inject;
import java.util.List;

/**
 * Created by benoit on 23/07/17.
 */
@Service
@Transactional
class InternalCustomFieldValueUpdaterServiceImpl implements RestInternalCustomFieldValueUpdaterService{

    @Inject
    private CustomFieldValueFinderService cufService;

    @Inject
    private DenormalizedFieldValueManager denoService;

    @Override
    public void mergeCustomFields(WrappedDtoWithCustomFields<?> wrappedDto) {
        mergeCustomFields(wrappedDto.getWrapped(), wrappedDto.getCustomFields());
    }

    @Override
    public void mergeCustomFields(BoundEntity entity, List<CustomFieldValueDto> dtos) {

        if (dtos == null || dtos.isEmpty()){
            return;
        }

        List<CustomFieldValue> currentValues = cufService.findAllCustomFieldValues(entity);

        for (CustomFieldValueDto dto : dtos){

            String code = dto.getCode();
            RawValue value = dto.getValue();
            CustomFieldValue targetValue = locateActualValue(currentValues, code);
            if (targetValue != null && value != null){
                value.setValueFor(targetValue);
            }

        }

    }

    @Override
    public void mergeDenormalizedFields(WrappedDtoWithDenormalizedFields wrappedDto) {
        // if you encounter ClassCastException, consider that you have done a poor job
        DenormalizedFieldHolder wrapped = wrappedDto.getWrapped();
        List<CustomFieldValueDto> dtos = wrappedDto.getDenormalizedFields();
        mergeDenormalizedFields(wrapped, dtos);
    }

    @Override
    public void mergeDenormalizedFields(DenormalizedFieldHolder entity, List<CustomFieldValueDto> dtos) {

        if (dtos == null || dtos.isEmpty()){
            return;
        }

        List<DenormalizedFieldValue> currentValues = denoService.findAllForEntity(entity);

        for (CustomFieldValueDto dto : dtos){
            String code = dto.getCode();
            RawValue value = dto.getValue();
            DenormalizedFieldValue targetValue = locateDenormalizedValue(currentValues, code);
            if (targetValue != null && value != null){
                value.setValueFor(targetValue);
            }
        }

    }


    private CustomFieldValue locateActualValue(List<CustomFieldValue> currentValues, String code){
        for (CustomFieldValue cfv : currentValues){
            if (cfv.getCustomField().getCode().equals(code)){
                return cfv;
            }
        }
        return null;
    }


    private DenormalizedFieldValue locateDenormalizedValue(List<DenormalizedFieldValue> currentValues, String code){
        for (DenormalizedFieldValue dfv : currentValues){
            if (dfv.getCode().equals(code)){
                return dfv;
            }
        }
        return null;
    }
}
