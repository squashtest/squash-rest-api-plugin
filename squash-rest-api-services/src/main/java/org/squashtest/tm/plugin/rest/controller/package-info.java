/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * <h3>Controller Guidelines</h3>
 * 
 * <p> A controller :  
 * 
 * 	<ul>
 * 		<li>
 * 			<em>MUST</em> : Use the annotation {@link org.springframework.data.rest.webmvc.RestApiController}, in order to be detected as such. You must also supply wich
 * 			domain entity that controller is used for.
 * 		</li>
 * 
 * 		<li>
 * 			<em>MUST</em> : extends from BaseRestController
 * 		</li>
 * 		
 * 		<li><em>MUST NOT</em> use {@link org.springframework.web.bind.annotation.RequestMapping} NOR {@link org.springframework.stereotype.Controller} AT THE TOP LEVEL ! 
 * 			If you do that, your mapping will be also
 * 			registered as a regular handler mapping because those annotations are the markers by which Spring identifies the main controllers. The spurious registration of 
 * 			a Rest API controller can potentially conflict with existing URL. And you will know it only when a user encounters the bug as it 
 * 			will stay invisible at boot time.
 * 		</li>
 * 
 * 		<li>
 * 			<em>SHOULD</em> : use the annotation {@link org.squashtest.tm.plugin.rest.web.UseDefaultRestApiConfiguration}, which will configure the request mapping with
 * 			extra goodies like the MIME type produced, how to marshall the response etc. If you need something more specific just don't use it and configure the mapping 
 * 			manually (see below)
 * 		</li>
 * 
 * 		<li>
 * 			<em>MUST</em> : use {@link org.springframework.web.bind.annotation.RequestMapping} only. Spring HATEOAS doesn't like {@link org.springframework.web.bind.annotation.GetMapping} 
 * 			nor {@link org.springframework.web.bind.annotation.PostMapping}
 * 		</li>
 * 
 * 		<li>
 * 			<em>MUST</em> : use the inherited method #fromBasePath to build the HATEOAS links. If not, the base path will not be recognized and included in the URL.
 * 		</li>
 * 
 * 		<li>
 * 			<em>SHOULD</em> : annotate the method that fetches the entity by id with {@link org.squashtest.tm.plugin.rest.web.EntityGetter}. That method should accept 
 * 			a {@link java.lang.Long} as only parameter (that is, the entity id). The entity fetched must be the one specified in RestApiController. If that getter should also serve subclasses
 * 			of that type, you may specify those subclass types as the value of EntityGetter. 
 * 		</li>
 * 
 * 		<li>
 * 			<em>CAN</em> : use the inherited method #createSelfLink to create the self link to a resource (it's a convenience method). Requires that an {@link org.squashtest.tm.plugin.rest.web.EntityGetter}
 * 			method has been defined.  
 * 		</li>
 * 
 * 		<li>
 * 			<em>SHOULD</em> : specify the annotation {@link org.squashtest.tm.plugin.rest.jackson.filter.DynamicFilterExpression} on controller methods that return non trivial json.
 * 		</li>
 *
 * 		<li>
 * 		    <em>SHOULD NOT</em> : return any content when adding a new element to a resource's collection (example: adding a new member to a team, should get an empty response with a http status code 204)
 * 		</li>
 * 	</ul>
 * 
 * </p>
 * 
 */

package org.squashtest.tm.plugin.rest.controller;