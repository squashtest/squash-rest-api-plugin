/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.controller;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.PagedResources;
import org.springframework.hateoas.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.squashtest.tm.domain.campaign.IterationTestPlanItem;
import org.squashtest.tm.domain.execution.Execution;
import org.squashtest.tm.plugin.rest.controller.helper.ResourceLinksHelper;
import org.squashtest.tm.plugin.rest.core.jackson.DynamicFilterExpression;
import org.squashtest.tm.plugin.rest.core.web.BaseRestController;
import org.squashtest.tm.plugin.rest.core.web.EntityGetter;
import org.squashtest.tm.plugin.rest.core.web.RestApiController;
import org.squashtest.tm.plugin.rest.core.web.UseDefaultRestApiConfiguration;
import org.squashtest.tm.plugin.rest.jackson.model.IterationTestPlanItemDto;
import org.squashtest.tm.plugin.rest.service.RestIterationTestPlanItemService;
import org.squashtest.tm.plugin.rest.validators.IterationTestPlanItemPostValidator;

import javax.inject.Inject;
import java.util.List;

/**
 * Created by jthebault on 16/06/2017.
 */
@RestApiController(IterationTestPlanItem.class)
@UseDefaultRestApiConfiguration
public class RestIterationTestPlanItemController extends BaseRestController {

    public static final String ITPI_DYNAMIC_FILTER = "*, referenced_test_case[name, reference], referenced_dataset[name], iteration[name,reference], executions[execution_status,last_executed_by,last_executed_on]";

    @Inject
    private RestIterationTestPlanItemService service;
    @Inject
    private IterationTestPlanItemPostValidator iterationTestPlanItemValidator;
    @Inject
    private IterationTestPlanItemPostValidator iterationTestPlanItemPostValidator;



    @Inject
    private ResourceLinksHelper linksHelper;

    @RequestMapping(value = "/iteration-test-plan-items/{id}", method = RequestMethod.GET)
    @EntityGetter
    @ResponseBody
    @DynamicFilterExpression(ITPI_DYNAMIC_FILTER)
    public ResponseEntity<Resource<IterationTestPlanItem>> findIterationTestPlanItem(@PathVariable("id") long id) {

        IterationTestPlanItem itpi = service.getOne(id);

        Resource<IterationTestPlanItem> res = toResource(itpi);

        linksHelper.addAllLinksForIterationTestPlanItem(res);

       /* res.add(linkService.createLinkTo(itpi.getProject()));

        TestCase referencedTestCase = itpi.getReferencedTestCase();
        if (referencedTestCase != null) {
            res.add(createLinkTo(referencedTestCase));
        }

        Dataset referencedDataset = itpi.getReferencedDataset();
        if (referencedDataset != null) {
            res.add(createLinkTo(referencedDataset));
        }

        res.add(createLinkTo(itpi.getIteration()));
        res.add(createRelationTo("executions"));*/

        return ResponseEntity.ok(res);
    }

    @RequestMapping(value="/iteration-test-plan-items/{id}/executions", method = RequestMethod.GET)
    @ResponseBody
    @DynamicFilterExpression("name, execution_order, execution_status, last_executed_by, last_executed_on")
    public ResponseEntity<PagedResources<Resource>> findItpiExecutions(@PathVariable("id") long itpiId, Pageable pageable){

        Page<Execution> execs = service.findExecutions(itpiId, pageable);

        PagedResources<Resource> res = toPagedResourcesWithRel(execs, "executions");

        return ResponseEntity.ok(res);

    }

    @RequestMapping(value="/iteration-test-plan-items/{id}/executions", method=RequestMethod.POST )
    @ResponseBody
    @DynamicFilterExpression(RestExecutionController.EXECUTION_DYNAMIC_FILTER + ", -test_plan_item")
    public ResponseEntity<Resource<Execution>> createNewExecution(@PathVariable("id") long itpiId){

        Execution exec = service.createExecution(itpiId);

        Resource<Execution> res = toResource(exec);
        linksHelper.addAllLinksForExecution(res);

        return ResponseEntity.status(HttpStatus.CREATED).body(res);

    }
    
    
    /*
     * Hidden feature intended for the jira plugin. Will not be publicly documented. It exists 
     * because there are no search service that would fulfill such use case. 
     * See also RestRequirementController#findSynchronizedRequirement()
     */
    
    @RequestMapping(value="/iteration-test-plan-items", method = RequestMethod.GET, params={"remote_key", "server_name"})
    @ResponseBody
    @DynamicFilterExpression("id") // we want minimal informations on this
    public ResponseEntity<PagedResources<Resource>> findItemsByCoveredRemoteRequirement
    	(@RequestParam("remote_key") String remoteKey, @RequestParam("server_name") String serverName, Pageable pageable){
    	
    	Page<IterationTestPlanItem> items = service.findItemsByCoveredRemoteRequirement(pageable, remoteKey, serverName);
    	
    	PagedResources<Resource> res = toPagedResourcesWithRel(items, "iteration-test-plan-items");
    	
    	return ResponseEntity.ok(res);
    	
    }

    /*AMK: Modify test plan item to iteration*/

    @RequestMapping(value = "/iteration-test-plan-items/{id}", method = RequestMethod.PATCH)
    @ResponseBody
    @DynamicFilterExpression(ITPI_DYNAMIC_FILTER)
    public ResponseEntity<Resource<IterationTestPlanItem>> modifyTestPlanItemToCampaign(@RequestBody IterationTestPlanItemDto itpiDto,
                                                                                       @PathVariable("id") long testPlanId) throws BindException {

        //validation DTO DataSet assigned to
        iterationTestPlanItemValidator.validatePatchTestPlanItem(itpiDto,testPlanId);
        //change test Plan item
        IterationTestPlanItem itpi = service.modifyIterationTestPlan(itpiDto,testPlanId);

        Resource<IterationTestPlanItem> res = toResource(itpi);

        linksHelper.addAllLinksForIterationTestPlanItem(res);

        return ResponseEntity.ok(res);
    }

    /*AMK: delete iteration test plan item*/
    @ResponseBody
    @RequestMapping(value = "/iteration-test-plan-items/{testPlanItemsIds}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> removeTestPlanItemsFromIteration(@PathVariable("testPlanItemsIds") List<Long> testPlanItemsIds) {

        service.deleteIterationTestPlan(testPlanItemsIds);
        return ResponseEntity.noContent().build();
    }
    /*AMK: add test plan to iteration*/

    @RequestMapping(value = "/iterations/{iterationId}/test-plan", method = RequestMethod.POST)
    @ResponseBody
    @DynamicFilterExpression("*,iteration-test-plan-item, referenced_test_case, referenced_dataset ")
    /*addIterationTestPlain*/
    public  ResponseEntity<Resource<IterationTestPlanItem>>   addTestCasesToIteration(@RequestBody IterationTestPlanItemDto testPlanItemDto ,
                                                                                      @PathVariable("iterationId") long iterationId) throws BindException {

        //validation DTO DataSet assigned to
        iterationTestPlanItemPostValidator.validatePostTestPlanItem(testPlanItemDto,iterationId);

        //add test Plan item
        IterationTestPlanItem itp = service.addIterationTestPlanItem(testPlanItemDto,iterationId);

        Resource<IterationTestPlanItem> res = toResource(itp);

        linksHelper.addAllLinksForIterationTestPlanItem(res);

        return ResponseEntity.status(HttpStatus.CREATED).body(res);
    }

}
