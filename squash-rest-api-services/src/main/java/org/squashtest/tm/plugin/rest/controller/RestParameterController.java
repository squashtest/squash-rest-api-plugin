/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.controller;

import org.springframework.hateoas.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.squashtest.tm.domain.testcase.Parameter;
import org.squashtest.tm.plugin.rest.core.jackson.DynamicFilterExpression;
import org.squashtest.tm.plugin.rest.core.web.BaseRestController;
import org.squashtest.tm.plugin.rest.core.web.EntityGetter;
import org.squashtest.tm.plugin.rest.core.web.RestApiController;
import org.squashtest.tm.plugin.rest.core.web.UseDefaultRestApiConfiguration;
import org.squashtest.tm.plugin.rest.jackson.model.ParameterDto;
import org.squashtest.tm.plugin.rest.service.RestParameterService;
import org.squashtest.tm.plugin.rest.validators.ParameterValidator;

import javax.inject.Inject;


@RestApiController(Parameter.class)
@UseDefaultRestApiConfiguration
public class RestParameterController extends BaseRestController {

	@Inject
	private RestParameterService restParameterService;
	@Inject
	private ParameterValidator parameterValidator;
	
	@RequestMapping("/parameters/{id}")
	@EntityGetter
	@DynamicFilterExpression("*,name,test_case[name]")
	public ResponseEntity<Resource<Parameter>> findParameter(@PathVariable("id") long id) {

        Parameter p = restParameterService.getOne(id);

        Resource<Parameter> res = new Resource<Parameter>(p);
		
		res.add(createSelfLink(p));
		
		return ResponseEntity.ok(res);
	}

	/*AMK: add parameter*/
	@RequestMapping(value="/parameters", method = RequestMethod.POST)
	@ResponseBody
	@DynamicFilterExpression("*,name,test_case[name]")

	public ResponseEntity<Resource<Parameter>> postParameter(@RequestBody ParameterDto parameterDto)throws BindException {
		parameterValidator.validatePostParameter(parameterDto);
		Parameter parameter = restParameterService.addParameter(parameterDto);
		Resource<Parameter> res = toResource(parameter);
		res.add(createSelfLink(parameter));
		return ResponseEntity.status(HttpStatus.CREATED).body(res);
	}
	/*AMK: modify parameter*/
	@RequestMapping(value = "/parameters/{id}", method = RequestMethod.PATCH)
	@ResponseBody
	@DynamicFilterExpression("*,name,test_case[name]")
	public ResponseEntity<Resource<Parameter>> ModifyParameter(@RequestBody ParameterDto parameterDto ,
															   @PathVariable("id") Long parameterId)throws BindException {
		parameterValidator.validatePatchParameter(parameterDto, parameterId);
		Parameter parameter = restParameterService.modifyParameter(parameterDto);
		Resource<Parameter> res = toResource(parameter);
		res.add(createSelfLink(parameter));
		return ResponseEntity.ok(res);
	}

	/*AMK: delete parameter*/
	@RequestMapping(value = "/parameters/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> deleteParameter(@PathVariable("id") Long parameterId) {
		restParameterService.deleteParameter(parameterId);
		return ResponseEntity.noContent().build();
	}

}
