/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.controller

import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.data.domain.PageImpl
import org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders
import org.squashtest.tm.domain.execution.ExecutionStep
import org.squashtest.tm.domain.testautomation.TestAutomationServerKind
import org.squashtest.tm.plugin.docutils.DocumentationSnippets
import org.squashtest.tm.plugin.mockmvc.BaseControllerSpec
import org.squashtest.tm.plugin.rest.service.RestExecutionService
import org.squashtest.tm.service.customfield.CustomFieldValueFinderService
import org.squashtest.tm.service.denormalizedfield.DenormalizedFieldValueManager
import org.squashtest.tm.test.domainbuilder.SquashEntityBuilder

import javax.inject.Inject

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import static org.squashtest.tm.plugin.docutils.DocumentationSnippets.DescriptorLists
import static org.squashtest.tm.plugin.docutils.DocumentationSnippets.documentationBuilder
import static org.squashtest.tm.plugin.mockmvc.TestHelper.selfRelIs
import static org.squashtest.tm.plugin.mockmvc.TestHelper.withResult

/**
 * Created by jlor on 24/07/2017.
 */
@WebMvcTest(RestExecutionController)
public class RestExecutionControllerIT extends BaseControllerSpec {

    @Inject
    RestExecutionService restExecutionService;

    @Inject
    private CustomFieldValueFinderService cufService;

    @Inject
    private DenormalizedFieldValueManager denormCufService;

    def "get-execution-of-standard-test-case"() {

        given:

        def exec = SquashEntityBuilder.execution {

            referencedTestCase = SquashEntityBuilder.testCase {
            }

            id = 56l
            name = "sample test case 56"
            executionOrder = 4
            executionStatus "BLOCKED"

            lastExecutedBy = "User-5"
            lastExecutedOn "2017/07/24"

            executionMode "AUTOMATED"
            reference = "SAMP_EXEC_56"
            datasetLabel = "sample dataset"

            steps = [
                    SquashEntityBuilder.executionStep {
                        id = 22l
                        executionStatus "SUCCESS"
                        action = "<p>First action</p>"
                        expectedResult = "<p>First result</p>"
                    },
                    SquashEntityBuilder.executionStep {
                        id = 23l
                        executionStatus "BLOCKED"
                        action = "<p>Second action</p>"
                        expectedResult = "<p>Second result</p>"
                    },
                    SquashEntityBuilder.executionStep {
                        id = 27l
                        executionStatus "SUCCESS"
                        action = "<p>The Action</p>"
                        expectedResult = "<p>The Result</p>"
                    }
            ]

            description = "<p>I have no comment</p>"
            prerequisite = "<p>Being alive.</p>"
            tcdescription = "<p>This is nice.</p>"
            importance "LOW"
            nature "NAT_SECURITY_TESTING"
            type "TYP_EVOLUTION_TESTING"
            status "APPROVED"
            testPlan = SquashEntityBuilder.iterationTestPlanItem {
                id = 15L
                iteration = SquashEntityBuilder.iteration {
                    campaign = SquashEntityBuilder.campaign {
                        project = SquashEntityBuilder.project {
                            id = 87L
                        }
                    }
                }
            }
        }

        def execCufs = [
                SquashEntityBuilder.cufValue {
                    label = "cuf text"
                    code = "CUF_TXT"
                    value = "cuf text value"
                },
                SquashEntityBuilder.cufValue {
                    label = "cuf text 2"
                    code = "CUF_TXT_2"
                    value = "cuf text value 2"
                }
        ] as List

        def tcCufs = [
                SquashEntityBuilder.denormCufValue {
                    label = "tc cuf text"
                    code = "TC_CUF_TXT"
                    value = "tc cuf text value"
                },
                SquashEntityBuilder.denormCufValue {
                    label = "tc cuf text 2"
                    code = "TC_CUF_TXT_2"
                    value = "tc cuf text value 2"
                }
        ] as List

        def autoTest = SquashEntityBuilder.automatedTest {
            id = 569L
            name = "auto test"
            project = SquashEntityBuilder.testAutomatioProject {
                id = 569L
                jobName = "job 1"
                label = "wan wan"
                server = SquashEntityBuilder.testAutomationServer {
                    id = 569L
                    name = "TA server"
                    url = "http://1234:4567/jenkins/"
                    login = "ajenkins"
                    password = "password"
                    kind = TestAutomationServerKind.jenkins
                    description = "<p>nice try</p>"
                }
                tmProject = SquashEntityBuilder.project {
                    id = 367L
                    description = "<p>This project is the main sample project</p>"
                    label = "Main Sample Project"
                    active = true
                }
                slaves = "no slavery"
            }
        }

        def extender = SquashEntityBuilder.automatedExecutionExtender {
            id = 778L
            automatedTest = autoTest
            execution = exec
            resultURL = new URL("http://1234:4567/jenkins/report")
            resultSummary = "all right"
            nodeName = "no root"
        }

        exec.automatedExecutionExtender = extender

        and:

        restExecutionService.getOne(56) >> exec

        cufService.findAllCustomFieldValues(exec) >> execCufs
        denormCufService.findAllForEntity(exec) >> tcCufs

        when:

        def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/executions/{id}", 56)
                .header("Accept", "application/json"))

        then:

        /*
        * Test (using the TestHelper)
        * */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))

        withResult(res) {
            "_type".is "execution"
            "id".is 56
            "name".is "sample test case 56"

            "execution_order".is 4
            "execution_status".is "BLOCKED"
            "last_executed_by".is "User-5"
            "last_executed_on".is "2017-07-24T10:00:00.000+0000"
            "execution_mode".is "AUTOMATED"

            "reference".is "SAMP_EXEC_56"
            "dataset_label".is "sample dataset"

            "execution_steps".hasSize 3
            "execution_steps".test {
                "[0]".test {
                    "_type".is "execution-step"
                    "id".is 22
                    "execution_status".is "SUCCESS"
                    "action".is "<p>First action</p>"
                    "expected_result".is "<p>First result</p>"
                    selfRelIs "http://localhost:8080/api/rest/latest/execution-steps/22"
                }
                "[1]".test {
                    "_type".is "execution-step"
                    "id".is 23
                    "execution_status".is "BLOCKED"
                    "action".is "<p>Second action</p>"
                    "expected_result".is "<p>Second result</p>"
                    selfRelIs "http://localhost:8080/api/rest/latest/execution-steps/23"
                }
                "[2]".test {
                    "_type".is "execution-step"
                    "id".is 27
                    "execution_status".is "SUCCESS"
                    "action".is "<p>The Action</p>"
                    "expected_result".is "<p>The Result</p>"
                    selfRelIs "http://localhost:8080/api/rest/latest/execution-steps/27"
                }
            }

            "comment".is "<p>I have no comment</p>"
            "prerequisite".is "<p>Being alive.</p>"
            "description".is "<p>This is nice.</p>"
            "importance".is "LOW"
            "nature.code".is "NAT_SECURITY_TESTING"
            "type.code".is "TYP_EVOLUTION_TESTING"
            "test_case_status".is "APPROVED"

            "test_plan_item".test {
                "_type".is "iteration-test-plan-item"
                "id".is 15
                selfRelIs "http://localhost:8080/api/rest/latest/iteration-test-plan-items/15"
            }
            "custom_fields".hasSize 2
            "custom_fields".test {
                "[0]".test {
                    "code".is "CUF_TXT"
                    "label".is "cuf text"
                    "value".is "cuf text value"
                }
                "[1]".test {
                    "code".is "CUF_TXT_2"
                    "label".is "cuf text 2"
                    "value".is "cuf text value 2"
                }
            }

            "test_case_custom_fields".hasSize 2
            "test_case_custom_fields".test {
                "[0]".test {
                    "code".is "TC_CUF_TXT"
                    "label".is "tc cuf text"
                    "value".is "tc cuf text value"
                }
                "[1]".test {
                    "code".is "TC_CUF_TXT_2"
                    "label".is "tc cuf text 2"
                    "value".is "tc cuf text value 2"
                }
            }

            "automated_execution_extender.id".is 778

            "_links".test {
                "self.href".is "http://localhost:8080/api/rest/latest/executions/56"
                "project.href".is "http://localhost:8080/api/rest/latest/projects/87"
                "test_plan_item.href".is "http://localhost:8080/api/rest/latest/iteration-test-plan-items/15"
                "execution-steps.href".is "http://localhost:8080/api/rest/latest/executions/56/execution-steps"
            }
        }

        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "id : the id of the execution"
                    }
                    requestParams {
                        add DescriptorLists.fieldsParams
                    }
                    fields {
                        add "_type (string) : the type of the entity"
                        add "id (number) : the id of the execution"
                        add "name (string) : the name of the execution"
                        add "execution_order (number) : the order of the execution"
                        add "execution_status (string) : the status of the execution"
                        add "last_executed_by (string) : the date this execution was last executed"
                        add "last_executed_on (string) : the user who last executed this execution"
                        add "execution_mode (string) : the execution mode of the execution"
                        add "reference (string) : the reference of this execution"
                        add "dataset_label (string) : the label of the dataset used in this execution"
                        addAndStop "execution_steps (array) : the steps of this execution"
                        add "comment (string) : the comment of this execution"
                        add "prerequisite (string) : the prerequisite of this execution"
                        add "description (string) : the description of this execution"
                        add "importance (string) : the importance of this execution"
                        addAndStop "nature (object) : the nature of this execution"
                        addAndStop "type (object) : the type of this execution"
                        add "test_case_status (string) : the status of the test case referenced by this execution"
                        addAndStop "test_plan_item (object) : the test plan item referenced by this execution"
                        addAndStop "automated_execution_extender (object) : the automated execution extender referenced by this execution (will be hidden if execution mode is manual)"
                        addAndStop "custom_fields (array) : the denormalized custom fields of this execution"
                        addAndStop "test_case_custom_fields (array) : the custom fields of the referenced test case"
                        add "attachments (array) : the attachments of this execution"

                        add DescriptorLists.linksFields
                    }
                    _links {
                        add "self : link to this execution"
                        add "project : link to the project of this execution"
                        add "test_plan_item : link to the test plan item of this execution"
                        add "execution-steps : link to the execution steps of this execution"
                        add "attachments : link to the attachments of this execution"

                    }
                }
        ))
    }

    def "get-execution-of-scripted-test-case"() {

        given:

        def exec = SquashEntityBuilder.scriptedExecution {

            referencedTestCase = SquashEntityBuilder.scriptedTestCase {
            }

            id = 56l
            name = "sample test case 56"
            executionOrder = 4
            executionStatus "BLOCKED"

            lastExecutedBy = "User-5"
            lastExecutedOn "2017/07/24"

            executionMode "AUTOMATED"
            reference = "SAMP_EXEC_56"
            datasetLabel = "sample dataset"

            scriptName = "This is new script name"

            steps = [
                    SquashEntityBuilder.executionStep {
                        id = 22l
                        executionStatus "SUCCESS"
                        action = "<p>First action</p>"
                        expectedResult = "<p>First result</p>"
                    },
                    SquashEntityBuilder.executionStep {
                        id = 23l
                        executionStatus "BLOCKED"
                        action = "<p>Second action</p>"
                        expectedResult = "<p>Second result</p>"
                    },
                    SquashEntityBuilder.executionStep {
                        id = 27l
                        executionStatus "SUCCESS"
                        action = "<p>The Action</p>"
                        expectedResult = "<p>The Result</p>"
                    }
            ]

            description = "<p>I have no comment</p>"
            prerequisite = "<p>Being alive.</p>"
            tcdescription = "<p>This is nice.</p>"
            importance "LOW"
            nature "NAT_SECURITY_TESTING"
            type "TYP_EVOLUTION_TESTING"
            status "APPROVED"
            testPlan = SquashEntityBuilder.iterationTestPlanItem {
                id = 15L
                iteration = SquashEntityBuilder.iteration {
                    campaign = SquashEntityBuilder.campaign {
                        project = SquashEntityBuilder.project {
                            id = 87L
                        }
                    }
                }
            }
        }

        def execCufs = [
                SquashEntityBuilder.cufValue {
                    label = "cuf text"
                    code = "CUF_TXT"
                    value = "cuf text value"
                },
                SquashEntityBuilder.cufValue {
                    label = "cuf text 2"
                    code = "CUF_TXT_2"
                    value = "cuf text value 2"
                }
        ] as List

        def tcCufs = [
                SquashEntityBuilder.denormCufValue {
                    label = "tc cuf text"
                    code = "TC_CUF_TXT"
                    value = "tc cuf text value"
                },
                SquashEntityBuilder.denormCufValue {
                    label = "tc cuf text 2"
                    code = "TC_CUF_TXT_2"
                    value = "tc cuf text value 2"
                }
        ] as List

        def autoTest = SquashEntityBuilder.automatedTest {
            id = 569L
            name = "auto test"
            project = SquashEntityBuilder.testAutomatioProject {
                id = 569L
                jobName = "job 1"
                label = "wan wan"
                server = SquashEntityBuilder.testAutomationServer {
                    id = 569L
                    name = "TA server"
                    url = "http://1234:4567/jenkins/"
                    login = "ajenkins"
                    password = "password"
                    kind = TestAutomationServerKind.jenkins
                    description = "<p>nice try</p>"
                }
                tmProject = SquashEntityBuilder.project {
                    id = 367L
                    description = "<p>This project is the main sample project</p>"
                    label = "Main Sample Project"
                    active = true
                }
                slaves = "no slavery"
            }
        }

        def extender = SquashEntityBuilder.automatedExecutionExtender {
            id = 778L
            automatedTest = autoTest
            execution = exec
            resultURL = new URL("http://1234:4567/jenkins/report")
            resultSummary = "all right"
            nodeName = "no root"
        }

        exec.automatedExecutionExtender = extender

        and:

        restExecutionService.getOne(56) >> exec

        cufService.findAllCustomFieldValues(exec) >> execCufs
        denormCufService.findAllForEntity(exec) >> tcCufs

        when:

        def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/executions/{id}", 56)
                .header("Accept", "application/json"))

        then:

        /*
        * Test (using the TestHelper)
        * */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))

        withResult(res) {
            "_type".is "scripted-execution"
            "id".is 56
            "name".is "sample test case 56"

            "execution_order".is 4
            "execution_status".is "BLOCKED"
            "last_executed_by".is "User-5"
            "last_executed_on".is "2017-07-24T10:00:00.000+0000"
            "execution_mode".is "AUTOMATED"

            "reference".is "SAMP_EXEC_56"
            "dataset_label".is "sample dataset"

            "execution_steps".hasSize 3
            "execution_steps".test {
                "[0]".test {
                    "_type".is "execution-step"
                    "id".is 22
                    "execution_status".is "SUCCESS"
                    "action".is "<p>First action</p>"
                    "expected_result".is "<p>First result</p>"
                    selfRelIs "http://localhost:8080/api/rest/latest/execution-steps/22"
                }
                "[1]".test {
                    "_type".is "execution-step"
                    "id".is 23
                    "execution_status".is "BLOCKED"
                    "action".is "<p>Second action</p>"
                    "expected_result".is "<p>Second result</p>"
                    selfRelIs "http://localhost:8080/api/rest/latest/execution-steps/23"
                }
                "[2]".test {
                    "_type".is "execution-step"
                    "id".is 27
                    "execution_status".is "SUCCESS"
                    "action".is "<p>The Action</p>"
                    "expected_result".is "<p>The Result</p>"
                    selfRelIs "http://localhost:8080/api/rest/latest/execution-steps/27"
                }
            }

            "comment".is "<p>I have no comment</p>"
            "prerequisite".is "<p>Being alive.</p>"
            "description".is "<p>This is nice.</p>"
            "importance".is "LOW"
            "nature.code".is "NAT_SECURITY_TESTING"
            "type.code".is "TYP_EVOLUTION_TESTING"
            "test_case_status".is "APPROVED"

            "test_plan_item".test {
                "_type".is "iteration-test-plan-item"
                "id".is 15
                selfRelIs "http://localhost:8080/api/rest/latest/iteration-test-plan-items/15"
            }

            "script_name".is "This is new script name"

            "custom_fields".hasSize 2
            "custom_fields".test {
                "[0]".test {
                    "code".is "CUF_TXT"
                    "label".is "cuf text"
                    "value".is "cuf text value"
                }
                "[1]".test {
                    "code".is "CUF_TXT_2"
                    "label".is "cuf text 2"
                    "value".is "cuf text value 2"
                }
            }

            "test_case_custom_fields".hasSize 2
            "test_case_custom_fields".test {
                "[0]".test {
                    "code".is "TC_CUF_TXT"
                    "label".is "tc cuf text"
                    "value".is "tc cuf text value"
                }
                "[1]".test {
                    "code".is "TC_CUF_TXT_2"
                    "label".is "tc cuf text 2"
                    "value".is "tc cuf text value 2"
                }
            }

            "automated_execution_extender.id".is 778

            "_links".test {
                "self.href".is "http://localhost:8080/api/rest/latest/executions/56"
                "project.href".is "http://localhost:8080/api/rest/latest/projects/87"
                "test_plan_item.href".is "http://localhost:8080/api/rest/latest/iteration-test-plan-items/15"
                "execution-steps.href".is "http://localhost:8080/api/rest/latest/executions/56/execution-steps"
            }
        }

        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "id : the id of the execution"
                    }
                    requestParams {
                        add DescriptorLists.fieldsParams
                    }
                    fields {
                        add "_type (string) : the type of the entity"
                        add "id (number) : the id of the execution"
                        add "name (string) : the name of the execution"
                        add "execution_order (number) : the order of the execution"
                        add "execution_status (string) : the status of the execution"
                        add "last_executed_by (string) : the date this execution was last executed"
                        add "last_executed_on (string) : the user who last executed this execution"
                        add "execution_mode (string) : the execution mode of the execution"
                        add "reference (string) : the reference of this execution"
                        add "script_name (string) : the name of reference scripted test case of this execution"
                        add "dataset_label (string) : the label of the dataset used in this execution"
                        addAndStop "execution_steps (array) : the steps of this execution"
                        add "comment (string) : the comment of this execution"
                        add "prerequisite (string) : the prerequisite of this execution"
                        add "description (string) : the description of this execution"
                        add "importance (string) : the importance of this execution"
                        addAndStop "nature (object) : the nature of this execution"
                        addAndStop "type (object) : the type of this execution"
                        add "test_case_status (string) : the status of the test case referenced by this execution"
                        addAndStop "test_plan_item (object) : the test plan item referenced by this execution"
                        addAndStop "automated_execution_extender (object) : the automated execution extender referenced by this execution (will be hidden if execution mode is manual)"
                        addAndStop "custom_fields (array) : the denormalized custom fields of this execution"
                        addAndStop "test_case_custom_fields (array) : the custom fields of the referenced test case"
                        add "attachments (array) : the attachments of this execution"

                        add DescriptorLists.linksFields
                    }
                    _links {
                        add "self : link to this execution"
                        add "project : link to the project of this execution"
                        add "test_plan_item : link to the test plan item of this execution"
                        add "execution-steps : link to the execution steps of this execution"
                        add "attachments : link to the attachments of this execution"

                    }
                }
        ))
    }

    def "get-execution-of-keyword-test-case"() {

        given:

        def exec = SquashEntityBuilder.keywordExecution {

            referencedTestCase = SquashEntityBuilder.keywordTestCase {
            }

            id = 56l
            name = "sample test case 56"
            executionOrder = 4
            executionStatus "BLOCKED"

            lastExecutedBy = "User-5"
            lastExecutedOn "2017/07/24"

            executionMode "AUTOMATED"
            reference = "SAMP_EXEC_56"
            datasetLabel = "sample dataset"

            steps = [
                    SquashEntityBuilder.executionStep {
                        id = 22l
                        executionStatus "SUCCESS"
                        action = "<p>First action</p>"
                        expectedResult = "<p>First result</p>"
                    },
                    SquashEntityBuilder.executionStep {
                        id = 23l
                        executionStatus "BLOCKED"
                        action = "<p>Second action</p>"
                        expectedResult = "<p>Second result</p>"
                    },
                    SquashEntityBuilder.executionStep {
                        id = 27l
                        executionStatus "SUCCESS"
                        action = "<p>The Action</p>"
                        expectedResult = "<p>The Result</p>"
                    }
            ]

            description = "<p>I have no comment</p>"
            prerequisite = "<p>Being alive.</p>"
            tcdescription = "<p>This is nice.</p>"
            importance "LOW"
            nature "NAT_SECURITY_TESTING"
            type "TYP_EVOLUTION_TESTING"
            status "APPROVED"
            testPlan = SquashEntityBuilder.iterationTestPlanItem {
                id = 15L
                iteration = SquashEntityBuilder.iteration {
                    campaign = SquashEntityBuilder.campaign {
                        project = SquashEntityBuilder.project {
                            id = 87L
                        }
                    }
                }
            }
        }

        def execCufs = [
                SquashEntityBuilder.cufValue {
                    label = "cuf text"
                    code = "CUF_TXT"
                    value = "cuf text value"
                },
                SquashEntityBuilder.cufValue {
                    label = "cuf text 2"
                    code = "CUF_TXT_2"
                    value = "cuf text value 2"
                }
        ] as List

        def tcCufs = [
                SquashEntityBuilder.denormCufValue {
                    label = "tc cuf text"
                    code = "TC_CUF_TXT"
                    value = "tc cuf text value"
                },
                SquashEntityBuilder.denormCufValue {
                    label = "tc cuf text 2"
                    code = "TC_CUF_TXT_2"
                    value = "tc cuf text value 2"
                }
        ] as List

        def autoTest = SquashEntityBuilder.automatedTest {
            id = 569L
            name = "auto test"
            project = SquashEntityBuilder.testAutomatioProject {
                id = 569L
                jobName = "job 1"
                label = "wan wan"
                server = SquashEntityBuilder.testAutomationServer {
                    id = 569L
                    name = "TA server"
                    url = "http://1234:4567/jenkins/"
                    login = "ajenkins"
                    password = "password"
                    kind = TestAutomationServerKind.jenkins
                    description = "<p>nice try</p>"
                }
                tmProject = SquashEntityBuilder.project {
                    id = 367L
                    description = "<p>This project is the main sample project</p>"
                    label = "Main Sample Project"
                    active = true
                }
                slaves = "no slavery"
            }
        }

        def extender = SquashEntityBuilder.automatedExecutionExtender {
            id = 778L
            automatedTest = autoTest
            execution = exec
            resultURL = new URL("http://1234:4567/jenkins/report")
            resultSummary = "all right"
            nodeName = "no root"
        }

        exec.automatedExecutionExtender = extender

        and:

        restExecutionService.getOne(56) >> exec

        cufService.findAllCustomFieldValues(exec) >> execCufs
        denormCufService.findAllForEntity(exec) >> tcCufs

        when:

        def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/executions/{id}", 56)
                .header("Accept", "application/json"))

        then:

        /*
        * Test (using the TestHelper)
        * */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))

        withResult(res) {
            "_type".is "keyword-execution"
            "id".is 56
            "name".is "sample test case 56"

            "execution_order".is 4
            "execution_status".is "BLOCKED"
            "last_executed_by".is "User-5"
            "last_executed_on".is "2017-07-24T10:00:00.000+0000"
            "execution_mode".is "AUTOMATED"

            "reference".is "SAMP_EXEC_56"
            "dataset_label".is "sample dataset"

            "execution_steps".hasSize 3
            "execution_steps".test {
                "[0]".test {
                    "_type".is "execution-step"
                    "id".is 22
                    "execution_status".is "SUCCESS"
                    "action".is "<p>First action</p>"
                    "expected_result".is "<p>First result</p>"
                    selfRelIs "http://localhost:8080/api/rest/latest/execution-steps/22"
                }
                "[1]".test {
                    "_type".is "execution-step"
                    "id".is 23
                    "execution_status".is "BLOCKED"
                    "action".is "<p>Second action</p>"
                    "expected_result".is "<p>Second result</p>"
                    selfRelIs "http://localhost:8080/api/rest/latest/execution-steps/23"
                }
                "[2]".test {
                    "_type".is "execution-step"
                    "id".is 27
                    "execution_status".is "SUCCESS"
                    "action".is "<p>The Action</p>"
                    "expected_result".is "<p>The Result</p>"
                    selfRelIs "http://localhost:8080/api/rest/latest/execution-steps/27"
                }
            }

            "comment".is "<p>I have no comment</p>"
            "prerequisite".is "<p>Being alive.</p>"
            "description".is "<p>This is nice.</p>"
            "importance".is "LOW"
            "nature.code".is "NAT_SECURITY_TESTING"
            "type.code".is "TYP_EVOLUTION_TESTING"
            "test_case_status".is "APPROVED"

            "test_plan_item".test {
                "_type".is "iteration-test-plan-item"
                "id".is 15
                selfRelIs "http://localhost:8080/api/rest/latest/iteration-test-plan-items/15"
            }
            "custom_fields".hasSize 2
            "custom_fields".test {
                "[0]".test {
                    "code".is "CUF_TXT"
                    "label".is "cuf text"
                    "value".is "cuf text value"
                }
                "[1]".test {
                    "code".is "CUF_TXT_2"
                    "label".is "cuf text 2"
                    "value".is "cuf text value 2"
                }
            }

            "test_case_custom_fields".hasSize 2
            "test_case_custom_fields".test {
                "[0]".test {
                    "code".is "TC_CUF_TXT"
                    "label".is "tc cuf text"
                    "value".is "tc cuf text value"
                }
                "[1]".test {
                    "code".is "TC_CUF_TXT_2"
                    "label".is "tc cuf text 2"
                    "value".is "tc cuf text value 2"
                }
            }

            "automated_execution_extender.id".is 778

            "_links".test {
                "self.href".is "http://localhost:8080/api/rest/latest/executions/56"
                "project.href".is "http://localhost:8080/api/rest/latest/projects/87"
                "test_plan_item.href".is "http://localhost:8080/api/rest/latest/iteration-test-plan-items/15"
                "execution-steps.href".is "http://localhost:8080/api/rest/latest/executions/56/execution-steps"
            }
        }

        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "id : the id of the execution"
                    }
                    requestParams {
                        add DescriptorLists.fieldsParams
                    }
                    fields {
                        add "_type (string) : the type of the entity"
                        add "id (number) : the id of the execution"
                        add "name (string) : the name of the execution"
                        add "execution_order (number) : the order of the execution"
                        add "execution_status (string) : the status of the execution"
                        add "last_executed_by (string) : the date this execution was last executed"
                        add "last_executed_on (string) : the user who last executed this execution"
                        add "execution_mode (string) : the execution mode of the execution"
                        add "reference (string) : the reference of this execution"
                        add "dataset_label (string) : the label of the dataset used in this execution"
                        addAndStop "execution_steps (array) : the steps of this execution"
                        add "comment (string) : the comment of this execution"
                        add "prerequisite (string) : the prerequisite of this execution"
                        add "description (string) : the description of this execution"
                        add "importance (string) : the importance of this execution"
                        addAndStop "nature (object) : the nature of this execution"
                        addAndStop "type (object) : the type of this execution"
                        add "test_case_status (string) : the status of the test case referenced by this execution"
                        addAndStop "test_plan_item (object) : the test plan item referenced by this execution"
                        addAndStop "automated_execution_extender (object) : the automated execution extender referenced by this execution (will be hidden if execution mode is manual)"
                        addAndStop "custom_fields (array) : the denormalized custom fields of this execution"
                        addAndStop "test_case_custom_fields (array) : the custom fields of the referenced test case"
                        add "attachments (array) : the attachments of this execution"

                        add DescriptorLists.linksFields
                    }
                    _links {
                        add "self : link to this execution"
                        add "project : link to the project of this execution"
                        add "test_plan_item : link to the test plan item of this execution"
                        add "execution-steps : link to the execution steps of this execution"
                        add "attachments : link to the attachments of this execution"

                    }
                }
        ))
    }

    def "get-execution-execution-steps"() {

        given:

        def execStep = SquashEntityBuilder.executionStep {
            id = 10L
            executionStatus "SUCCESS"
            action = "<p>This is the first action.</p>"
            expectedResult = "<p>This is the first result.</p>"
            comment = "<p>And that is the comment</p>"
            lastExecutedBy = "User-8U122"
            lastExecutedOn "2017/07/31"
            executionStepOrder = 0
            execution = SquashEntityBuilder.execution {
                id = 7L
                executionStatus "SUCCESS"
            }
        }

        def execCufs = [
                SquashEntityBuilder.cufValue {
                    label = "Tag Cuf"
                    inputType "TAG"
                    code = "CUF_TAG"
                    value = ["tag_1", "tag_2", "tag_3"]
                }]

        def stepCufs = [
                SquashEntityBuilder.denoValue {
                    label = "Basic Text Cuf"
                    inputType "PLAIN_TEXT"
                    code = "CUF_TXT"
                    value = "The Value"
                }]

        and:

        restExecutionService.findExecutionSteps(10, _) >> { args ->
            new PageImpl<ExecutionStep>([execStep], args[1], 3)
        }

        cufService.findAllCustomFieldValues(execStep) >> execCufs
        denormCufService.findAllForEntity(_) >> stepCufs

        when:

        def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/executions/{id}/execution-steps?size=1&page=1", 10)
                .header("Accept", "application/json"))

        then:

        /*
        * Test (using the TestHelper)
        * */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))

        withResult(res) {
            "_embedded.execution-steps".hasSize 1
            "_embedded.execution-steps".test {
                "[0]".test {
                    "_type".is "execution-step"
                    "id".is 10
                    "execution_status".is "SUCCESS"
                    "action".is "<p>This is the first action.</p>"
                    "expected_result".is "<p>This is the first result.</p>"
                    "comment".is "<p>And that is the comment</p>"
                    "last_executed_by".is "User-8U122"
                    "last_executed_on".is "2017-07-31T10:00:00.000+0000"
                    "execution_step_order".is 0
                    "execution".test {
                        "_type".is "execution"
                        "id".is 7
                        "execution_status".is "SUCCESS"
                        selfRelIs "http://localhost:8080/api/rest/latest/executions/7"
                    }
                    "custom_fields".hasSize 1
                    "custom_fields[0]".test {
                        "code".is "CUF_TAG"
                        "label".is "Tag Cuf"
                        "value".contains "tag_1", "tag_2", "tag_3"
                    }
                    "test_step_custom_fields".hasSize 1
                    "test_step_custom_fields[0]".test {
                        "code".is "CUF_TXT"
                        "label".is "Basic Text Cuf"
                        "value".is "The Value"
                    }
                    selfRelIs "http://localhost:8080/api/rest/latest/execution-steps/10"
                }
            }
            selfRelIs "http://localhost:8080/api/rest/latest/executions/10/execution-steps?page=1&size=1"
            "page".test {
                "size".is 1
                "totalElements".is 3
                "totalPages".is 3
                "number".is 1
            }
        }

        /*
        * Documentation
        * */
        res.andDo(doc.document(
                DocumentationSnippets.AllInOne.createListEntityContent("execution-steps", "execution")
        ))
    }

    def "get-scripted-execution-execution-steps"() {

        given:

        def execStep = SquashEntityBuilder.executionStep {
            id = 10L
            executionStatus "SUCCESS"
            action = "<p>This is the first action.</p>"
            expectedResult = "<p>This is the first result.</p>"
            comment = "<p>And that is the comment</p>"
            lastExecutedBy = "User-8U122"
            lastExecutedOn "2017/07/31"
            executionStepOrder = 0
            execution = SquashEntityBuilder.scriptedExecution {
                id = 7L
                executionStatus "SUCCESS"
            }
        }

        def execCufs = [
                SquashEntityBuilder.cufValue {
                    label = "Tag Cuf"
                    inputType "TAG"
                    code = "CUF_TAG"
                    value = ["tag_1", "tag_2", "tag_3"]
                }]

        def stepCufs = [
                SquashEntityBuilder.denoValue {
                    label = "Basic Text Cuf"
                    inputType "PLAIN_TEXT"
                    code = "CUF_TXT"
                    value = "The Value"
                }]

        and:

        restExecutionService.findExecutionSteps(10, _) >> { args ->
            new PageImpl<ExecutionStep>([execStep], args[1], 3)
        }

        cufService.findAllCustomFieldValues(execStep) >> execCufs
        denormCufService.findAllForEntity(_) >> stepCufs

        when:

        def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/executions/{id}/execution-steps?size=1&page=1", 10)
                .header("Accept", "application/json"))

        then:

        /*
        * Test (using the TestHelper)
        * */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))

        withResult(res) {
            "_embedded.execution-steps".hasSize 1
            "_embedded.execution-steps".test {
                "[0]".test {
                    "_type".is "execution-step"
                    "id".is 10
                    "execution_status".is "SUCCESS"
                    "action".is "<p>This is the first action.</p>"
                    "expected_result".is "<p>This is the first result.</p>"
                    "comment".is "<p>And that is the comment</p>"
                    "last_executed_by".is "User-8U122"
                    "last_executed_on".is "2017-07-31T10:00:00.000+0000"
                    "execution_step_order".is 0
                    "execution".test {
                        "_type".is "scripted-execution"
                        "id".is 7
                        "execution_status".is "SUCCESS"
                        selfRelIs "http://localhost:8080/api/rest/latest/executions/7"
                    }
                    "custom_fields".hasSize 1
                    "custom_fields[0]".test {
                        "code".is "CUF_TAG"
                        "label".is "Tag Cuf"
                        "value".contains "tag_1", "tag_2", "tag_3"
                    }
                    "test_step_custom_fields".hasSize 1
                    "test_step_custom_fields[0]".test {
                        "code".is "CUF_TXT"
                        "label".is "Basic Text Cuf"
                        "value".is "The Value"
                    }
                    selfRelIs "http://localhost:8080/api/rest/latest/execution-steps/10"
                }
            }
            selfRelIs "http://localhost:8080/api/rest/latest/executions/10/execution-steps?page=1&size=1"
            "page".test {
                "size".is 1
                "totalElements".is 3
                "totalPages".is 3
                "number".is 1
            }
        }
    }

    def "get-keyword-execution-execution-steps"() {

        given:

        def execStep = SquashEntityBuilder.executionStep {
            id = 10L
            executionStatus "SUCCESS"
            action = "<p>This is the first action.</p>"
            expectedResult = "<p>This is the first result.</p>"
            comment = "<p>And that is the comment</p>"
            lastExecutedBy = "User-8U122"
            lastExecutedOn "2017/07/31"
            executionStepOrder = 0
            execution = SquashEntityBuilder.keywordExecution {
                id = 7L
                executionStatus "SUCCESS"
            }
        }

        def execCufs = [
                SquashEntityBuilder.cufValue {
                    label = "Tag Cuf"
                    inputType "TAG"
                    code = "CUF_TAG"
                    value = ["tag_1", "tag_2", "tag_3"]
                }]

        def stepCufs = [
                SquashEntityBuilder.denoValue {
                    label = "Basic Text Cuf"
                    inputType "PLAIN_TEXT"
                    code = "CUF_TXT"
                    value = "The Value"
                }]

        and:

        restExecutionService.findExecutionSteps(10, _) >> { args ->
            new PageImpl<ExecutionStep>([execStep], args[1], 3)
        }

        cufService.findAllCustomFieldValues(execStep) >> execCufs
        denormCufService.findAllForEntity(_) >> stepCufs

        when:

        def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/executions/{id}/execution-steps?size=1&page=1", 10)
                .header("Accept", "application/json"))

        then:

        /*
        * Test (using the TestHelper)
        * */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))

        withResult(res) {
            "_embedded.execution-steps".hasSize 1
            "_embedded.execution-steps".test {
                "[0]".test {
                    "_type".is "execution-step"
                    "id".is 10
                    "execution_status".is "SUCCESS"
                    "action".is "<p>This is the first action.</p>"
                    "expected_result".is "<p>This is the first result.</p>"
                    "comment".is "<p>And that is the comment</p>"
                    "last_executed_by".is "User-8U122"
                    "last_executed_on".is "2017-07-31T10:00:00.000+0000"
                    "execution_step_order".is 0
                    "execution".test {
                        "_type".is "keyword-execution"
                        "id".is 7
                        "execution_status".is "SUCCESS"
                        selfRelIs "http://localhost:8080/api/rest/latest/executions/7"
                    }
                    "custom_fields".hasSize 1
                    "custom_fields[0]".test {
                        "code".is "CUF_TAG"
                        "label".is "Tag Cuf"
                        "value".contains "tag_1", "tag_2", "tag_3"
                    }
                    "test_step_custom_fields".hasSize 1
                    "test_step_custom_fields[0]".test {
                        "code".is "CUF_TXT"
                        "label".is "Basic Text Cuf"
                        "value".is "The Value"
                    }
                    selfRelIs "http://localhost:8080/api/rest/latest/execution-steps/10"
                }
            }
            selfRelIs "http://localhost:8080/api/rest/latest/executions/10/execution-steps?page=1&size=1"
            "page".test {
                "size".is 1
                "totalElements".is 3
                "totalPages".is 3
                "number".is 1
            }
        }
    }

    def "delete-execution"() {
        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.delete("/api/rest/latest/executions/{id}", 44L)
                .accept("application/json")
                .contentType("application/json"))

        then:
        /*
         * Test
         */
        res.andExpect(status().isNoContent())
        1 * restExecutionService.deleteExecution(_)


        /*
         * Documentation
         */
        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "id : the list of ids of the execution"
                    }
                }
        ))
    }

}
