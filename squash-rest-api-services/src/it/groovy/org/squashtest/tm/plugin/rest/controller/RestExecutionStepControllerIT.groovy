/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.controller

import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders
import org.squashtest.tm.plugin.docutils.DocumentationSnippets
import org.squashtest.tm.plugin.mockmvc.BaseControllerSpec
import org.squashtest.tm.plugin.rest.core.jackson.DeserializationDynamicFilter
import org.squashtest.tm.plugin.rest.core.jackson.DeserializationHints
import org.squashtest.tm.plugin.rest.core.utils.DeserializationConfigHelper
import org.squashtest.tm.plugin.rest.service.RestExecutionStepService
import org.squashtest.tm.plugin.rest.validators.CustomFieldValueHintedValidator
import org.squashtest.tm.plugin.rest.validators.DenormalizedFieldValueHintedValidator
import org.squashtest.tm.plugin.rest.validators.ExecutionHintedValidator
import org.squashtest.tm.service.customfield.CustomFieldValueFinderService
import org.squashtest.tm.service.denormalizedfield.DenormalizedFieldValueManager
import org.squashtest.tm.service.security.PermissionEvaluationService
import org.squashtest.tm.test.domainbuilder.SquashEntityBuilder

import javax.inject.Inject

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import static org.squashtest.tm.plugin.docutils.DocumentationSnippets.DescriptorLists
import static org.squashtest.tm.plugin.docutils.DocumentationSnippets.documentationBuilder
import static org.squashtest.tm.plugin.mockmvc.TestHelper.selfRelIs
import static org.squashtest.tm.plugin.mockmvc.TestHelper.withResult

/**
 * Created by jlor on 02/08/2017.
 */
@WebMvcTest(RestDatasetController)
public class RestExecutionStepControllerIT extends BaseControllerSpec {

    @Inject
    private RestExecutionStepService restExecStepService;
    @Inject
    private CustomFieldValueFinderService cufService;
    @Inject
    private DenormalizedFieldValueManager denormCufService;
    @Inject
    private PermissionEvaluationService permService
    @Inject
    private DeserializationConfigHelper confHelper
    @Inject
    private CustomFieldValueHintedValidator validator
    @Inject
    private DenormalizedFieldValueHintedValidator denoValidator
    @Inject
    private ExecutionHintedValidator execValidator;

    def setup() {
        validator.supports(_) >> true
        denoValidator.supports(_) >> true
        execValidator.supports(_) >> true
    }

    def "get-execution-step"() {

        given:

        def execStep = SquashEntityBuilder.executionStep {
            id = 6L
            executionStatus "BLOCKED"
            action = "<p>Click the button</p>"
            expectedResult = "<p>The page shows up</p>"
            comment = "<p>This is quite simple.</p>"
            lastExecutedBy = "User-J9"
            lastExecutedOn "2015/04/26"
            executionStepOrder = 1
            referencedTestStep = SquashEntityBuilder.actionStep {
                id = 2L
            }
            execution = SquashEntityBuilder.execution {
                id = 3L
                executionStatus "BLOCKED"
                testPlan = SquashEntityBuilder.iterationTestPlanItem {
                    iteration = SquashEntityBuilder.iteration {
                        campaign = SquashEntityBuilder.campaign {
                            project = SquashEntityBuilder.project {
                                id = 10L
                            }
                        }
                    }
                }
            }
        }

        def stepCufs = [
                SquashEntityBuilder.cufValue {
                    code = "CUF_TAG"
                    label = "Tag Cuf"
                    inputType "TAG"
                    value = ["tag_1", "tag_2", "tag_3"]
                }]

        def testStepCufs = [
                SquashEntityBuilder.denoValue {
                    code = "CUF_TXT"
                    label = "Basic Text Cuf"
                    value = "The Value"
                }]

        and:

        restExecStepService.getOne(6) >> execStep

        cufService.findAllCustomFieldValues(execStep) >> stepCufs

        denormCufService.findAllForEntity(execStep) >> testStepCufs

        permService.canRead(execStep.referencedTestStep) >> true

        when:

        def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/execution-steps/{id}", 6).header("Accept", "application/json"))

        then:

        /*
        * Test
         */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))

        withResult(res) {
            "_type".is "execution-step"
            "id".is 6
            "execution_status".is "BLOCKED"
            "action".is "<p>Click the button</p>"
            "expected_result".is "<p>The page shows up</p>"
            "comment".is "<p>This is quite simple.</p>"
            "last_executed_by".is "User-J9"
            "last_executed_on".is "2015-04-26T10:00:00.000+0000"
            "execution_step_order".is 1
            "referenced_test_step".test {
                "_type".is "action-step"
                "id".is 2
                selfRelIs "http://localhost:8080/api/rest/latest/test-steps/2"
            }
            "execution".test {
                "_type".is "execution"
                "id".is 3
                "execution_status".is "BLOCKED"
                selfRelIs "http://localhost:8080/api/rest/latest/executions/3"
            }
            "custom_fields".hasSize 1
            "custom_fields[0]".test {
                "code".is "CUF_TAG"
                "label".is "Tag Cuf"
                "value".contains "tag_1", "tag_2", "tag_3"
            }
            "test_step_custom_fields".hasSize 1
            "test_step_custom_fields[0]".test {
                "code".is "CUF_TXT"
                "label".is "Basic Text Cuf"
                "value".is "The Value"
            }
            "_links".test {
                "self.href".is "http://localhost:8080/api/rest/latest/execution-steps/6"
                "project.href".is "http://localhost:8080/api/rest/latest/projects/10"
                "execution.href".is "http://localhost:8080/api/rest/latest/executions/3"
            }
        }

        /*
        * Documentation
         */
        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "id : the id of the execution step"
                    }
                    requestParams {
                        add DocumentationSnippets.DescriptorLists.fieldsParams
                    }
                    fields {
                        add "_type (string) : the type of the entity"
                        add "id (number) : the id of the execution step"
                        add "execution_status (string) : the status of this execution step"
                        add "action (string) : the action to be accomplished, format is html"
                        add "expected_result (string) : the state or behavior that should be observable when the action has been performed, format is html)"
                        add "comment (string) : the comment left after executing the step"
                        add "last_executed_by (string) : the date this execution step was last executed"
                        add "last_executed_on (string) : the user who last executed this execution"
                        add "execution_step_order (number) : the order of the step in the execution"
                        addAndStop "referenced_test_step (object) : the test step referenced by this execution step"
                        addAndStop "execution (object) : the execution this step belongs to"
                        addAndStop "custom_fields (array) : the custom fields of this execution step"
                        addAndStop "test_step_custom_fields (array) : the denormalized custom fields of the referenced test step"
                        add "attachments (array) : the attachments of the this step"
                        add DescriptorLists.linksFields
                    }
                    _links {
                        add "self : link to this execution step"
                        add "project : link to the project of this execution step"
                        add "execution : link to the execution of this execution step"
                        add "attachments : link to the attachments of this execution step"

                    }
                }))
    }

    //NO execution step for scripted test case / execution

    def "get-execution-step for keyword execution"() {

        given:

        def execStep = SquashEntityBuilder.executionStep {
            id = 6L
            executionStatus "BLOCKED"
            action = "<p>Click the button</p>"
            expectedResult = "<p>The page shows up</p>"
            comment = "<p>This is quite simple.</p>"
            lastExecutedBy = "User-J9"
            lastExecutedOn "2015/04/26"
            executionStepOrder = 1
            referencedTestStep = SquashEntityBuilder.keywordStep {
                id = 2L
            }
            execution = SquashEntityBuilder.keywordExecution {
                id = 3L
                executionStatus "BLOCKED"
                testPlan = SquashEntityBuilder.iterationTestPlanItem {
                    iteration = SquashEntityBuilder.iteration {
                        campaign = SquashEntityBuilder.campaign {
                            project = SquashEntityBuilder.project {
                                id = 10L
                            }
                        }
                    }
                }
            }
        }

        def stepCufs = [
                SquashEntityBuilder.cufValue {
                    code = "CUF_TAG"
                    label = "Tag Cuf"
                    inputType "TAG"
                    value = ["tag_1", "tag_2", "tag_3"]
                }]

        def testStepCufs = [
                SquashEntityBuilder.denoValue {
                    code = "CUF_TXT"
                    label = "Basic Text Cuf"
                    value = "The Value"
                }]

        and:

        restExecStepService.getOne(6) >> execStep

        cufService.findAllCustomFieldValues(execStep) >> stepCufs

        denormCufService.findAllForEntity(execStep) >> testStepCufs

        permService.canRead(execStep.referencedTestStep) >> true

        when:

        def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/execution-steps/{id}", 6).header("Accept", "application/json"))

        then:

        /*
        * Test
         */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))

        withResult(res) {
            "_type".is "execution-step"
            "id".is 6
            "execution_status".is "BLOCKED"
            "action".is "<p>Click the button</p>"
            "expected_result".is "<p>The page shows up</p>"
            "comment".is "<p>This is quite simple.</p>"
            "last_executed_by".is "User-J9"
            "last_executed_on".is "2015-04-26T10:00:00.000+0000"
            "execution_step_order".is 1
            "referenced_test_step".test {
                "_type".is "keyword-step"
                "id".is 2
                selfRelIs "http://localhost:8080/api/rest/latest/test-steps/2"
            }
            "execution".test {
                "_type".is "keyword-execution"
                "id".is 3
                "execution_status".is "BLOCKED"
                selfRelIs "http://localhost:8080/api/rest/latest/executions/3"
            }
            "custom_fields".hasSize 1
            "custom_fields[0]".test {
                "code".is "CUF_TAG"
                "label".is "Tag Cuf"
                "value".contains "tag_1", "tag_2", "tag_3"
            }
            "test_step_custom_fields".hasSize 1
            "test_step_custom_fields[0]".test {
                "code".is "CUF_TXT"
                "label".is "Basic Text Cuf"
                "value".is "The Value"
            }
            "_links".test {
                "self.href".is "http://localhost:8080/api/rest/latest/execution-steps/6"
                "project.href".is "http://localhost:8080/api/rest/latest/projects/10"
                "keyword-execution.href".is "http://localhost:8080/api/rest/latest/executions/3"
            }
        }
    }

    def "change-execution-status"() {
        given:

        def execStep = SquashEntityBuilder.executionStep {
            id = 6L
            executionStatus "SUCCESS"
            action = "<p>Click the button</p>"
            expectedResult = "<p>The page shows up</p>"
            comment = "<p>This is quite simple.</p>"
            lastExecutedBy = "User-J9"
            lastExecutedOn "2015/04/26"
            executionStepOrder = 1
            referencedTestStep = SquashEntityBuilder.actionStep {
                id = 2L
            }
            execution = SquashEntityBuilder.execution {
                id = 3L
                executionStatus "BLOCKED"
                testPlan = SquashEntityBuilder.iterationTestPlanItem {
                    iteration = SquashEntityBuilder.iteration {
                        campaign = SquashEntityBuilder.campaign {
                            project = SquashEntityBuilder.project {
                                id = 10L
                            }
                        }
                    }
                }
            }
        }

        def stepCufs = [
                SquashEntityBuilder.cufValue {
                    code = "CUF_TAG"
                    label = "Tag Cuf"
                    inputType "TAG"
                    value = ["tag_1", "tag_2", "tag_3"]
                }]

        def testStepCufs = [
                SquashEntityBuilder.denoValue {
                    code = "CUF_TXT"
                    label = "Basic Text Cuf"
                    value = "The Value"
                }]

        and:

        restExecStepService.getOne(6) >> execStep

        cufService.findAllCustomFieldValues(execStep) >> stepCufs

        denormCufService.findAllForEntity(execStep) >> testStepCufs

        permService.canRead(execStep.referencedTestStep) >> true

        when:

        def res = mockMvc.perform(RestDocumentationRequestBuilders.patch("/api/rest/latest/execution-steps/{id}/execution-status/{status}", 6, "Success").header("Accept", "application/json"))

        then:

        /*
        * Test
         */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))

        withResult(res) {
            "_type".is "execution-step"
            "id".is 6
            "execution_status".is "SUCCESS"
            "action".is "<p>Click the button</p>"
            "expected_result".is "<p>The page shows up</p>"
            "comment".is "<p>This is quite simple.</p>"
            "last_executed_by".is "User-J9"
            "last_executed_on".is "2015-04-26T10:00:00.000+0000"
            "execution_step_order".is 1
            "referenced_test_step".test {
                "_type".is "action-step"
                "id".is 2
                selfRelIs "http://localhost:8080/api/rest/latest/test-steps/2"
            }
            "execution".test {
                "_type".is "execution"
                "id".is 3
                "execution_status".is "BLOCKED"
                selfRelIs "http://localhost:8080/api/rest/latest/executions/3"
            }
            "custom_fields".hasSize 1
            "custom_fields[0]".test {
                "code".is "CUF_TAG"
                "label".is "Tag Cuf"
                "value".contains "tag_1", "tag_2", "tag_3"
            }
            "test_step_custom_fields".hasSize 1
            "test_step_custom_fields[0]".test {
                "code".is "CUF_TXT"
                "label".is "Basic Text Cuf"
                "value".is "The Value"
            }
            "_links".test {
                "self.href".is "http://localhost:8080/api/rest/latest/execution-steps/6"
                "project.href".is "http://localhost:8080/api/rest/latest/projects/10"
                "execution.href".is "http://localhost:8080/api/rest/latest/executions/3"
            }
        }

        /*
        * Documentation
         */
        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "id : the id of the execution step"
                        add "status: the new status of that execution step (success, blocked, ready, " +
                                "running, error, failure, not-found, not-run, settled, untestable or warning)"
                    }
                    requestParams {
                        add DocumentationSnippets.DescriptorLists.fieldsParams
                    }
                    fields {
                        add "_type (string) : the type of the entity"
                        add "id (number) : the id of the execution step"
                        add "execution_status (string) : the status of this execution step"
                        add "action (string) : the action to be accomplished, format is html"
                        add "expected_result (string) : the state or behavior that should be observable when the action has been performed, format is html)"
                        add "comment (string) : the comment left after executing the step"
                        add "last_executed_by (string) : the date this execution step was last executed"
                        add "last_executed_on (string) : the user who last executed this execution"
                        add "execution_step_order (number) : the order of the step in the execution"
                        addAndStop "referenced_test_step (object) : the test step referenced by this execution step"
                        addAndStop "execution (object) : the execution this step belongs to"
                        addAndStop "custom_fields (array) : the custom fields of this execution step"
                        addAndStop "test_step_custom_fields (array) : the denormalized custom fields of the referenced test step"
                        add "attachments (array) : the attachments of the test step"

                        add DescriptorLists.linksFields
                    }
                    _links {
                        add "self : link to this execution step"
                        add "project : link to the project of this execution step"
                        add "execution : link to the execution of this execution step"

                    }
                }))
    }

    def "change-execution-status for keyword execution"() {
        given:

        def execStep = SquashEntityBuilder.executionStep {
            id = 6L
            executionStatus "SUCCESS"
            action = "<p>Click the button</p>"
            expectedResult = "<p>The page shows up</p>"
            comment = "<p>This is quite simple.</p>"
            lastExecutedBy = "User-J9"
            lastExecutedOn "2015/04/26"
            executionStepOrder = 1
            referencedTestStep = SquashEntityBuilder.keywordStep {
                id = 2L
            }
            execution = SquashEntityBuilder.keywordExecution {
                id = 3L
                executionStatus "BLOCKED"
                testPlan = SquashEntityBuilder.iterationTestPlanItem {
                    iteration = SquashEntityBuilder.iteration {
                        campaign = SquashEntityBuilder.campaign {
                            project = SquashEntityBuilder.project {
                                id = 10L
                            }
                        }
                    }
                }
            }
        }

        def stepCufs = [
                SquashEntityBuilder.cufValue {
                    code = "CUF_TAG"
                    label = "Tag Cuf"
                    inputType "TAG"
                    value = ["tag_1", "tag_2", "tag_3"]
                }]

        def testStepCufs = [
                SquashEntityBuilder.denoValue {
                    code = "CUF_TXT"
                    label = "Basic Text Cuf"
                    value = "The Value"
                }]

        and:

        restExecStepService.getOne(6) >> execStep

        cufService.findAllCustomFieldValues(execStep) >> stepCufs

        denormCufService.findAllForEntity(execStep) >> testStepCufs

        permService.canRead(execStep.referencedTestStep) >> true

        when:

        def res = mockMvc.perform(RestDocumentationRequestBuilders.patch("/api/rest/latest/execution-steps/{id}/execution-status/{status}", 6, "Success").header("Accept", "application/json"))

        then:

        /*
        * Test
         */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))

        withResult(res) {
            "_type".is "execution-step"
            "id".is 6
            "execution_status".is "SUCCESS"
            "action".is "<p>Click the button</p>"
            "expected_result".is "<p>The page shows up</p>"
            "comment".is "<p>This is quite simple.</p>"
            "last_executed_by".is "User-J9"
            "last_executed_on".is "2015-04-26T10:00:00.000+0000"
            "execution_step_order".is 1
            "referenced_test_step".test {
                "_type".is "keyword-step"
                "id".is 2
                selfRelIs "http://localhost:8080/api/rest/latest/test-steps/2"
            }
            "execution".test {
                "_type".is "keyword-execution"
                "id".is 3
                "execution_status".is "BLOCKED"
                selfRelIs "http://localhost:8080/api/rest/latest/executions/3"
            }
            "custom_fields".hasSize 1
            "custom_fields[0]".test {
                "code".is "CUF_TAG"
                "label".is "Tag Cuf"
                "value".contains "tag_1", "tag_2", "tag_3"
            }
            "test_step_custom_fields".hasSize 1
            "test_step_custom_fields[0]".test {
                "code".is "CUF_TXT"
                "label".is "Basic Text Cuf"
                "value".is "The Value"
            }
            "_links".test {
                "self.href".is "http://localhost:8080/api/rest/latest/execution-steps/6"
                "project.href".is "http://localhost:8080/api/rest/latest/projects/10"
                "keyword-execution.href".is "http://localhost:8080/api/rest/latest/executions/3"
            }
        }
    }

    def "change-execution-status-with-wrong-status"() {
        given:

        def execStep = SquashEntityBuilder.executionStep {
            id = 6L
            executionStatus "BLOCKED"
            action = "<p>Click the button</p>"
            expectedResult = "<p>The page shows up</p>"
            comment = "<p>This is quite simple.</p>"
            lastExecutedBy = "User-J9"
            lastExecutedOn "2015/04/26"
            executionStepOrder = 1
            referencedTestStep = SquashEntityBuilder.actionStep {
                id = 2L
            }
            execution = SquashEntityBuilder.execution {
                id = 3L
                executionStatus "BLOCKED"
                testPlan = SquashEntityBuilder.iterationTestPlanItem {
                    iteration = SquashEntityBuilder.iteration {
                        campaign = SquashEntityBuilder.campaign {
                            project = SquashEntityBuilder.project {
                                id = 10L
                            }
                        }
                    }
                }
            }
        }

        def stepCufs = [
                SquashEntityBuilder.cufValue {
                    code = "CUF_TAG"
                    label = "Tag Cuf"
                    inputType "TAG"
                    value = ["tag_1", "tag_2", "tag_3"]
                }]

        def testStepCufs = [
                SquashEntityBuilder.denoValue {
                    code = "CUF_TXT"
                    label = "Basic Text Cuf"
                    value = "The Value"
                }]

        and:

        restExecStepService.getOne(6) >> execStep

        cufService.findAllCustomFieldValues(execStep) >> stepCufs

        denormCufService.findAllForEntity(execStep) >> testStepCufs

        permService.canRead(execStep.referencedTestStep) >> true

        when:

        def res = mockMvc.perform(RestDocumentationRequestBuilders.patch("/api/rest/latest/execution-steps/{id}/execution-status/{status}", 6, "toto").header("Accept", "application/json"))

        then:

        /*
        * Test
         */
        res.andExpect(status().isInternalServerError())
                .andExpect(content().contentType("application/json;charset=UTF-8"))
    }

    def "change-execution-status-with-wrong-status for keyword execution"() {
        given:

        def execStep = SquashEntityBuilder.executionStep {
            id = 6L
            executionStatus "BLOCKED"
            action = "<p>Click the button</p>"
            expectedResult = "<p>The page shows up</p>"
            comment = "<p>This is quite simple.</p>"
            lastExecutedBy = "User-J9"
            lastExecutedOn "2015/04/26"
            executionStepOrder = 1
            referencedTestStep = SquashEntityBuilder.keywordStep {
                id = 2L
            }
            execution = SquashEntityBuilder.keywordExecution {
                id = 3L
                executionStatus "BLOCKED"
                testPlan = SquashEntityBuilder.iterationTestPlanItem {
                    iteration = SquashEntityBuilder.iteration {
                        campaign = SquashEntityBuilder.campaign {
                            project = SquashEntityBuilder.project {
                                id = 10L
                            }
                        }
                    }
                }
            }
        }

        def stepCufs = [
                SquashEntityBuilder.cufValue {
                    code = "CUF_TAG"
                    label = "Tag Cuf"
                    inputType "TAG"
                    value = ["tag_1", "tag_2", "tag_3"]
                }]

        def testStepCufs = [
                SquashEntityBuilder.denoValue {
                    code = "CUF_TXT"
                    label = "Basic Text Cuf"
                    value = "The Value"
                }]

        and:

        restExecStepService.getOne(6) >> execStep

        cufService.findAllCustomFieldValues(execStep) >> stepCufs

        denormCufService.findAllForEntity(execStep) >> testStepCufs

        permService.canRead(execStep.referencedTestStep) >> true

        when:

        def res = mockMvc.perform(RestDocumentationRequestBuilders.patch("/api/rest/latest/execution-steps/{id}/execution-status/{status}", 6, "toto").header("Accept", "application/json"))

        then:

        /*
        * Test
         */
        res.andExpect(status().isInternalServerError())
                .andExpect(content().contentType("application/json;charset=UTF-8"))
    }

    def "change-comment-execution-step"() {
        given:
        def json = """{
                        "_type" :"execution-step",
                        "comment" : "<p>Update comment.</p>"
                     }"""
        def execStep = SquashEntityBuilder.executionStep {
            id = 6L
            executionStatus "SUCCESS"
            action = "<p>Click the button</p>"
            expectedResult = "<p>The page shows up</p>"
            comment = "<p>Update comment.</p>"
            lastExecutedBy = "User-J9"
            lastExecutedOn "2015/04/26"
            executionStepOrder = 1
            execution = SquashEntityBuilder.execution {
                id = 3L
                executionStatus "BLOCKED"
                testPlan = SquashEntityBuilder.iterationTestPlanItem {
                    iteration = SquashEntityBuilder.iteration {
                        campaign = SquashEntityBuilder.campaign {
                            project = SquashEntityBuilder.project {
                                id = 10L
                            }
                        }
                    }
                }
            }
        }

        and:
        confHelper.createHints(_, _) >> new DeserializationHints(
                targetEntity: execStep,
                mode: DeserializationHints.Mode.DESERIALIZE_UPDATE,
                filter: new DeserializationDynamicFilter(RestExecutionStepController.PATCH_DYNAMIC_FILTER),
                project: SquashEntityBuilder.project { id = 14L }
        )
        cufService.findAllCustomFieldValues(execStep) >> []
        denormCufService.findAllForEntity(execStep) >> []

        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.patch("/api/rest/latest/execution-steps/{id}", 6L)
                .accept("application/json")
                .contentType("application/json")
                .content(json))

        then:
        // check the deserialized content that is given to the service
        1 * restExecStepService.updateStep({ args ->
            def exStep = args.wrapped

            return (
                    exStep.comment == "<p>Update comment.</p>"
            )
        })

        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))

        withResult(res) {
            "_type".is "execution-step"
            "id".is 6
            "execution_status".is "SUCCESS"
            "action".is "<p>Click the button</p>"
            "expected_result".is "<p>The page shows up</p>"
            "comment".is "<p>Update comment.</p>"
            "last_executed_by".is "User-J9"
            "last_executed_on".is "2015-04-26T10:00:00.000+0000"
            "execution_step_order".is 1
            "execution".test {
                "_type".is "execution"
                "id".is 3
                "execution_status".is "BLOCKED"
                selfRelIs "http://localhost:8080/api/rest/latest/executions/3"
            }
            "custom_fields".hasSize 0
            "test_step_custom_fields".hasSize 0
            "_links".test {
                "self.href".is "http://localhost:8080/api/rest/latest/execution-steps/6"
                "project.href".is "http://localhost:8080/api/rest/latest/projects/10"
                "execution.href".is "http://localhost:8080/api/rest/latest/executions/3"
            }
        }
        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "id : the id of the execution step"
                    }
                }))

    }

    def "change-comment-execution-step for keyword execution"() {
        given:
        def json = """{
                        "_type" :"execution-step",
                        "comment" : "<p>Update comment.</p>"
                     }"""
        def execStep = SquashEntityBuilder.executionStep {
            id = 6L
            executionStatus "SUCCESS"
            action = "<p>Click the button</p>"
            expectedResult = "<p>The page shows up</p>"
            comment = "<p>Update comment.</p>"
            lastExecutedBy = "User-J9"
            lastExecutedOn "2015/04/26"
            executionStepOrder = 1
            execution = SquashEntityBuilder.keywordExecution {
                id = 3L
                executionStatus "BLOCKED"
                testPlan = SquashEntityBuilder.iterationTestPlanItem {
                    iteration = SquashEntityBuilder.iteration {
                        campaign = SquashEntityBuilder.campaign {
                            project = SquashEntityBuilder.project {
                                id = 10L
                            }
                        }
                    }
                }
            }
        }

        and:
        confHelper.createHints(_, _) >> new DeserializationHints(
                targetEntity: execStep,
                mode: DeserializationHints.Mode.DESERIALIZE_UPDATE,
                filter: new DeserializationDynamicFilter(RestExecutionStepController.PATCH_DYNAMIC_FILTER),
                project: SquashEntityBuilder.project { id = 14L }
        )
        cufService.findAllCustomFieldValues(execStep) >> []
        denormCufService.findAllForEntity(execStep) >> []

        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.patch("/api/rest/latest/execution-steps/{id}", 6L)
                .accept("application/json")
                .contentType("application/json")
                .content(json))

        then:
        // check the deserialized content that is given to the service
        1 * restExecStepService.updateStep({ args ->
            def exStep = args.wrapped

            return (
                    exStep.comment == "<p>Update comment.</p>"
            )
        })

        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))

        withResult(res) {
            "_type".is "execution-step"
            "id".is 6
            "execution_status".is "SUCCESS"
            "action".is "<p>Click the button</p>"
            "expected_result".is "<p>The page shows up</p>"
            "comment".is "<p>Update comment.</p>"
            "last_executed_by".is "User-J9"
            "last_executed_on".is "2015-04-26T10:00:00.000+0000"
            "execution_step_order".is 1
            "execution".test {
                "_type".is "keyword-execution"
                "id".is 3
                "execution_status".is "BLOCKED"
                selfRelIs "http://localhost:8080/api/rest/latest/executions/3"
            }
            "custom_fields".hasSize 0
            "test_step_custom_fields".hasSize 0
            "_links".test {
                "self.href".is "http://localhost:8080/api/rest/latest/execution-steps/6"
                "project.href".is "http://localhost:8080/api/rest/latest/projects/10"
                "keyword-execution.href".is "http://localhost:8080/api/rest/latest/executions/3"
            }
        }

    }

}

