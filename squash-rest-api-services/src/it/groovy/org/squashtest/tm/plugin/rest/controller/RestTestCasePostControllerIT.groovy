/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.controller

import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.context.i18n.LocaleContextHolder
import org.springframework.http.HttpHeaders
import org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders
import org.squashtest.tm.plugin.mockmvc.BaseControllerSpec
import org.squashtest.tm.plugin.rest.core.service.NodeHierarchyHelpService
import org.squashtest.tm.plugin.rest.service.RestTestCaseService
import org.squashtest.tm.plugin.rest.service.RestVerifyingRequirementManagerService
import org.squashtest.tm.service.customfield.CustomFieldValueFinderService
import org.squashtest.tm.service.internal.library.PathService
import org.squashtest.tm.service.security.PermissionEvaluationService
import org.squashtest.tm.test.domainbuilder.SquashEntityBuilder

import javax.inject.Inject

import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.delete
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import static org.squashtest.tm.plugin.docutils.DocumentationSnippets.documentationBuilder
import static org.squashtest.tm.plugin.mockmvc.TestHelper.withResult
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.cufValue

@WebMvcTest(RestTestCaseController)
public class RestTestCasePostControllerIT extends BaseControllerSpec {

    @Inject
    private RestTestCaseService restTestCaseService;

    @Inject
    private CustomFieldValueFinderService cufService

    @Inject
    private PathService pathService

    @Inject
    private NodeHierarchyHelpService hierService

    @Inject
    private RestVerifyingRequirementManagerService verifyingRequirementManagerService;

    @Inject
    private PermissionEvaluationService permService

    @Inject
    private NodeHierarchyHelpService nodeHierarchyHelpService

    def "post-test-case"() {

        given:
        def json = """{
                "_type" : "test-case",
                "name" : "Christmas turkey test flight",
                "parent" : {
                    "_type" : "project",
                    "id" : 15
                }
            }
            """

        def proj = SquashEntityBuilder.project {
            id = 15L
            name = "Christmas Eve"
        }
        def tc = SquashEntityBuilder.testCase {
            id = 240L
            name = "Christmas turkey test flight"
            project = proj
        }

        and:
        restTestCaseService.createTestCase(_) >> tc
        cufService.findAllCustomFieldValues(_) >> []
        pathService.buildTestCasePath(_) >> '/Christmas Eve/Christmas turkey test flight'
        hierService.findParentFor(_) >> SquashEntityBuilder.tcLibrary {
            project = proj
        }

        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.post("/api/rest/latest/test-cases")
                .accept("application/json")
                .contentType("application/json")
                .content(json))


        then:
        /*
         * Test
         */
        res.andExpect(status().isCreated())
                .andExpect(content().contentType("application/json;charset=UTF-8"))

        withResult(res) {

            "_type".is "test-case"
            "id".is 240
            "name".is "Christmas turkey test flight"
            "parent".test {
                "_type".is "project"
                "id".is 15
            }
        }

        /*
         * No documentation, it is essentially the same as get-test-case
         */

    }

    def "post-scripted-test-case"() {

        given:
        def json = """{
                "_type" : "scripted-test-case",
                "name" : "Christmas turkey test flight",
                "parent" : {
                    "_type" : "project",
                    "id" : 15
                },
                "script" : "this is Gherkin script"
            }
            """

        def proj = SquashEntityBuilder.project {
            id = 15L
            name = "Christmas Eve"
        }
        def tc = SquashEntityBuilder.scriptedTestCase {
            id = 240L
            name = "Christmas turkey test flight"
            project = proj
            script = "this is Gherkin script"
        }

        and:
        restTestCaseService.createTestCase(_) >> tc
        cufService.findAllCustomFieldValues(_) >> []
        pathService.buildTestCasePath(_) >> '/Christmas Eve/Christmas turkey test flight'
        hierService.findParentFor(_) >> SquashEntityBuilder.tcLibrary {
            project = proj
        }

        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.post("/api/rest/latest/test-cases")
                .accept("application/json")
                .contentType("application/json")
                .content(json))


        then:
        /*
         * Test
         */
        res.andExpect(status().isCreated())
                .andExpect(content().contentType("application/json;charset=UTF-8"))

        withResult(res) {

            "_type".is "scripted-test-case"
            "id".is 240
            "name".is "Christmas turkey test flight"
            "parent".test {
                "_type".is "project"
                "id".is 15
            }
            "script".is "this is Gherkin script"
        }

        /*
         * No documentation, it is essentially the same as get-test-case
         */

    }

    def "post-keyword-test-case"() {

        given:
        def json = """{
                "_type" : "keyword-test-case",
                "name" : "Christmas turkey test flight",
                "parent" : {
                    "_type" : "project",
                    "id" : 15
                },
                "steps" : [                    
                ]
            }
            """

        def proj = SquashEntityBuilder.project {
            id = 15L
            name = "Christmas Eve"
        }
        def tc = SquashEntityBuilder.keywordTestCase {
            id = 240L
            name = "Christmas turkey test flight"
            project = proj
        }

        and:
        restTestCaseService.createTestCase(_) >> tc
        cufService.findAllCustomFieldValues(_) >> []
        pathService.buildTestCasePath(_) >> '/Christmas Eve/Christmas turkey test flight'
        hierService.findParentFor(_) >> SquashEntityBuilder.tcLibrary {
            project = proj
        }

        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.post("/api/rest/latest/test-cases")
                .accept("application/json")
                .contentType("application/json")
                .content(json))


        then:
        /*
         * Test
         */
        res.andExpect(status().isCreated())
                .andExpect(content().contentType("application/json;charset=UTF-8"))

        withResult(res) {

            "_type".is "keyword-test-case"
            "id".is 240
            "name".is "Christmas turkey test flight"
            "parent".test {
                "_type".is "project"
                "id".is 15
            }
            "steps".hasSize 0
        }

        /*
         * No documentation, it is essentially the same as get-test-case
         */

    }

    def "post-test-case-with-automation-attributes"() {

        given:
        def json = """{
                "_type" : "keyword-test-case",
                "name" : "Christmas turkey test flight",
                "parent" : {
                    "_type" : "project",
                    "id" : 15
                },
                "automated_test_technology" : "Robot Framework",
                "scm_repository_id" : 6,
                "automated_test_reference" : ""
            }
            """

        def proj = SquashEntityBuilder.project {
            id = 15L
            name = "Christmas Eve"
        }
        def robotTechnology = SquashEntityBuilder.automatedTestTechnology {
            id = 3L
            name = "Robot Framework"
            actionProviderKey = "robotframework/execute@v1"
        }
        def scmRepo = SquashEntityBuilder.scmRepository {
            id = 6l
            name = "repo01"
            workingBranch = "master"
            scmServer = SquashEntityBuilder.scmServer {
                id = 2l
                url = "https://github.com/test"
            }
        }
        def tc = SquashEntityBuilder.keywordTestCase {
            id = 240L
            name = "Christmas turkey test flight"
            project = proj
            automatedTestTechnology = robotTechnology
            scmRepository = scmRepo
            automatedTestReference = ""
        }

        and:
        restTestCaseService.createTestCase(_) >> tc
        cufService.findAllCustomFieldValues(_) >> []
        pathService.buildTestCasePath(_) >> '/Christmas Eve/Christmas turkey test flight'
        hierService.findParentFor(_) >> SquashEntityBuilder.tcLibrary {
            project = proj
        }

        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.post("/api/rest/latest/test-cases")
                .accept("application/json")
                .contentType("application/json")
                .content(json))


        then:
        /*
         * Test
         */
        res.andExpect(status().isCreated())
                .andExpect(content().contentType("application/json;charset=UTF-8"))

        withResult(res) {

            "_type".is "keyword-test-case"
            "id".is 240
            "name".is "Christmas turkey test flight"
            "parent".test {
                "_type".is "project"
                "id".is 15
            }
            "automated_test_technology".is "Robot Framework"
            "scm_repository_id".is 6
            "scm_repository_url".is "https://github.com/test/repo01 (master)"
            "automated_test_reference".is ""
        }

        /*
         * No documentation, it is essentially the same as get-test-case
         */

    }


    //**************************************** PATCH ****************************************
    def "patch-test-case"() {

        given:
        def json = """{
    "_type" : "test-case",
    "name" : "Christmas turkey test launch"
}"""

        def proj = SquashEntityBuilder.project {
            id = 15L
            name = "Christmas Eve"
        }
        def tc = SquashEntityBuilder.testCase {
            id = 240L
            name = "Christmas turkey test launch"
            project = proj
        }

        and:
        restTestCaseService.patchTestCase(_, _) >> tc
        cufService.findAllCustomFieldValues(_) >> []
        pathService.buildTestCasePath(_) >> '/Christmas Eve/Christmas turkey test launch'
        hierService.findParentFor(_) >> SquashEntityBuilder.tcLibrary {
            project = proj
        }


        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.patch("/api/rest/latest/test-cases/{id}", 240L)
                .accept("application/json")
                .contentType("application/json")
                .content(json))


        then:
        /*
         * Test
         */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))

        withResult(res) {

            "_type".is "test-case"
            "id".is 240
            "name".is "Christmas turkey test launch"
            "parent".test {
                "_type".is "project"
                "id".is 15
            }
        }

        /*
         * No documentation, it is essentially the same as get-test-case
         */


    }


    def "patch-scripted-test-case"() {

        given:
        def json = """{
    "_type" : "scripted-test-case",
    "name" : "Christmas turkey test launch",
    "script" : "this is Christmas Eve"
}"""

        def proj = SquashEntityBuilder.project {
            id = 15L
            name = "Christmas Eve"
        }
        def tc = SquashEntityBuilder.scriptedTestCase {
            id = 240L
            name = "Christmas turkey test launch"
            project = proj
            script = "this is Christmas Eve"
        }

        and:
        restTestCaseService.patchTestCase(_, _) >> tc
        cufService.findAllCustomFieldValues(_) >> []
        pathService.buildTestCasePath(_) >> '/Christmas Eve/Christmas turkey test launch'
        hierService.findParentFor(_) >> SquashEntityBuilder.tcLibrary {
            project = proj
        }


        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.patch("/api/rest/latest/test-cases/{id}", 240L)
                .accept("application/json")
                .contentType("application/json")
                .content(json))


        then:
        /*
         * Test
         */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))

        withResult(res) {

            "_type".is "scripted-test-case"
            "id".is 240
            "name".is "Christmas turkey test launch"
            "parent".test {
                "_type".is "project"
                "id".is 15
            }
            "script".is "this is Christmas Eve"
        }

        /*
         * No documentation, it is essentially the same as get-test-case
         */


    }


    def "patch-keyword-test-case"() {

        given:
        def json = """{
    "_type" : "keyword-test-case",
    "name" : "Christmas turkey test launch"
}"""

        def proj = SquashEntityBuilder.project {
            id = 15L
            name = "Christmas Eve"
        }
        def tc = SquashEntityBuilder.keywordTestCase {
            id = 240L
            name = "Christmas turkey test launch"
            project = proj
        }

        and:
        restTestCaseService.patchTestCase(_, _) >> tc
        cufService.findAllCustomFieldValues(_) >> []
        pathService.buildTestCasePath(_) >> '/Christmas Eve/Christmas turkey test launch'
        hierService.findParentFor(_) >> SquashEntityBuilder.tcLibrary {
            project = proj
        }


        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.patch("/api/rest/latest/test-cases/{id}", 240L)
                .accept("application/json")
                .contentType("application/json")
                .content(json))


        then:
        /*
         * Test
         */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))

        withResult(res) {

            "_type".is "keyword-test-case"
            "id".is 240
            "name".is "Christmas turkey test launch"
            "parent".test {
                "_type".is "project"
                "id".is 15
            }
        }

        /*
         * No documentation, it is essentially the same as get-test-case
         */


    }

    def "patch-test-case-with-automation-attributes"() {

        given:
        def json = """{
    "_type" : "scripted-test-case",
    "name" : "Christmas turkey test launch",
    "script" : "this is Christmas Eve",
    "automated_test_technology" : "Cucumber 4",
    "scm_repository_id" : 6,
    "automated_test_reference" : ""
}"""

        def proj = SquashEntityBuilder.project {
            id = 15L
            name = "Christmas Eve"
        }
        def cucumberTechnology = SquashEntityBuilder.automatedTestTechnology {
            id = 4L
            name = "Cucumber 4"
            actionProviderKey = "cucumber/execute@v1"
        }
        def scmRepo = SquashEntityBuilder.scmRepository {
            id = 6l
            name = "repo01"
            workingBranch = "master"
            scmServer = SquashEntityBuilder.scmServer {
                id = 2l
                url = "https://github.com/test"
            }
        }
        def tc = SquashEntityBuilder.scriptedTestCase {
            id = 240L
            name = "Christmas turkey test launch"
            project = proj
            script = "this is Christmas Eve"
            automatedTestTechnology = cucumberTechnology
            scmRepository = scmRepo
            automatedTestReference = ""
        }

        and:
        restTestCaseService.patchTestCase(_, _) >> tc
        cufService.findAllCustomFieldValues(_) >> []
        pathService.buildTestCasePath(_) >> '/Christmas Eve/Christmas turkey test launch'
        hierService.findParentFor(_) >> SquashEntityBuilder.tcLibrary {
            project = proj
        }


        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.patch("/api/rest/latest/test-cases/{id}", 240L)
                .accept("application/json")
                .contentType("application/json")
                .content(json))


        then:
        /*
         * Test
         */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))

        withResult(res) {

            "_type".is "scripted-test-case"
            "id".is 240
            "name".is "Christmas turkey test launch"
            "parent".test {
                "_type".is "project"
                "id".is 15
            }
            "script".is "this is Christmas Eve"
            "automated_test_technology".is "Cucumber 4"
            "scm_repository_id".is 6
            "scm_repository_url".is "https://github.com/test/repo01 (master)"
            "automated_test_reference".is ""
        }

        /*
         * No documentation, it is essentially the same as get-test-case
         */

    }


    //**************************************** DELETE ****************************************
    def "delete-test-case"() {
        given:
        def reportMsg = ["Les cas de test suivants ne seront pas supprimés : Test-Case3<br/>parce qu'ils sont appelés par les cas de test suivants :\",\n" +
                                 " Test-Case 1, Test-Case 4, Test-Case2<br/>\",\n" +
                                 "Le cas de test :Test-Case3<br/>est référencé dans au moins une itération. Après sa suppression, il ne pourra plus être exécuté.<br/>"
        ]

        and:
        restTestCaseService.deleteTestCase(_, _, _) >> reportMsg
        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.delete("/api/rest/latest/test-cases/{ids}?dry-run=true", "2,3", true)
                .accept("application/json")
                .contentType("application/json")
                .header(HttpHeaders.ACCEPT_LANGUAGE, LocaleContextHolder.getLocale().toString())

        )


        then:

        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))
        withResult(res) {
        }


        /*
         * Documentation
         */
        res.andDo(doc.document(
                documentationBuilder {
                    requestParams {
                        add "dry-run : indicates if you really want to delete the test case or you just want to do a simulation: if dryRun = true : to do just a delete simulation, if dryRun = false or null:  for delete test case"
                    }
                    pathParams {
                        add "ids : the list of ids of the test case"
                    }

                }
        ))
    }

    def "link-requirements"() {
        given:
        def verifiedRequirementVersions = [
            SquashEntityBuilder.requirementVersion {
                id = 12L
                name = "My first requirement"
                withDefaultRequirement()
            },
            SquashEntityBuilder.requirementVersion {
                id = 13L
                name = "My second requirement"
                withDefaultRequirement()
            },
            SquashEntityBuilder.requirementVersion {
                id = 14L
                name = "My third requirement"
                withDefaultRequirement()
            }
        ]

        def proj = SquashEntityBuilder.project {
            id = 15L
            name = "My project"
        }

        def folder = SquashEntityBuilder.testCaseFolder {
            id = 305L
            name= "My folder"
            project = proj
        }

        def testCase = SquashEntityBuilder.testCase {
            id = 240L
            name = "My test case"
            project = proj
            requirementVersions = verifiedRequirementVersions
        }

        def customFields = [cufValue {
            label = "test_is_automated"
            inputType "CHECKBOX"
            code = "AUTOMATED"
            value = false
        }]

        and:
        restTestCaseService.getOne(240) >> testCase
        1 * verifyingRequirementManagerService.linkRequirementsToTestCase([12, 13, 14], 240)
        nodeHierarchyHelpService.findParentFor(testCase) >> folder
        permService.canRead(_) >> true
        cufService.findAllCustomFieldValues(testCase) >> customFields

        when:
        def res = mockMvc.perform(
                post("/api/rest/latest/test-cases/{id}/coverages/{requirementIds}", 240L, "12,13,14")
                        .accept("application/json"))

        then:
        res.andExpect(status().isOk())
           .andExpect(content().contentType("application/json;charset=UTF-8"))

        withResult(res) {
            "_type".is "test-case"
            "id".is 240
            "name".is "My test case"
            "verified_requirements".hasSize 3
            "verified_requirements".test {
                "[0]".test {
                    "_type".is "requirement-version"
                    "id".is 12
                    "name".is "My first requirement"
                }
                "[1]".test {
                    "_type".is "requirement-version"
                    "id".is 13
                    "name".is "My second requirement"
                }
                "[2]".test {
                    "_type".is "requirement-version"
                    "id".is 14
                    "name".is "My third requirement"
                }
            }
        }

        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "id : the id of the test case"
                        add "requirementIds : the ids of the requirements to link"
                    }
                }
        ))
    }


    def "unlink-requirements"() {
        when:
        def res = mockMvc.perform(
                delete("/api/rest/latest/test-cases/{id}/coverages/{requirementIds}", 543L, "350,351")
                        .accept("application/json")
                        .contentType("application/json"))
        then:
        /*
         * Test
         */
        res.andExpect(status().isNoContent())
        1 * verifyingRequirementManagerService.unlinkRequirementsFromTestCase(_, _)

        /*
        * Documentation
        */
        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "id : the id of the test case"
                        add "requirementIds : the ids of the requirements to unlink"
                    }
                }
        ))
    }

}
