/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.controller

import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.Pageable
import org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders
import org.squashtest.tm.domain.users.Team
import org.squashtest.tm.domain.users.User
import org.squashtest.tm.domain.users.UsersGroup
import org.squashtest.tm.plugin.docutils.DocumentationSnippets
import org.squashtest.tm.plugin.mockmvc.BaseControllerSpec
import org.squashtest.tm.plugin.rest.service.RestPartyService
import org.squashtest.tm.test.domainbuilder.SquashEntityBuilder

import javax.inject.Inject

import static org.hamcrest.Matchers.any
import static org.hamcrest.Matchers.hasSize
import static org.hamcrest.Matchers.is
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import static org.squashtest.tm.plugin.docutils.DocumentationSnippets.documentationBuilder
import static org.squashtest.tm.plugin.mockmvc.TestHelper.selfRelIs
import static org.squashtest.tm.plugin.mockmvc.TestHelper.withResult

@WebMvcTest(RestUserController)
public class RestUserControllerIT extends BaseControllerSpec {

    @Inject
    private RestPartyService service

    def "browse-users"() {
        given:
        service.findAllUsers(any(Pageable.class)) >> { args ->

            def user = SquashEntityBuilder.user {
                id = 486L
                firstName = "Charles"
                lastName = "Dupond"
                login = "User-1"
                email = "charlesdupond@aaaa.aa"
                active = true
                group UsersGroup.USER
                lastConnectedOn "2018/02/11"
            }

            new PageImpl<User>([user], args[0], 5)
        }


        when:
        def res = mockMvc.perform(get("/api/rest/latest/users?page=2&size=1").header("Accept", "application/json"))


        then:

        /*
         * Test (style is plain Spring testmvc)
         */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))

        // embedded section
                .andExpect(jsonPath("_embedded.users", hasSize(1)))
                .andExpect(jsonPath("_embedded.users[0]._type", is("user")))
                .andExpect(jsonPath("_embedded.users[0].id", is(486)))
                .andExpect(jsonPath("_embedded.users[0].login", is("User-1")))
                .andExpect(jsonPath("_embedded.users[0].active", is(true)))
                .andExpect(jsonPath("_embedded.users[0].group", is("User")))
                .andExpect(jsonPath("_embedded.users[0]._links.self.href", is("http://localhost:8080/api/rest/latest/users/486")))

        // links section
                .andExpect(jsonPath("_links.first.href", is("http://localhost:8080/api/rest/latest/users?page=0&size=1")))
                .andExpect(jsonPath("_links.prev.href", is("http://localhost:8080/api/rest/latest/users?page=1&size=1")))
                .andExpect(jsonPath("_links.self.href", is("http://localhost:8080/api/rest/latest/users?page=2&size=1")))
                .andExpect(jsonPath("_links.next.href", is("http://localhost:8080/api/rest/latest/users?page=3&size=1")))
                .andExpect(jsonPath("_links.last.href", is("http://localhost:8080/api/rest/latest/users?page=4&size=1")))

        // page section
                .andExpect(jsonPath("page.size", is(1)))
                .andExpect(jsonPath("page.totalElements", is(5)))
                .andExpect(jsonPath("page.totalPages", is(5)))
                .andExpect(jsonPath("page.number", is(2)))

        /*
         * Documentation
         */
        res.andDo(doc.document(
                DocumentationSnippets.AllInOne.createBrowseAllEntities("users")
        ))

    }

    def "get-user"() {

        given:
        def team = SquashEntityBuilder.team {
            id = 567L
            name = "Team A"
            description = "<p>black panther</p>"
        }

        def user = SquashEntityBuilder.user {
            id = 486L
            firstName = "Charles"
            lastName = "Dupond"
            login = "User-1"
            email = "charlesdupond@aaaa.aa"
            active = true
            group UsersGroup.USER
            lastConnectedOn "2018/02/11"
        }

        and:
        service.findById(486) >> user

        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/users/{id}", 486).header("Accept", "application/json"))

        then:
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))

        withResult(res) {
            "_type".is "user"
            "id".is 486
            "first_name".is "Charles"
            "last_name".is "Dupond"
            "login".is "User-1"
            "email".is "charlesdupond@aaaa.aa"
            "active".is true
            "group".is "User"
            selfRelIs "http://localhost:8080/api/rest/latest/users/486"
        }

        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "id : the id of the user"
                    }
                    requestParams {
                        add DocumentationSnippets.DescriptorLists.fieldsParams
                    }
                    fields {
                        add "_type (string) : the type of the entity"
                        add "id (number) : the id of the user"
                        add "first_name (string) : the first name of the user"
                        add "last_name (string) : the last name of the user"
                        add "login (string) : the login of the user"
                        add "email (string) : the email address of the user"
                        add "active (boolean) : whether the user is activate or not"
                        add "group (string) : the group of the user belong (admin or user)"
                        add "teams (array) : the team of the user participate"
                        add "last_connected_on (string) : the date of this user was last connected"
                        add "created_by (string) : the user who created this user account"
                        add "created_on (string) : the date of this user account was created"
                        add "last_modified_by (string) : the user who last modified this user account"
                        add "last_modified_on (string) : the date of this user account was last modified"
                        add DocumentationSnippets.DescriptorLists.linksFields
                    }
                    _links {
                        add "self : link to this user"
                    }
                }
        ))
    }

    def "post-user"() {
        given:
        def json = """{
            "_type": "user",
            "first_name": "Charles",
            "last_name": "Dupond", 
            "login": "User-1",
            "password": "123456",
            "email": "charlesdupond@aaaa@aa",
            "group": "User"
        }"""

        def user = SquashEntityBuilder.user {
            id = 987L
            firstName = "Charles"
            lastName = "Dupond"
            login = "User-1"
            email = "charlesdupond@aaaa.aa"
            active = true
            group UsersGroup.USER
            lastConnectedOn "2018/03/05"
        }
        and:
        service.createParty(_) >> user

        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.post("/api/rest/latest/users")
                .accept("application/json")
                .contentType("application/json")
                .content(json))

        then:

        res.andExpect(status().isCreated())
                .andExpect(content().contentType("application/json;charset=UTF-8"))

        withResult(res) {
            "_type".is "user"
            "id".is 987
            "first_name".is "Charles"
            "last_name".is "Dupond"
            "login".is "User-1"
            "email".is "charlesdupond@aaaa.aa"
            "active".is true
            "group".is "User"
            selfRelIs "http://localhost:8080/api/rest/latest/users/987"
        }

        /*
         * Documentation
         */
        res.andDo(doc.document(
                documentationBuilder {
                    requestParams {
                        add DocumentationSnippets.DescriptorLists.fieldsParams
                    }
                    requestFields {
                        add "_type (string) : the type of the entity"
                        add "first_name (string) : the first name of the user"
                        add "last_name (string) : the last name of the user"
                        add "login (string) : the login of the user"
                        add "password (string) : the password of the user"
                        add "email (string) : the email address of the user"
                        add "group (string) : the group of the user belong (admin or user)"
                    }

                    fields {
                        relaxed = true
                        add "id (number) : the id of the user"
                        add "active (boolean) : whether the user is activate or not"
                        add "teams (array) : the team of the user participate"
                        add "last_connected_on (string) : the date of this user was last connected"
                        add "created_by (string) : the user who created this user account"
                        add "created_on (string) : the date of this user account was created"
                        add "last_modified_by (string) : the user who last modified this user account"
                        add "last_modified_on (string) : the date of this user account was last modified"
                        add DocumentationSnippets.DescriptorLists.linksFields
                    }
                    _links {
                        add "self : link to this user"
                    }
                }
        ))
    }

    def "delete-user"() {
        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.delete("/api/rest/latest/users/{ids}", "169,189")
                .accept("application/json")
                .contentType("application/json"))

        then:
        /*
         * Test
         */
        res.andExpect(status().isNoContent())
        1 * service.deleteUsers(_)

        /*
         * Documentation
         */
        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "ids : the list of ids of the users"
                    }
                }
        ))
    }

    def "get-teams"() {
        given:
        def user = SquashEntityBuilder.user {
            id = 486L
            firstName = "Charles"
            lastName = "Dupond"
            login = "User-1"
            email = "charlesdupond@aaaa.aa"
            active = true
            group UsersGroup.USER
            lastConnectedOn "2018/02/11"
        }

        def user2 = SquashEntityBuilder.user {
            id = 487L
            firstName = "John"
            lastName = "Doe"
            login = "User-2"
            email = "johndoe@aaaa.aa"
            active = true
            group UsersGroup.USER
            lastConnectedOn "2018/02/11"
        }

        and:
        service.findTeamsByUser(486L, any(Pageable.class)) >> { args ->

            def team = SquashEntityBuilder.team {
                id = 567L
                name = "Team A"
                description = "<p>black panther</p>"
                members = [user, user2].toSet()
            }

            def team2 = SquashEntityBuilder.team {
                id = 568L
                name = "Team B"
                description = "<p>black widow</p>"
                members = [user].toSet()
            }

            new PageImpl<Team>([team, team2], args[1], 2)

        }

        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/users/{id}/teams", 486).header("Accept", "application/json"))

        then:
        /*
         * Test
         */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))

        withResult(res) {
            "_embedded.teams".hasSize 2
            "_embedded.teams".test {
                "[0]".test {
                    "_type".is "team"
                    "name".is "Team A"
                    "description".is "<p>black panther</p>"
                }
                "[1]".test {
                    "_type".is "team"
                    "name".is "Team B"
                    "description".is "<p>black widow</p>"
                }
            }
            "page".test {
                "size".is 20
                "totalElements".is 2
                "totalPages".is 1
                "number".is 0
            }
            selfRelIs "http://localhost:8080/api/rest/latest/users/486/teams?page=0&size=20"
        }

        /*
         * Documentation
         */
        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "id : the id of the user"
                    }
                    requestParams {
                        add DocumentationSnippets.DescriptorLists.paginationParams
                        add DocumentationSnippets.DescriptorLists.fieldsParams
                    }
                    fields {
                        embeddedAndStop "teams (array) : the teams of this user"
                        add DocumentationSnippets.DescriptorLists.paginationFields
                        add DocumentationSnippets.DescriptorLists.linksFields
                    }
                    _links {
                        add DocumentationSnippets.DescriptorLists.paginationLinks
                    }
                }
        ))
    }

    def "add-teams"() {
        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.post("/api/rest/latest/users/{userId}/teams", 987L)
                .param("teamIds", "486, 487")
                .accept("application/json")
                .contentType("application/json"))

        then:
        /*
         * Test
         */
        res.andExpect(status().isNoContent())
        1 * service.addTeamsToUser(987, [486, 487])

        /*
         * Documentation
         */
        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "userId : the id of the user"
                    }
                    requestParams {
                        add "teamIds : the list of ids of the teams"
                    }
                }
        ))
    }

    def "disassociate-teams"() {
        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.delete("/api/rest/latest/users/{userId}/teams?teamIds=486,487", 987L)
                .accept("application/json")
                .contentType("application/json"))

        then:
        /*
         * Test
         */
        res.andExpect(status().isNoContent())
        1 * service.disassociateUserFromTeams(987, [486, 487])

        /*
         * Documentation
         */
        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "userId : the id of the user"
                    }
                    requestParams {
                        add "teamIds : the list of ids of the teams"
                    }
                }
        ))
    }

    def "patch-user"() {
        given:
        def json = """{
            "_type": "user",
            "first_name": "Charles",
            "last_name": "Dupond", 
            "login": "User-42",
            "password": "123456",
            "email": "charlesdupond@bbbb@bb",
            "active": false,
            "group": "User"
        }"""

        def user = SquashEntityBuilder.user {
            id = 987L
            firstName = "Charles"
            lastName = "Dupond"
            login = "User-42"
            email = "charlesdupond@bbbb.bb"
            active = false
            group UsersGroup.USER
            lastConnectedOn "2018/03/05"
        }

        and:
        service.patchParty(_, 987) >> user


        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.patch("/api/rest/latest/users/{id}", 987L)
                .accept("application/json")
                .contentType("application/json")
                .content(json))


        then:
        /*
         * Test
         */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))

        withResult(res) {

            "id".is 987
            "first_name".is "Charles"
            "last_name".is "Dupond"
            "login".is "User-42"
            "email".is "charlesdupond@bbbb.bb"
            "active".is false
            "group".is "User"
            selfRelIs "http://localhost:8080/api/rest/latest/users/987"
        }

        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "id : the id of the user"
                    }
                    requestParams {
                        add DocumentationSnippets.DescriptorLists.fieldsParams
                    }
                    requestFields {
                        add "_type (string) : the type of the entity"
                        add "first_name (string) : the first name of the user"
                        add "last_name (string) : the last name of the user"
                        add "login (string) : the login of the user"
                        add "password (string) : the password of the user"
                        add "email (string) : the email address of the user"
                        add "active (boolean) : whether the user is activate or not"
                        add "group (string) : the group of the user belong (admin or user)"
                    }

                    fields {
                        relaxed = true
                        add "id (number) : the id of the user"
                        add "teams (array) : the team of which the user is a member"
                        add "last_connected_on (string) : the date of this user was last connected"
                        add "created_by (string) : the user who created this user account"
                        add "created_on (string) : the date of this user account was created"
                        add "last_modified_by (string) : the user who last modified this user account"
                        add "last_modified_on (string) : the date of this user account was last modified"
                        add DocumentationSnippets.DescriptorLists.linksFields
                    }
                    _links {
                        add "self : link to this user"
                    }
                }
        ))
    }


}
