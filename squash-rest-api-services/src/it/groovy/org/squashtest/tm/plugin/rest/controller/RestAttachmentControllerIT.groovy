/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.controller

import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.data.domain.PageImpl
import org.springframework.mock.web.MockMultipartFile
import org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders
import org.squashtest.tm.domain.attachment.AttachmentContent
import org.squashtest.tm.plugin.mockmvc.BaseControllerSpec
import org.squashtest.tm.plugin.rest.service.RestAttachmentService
import org.squashtest.tm.test.domainbuilder.SquashEntityBuilder

import javax.inject.Inject

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import static org.squashtest.tm.plugin.docutils.DocumentationSnippets.DescriptorLists
import static org.squashtest.tm.plugin.docutils.DocumentationSnippets.documentationBuilder
import static org.squashtest.tm.plugin.mockmvc.TestHelper.selfRelIs
import static org.squashtest.tm.plugin.mockmvc.TestHelper.withResult


@WebMvcTest(RestAttachmentController)
class RestAttachmentControllerIT extends BaseControllerSpec {

    @Inject
    private RestAttachmentService restAttachmentService;

    // ***** GETting ***** //

    def "browse-attachments-by-entity"() {
        given:
        def attachments = [
                SquashEntityBuilder.attachment {
                    id = 5L
                    name = "new attachment 1"
                    type = "docx"
                    contentSize = 413168L
                    addedOn "2018/06/15"
                },
                SquashEntityBuilder.attachment {
                    id = 6L
                    name = "new attachment 2"
                    type = "pdf"
                    contentSize = 413168L
                    addedOn "2018/06/15"
                }
        ] as List

        and:

        restAttachmentService.findPagedAttachments(_, _) >> { args -> new PageImpl<>(attachments, args[1], 5) }

        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/{owner}/{ownerId}/attachments?page=1&size=2", "test-cases", "336")
                .header("Accept", "application/json"))

        then:

        /*
        * Test (using TestHelper)
        * */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))

        withResult(res) {
            "_embedded.attachments".hasSize 2
            "_embedded.attachments".test {
                "[0]".test {
                    "_type".is "attachment"
                    "id".is 5
                    "name".is "new attachment 1"
                    "file_type".is "docx"

                    selfRelIs "http://localhost:8080/api/rest/latest/attachments/5"
                }
                "[1]".test {
                    "_type".is "attachment"
                    "id".is 6
                    "name".is "new attachment 2"
                    "file_type".is "pdf"

                    selfRelIs "http://localhost:8080/api/rest/latest/attachments/6"
                }
            }

            selfRelIs "http://localhost:8080/api/rest/latest/test-cases/336/attachments?page=1&size=2"
            "page".test {
                "size".is 2
                "totalElements".is 5
                "totalPages".is 3
                "number".is 1
            }
        }
    }

    def "get-attachment"() {

        given:

        def attachment = SquashEntityBuilder.attachment {
            id = 3L
            name = "sample attachment"
            type = "pdf"
            contentSize = 413168L
            addedOn "2018/06/15"
        }

        and:

        restAttachmentService.findById(_) >> attachment

        when:

        def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/attachments/{id}", 3).header("Accept", "application/json"))

        then:

        /*
        * Test
        * */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))

        withResult(res) {
            "_type".is "attachment"
            "id".is 3
            "name".is "sample attachment"
            "file_type".is "pdf"
            "size".is 413168
            "added_on".is "2018-06-15T10:00:00.000+0000"

            selfRelIs "http://localhost:8080/api/rest/latest/attachments/3"
        }

        /*
        * Documentation
        * */
        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "id : the id of the attachment"
                    }
                    requestParams {
                        add DescriptorLists.fieldsParams
                    }
                    fields {
                        add "id (number) : the id of the attachment"
                        add "_type (string) : the type of the entity"
                        add "name (string) : the name of the attachment"
                        add "file_type (string) : the file type of the attachment"
                        add "size (number) : the size of the attachment"
                        add "added_on (string) : the date this attachment was uploaded"
                        addAndStop "_links (object) : related links"
                    }
                    _links {
                        add "self : link to this attachment"
                        add "content : link to download this attachment"
                    }
                }
        ))

    }

    def "rename-attachment"() {

        given:

        def attachment = SquashEntityBuilder.attachment {
            id = 3L
            name = "same stuff with a new name"
            type = "pdf"
            contentSize = 413168L
            addedOn "2018/06/15"
        }

        and:

        restAttachmentService.renameAttachment(_, _) >> attachment

        when:

        def res = mockMvc.perform(RestDocumentationRequestBuilders.patch("/api/rest/latest/attachments/{id}", 3)
                .param("name", "same stuff with a new name")
                .header("Accept", "application/json"))

        then:

        /*
        * Test
        * */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))

        withResult(res) {
            "_type".is "attachment"
            "id".is 3
            "name".is "same stuff with a new name"
            "file_type".is "pdf"
            "size".is 413168
            "added_on".is "2018-06-15T10:00:00.000+0000"

            selfRelIs "http://localhost:8080/api/rest/latest/attachments/3"
        }

        /*
        * Documentation
        * */
        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "id : the id of the attachment"
                    }
                    requestParams {
                        add "name : the new name for this attachment"
                        add DescriptorLists.fieldsParams
                    }
                    fields {
                        add "id (number) : the id of the attachment"
                        add "_type (string) : the type of the entity"
                        add "name (string) : the name of the attachment"
                        add "file_type (string) : the file type of the attachment"
                        add "size (number) : the size of the attachment"
                        add "added_on (string) : the date this attachment was uploaded"
                        addAndStop "_links (object) : related links"
                    }
                    _links {
                        add "self : link to this attachment"
                        add "content : link to download this attachment"
                    }
                }
        ))

    }

    def "download-attachment"() {
        given:

        AttachmentContent ac = new AttachmentContent()
        java.sql.Blob blob = Mock()
        InputStream is = new ByteArrayInputStream("some content".getBytes())
        blob.binaryStream >> is
        ac.streamContent = blob

        def attachment = SquashEntityBuilder.attachment {
            id = 3L
            name = "sample attachment"
            content = ac
            type = "pdf"
            contentSize = 413168L
            addedOn "2018/06/15"
        }
        and:

        restAttachmentService.findById(_) >> attachment

        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/attachments/{id}/content", 3)
                .accept("application/octet-stream")
                .contentType("application/json"))

        then:
        /*
         * Test
         */
        res.andExpect(status().isOk())

        /*
         * Documentation
         */
        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "id : the id of the attachment"
                    }
                }
        ))
    }

    def "upload-attachment"() {

        given:

        MockMultipartFile file1 = new MockMultipartFile("files", "new attachment 1.docx", "application/vnd.openxmlformats-officedocument.wordprocessingml.document", "something right".getBytes());
        MockMultipartFile file2 = new MockMultipartFile("files", "new attachment 2.pdf", "application/pdf", "even more".getBytes());

        def attachments = [
                SquashEntityBuilder.attachment {
                    id = 5L
                    name = "new attachment 1"
                    type = "docx"
                    contentSize = 413168L
                    addedOn "2018/06/15"
                },
                SquashEntityBuilder.attachment {
                    id = 6L
                    name = "new attachment 2"
                    type = "pdf"
                    contentSize = 413168L
                    addedOn "2018/06/15"
                }
        ] as List

        and:

        restAttachmentService.addAttachments(_, _) >> attachments

        when:

        def res = mockMvc.perform(RestDocumentationRequestBuilders.fileUpload("/api/rest/latest/{owner}/{ownerId}/attachments", "campaigns", "1")
                .file(file1)
                .file(file2)
                .accept("application/json")
                .contentType("multipart/form-data")
        )

        then:

        /*
        * Test
        * */
        res.andExpect(status().isCreated())
                .andExpect(content().contentType("application/json;charset=UTF-8"))

        withResult(res) {
            "_embedded.attachments".hasSize 2
            "_embedded.attachments".test {
                "[0]".test {
                    "_type".is "attachment"
                    "id".is 5
                    "name".is "new attachment 1"
                    "file_type".is "docx"

                    selfRelIs "http://localhost:8080/api/rest/latest/attachments/5"
                }
                "[1]".test {
                    "_type".is "attachment"
                    "id".is 6
                    "name".is "new attachment 2"
                    "file_type".is "pdf"

                    selfRelIs "http://localhost:8080/api/rest/latest/attachments/6"
                }
            }
        }

        /*
        * Documentation
        * */
        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "owner : the owner of the attachment"
                        add "ownerId : the id of the owner"
                    }

                    fields {
                        embeddedAndStop "attachments (array) : the attachments just uploaded"
                    }
                }
        ))

    }

    def "delete-attachment"() {

        when:

        def res = mockMvc.perform(RestDocumentationRequestBuilders.delete("/api/rest/latest/{owner}/{ownerId}/attachments?attachmentIds=7,8,9", "campaigns", "1")
                .accept("application/json")
                .contentType("multipart/form-data")
        )

        then:

        /*
        * Test
        * */

        res.andExpect(status().isNoContent())
        1 * restAttachmentService.deleteAttachments(_, _)

        /*
        * Documentation
        * */
        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "owner : the owner of the attachment"
                        add "ownerId : the id of the owner"
                    }
                    requestParams {
                        add "attachmentIds : the ids of the attachments to delete"
                    }
                }
        ))

    }

}
