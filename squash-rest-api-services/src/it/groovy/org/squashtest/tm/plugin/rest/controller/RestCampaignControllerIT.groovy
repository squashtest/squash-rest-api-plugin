/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.controller

import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.data.domain.PageImpl
import org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders
import org.squashtest.tm.domain.campaign.Campaign
import org.squashtest.tm.domain.campaign.CampaignStatus
import org.squashtest.tm.domain.campaign.CampaignTestPlanItem
import org.squashtest.tm.plugin.mockmvc.BaseControllerSpec
import org.squashtest.tm.plugin.rest.core.service.NodeHierarchyHelpService
import org.squashtest.tm.plugin.rest.service.RestCampaignService
import org.squashtest.tm.service.customfield.CustomFieldValueFinderService
import org.squashtest.tm.service.internal.library.PathService
import org.squashtest.tm.test.domainbuilder.SquashEntityBuilder

import javax.inject.Inject

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import static org.squashtest.tm.plugin.docutils.DocumentationSnippets.AllInOne
import static org.squashtest.tm.plugin.docutils.DocumentationSnippets.DescriptorLists
import static org.squashtest.tm.plugin.docutils.DocumentationSnippets.documentationBuilder
import static org.squashtest.tm.plugin.mockmvc.TestHelper.selfRelIs
import static org.squashtest.tm.plugin.mockmvc.TestHelper.withResult
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.cufValue

/**
 * Created by jlor on 20/07/2017.
 */
@WebMvcTest(RestCampaignController)
public class RestCampaignControllerIT extends BaseControllerSpec {

    @Inject
    private RestCampaignService restCampaignService;

    @Inject
    private PathService pathService;

    @Inject
    private NodeHierarchyHelpService hierService;

    @Inject
    private CustomFieldValueFinderService cufService;

    def "post-campaign"() {
        given:
        def json = """{

            "_type": "campaign",
            "name": "Campaign Test",
            "reference": "ABCD",
            "status": "PLANNED",
            "description": "<p>Sed eget rhoncus sapien. Nam et pulvinar nisi. su Do</p>",
            "scheduled_start_date": "2021-08-31T22:00:00.000+0000",
            "scheduled_end_date": "2031-09-29T22:00:00.000+0000",
            "actual_start_date": "2034-09-29T22:00:00.000+0000",
            "actual_end_date": "2035-09-29T22:00:00.000+0000",
            "actual_start_auto": false,
            "actual_end_auto": false,
            "custom_fields" : [ {
                "code" : "CUF_A",
                "label" : "Cuf A",
                "value" : "value of A"
                }, {
                "code" : "CUF_B",
                "label" : "Cuf B",
                "value" : "value of B"
             } ],
            "parent": {
                "_type": "campaign-folder",
                "id": 104
            }
        }
        """

        def campaignProject = SquashEntityBuilder.project {
            id = 44l
            name = "sample project"
        }
        def campaignFolder = SquashEntityBuilder.campaignFolder {
            id = 7l
            name = "campaign folder"
            project = campaignProject
        }
        def campaign = SquashEntityBuilder.campaign {
            id = 332L
            name = "Campaign Test"
            reference = "ABCD"
            description = "<p>Sed eget rhoncus sapien. Nam et pulvinar nisi. su Do</p>"
            status = CampaignStatus.PLANNED
            project = campaignProject

            actualStartDate "2034/09/29"
            actualEndDate "2035/09/29"
            actualStartAuto = false
            actualEndAuto = false
            scheduledStartDate "2021/08/31"
            scheduledEndDate "2031/09/29"
        }
        def cufs = [
                cufValue {
                    label = "Cuf A"
                    inputType "PLAIN_TEXT"
                    code = "CUF_A"
                    value = "value of A"
                },
                cufValue {
                    label = "Cuf B"
                    inputType "PLAIN_TEXT"
                    code = "CUF_B"
                    value = "value of B"
                }
        ]

        and:
        restCampaignService.createCampaign(_) >> campaign
        hierService.findParentFor(_) >> campaignFolder
        cufService.findAllCustomFieldValues(_) >> cufs
        pathService.buildCampaignPath(332L) >> "/${campaignProject.name}/${campaignFolder.name}/${campaign.name}"

        when:

        def res = mockMvc.perform(RestDocumentationRequestBuilders.post("/api/rest/latest/campaigns")
                .accept("application/json")
                .contentType("application/json")
                .content(json))

        then:

        /*
        * Test (using the TestHelper)
        * */
        res.andExpect(status().isCreated())
                .andExpect(content().contentType("application/json;charset=UTF-8"))

        withResult(res) {
            "_type".is "campaign"
            "id".is 332
            "name".is "Campaign Test"
            "reference".is "ABCD"
            "description".is "<p>Sed eget rhoncus sapien. Nam et pulvinar nisi. su Do</p>"
            "status".is "PLANNED"
            "project".test {
                "_type".is "project"
                "id".is 44
                selfRelIs "http://localhost:8080/api/rest/latest/projects/44"
            }
            "path".is "/sample project/campaign folder/Campaign Test"
            "parent".test {
                "_type".is "campaign-folder"
                "id".is 7
                selfRelIs "http://localhost:8080/api/rest/latest/campaign-folders/7"
            }

            "created_by".is "admin"
            "created_on".is "2017-06-15T10:00:00.000+0000"
            "last_modified_by".is "admin"
            "last_modified_on".is "2017-06-15T10:00:00.000+0000"

            "actual_start_date".is "2034-09-29T10:00:00.000+0000"
            "actual_end_date".is "2035-09-29T10:00:00.000+0000"
            "actual_start_auto".is false
            "actual_end_auto".is false
            "scheduled_start_date".is "2021-08-31T10:00:00.000+0000"
            "scheduled_end_date".is "2031-09-29T10:00:00.000+0000"

            "custom_fields".hasSize 2
            "custom_fields".test {
                "[0]".test {
                    "code".is "CUF_A"
                    "label".is "Cuf A"
                    "value".is "value of A"
                }
                "[1]".test {
                    "code".is "CUF_B"
                    "label".is "Cuf B"
                    "value".is "value of B"
                }
            }

            "iterations".hasSize 0

            "test_plan".hasSize 0

            "_links".test {
                "self.href".is "http://localhost:8080/api/rest/latest/campaigns/332"
            }
        }

        /*
             * No documentation, it is essentially the same as get-campaign
             */
    }

    def "get-campaign"() {

        given:

        def campaignProject = SquashEntityBuilder.project {
            id = 44l
            name = "sample project"
        }
        def campaignFolder = SquashEntityBuilder.campaignFolder {
            id = 7l
            name = "campaign folder"
        }
        def campaign = SquashEntityBuilder.campaign {
            id = 112l
            name = "sample campaign"
            reference = "SAMP_CAMP"
            description = "<p>This is a sample campaign.</p>"
            project = campaignProject

            createdBy "User-1"
            createdOn "2017/07/20"
            lastModifiedBy "User-2"
            lastModifiedOn "2017/07/21"

            actualStartDate "2017/08/01"
            actualEndDate "2017/08/31"
            actualStartAuto = false
            actualEndAuto = false
            scheduledStartDate "2017/08/01"
            scheduledEndDate "2017/08/31"

            iterations = [
                    SquashEntityBuilder.iteration {
                        id = 91l
                        name = "iteration 1"
                    },
                    SquashEntityBuilder.iteration {
                        id = 92l
                        name = "iteration 2"
                    }
            ] as List

            testPlan = [
                    SquashEntityBuilder.campaignTestPlanItem {
                        id = 41l
                    },
                    SquashEntityBuilder.campaignTestPlanItem {
                        id = 42l
                    },
                    SquashEntityBuilder.campaignTestPlanItem {
                        id = 43l
                    }
            ] as List
        }

        def cufs = [
                SquashEntityBuilder.cufValue {
                    label = "Cuf A"
                    inputType "PLAIN_TEXT"
                    code = "CUF_A"
                    value = "value of A"
                },
                SquashEntityBuilder.cufValue {
                    label = "Cuf B"
                    inputType "PLAIN_TEXT"
                    code = "CUF_B"
                    value = "value of B"
                }
        ]

        and:

        restCampaignService.getOne(112) >> campaign

        pathService.buildCampaignPath(112) >> "/${campaignProject.name}/${campaignFolder.name}/${campaign.name}"

        hierService.findParentFor(campaign) >> campaignFolder

        cufService.findAllCustomFieldValues(campaign) >> cufs

        when:

        def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/campaigns/{id}", 112)
                .header("Accept", "application/json"))

        then:

        /*
        * Test (using the TestHelper)
        * */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))

        withResult(res) {
            "_type".is "campaign"
            "id".is 112
            "name".is "sample campaign"
            "reference".is "SAMP_CAMP"
            "description".is "<p>This is a sample campaign.</p>"
            "status".is "UNDEFINED"
            "project".test {
                "_type".is "project"
                "id".is 44
                "name".is "sample project"
                selfRelIs "http://localhost:8080/api/rest/latest/projects/44"
            }
            "path".is "/sample project/campaign folder/sample campaign"
            "parent".test {
                "_type".is "campaign-folder"
                "id".is 7
                "name".is "campaign folder"
                selfRelIs "http://localhost:8080/api/rest/latest/campaign-folders/7"
            }

            "created_by".is "User-1"
            "created_on".is "2017-07-20T10:00:00.000+0000"
            "last_modified_by".is "User-2"
            "last_modified_on".is "2017-07-21T10:00:00.000+0000"

            "actual_start_date".is "2017-08-01T10:00:00.000+0000"
            "actual_end_date".is "2017-08-31T10:00:00.000+0000"
            "actual_start_auto".is false
            "actual_end_auto".is false
            "scheduled_start_date".is "2017-08-01T10:00:00.000+0000"
            "scheduled_end_date".is "2017-08-31T10:00:00.000+0000"

            "custom_fields".hasSize 2
            "custom_fields".test {
                "[0]".test {
                    "code".is "CUF_A"
                    "label".is "Cuf A"
                    "value".is "value of A"
                }
                "[1]".test {
                    "code".is "CUF_B"
                    "label".is "Cuf B"
                    "value".is "value of B"
                }
            }

            "iterations".hasSize 2
            "iterations".test {
                "[0]".test {
                    "_type".is "iteration"
                    "id".is 91
                    "name".is "iteration 1"
                    selfRelIs "http://localhost:8080/api/rest/latest/iterations/91"
                }
                "[1]".test {
                    "_type".is "iteration"
                    "id".is 92
                    "name".is "iteration 2"
                    selfRelIs "http://localhost:8080/api/rest/latest/iterations/92"
                }
            }

            "test_plan".hasSize 3
            "test_plan".test {
                "[0]".test {
                    "_type".is "campaign-test-plan-item"
                    "id".is 41
                    selfRelIs "http://localhost:8080/api/rest/latest/campaign-test-plan-items/41"
                }
                "[1]".test {
                    "_type".is "campaign-test-plan-item"
                    "id".is 42
                    selfRelIs "http://localhost:8080/api/rest/latest/campaign-test-plan-items/42"
                }
                "[2]".test {
                    "_type".is "campaign-test-plan-item"
                    "id".is 43
                    selfRelIs "http://localhost:8080/api/rest/latest/campaign-test-plan-items/43"
                }
            }

            "_links".test {
                "self.href".is "http://localhost:8080/api/rest/latest/campaigns/112"
                "project.href".is "http://localhost:8080/api/rest/latest/projects/44"
                "iterations.href".is "http://localhost:8080/api/rest/latest/campaigns/112/iterations"
                "test-plan.href".is "http://localhost:8080/api/rest/latest/campaigns/112/test-plan"
            }
        }

        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "id : the id of the campaign"
                    }
                    requestParams {
                        add DescriptorLists.fieldsParams
                    }

                    fields {
                        add "_type (string) : the type of this entity"
                        add "id (number) : the id of this campaign"
                        add "name (string) : the name of this campaign"
                        add "reference (string) : the reference of this campaign"
                        add "description (string) : the description of this campaign"
                        add "status (string) : the status of this campaign"
                        addAndStop "project (object) : the project of this campaign"
                        add "path (string) : the path of this campaign"
                        addAndStop "parent (object) : the parent entity of this campaign"
                        add DescriptorLists.auditableFields
                        add DescriptorLists.scheduleFields
                        add "scheduled_start_date (string) : scheduled start date"
                        add "scheduled_end_date (string) : scheduled end date"
                        addAndStop "custom_fields (array) : the custom fields of this campaign"
                        addAndStop "iterations (array) : the iterations of this campaign"
                        addAndStop "test_plan (array) : the test-plan of this campaign"
                        add "attachments (array) : the attachments of this campaign"
                        add DescriptorLists.linksFields
                    }

                    _links {
                        add "self : link to this campaign"
                        add "project : link to the project of this campaign"
                        add "iterations : link to the iterations of this campaign"
                        add "test-plan : link to the test plan of this campaign"
                        add "attachments : link to the attachments of this campaign"
                    }
                }
        ))
    }

    def "get-campaign-by-name"() {
        given:
        restCampaignService.findAllByName("sample campaign",_) >> { args ->
            def campaign1 = SquashEntityBuilder.campaign {
                id = 41l
                name = "sample campaign"
                reference = "SAMP_CAMP_1"
            }
            def campaign2 = SquashEntityBuilder.campaign {
                id = 46l
                name = "sample campaign"
                reference = "SAMP_CAMP_2"
            }

            new PageImpl<Campaign>([campaign1, campaign2], args[1], 2)
        }

        when:
        def res = mockMvc.perform(
                RestDocumentationRequestBuilders.get("/api/rest/latest/campaignsByName/{name}", "sample campaign")
                        .header("Accept", "application/json"))
        then:
        /* Test (using TestHelper) */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))

        withResult(res) {
            "_embedded.campaigns".hasSize 2
            "_embedded.campaigns".test {
                "[0]".test {
                    "_type".is "campaign"
                    "id".is 41
                    "name".is "sample campaign"
                    "reference".is "SAMP_CAMP_1"
                    selfRelIs "http://localhost:8080/api/rest/latest/campaigns/41"
                }
                "[1]".test {
                    "_type".is "campaign"
                    "id".is 46
                    "name".is "sample campaign"
                    "reference".is "SAMP_CAMP_2"
                    selfRelIs "http://localhost:8080/api/rest/latest/campaigns/46"
                }
            }
            selfRelIs "http://localhost:8080/api/rest/latest/campaignsByName/sample%20campaign?page=0&size=20"
            "page".test {
                "size".is 20
                "totalElements".is 2
                "totalPages".is 1
                "number".is 0
            }
        }

        /* Documentation */
        res.andDo(doc.document(
                AllInOne.createBrowseAllEntities("campaigns")
        ))
    }

    def "patch-campaign"() {
        given:
        def json = """{

            "_type": "campaign",
            "name": "Campaign Test",
            "reference": "ABCD",
            "status": "IN_PROGRESS",
            "description": "<p>Sed eget rhoncus sapien. Nam et pulvinar nisi. su Do</p>",
            "scheduled_start_date": "2021-08-31T22:00:00.000+0000",
            "scheduled_end_date": "2031-09-29T22:00:00.000+0000",
            "actual_start_date": "2034-09-29T22:00:00.000+0000",
            "actual_end_date": "2035-09-29T22:00:00.000+0000"
        }
        """

        def campaignProject = SquashEntityBuilder.project {
            id = 44l
            name = "sample project"
        }
        def campaignFolder = SquashEntityBuilder.campaignFolder {
            id = 7l
            name = "campaign folder"
            project = campaignProject
        }


        def campaignPatched = SquashEntityBuilder.campaign {
            id = 332L
            name = "Campaign Test"
            reference = "ABCD"
            description = "<p>Sed eget rhoncus sapien. Nam et pulvinar nisi. su Do</p>"
            status = CampaignStatus.IN_PROGRESS
            project = campaignProject
            custom_fields = cufs

            actualStartDate "2034/09/29"
            actualEndDate "2035/09/29"
            actualStartAuto = false
            actualEndAuto = false
            scheduledStartDate "2021/08/31"
            scheduledEndDate "2031/09/29"


        }


        and:
//        restCampaignService.getOne(_) >> campaign
        restCampaignService.patchCampaign(_, _) >> campaignPatched
        hierService.findParentFor(_) >> campaignFolder
        cufService.findAllCustomFieldValues(_) >> []
        pathService.buildCampaignPath(332L) >> "/${campaignProject.name}/${campaignFolder.name}/${campaignPatched.name}"

        when:

        def res = mockMvc.perform(RestDocumentationRequestBuilders.patch("/api/rest/latest/campaigns/{id}", 332)
                .accept("application/json")
                .contentType("application/json")
                .content(json))

        then:

        /*
        * Test (using the TestHelper)
        * */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))

        withResult(res) {
            "_type".is "campaign"
            "id".is 332
            "name".is "Campaign Test"
            "reference".is "ABCD"
            "description".is "<p>Sed eget rhoncus sapien. Nam et pulvinar nisi. su Do</p>"
            "status".is "IN_PROGRESS"
            "project".test {
                "_type".is "project"
                "id".is 44
                selfRelIs "http://localhost:8080/api/rest/latest/projects/44"
            }
            "path".is "/sample project/campaign folder/Campaign Test"
            "parent".test {
                "_type".is "campaign-folder"
                "id".is 7
                selfRelIs "http://localhost:8080/api/rest/latest/campaign-folders/7"
            }

            "created_by".is "admin"
            "created_on".is "2017-06-15T10:00:00.000+0000"
            "last_modified_by".is "admin"
            "last_modified_on".is "2017-06-15T10:00:00.000+0000"

            "actual_start_date".is "2034-09-29T10:00:00.000+0000"
            "actual_end_date".is "2035-09-29T10:00:00.000+0000"
            "actual_start_auto".is false
            "actual_end_auto".is false
            "scheduled_start_date".is "2021-08-31T10:00:00.000+0000"
            "scheduled_end_date".is "2031-09-29T10:00:00.000+0000"

            "iterations".hasSize 0

            "test_plan".hasSize 0

            "_links".test {
                "self.href".is "http://localhost:8080/api/rest/latest/campaigns/332"
            }
        }

        /*
             * No documentation, it is essentially the same as get-campaign
             */
    }

    def "delete-campaign"() {
        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.delete("/api/rest/latest/campaigns/{ids}", "169,189")
                .accept("application/json")
                .contentType("application/json"))

        then:
        /*
         * Test
         */
        res.andExpect(status().isNoContent())
        1 * restCampaignService.deleteCampaignsByIds(_)

        /*
         * Documentation
         */
        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "ids : the list of ids of the campaigns"
                    }
                }
        ))
    }

    def "browse-campaign"() {

        given:

        restCampaignService.findAllReadable(_) >> { args ->
            def campaign1 = SquashEntityBuilder.campaign {
                id = 41l
                name = "sample campaign 1"
                reference = "SAMP_CAMP_1"
            }
            def campaign2 = SquashEntityBuilder.campaign {
                id = 46l
                name = "sample campaign 2"
                reference = "SAMP_CAMP_2"
            }

            new PageImpl<Campaign>([campaign1, campaign2], args[0], 4)
        }

        when:

        def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/campaigns?size=2&page=1")
                .header("Accept", "application/json"))

        then:

        /*
        * Test (using the TestHelper)
        * */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))

        withResult(res) {
            "_embedded.campaigns".hasSize 2
            "_embedded.campaigns".test {
                "[0]".test {
                    "_type".is "campaign"
                    "id".is 41
                    "name".is "sample campaign 1"
                    "reference".is "SAMP_CAMP_1"
                    selfRelIs "http://localhost:8080/api/rest/latest/campaigns/41"
                }
                "[1]".test {
                    "_type".is "campaign"
                    "id".is 46
                    "name".is "sample campaign 2"
                    "reference".is "SAMP_CAMP_2"
                    selfRelIs "http://localhost:8080/api/rest/latest/campaigns/46"
                }
            }
            selfRelIs "http://localhost:8080/api/rest/latest/campaigns?page=1&size=2"
            "page".test {
                "size".is 2
                "totalElements".is 4
                "totalPages".is 2
                "number".is 1
            }
        }

        res.andDo(doc.document(
                AllInOne.createBrowseAllEntities("campaigns")
        ))
    }

    def "get-campaign-iterations"() {

        given:

        def iteration = SquashEntityBuilder.iteration {
            id = 10l
            name = "sample iteration 1"
            reference = "SAMP_IT_1"
            description = "<p>This iteration is a sample one...</p>"

            createdBy "User-1"
            createdOn "2017/07/21"
            lastModifiedBy "admin"
            lastModifiedOn "2017/07/22"
            actualStartDate "2017/08/01"
            actualEndDate "2017/08/30"
            actualStartAuto = false
            actualEndAuto = false

            campaign = SquashEntityBuilder.campaign {
                id = 36l
                name = "sample parent campaign"
            }

            testPlans = [
                    SquashEntityBuilder.iterationTestPlanItem {
                        id = 2l
                        executionStatus "FAILURE"
                        referencedTestCase = SquashEntityBuilder.testCase {
                            id = 5l
                            name = "ref test case 5"
                            reference = "TC5"
                        }
                        referencedDataset = SquashEntityBuilder.dataset {
                            id = 6l
                            name = "ref dataset 6"
                        }
                    },
                    SquashEntityBuilder.iterationTestPlanItem {
                        id = 4l
                        executionStatus "SUCCESS"
                        referencedTestCase = SquashEntityBuilder.testCase {
                            id = 7l
                            name = "ref test case 7"
                            reference = "TC7"
                        }
                        referencedDataset = SquashEntityBuilder.dataset {
                            id = 101l
                            name = "ref dataset 101"
                        }
                    }
            ]

            testSuites = [
                    SquashEntityBuilder.testSuite {
                        id = 88l
                    },
                    SquashEntityBuilder.testSuite {
                        id = 11l
                    },
                    SquashEntityBuilder.testSuite {
                        id = 14l
                    }
            ]
        }

        def cufs = [
                SquashEntityBuilder.cufValue {
                    label = "Cuf Z"
                    inputType "PLAIN_TEXT"
                    code = "CUF_Z"
                    value = "value of Z"
                },
                SquashEntityBuilder.cufValue {
                    label = "Cuf Y"
                    inputType "PLAIN_TEXT"
                    code = "CUF_Y"
                    value = "value of Y"
                }
        ]
        and:

        restCampaignService.findIterations(36, _) >> { args ->
            new PageImpl<>([iteration], args[1], 3)
        }

        cufService.findAllCustomFieldValues(iteration) >> cufs

        when:

        def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/campaigns/{id}/iterations?size=1&page=1", 36)
                .header("Accept", "application/json"))

        then:

        /*
        * Test (using the TestHelper)
        */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))

        withResult(res) {
            "_embedded.iterations".hasSize 1
            "_embedded.iterations".test {
                "[0]".test {
                    "_type".is "iteration"
                    "id".is 10
                    "name".is "sample iteration 1"
                    "reference".is "SAMP_IT_1"
                    "description".is "<p>This iteration is a sample one...</p>"
                    "parent".test {
                        "_type".is "campaign"
                        "id".is 36
                        "name".is "sample parent campaign"
                        selfRelIs "http://localhost:8080/api/rest/latest/campaigns/36"
                    }
                    "created_by".is "User-1"
                    "created_on".is "2017-07-21T10:00:00.000+0000"
                    "last_modified_by".is "admin"
                    "last_modified_on".is "2017-07-22T10:00:00.000+0000"
                    "actual_start_date".is "2017-08-01T10:00:00.000+0000"
                    "actual_end_date".is "2017-08-30T10:00:00.000+0000"
                    "actual_start_auto".is false
                    "actual_end_auto".is false
                    "custom_fields".test {
                        "[0]".test {
                            "code".is "CUF_Z"
                            "label".is "Cuf Z"
                            "value".is "value of Z"
                        }
                        "[1]".test {
                            "code".is "CUF_Y"
                            "label".is "Cuf Y"
                            "value".is "value of Y"
                        }
                    }
                    /*
                    Removed the test plan from the default view, because some test plans can be so large (several thousands)
                    it would be unwise to dump them all on a unsuspecting client

                    "test_plan".hasSize 2
                    "test_plan".test {
                        "[0]".test {
                            "_type".is "iteration-test-plan-item"
                            "id".is 2
                            "execution_status".is "FAILURE"
                            "referenced_test_case".test {
                                "_type".is "test-case"
                                "id".is 5
                                "name".is "ref test case 5"
                                "reference".is "TC5"
                                selfRelIs "http://localhost:8080/api/rest/latest/test-cases/5"
                            }
                            "referenced_dataset".test {
                                "_type".is "dataset"
                                "id".is 6
                                "name".is "ref dataset 6"
                                selfRelIs "http://localhost:8080/api/rest/latest/datasets/6"
                            }
                            selfRelIs "http://localhost:8080/api/rest/latest/iteration-test-plan-items/2"
                        }
                        "[1]".test {
                            "_type".is "iteration-test-plan-item"
                            "id".is 4
                            "execution_status".is "SUCCESS"
                            "referenced_test_case".test {
                                "_type".is "test-case"
                                "id".is 7
                                "name".is "ref test case 7"
                                "reference".is "TC7"
                                selfRelIs "http://localhost:8080/api/rest/latest/test-cases/7"
                            }
                            "referenced_dataset".test {
                                "_type".is "dataset"
                                "id".is 101
                                "name".is "ref dataset 101"
                                selfRelIs "http://localhost:8080/api/rest/latest/datasets/101"
                            }
                            selfRelIs "http://localhost:8080/api/rest/latest/iteration-test-plan-items/4"
                        }
                    }
                    */
                    "test_suites".hasSize 3
                    "test_suites".test {
                        "[0]".test {
                            "_type".is "test-suite"
                            "id".is 88
                            selfRelIs "http://localhost:8080/api/rest/latest/test-suites/88"
                        }
                        "[1]".test {
                            "_type".is "test-suite"
                            "id".is 11
                            selfRelIs "http://localhost:8080/api/rest/latest/test-suites/11"
                        }
                        "[2]".test {
                            "_type".is "test-suite"
                            "id".is 14
                            selfRelIs "http://localhost:8080/api/rest/latest/test-suites/14"
                        }
                    }
                    selfRelIs "http://localhost:8080/api/rest/latest/iterations/10"
                }
            }
            "_links.first.href".is "http://localhost:8080/api/rest/latest/campaigns/36/iterations?page=0&size=1"
            "_links.prev.href".is "http://localhost:8080/api/rest/latest/campaigns/36/iterations?page=0&size=1"
            selfRelIs "http://localhost:8080/api/rest/latest/campaigns/36/iterations?page=1&size=1"
            "_links.next.href".is "http://localhost:8080/api/rest/latest/campaigns/36/iterations?page=2&size=1"
            "_links.last.href".is "http://localhost:8080/api/rest/latest/campaigns/36/iterations?page=2&size=1"
            "page".test {
                "size".is 1
                "totalElements".is 3
                "totalPages".is 3
                "number".is 1
            }
        }

        res.andDo(doc.document(
                AllInOne.createListEntityContent("iterations", "campaign")
        ))
    }

    def "get-campaign-test-plan"() {

        given:

        def parentCampaign = SquashEntityBuilder.campaign {
            id = 64l
            name = "sample campaign 64"
            reference = "SAMP_CAMP_64"
        }
        def ctpi1 = SquashEntityBuilder.campaignTestPlanItem {
            id = 4l
            referencedTestCase = SquashEntityBuilder.testCase {
                id = 8l
                name = "sample test case 8"
                reference = "TC-8"
            }
            referencedDataset = SquashEntityBuilder.dataset {
                id = 90l
                name = "sample dataset 90"
            }
            assignedUser "User-1"
            campaign = parentCampaign
        }
        def ctpi2 = SquashEntityBuilder.campaignTestPlanItem {
            id = 70l
            referencedTestCase = SquashEntityBuilder.scriptedTestCase {
                id = 10l
                name = "sample test case 10"
                reference = "TC-10"
            }
            referencedDataset = SquashEntityBuilder.dataset {
                id = 2l
                name = "sample dataset 2"
            }
            assignedUser "User-1"
            campaign = parentCampaign
        }
        def ctpi3 = SquashEntityBuilder.campaignTestPlanItem {
            id = 71l
            referencedTestCase = SquashEntityBuilder.keywordTestCase {
                id = 11l
                name = "sample test case 11"
                reference = "TC-11"
            }
            referencedDataset = SquashEntityBuilder.dataset {
                id = 3l
                name = "sample dataset 3"
            }
            assignedUser "User-1"
            campaign = parentCampaign
        }
        and:

        restCampaignService.findTestPlan(64, _) >> { args ->
            new PageImpl<CampaignTestPlanItem>([ctpi1, ctpi2, ctpi3], args[1], 4)
        }

        when:

        def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/campaigns/{id}/test-plan?size=2&page=1", 64)
                .header("Accept", "application/json"))

        then:

        /*
        * Test (using the TestHelper)
        */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))

        withResult(res) {
            "_embedded.campaign-test-plan-items".hasSize 3
            "_embedded.campaign-test-plan-items".test {
                "[0]".test {
                    "_type".is "campaign-test-plan-item"
                    "id".is 4
                    "referenced_test_case".test {
                        "_type".is "test-case"
                        "id".is 8
                        "reference".is "TC-8"
                        "name".is "sample test case 8"
                        selfRelIs "http://localhost:8080/api/rest/latest/test-cases/8"
                    }
                    "referenced_dataset".test {
                        "_type".is "dataset"
                        "id".is 90
                        "name".is "sample dataset 90"
                        selfRelIs "http://localhost:8080/api/rest/latest/datasets/90"
                    }
                    "assigned_to".is "User-1"
                    "campaign".test {
                        "_type".is "campaign"
                        "id".is 64
                        "name".is "sample campaign 64"
                        "reference".is "SAMP_CAMP_64"
                        selfRelIs "http://localhost:8080/api/rest/latest/campaigns/64"
                    }
                    selfRelIs "http://localhost:8080/api/rest/latest/campaign-test-plan-items/4"
                }
                "[1]".test {
                    "_type".is "campaign-test-plan-item"
                    "id".is 70
                    "referenced_test_case".test {
                        "_type".is "scripted-test-case"
                        "id".is 10
                        "reference".is "TC-10"
                        "name".is "sample test case 10"
                        selfRelIs "http://localhost:8080/api/rest/latest/test-cases/10"
                    }
                    "referenced_dataset".test {
                        "_type".is "dataset"
                        "id".is 2
                        "name".is "sample dataset 2"
                        selfRelIs "http://localhost:8080/api/rest/latest/datasets/2"
                    }
                    "assigned_to".is "User-1"
                    "campaign".test {
                        "_type".is "campaign"
                        "id".is 64
                        "name".is "sample campaign 64"
                        "reference".is "SAMP_CAMP_64"
                        selfRelIs "http://localhost:8080/api/rest/latest/campaigns/64"
                    }
                    selfRelIs "http://localhost:8080/api/rest/latest/campaign-test-plan-items/70"
                }
                "[2]".test {
                    "_type".is "campaign-test-plan-item"
                    "id".is 71
                    "referenced_test_case".test {
                        "_type".is "keyword-test-case"
                        "id".is 11
                        "reference".is "TC-11"
                        "name".is "sample test case 11"
                        selfRelIs "http://localhost:8080/api/rest/latest/test-cases/11"
                    }
                    "referenced_dataset".test {
                        "_type".is "dataset"
                        "id".is 3
                        "name".is "sample dataset 3"
                        selfRelIs "http://localhost:8080/api/rest/latest/datasets/3"
                    }
                    "assigned_to".is "User-1"
                    "campaign".test {
                        "_type".is "campaign"
                        "id".is 64
                        "name".is "sample campaign 64"
                        "reference".is "SAMP_CAMP_64"
                        selfRelIs "http://localhost:8080/api/rest/latest/campaigns/64"
                    }
                    selfRelIs "http://localhost:8080/api/rest/latest/campaign-test-plan-items/71"
                }
            }
            selfRelIs "http://localhost:8080/api/rest/latest/campaigns/64/test-plan?page=1&size=2"
            "page".test {
                "size".is 2
                "totalElements".is 4
                "totalPages".is 2
                "number".is 1
            }
        }

        res.andDo(doc.document(
                AllInOne.createListEntityContent("campaign-test-plan-items", "campaign")
        ))
    }

}
