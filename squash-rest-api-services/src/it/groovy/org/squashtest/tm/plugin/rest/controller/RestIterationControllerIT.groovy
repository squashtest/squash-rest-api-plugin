/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.controller

import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.data.domain.PageImpl
import org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders
import org.squashtest.tm.domain.campaign.IterationTestPlanItem
import org.squashtest.tm.domain.campaign.TestSuite
import org.squashtest.tm.plugin.docutils.DocumentationSnippets
import org.squashtest.tm.plugin.mockmvc.BaseControllerSpec
import org.squashtest.tm.plugin.rest.core.service.NodeHierarchyHelpService
import org.squashtest.tm.plugin.rest.service.RestCampaignService
import org.squashtest.tm.plugin.rest.service.RestIterationService
import org.squashtest.tm.service.customfield.CustomFieldValueFinderService
import org.squashtest.tm.test.domainbuilder.SquashEntityBuilder

import javax.inject.Inject

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import static org.squashtest.tm.plugin.docutils.DocumentationSnippets.AllInOne
import static org.squashtest.tm.plugin.docutils.DocumentationSnippets.documentationBuilder
import static org.squashtest.tm.plugin.mockmvc.TestHelper.selfRelIs
import static org.squashtest.tm.plugin.mockmvc.TestHelper.withResult

/**
 * Created by jlor on 03/08/2017.
 */
@WebMvcTest(RestIterationController)
public class RestIterationControllerIT extends BaseControllerSpec {

    @Inject
    private RestIterationService restIterationService

    @Inject
    private RestCampaignService campaignService

    @Inject
    private CustomFieldValueFinderService cufService

    @Inject
    private NodeHierarchyHelpService hierService


    def "post-iteration"() {
        given:

        def json = """{
            "_type": "iteration",
            "name": "new iteration",
            "reference": "NEW_IT",
            "description": "<p>A new iteration</p>",
            "parent": {
                "_type": "campaign",
                "id": 2
            },
            "scheduled_start_date": "2017-04-09T22:00:00.000+0000",
            "scheduled_end_date": "2017-04-14T23:00:00.000+0000",
            "actual_start_date": "2017-04-10T22:00:00.000+0000",
            "actual_end_date": "2017-04-15T22:00:00.000+0000",
            "actual_start_auto": false,
            "actual_end_auto": true,
            "custom_fields" : [ {
                "code" : "CUF_A",
                "label" : "Cuf A",
                "value" : "value of A"
            }]
            
        }"""

        def parentCampaign = SquashEntityBuilder.campaign {
            id = 2L
            name = "parent campaign"
            project = SquashEntityBuilder.project {
                id = 4L
            }
        }

        def iteration = SquashEntityBuilder.iteration {

            id = 22L
            name = "new iteration"
            reference = "NEW_IT"
            description = "<p>A new iteration</p>"
            createdBy "User-A"
            createdOn "2017/04/07"
            lastModifiedBy "User-B"
            lastModifiedOn "2017/04/15"

            scheduledStartDate "2017/04/09"
            scheduledEndDate "2017/04/14"
            actualStartDate "2017/04/10"
            actualEndDate "2017/04/15"
            actualStartAuto false
            actualEndAuto true

            campaign = parentCampaign
        }

        def iterCufs = [
                SquashEntityBuilder.cufValue {
                    label = "cuf"
                    inputType "PLAIN_TEXT"
                    code = "CUF"
                    value = "value"
                }]

        and:

        cufService.findAllCustomFieldValues(iteration) >> iterCufs

        restIterationService.createIteration(_, _) >> iteration

        campaignService.getOne(_) >> parentCampaign

        when:

        def res = mockMvc.perform(RestDocumentationRequestBuilders.post("/api/rest/latest/campaigns/{id}/iterations", 2L)
                .accept("application/json")
                .contentType("application/json")
                .content(json))


        then:

        /*
        * Test (using the TestHelper)
        * */
        res.andExpect(status().isCreated())
                .andExpect(content().contentType("application/json;charset=UTF-8"))

        withResult(res) {
            "_type".is "iteration"
            "id".is 22
            "name".is "new iteration"
            "reference".is "NEW_IT"
            "description".is "<p>A new iteration</p>"
            "uuid".is "2f7194ca-eb2e-4379-f82d-ddc207c866bd"
            "parent".test {
                "_type".is "campaign"
                "id".is 2
                "name".is "parent campaign"
                selfRelIs "http://localhost:8080/api/rest/latest/campaigns/2"
            }
            "created_by".is "User-A"
            "created_on".is "2017-04-07T10:00:00.000+0000"
            "last_modified_by".is "User-B"
            "last_modified_on".is "2017-04-15T10:00:00.000+0000"

            "scheduled_start_date".is "2017-04-09T10:00:00.000+0000"
            "scheduled_end_date".is "2017-04-14T10:00:00.000+0000"
            "actual_start_date".is "2017-04-10T10:00:00.000+0000"
            "actual_end_date".is "2017-04-15T10:00:00.000+0000"
            "actual_start_auto".is false
            "actual_end_auto".is true

            "custom_fields".hasSize 1
            "custom_fields[0]".test {
                "code".is "CUF"
                "label".is "cuf"
                "value".is "value"
            }

            "_links".test {
                "self.href".is "http://localhost:8080/api/rest/latest/iterations/22"
                "project.href".is "http://localhost:8080/api/rest/latest/projects/4"
                "campaign.href".is "http://localhost:8080/api/rest/latest/campaigns/2"
                "test-suites.href".is "http://localhost:8080/api/rest/latest/iterations/22/test-suites"
                "test-plan.href".is "http://localhost:8080/api/rest/latest/iterations/22/test-plan"
            }
        }

        /*
        * Documentation
         */
        res.andDo(doc.document(
                documentationBuilder {

                    requestParams {
                        add DocumentationSnippets.DescriptorLists.fieldsParams
                    }

                    requestFields {
                        add "_type (string) : the type of the entity"
                        add "name (string) : the name of the iteration"
                        add "reference (string) : the reference of the iteration"
                        add "description (string) : the description of the iteration"
                        addAndStop "parent (object) : the parent campaign of this iteration"
                        add DocumentationSnippets.DescriptorLists.scheduleFields
                        add "scheduled_start_date (string) : scheduled start date"
                        add "scheduled_end_date (string) : scheduled end date"
                        addAndStop "custom_fields (array) : the custom fields of this iteration"
                    }

                    fields {
                        add "_type (string) : the type of this entity"
                        add "id (number) : the id of this iteration"
                        add "name (string) : the name of this iteration"
                        add "reference (string) : the reference of this iteration"
                        add "description (string) : the description of this iteration"
                        add "uuid (string) : the uuid of this iteration"
                        addAndStop "parent (object) : the parent campaign of this iteration"
                        add DocumentationSnippets.DescriptorLists.auditableFields
                        add DocumentationSnippets.DescriptorLists.scheduleFields
                        add "scheduled_start_date (string) : scheduled start date"
                        add "scheduled_end_date (string) : scheduled end date"
                        addAndStop "custom_fields (array) : the custom fields of this iteration"
                        addAndStop "test_suites (array) : the test-suites of this iteration"
                        add "attachments (array) : the attachments of this iteration"
                        add DocumentationSnippets.DescriptorLists.linksFields
                    }

                    _links {
                        add "self : link to this iteration"
                        add "project : link to the project of this iteration"
                        add "campaign : link to the campaign of this iteration"
                        add "test-suites : link to the test suites of this iteration"
                        add "test-plan : link to the test plan of this iteration"
                        add "attachments : link to the attachments of this iteration"

                    }
                }
        ))
    }

    def "get-iteration"() {

        given:

        def parentCampaign = SquashEntityBuilder.campaign {
            id = 2L
            name = "sample campaign"
            project = SquashEntityBuilder.project {
                id = 4L
            }
        }

        def iteration = SquashEntityBuilder.iteration {

            id = 22L
            name = "sample iteration"
            reference = "SAMP_IT"
            description = "<p>A sample iteration</p>"
            createdBy "User-A"
            createdOn "2017/04/07"
            lastModifiedBy "User-B"
            lastModifiedOn "2017/04/15"

            scheduledStartDate "2017/04/09"
            scheduledEndDate "2017/04/14"
            actualStartDate "2017/04/10"
            actualEndDate "2017/04/15"
            actualStartAuto false
            actualEndAuto true
            testSuites = [
                    SquashEntityBuilder.testSuite {
                        id = 1L
                        name = "Suite_1"
                    },
                    SquashEntityBuilder.testSuite {
                        id = 2L
                        name = "Suite_2"
                    }
            ] as List

            campaign = parentCampaign
        }

        def iteCufs = [
                SquashEntityBuilder.cufValue {
                    label = "cuf"
                    inputType "PLAIN_TEXT"
                    code = "CUF"
                    value = "value"
                }]

        and:

        restIterationService.getOne(22) >> iteration

        cufService.findAllCustomFieldValues(iteration) >> iteCufs

        hierService.findParentFor(iteration) >> parentCampaign

        when:

        def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/iterations/{id}", 22)
                .header("Accept", "application/json"))

        then:

        /*
        * Test (using the TestHelper)
        * */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))

        withResult(res) {
            "_type".is "iteration"
            "id".is 22
            "name".is "sample iteration"
            "reference".is "SAMP_IT"
            "description".is "<p>A sample iteration</p>"
            "uuid".is "2f7194ca-eb2e-4379-f82d-ddc207c866bd"
            "parent".test {
                "_type".is "campaign"
                "id".is 2
                "name".is "sample campaign"
                selfRelIs "http://localhost:8080/api/rest/latest/campaigns/2"
            }
            "created_by".is "User-A"
            "created_on".is "2017-04-07T10:00:00.000+0000"
            "last_modified_by".is "User-B"
            "last_modified_on".is "2017-04-15T10:00:00.000+0000"

            "scheduled_start_date".is "2017-04-09T10:00:00.000+0000"
            "scheduled_end_date".is "2017-04-14T10:00:00.000+0000"
            "actual_start_date".is "2017-04-10T10:00:00.000+0000"
            "actual_end_date".is "2017-04-15T10:00:00.000+0000"
            "actual_start_auto".is false
            "actual_end_auto".is true

            "custom_fields".hasSize 1
            "custom_fields[0]".test {
                "code".is "CUF"
                "label".is "cuf"
                "value".is "value"
            }

            "test_suites".hasSize 2
            "test_suites[0]".test {
                "_type".is "test-suite"
                "id".is 1
                "name".is "Suite_1"
                selfRelIs "http://localhost:8080/api/rest/latest/test-suites/1"
            }
            "test_suites[1]".test {
                "_type".is "test-suite"
                "id".is 2
                "name".is "Suite_2"
                selfRelIs "http://localhost:8080/api/rest/latest/test-suites/2"
            }
            "_links".test {
                "self.href".is "http://localhost:8080/api/rest/latest/iterations/22"
                "project.href".is "http://localhost:8080/api/rest/latest/projects/4"
                "campaign.href".is "http://localhost:8080/api/rest/latest/campaigns/2"
                "test-suites.href".is "http://localhost:8080/api/rest/latest/iterations/22/test-suites"
                "test-plan.href".is "http://localhost:8080/api/rest/latest/iterations/22/test-plan"
            }
        }

        /*
        * Documentation
         */
        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "id : the id of the iteration"
                    }
                    requestParams {
                        add DocumentationSnippets.DescriptorLists.fieldsParams
                    }

                    fields {
                        add "_type (string) : the type of this entity"
                        add "id (number) : the id of this iteration"
                        add "name (string) : the name of this iteration"
                        add "reference (string) : the reference of this iteration"
                        add "description (string) : the description of this iteration"
                        add "uuid (string) : the uuid of this iteration"
                        addAndStop "parent (object) : the parent campaign of this iteration"
                        add DocumentationSnippets.DescriptorLists.auditableFields
                        add DocumentationSnippets.DescriptorLists.scheduleFields
                        add "scheduled_start_date (string) : scheduled start date"
                        add "scheduled_end_date (string) : scheduled end date"
                        addAndStop "custom_fields (array) : the custom fields of this iteration"
                        addAndStop "test_suites (array) : the test-suites of this iteration"
                        add "attachments (array) : the attachments of this iteration"
                        add DocumentationSnippets.DescriptorLists.linksFields
                    }

                    _links {
                        add "self : link to this iteration"
                        add "project : link to the project of this iteration"
                        add "campaign : link to the campaign of this iteration"
                        add "test-suites : link to the test suites of this iteration"
                        add "test-plan : link to the test plan of this iteration"
                        add "attachments : link to the attachments of this iteration"

                    }
                }
        ))
    }

    def "get-iteration-by-name"() {
        given:
        def parentCampaign = SquashEntityBuilder.campaign {
            id = 2L
            name = "sample campaign"
            project = SquashEntityBuilder.project {
                id = 4L
            }
        }

        def iteration = SquashEntityBuilder.iteration {

            id = 22L
            name = "sample iteration"
            reference = "SAMP_IT"
            description = "<p>A sample iteration</p>"
            createdBy "User-A"
            createdOn "2017/04/07"
            lastModifiedBy "User-B"
            lastModifiedOn "2017/04/15"

            scheduledStartDate "2017/04/09"
            scheduledEndDate "2017/04/14"
            actualStartDate "2017/04/10"
            actualEndDate "2017/04/15"
            actualStartAuto false
            actualEndAuto true
            testSuites = [
                    SquashEntityBuilder.testSuite {
                        id = 1L
                        name = "Suite_1"
                    },
                    SquashEntityBuilder.testSuite {
                        id = 2L
                        name = "Suite_2"
                    }
            ] as List

            campaign = parentCampaign
        }

        def iteCufs = [
                SquashEntityBuilder.cufValue {
                    label = "cuf"
                    inputType "PLAIN_TEXT"
                    code = "CUF"
                    value = "value"
                }]

        and:

        restIterationService.getOneByName("sample iteration") >> iteration

        cufService.findAllCustomFieldValues(iteration) >> iteCufs

        hierService.findParentFor(iteration) >> parentCampaign

        when:
        def res = mockMvc.perform(
                RestDocumentationRequestBuilders.get("/api/rest/latest/iterations")
                        .param("iterationName", "sample iteration")
                        .header("Accept", "application/json"))
        then:
        /* Test (using TestHelper) */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))
        withResult(res) {
            "_type".is "iteration"
            "id".is 22
            "name".is "sample iteration"
            "reference".is "SAMP_IT"
            "description".is "<p>A sample iteration</p>"
            "uuid".is "2f7194ca-eb2e-4379-f82d-ddc207c866bd"
            "parent".test {
                "_type".is "campaign"
                "id".is 2
                "name".is "sample campaign"
                selfRelIs "http://localhost:8080/api/rest/latest/campaigns/2"
            }
            "created_by".is "User-A"
            "created_on".is "2017-04-07T10:00:00.000+0000"
            "last_modified_by".is "User-B"
            "last_modified_on".is "2017-04-15T10:00:00.000+0000"

            "scheduled_start_date".is "2017-04-09T10:00:00.000+0000"
            "scheduled_end_date".is "2017-04-14T10:00:00.000+0000"
            "actual_start_date".is "2017-04-10T10:00:00.000+0000"
            "actual_end_date".is "2017-04-15T10:00:00.000+0000"
            "actual_start_auto".is false
            "actual_end_auto".is true

            "custom_fields".hasSize 1
            "custom_fields[0]".test {
                "code".is "CUF"
                "label".is "cuf"
                "value".is "value"
            }

            "test_suites".hasSize 2
            "test_suites[0]".test {
                "_type".is "test-suite"
                "id".is 1
                "name".is "Suite_1"
                selfRelIs "http://localhost:8080/api/rest/latest/test-suites/1"
            }
            "test_suites[1]".test {
                "_type".is "test-suite"
                "id".is 2
                "name".is "Suite_2"
                selfRelIs "http://localhost:8080/api/rest/latest/test-suites/2"
            }
            "_links".test {
                "self.href".is "http://localhost:8080/api/rest/latest/iterations/22"
                "project.href".is "http://localhost:8080/api/rest/latest/projects/4"
                "campaign.href".is "http://localhost:8080/api/rest/latest/campaigns/2"
                "test-suites.href".is "http://localhost:8080/api/rest/latest/iterations/22/test-suites"
                "test-plan.href".is "http://localhost:8080/api/rest/latest/iterations/22/test-plan"
            }
        }

/* Documentation */
        res.andDo(doc.document(
                documentationBuilder {
                    requestParams {
                        add "iterationName : the name of the iteration"
                        add DocumentationSnippets.DescriptorLists.fieldsParams
                    }

                    fields {
                        add "_type (string) : the type of this entity"
                        add "id (number) : the id of this iteration"
                        add "name (string) : the name of this iteration"
                        add "reference (string) : the reference of this iteration"
                        add "description (string) : the description of this iteration"
                        add "uuid (string) : the uuid of this iteration"
                        addAndStop "parent (object) : the parent campaign of this iteration"
                        add DocumentationSnippets.DescriptorLists.auditableFields
                        add DocumentationSnippets.DescriptorLists.scheduleFields
                        add "scheduled_start_date (string) : scheduled start date"
                        add "scheduled_end_date (string) : scheduled end date"
                        addAndStop "custom_fields (array) : the custom fields of this iteration"
                        addAndStop "test_suites (array) : the test-suites of this iteration"
                        add "attachments (array) : the attachments of this iteration"
                        add DocumentationSnippets.DescriptorLists.linksFields
                    }

                    _links {
                        add "self : link to this iteration"
                        add "project : link to the project of this iteration"
                        add "campaign : link to the campaign of this iteration"
                        add "test-suites : link to the test suites of this iteration"
                        add "test-plan : link to the test plan of this iteration"
                        add "attachments : link to the attachments of this iteration"

                    }
                }
        ))
    }

    def "patch-iteration"() {
        given:

        def json = """{
            "_type": "patched iteration",
            "name": "new iteration",
            "reference": "NEW_IT",
            "description": "<p>A new iteration</p>",
            "parent": {
                "_type": "campaign",
                "id": 2
            },
            "scheduled_start_date": "2017-04-09T22:00:00.000+0000",
            "scheduled_end_date": "2017-04-14T23:00:00.000+0000",
            "actual_start_date": "2017-04-10T22:00:00.000+0000",
            "actual_end_date": "2017-04-15T22:00:00.000+0000",
            "actual_start_auto": false,
            "actual_end_auto": true
         
            
        }"""

        def parentCampaign = SquashEntityBuilder.campaign {
            id = 2L
            name = "parent campaign"
            project = SquashEntityBuilder.project {
                id = 4L
            }
        }

        def iteration = SquashEntityBuilder.iteration {

            id = 22L
            name = "old iteration"
            reference = "NEW_IT"
            description = "<p>A new iteration</p>"
            createdBy "User-A"
            createdOn "2017/04/07"
            lastModifiedBy "User-B"
            lastModifiedOn "2017/04/15"

            scheduledStartDate "2017/04/09"
            scheduledEndDate "2017/04/14"
            actualStartDate "2017/04/10"
            actualEndDate "2017/04/15"
            actualStartAuto false
            actualEndAuto true

            campaign = parentCampaign
        }

        def iterationPatched = SquashEntityBuilder.iteration {

            id = 22L
            name = "patched iteration"
            reference = "NEW_IT"
            description = "<p>A new iteration</p>"
            createdBy "User-A"
            createdOn "2017/04/07"
            lastModifiedBy "User-B"
            lastModifiedOn "2017/04/15"

            scheduledStartDate "2017/04/09"
            scheduledEndDate "2017/04/14"
            actualStartDate "2017/04/10"
            actualEndDate "2017/04/15"
            actualStartAuto false
            actualEndAuto true

            campaign = parentCampaign
        }


        and:
        restIterationService.getOne(_) >> iteration
        restIterationService.patchIteration(_, _) >> iterationPatched
        cufService.findAllCustomFieldValues(_) >> []

        when:

        def res = mockMvc.perform(RestDocumentationRequestBuilders.patch("/api/rest/latest/iterations/{id}", 332)
                .accept("application/json")
                .contentType("application/json")
                .content(json))

        then:

        /*
        * Test (using the TestHelper)
        * */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))

        withResult(res) {
            "_type".is "iteration"
            "id".is 22
            "name".is "patched iteration"
            "reference".is "NEW_IT"
            "description".is "<p>A new iteration</p>"
            "uuid".is "2f7194ca-eb2e-4379-f82d-ddc207c866bd"
            "parent".test {
                "_type".is "campaign"
                "id".is 2
                "name".is "parent campaign"
                selfRelIs "http://localhost:8080/api/rest/latest/campaigns/2"
            }
            "created_by".is "User-A"
            "created_on".is "2017-04-07T10:00:00.000+0000"
            "last_modified_by".is "User-B"
            "last_modified_on".is "2017-04-15T10:00:00.000+0000"

            "scheduled_start_date".is "2017-04-09T10:00:00.000+0000"
            "scheduled_end_date".is "2017-04-14T10:00:00.000+0000"
            "actual_start_date".is "2017-04-10T10:00:00.000+0000"
            "actual_end_date".is "2017-04-15T10:00:00.000+0000"
            "actual_start_auto".is false
            "actual_end_auto".is true

            "custom_fields".hasSize 0

            "_links".test {
                "self.href".is "http://localhost:8080/api/rest/latest/iterations/22"
                "project.href".is "http://localhost:8080/api/rest/latest/projects/4"
                "campaign.href".is "http://localhost:8080/api/rest/latest/campaigns/2"
                "test-suites.href".is "http://localhost:8080/api/rest/latest/iterations/22/test-suites"
                "test-plan.href".is "http://localhost:8080/api/rest/latest/iterations/22/test-plan"
            }
        }

        /*
        * Documentation
         */
        res.andDo(doc.document(
                documentationBuilder {

                    pathParams {
                        add "id : the id of the iteration"
                    }

                    requestParams {
                        add DocumentationSnippets.DescriptorLists.fieldsParams
                    }

                    requestFields {
                        add "_type (string) : the type of the entity"
                        add "name (string) : the name of the iteration"
                        add "reference (string) : the reference of the iteration"
                        add "description (string) : the description of the iteration"
                        addAndStop "parent (object) : the parent campaign of this iteration"
                        add DocumentationSnippets.DescriptorLists.scheduleFields
                        add "scheduled_start_date (string) : scheduled start date"
                        add "scheduled_end_date (string) : scheduled end date"
                    }

                    fields {
                        add "_type (string) : the type of this entity"
                        add "id (number) : the id of this iteration"
                        add "name (string) : the name of this iteration"
                        add "reference (string) : the reference of this iteration"
                        add "description (string) : the description of this iteration"
                        add "uuid (string) : the uuid of this iteration"
                        addAndStop "parent (object) : the parent campaign of this iteration"
                        add DocumentationSnippets.DescriptorLists.auditableFields
                        add DocumentationSnippets.DescriptorLists.scheduleFields
                        add "scheduled_start_date (string) : scheduled start date"
                        add "scheduled_end_date (string) : scheduled end date"
                        addAndStop "custom_fields (array) : the custom fields of this iteration"
                        addAndStop "test_suites (array) : the test-suites of this iteration"
                        add "attachments (array) : the attachments of this iteration"
                        add DocumentationSnippets.DescriptorLists.linksFields

                    }

                    _links {
                        add "self : link to this iteration"
                        add "project : link to the project of this iteration"
                        add "campaign : link to the campaign of this iteration"
                        add "test-suites : link to the test suites of this iteration"
                        add "test-plan : link to the test plan of this iteration"
                        add "attachments : link to the attachments of this iteration"

                    }
                }
        ))
    }

    def "delete-iteration"() {
        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.delete("/api/rest/latest/iterations/{ids}", "169,189")
                .accept("application/json")
                .contentType("application/json"))

        then:
        /*
         * Test
         */
        res.andExpect(status().isNoContent())
        1 * restIterationService.deleteIterationsByIds(_)

        /*
         * Documentation
         */
        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "ids : the list of ids of the iterations"
                    }
                }
        ))
    }

    def "get-iteration-test-plan"() {

        given:

        def parentIteration = SquashEntityBuilder.iteration {
            id = 1L
            name = "sample iteration"
            reference = "IT1"
        }

        def itpi1 = SquashEntityBuilder.iterationTestPlanItem {
            id = 4L
            referencedTestCase = SquashEntityBuilder.testCase {
                id = 8L
                reference = "TC-8"
                name = "sample test case 8"
            }
            referencedDataset = SquashEntityBuilder.dataset {
                id = 90L
                name = "sample dataset 90"
            }
            lastExecutedBy = "User-1"
            lastExecutedOn "2017/06/25"
            assignedUser "User-1"
            executions = [
                    SquashEntityBuilder.execution {
                        id = 2L
                        executionStatus "BLOCKED"
                        lastExecutedBy = "User-1"
                        lastExecutedOn "2017/06/24"
                    },
                    SquashEntityBuilder.execution {
                        id = 3L
                        executionStatus "SUCCESS"
                        lastExecutedBy = "User-1"
                        lastExecutedOn "2017/06/25"
                    }
            ]
            iteration = parentIteration
        }

        def itpi2 = SquashEntityBuilder.iterationTestPlanItem {
            id = 12L
            referencedTestCase = SquashEntityBuilder.scriptedTestCase {
                id = 16L
                reference = "TC-16"
                name = "scripted test case 16"
            }
            referencedDataset = SquashEntityBuilder.dataset {
                id = 12L
                name = "sample dataset 12"
            }
            lastExecutedBy = "User-1"
            lastExecutedOn "2017/06/28"
            assignedUser "User-1"
            executions = [
                    SquashEntityBuilder.scriptedExecution {
                        id = 9L
                        executionStatus "FAILURE"
                        lastExecutedBy = "User-1"
                        lastExecutedOn "2017/06/26"
                    },
                    SquashEntityBuilder.scriptedExecution {
                        id = 35L
                        executionStatus "SUCCESS"
                        lastExecutedBy = "User-1"
                        lastExecutedOn "2017/06/28"
                    }
            ]
            iteration = parentIteration
        }

        def itpi3 = SquashEntityBuilder.iterationTestPlanItem {
            id = 13L
            referencedTestCase = SquashEntityBuilder.keywordTestCase {
                id = 17L
                reference = "TC-17"
                name = "keyword test case 17"
            }
            referencedDataset = SquashEntityBuilder.dataset {
                id = 13L
                name = "sample dataset 13"
            }
            lastExecutedBy = "User-1"
            lastExecutedOn "2017/06/28"
            assignedUser "User-1"
            executions = [
                    SquashEntityBuilder.keywordExecution {
                        id = 9L
                        executionStatus "FAILURE"
                        lastExecutedBy = "User-1"
                        lastExecutedOn "2017/06/26"
                    },
                    SquashEntityBuilder.keywordExecution {
                        id = 35L
                        executionStatus "SUCCESS"
                        lastExecutedBy = "User-1"
                        lastExecutedOn "2017/06/28"
                    }
            ]
            iteration = parentIteration
        }

        and:

        restIterationService.findIterationTestPlan(1, _) >> { args ->
            new PageImpl<IterationTestPlanItem>([itpi1, itpi2, itpi3], args[1], 6)
        }
        when:

        def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/iterations/{id}/test-plan?size=2&page=1", 1)
                .header("Accept", "application/json"))

        then:

        /*
        * Test (using the TestHelper)
        */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))

        withResult(res) {
            "_embedded.test-plan".hasSize 3
            "_embedded.test-plan".test {
                "[0]".test {
                    "_type".is "iteration-test-plan-item"
                    "id".is 4
                    "referenced_test_case".test {
                        "_type".is "test-case"
                        "id".is 8
                        "reference".is "TC-8"
                        "name".is "sample test case 8"
                        selfRelIs "http://localhost:8080/api/rest/latest/test-cases/8"
                    }
                    "referenced_dataset".test {
                        "_type".is "dataset"
                        "id".is 90
                        "name".is "sample dataset 90"
                        selfRelIs "http://localhost:8080/api/rest/latest/datasets/90"
                    }
                    "last_executed_by".is "User-1"
                    "last_executed_on".is "2017-06-25T10:00:00.000+0000"
                    "assigned_to".is "User-1"
                    "executions".hasSize 2
                    "executions[0]".test {
                        "_type".is "execution"
                        "id".is 2
                        "execution_status".is "BLOCKED"
                        "last_executed_by".is "User-1"
                        "last_executed_on".is "2017-06-24T10:00:00.000+0000"
                        selfRelIs "http://localhost:8080/api/rest/latest/executions/2"
                    }
                    "executions[1]".test {
                        "_type".is "execution"
                        "id".is 3
                        "execution_status".is "SUCCESS"
                        "last_executed_by".is "User-1"
                        "last_executed_on".is "2017-06-25T10:00:00.000+0000"
                        selfRelIs "http://localhost:8080/api/rest/latest/executions/3"
                    }
                    "iteration".test {
                        "_type".is "iteration"
                        "id".is 1
                        "name".is "sample iteration"
                        "reference".is "IT1"
                        selfRelIs "http://localhost:8080/api/rest/latest/iterations/1"
                    }
                    selfRelIs "http://localhost:8080/api/rest/latest/iteration-test-plan-items/4"
                }
                "[1]".test {
                    "_type".is "iteration-test-plan-item"
                    "id".is 12
                    "referenced_test_case".test {
                        "_type".is "scripted-test-case"
                        "id".is 16
                        "reference".is "TC-16"
                        "name".is "scripted test case 16"
                        selfRelIs "http://localhost:8080/api/rest/latest/test-cases/16"
                    }
                    "referenced_dataset".test {
                        "_type".is "dataset"
                        "id".is 12
                        "name".is "sample dataset 12"
                        selfRelIs "http://localhost:8080/api/rest/latest/datasets/12"
                    }
                    "last_executed_by".is "User-1"
                    "last_executed_on".is "2017-06-28T10:00:00.000+0000"
                    "assigned_to".is "User-1"
                    "executions".hasSize 2
                    "executions[0]".test {
                        "_type".is "scripted-execution"
                        "id".is 9
                        "execution_status".is "FAILURE"
                        "last_executed_by".is "User-1"
                        "last_executed_on".is "2017-06-26T10:00:00.000+0000"
                        selfRelIs "http://localhost:8080/api/rest/latest/executions/9"
                    }
                    "executions[1]".test {
                        "_type".is "scripted-execution"
                        "id".is 35
                        "execution_status".is "SUCCESS"
                        "last_executed_by".is "User-1"
                        "last_executed_on".is "2017-06-28T10:00:00.000+0000"
                        selfRelIs "http://localhost:8080/api/rest/latest/executions/35"
                    }
                    "iteration".test {
                        "_type".is "iteration"
                        "id".is 1
                        "name".is "sample iteration"
                        "reference".is "IT1"
                        selfRelIs "http://localhost:8080/api/rest/latest/iterations/1"
                    }
                    selfRelIs "http://localhost:8080/api/rest/latest/iteration-test-plan-items/12"
                }
                "[2]".test {
                    "_type".is "iteration-test-plan-item"
                    "id".is 13
                    "referenced_test_case".test {
                        "_type".is "keyword-test-case"
                        "id".is 17
                        "reference".is "TC-17"
                        "name".is "keyword test case 17"
                        selfRelIs "http://localhost:8080/api/rest/latest/test-cases/17"
                    }
                    "referenced_dataset".test {
                        "_type".is "dataset"
                        "id".is 13
                        "name".is "sample dataset 13"
                        selfRelIs "http://localhost:8080/api/rest/latest/datasets/13"
                    }
                    "last_executed_by".is "User-1"
                    "last_executed_on".is "2017-06-28T10:00:00.000+0000"
                    "assigned_to".is "User-1"
                    "executions".hasSize 2
                    "executions[0]".test {
                        "_type".is "keyword-execution"
                        "id".is 9
                        "execution_status".is "FAILURE"
                        "last_executed_by".is "User-1"
                        "last_executed_on".is "2017-06-26T10:00:00.000+0000"
                        selfRelIs "http://localhost:8080/api/rest/latest/executions/9"
                    }
                    "executions[1]".test {
                        "_type".is "keyword-execution"
                        "id".is 35
                        "execution_status".is "SUCCESS"
                        "last_executed_by".is "User-1"
                        "last_executed_on".is "2017-06-28T10:00:00.000+0000"
                        selfRelIs "http://localhost:8080/api/rest/latest/executions/35"
                    }
                    "iteration".test {
                        "_type".is "iteration"
                        "id".is 1
                        "name".is "sample iteration"
                        "reference".is "IT1"
                        selfRelIs "http://localhost:8080/api/rest/latest/iterations/1"
                    }
                    selfRelIs "http://localhost:8080/api/rest/latest/iteration-test-plan-items/13"
                }
            }
            selfRelIs "http://localhost:8080/api/rest/latest/iterations/1/test-plan?page=1&size=2"
            "page".test {
                "size".is 2
                "totalElements".is 6
                "totalPages".is 3
                "number".is 1
            }
        }

        res.andDo(doc.document(
                AllInOne.createListEntityContent("test-plan", "iteration")
        ))

    }

    def "get-iteration-test-suites"() {

        given:

        def parentIteration = SquashEntityBuilder.iteration {
            id = 1L
            name = "Iteration 1"
        }

        def testSuite1 = SquashEntityBuilder.testSuite {
            id = 9L
            name = "Suite 1"
            description = "<p>The first test suite.</p>"
            iteration = parentIteration

            createdBy "User 2B93"
            createdOn "2017/02/04 11:00:00"
            lastModifiedBy "User 1Z45"
            lastModifiedOn "2017/03/02 11:00:00"

            testPlan = [
                    SquashEntityBuilder.iterationTestPlanItem {
                        id = 7L
                        executionStatus "SUCCESS"
                        referencedTestCase = SquashEntityBuilder.testCase {
                            id = 3L
                            name = "test case 3"
                        }
                        referencedDataset = SquashEntityBuilder.dataset {
                            id = 2L
                            name = "dataset 2"
                        }
                    },
                    SquashEntityBuilder.iterationTestPlanItem {
                        id = 8L
                        executionStatus "RUNNING"
                        referencedTestCase = SquashEntityBuilder.scriptedTestCase {
                            id = 11L
                            name = "scripted test case 11"
                        }
                        referencedDataset = null
                    }
            ]
        }

        def testSuite2 = SquashEntityBuilder.testSuite {
            id = 10L
            name = "Suite 2"
            description = "<p>The second test suite.</p>"
            iteration = parentIteration

            createdBy "User 2B93"
            createdOn "2017/02/04 11:05:42"
            lastModifiedBy "User 2B93"
            lastModifiedOn "2017/03/04 13:00:00"

            testPlan = [
                    SquashEntityBuilder.iterationTestPlanItem {
                        id = 15L
                        executionStatus "READY"
                        referencedTestCase = SquashEntityBuilder.testCase {
                            id = 11L
                            name = "test case 11"
                        }
                        referencedDataset = null
                    },
                    SquashEntityBuilder.iterationTestPlanItem {
                        id = 13L
                        executionStatus "READY"
                        referencedTestCase = SquashEntityBuilder.keywordTestCase {
                            id = 17L
                            name = "keyword test case 17"
                        }
                    }
            ]
        }

        def suite1Cufs = [
                SquashEntityBuilder.cufValue {
                    label = "My Custom Field"
                    code = "MY_CUF"
                    value = "yellow"
                }
        ]
        def suite2Cufs = [
                SquashEntityBuilder.cufValue {
                    label = "My Custom Field"
                    code = "MY_CUF"
                    value = "blue"
                }
        ]

        and:

        restIterationService.findIterationTestSuite(1, _) >> { args ->
            new PageImpl<TestSuite>([testSuite1, testSuite2], args[1], 6)
        }

        hierService.findParentFor(_) >> parentIteration

        cufService.findAllCustomFieldValues(testSuite1) >> suite1Cufs
        cufService.findAllCustomFieldValues(testSuite2) >> suite2Cufs

        when:

        def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/iterations/{id}/test-suites?size=2&page=1", 1)
                .header("Accept", "application/json"))

        then:

        /*
        * Test
        * */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))

        withResult(res) {
            "_embedded.test-suites".hasSize 2
            "_embedded.test-suites".test {
                "[0]".test {
                    "_type".is "test-suite"
                    "id".is 9
                    "name".is "Suite 1"
                    "description".is "<p>The first test suite.</p>"
                    "parent".test {
                        "_type".is "iteration"
                        "id".is 1
                        "name".is "Iteration 1"
                        selfRelIs "http://localhost:8080/api/rest/latest/iterations/1"
                    }

                    "created_by".is "User 2B93"
                    "created_on".is "2017-02-04T10:00:00.000+0000"
                    "last_modified_by".is "User 1Z45"
                    "last_modified_on".is "2017-03-02T10:00:00.000+0000"

                    "custom_fields".hasSize 1
                    "custom_fields[0]".test {
                        "label".is "My Custom Field"
                        "code".is "MY_CUF"
                        "value".is "yellow"
                    }

                    "test_plan".hasSize 2
                    "test_plan[0]".test {
                        "_type".is "iteration-test-plan-item"
                        "id".is 7
                        "execution_status".is "SUCCESS"
                        "referenced_test_case".test {
                            "_type".is "test-case"
                            "id".is 3
                            "name".is "test case 3"
                            selfRelIs "http://localhost:8080/api/rest/latest/test-cases/3"
                        }
                        "referenced_dataset".test {
                            "_type".is "dataset"
                            "id".is 2
                            "name".is "dataset 2"
                            selfRelIs "http://localhost:8080/api/rest/latest/datasets/2"
                        }
                        selfRelIs "http://localhost:8080/api/rest/latest/iteration-test-plan-items/7"
                    }
                    "test_plan[1]".test {
                        "_type".is "iteration-test-plan-item"
                        "id".is 8
                        "execution_status"; is "RUNNING"
                        "referenced_test_case".test {
                            "_type".is "scripted-test-case"
                            "id".is 11
                            "name".is "scripted test case 11"
                            selfRelIs "http://localhost:8080/api/rest/latest/test-cases/11"
                        }
                        "referenced_dataset".is null
                        selfRelIs "http://localhost:8080/api/rest/latest/iteration-test-plan-items/8"
                    }
                    selfRelIs "http://localhost:8080/api/rest/latest/test-suites/9"
                }
                "[1]".test {
                    "_type".is "test-suite"
                    "id".is 10
                    "name".is "Suite 2"
                    "description".is "<p>The second test suite.</p>"
                    "parent".test {
                        "_type".is "iteration"
                        "id".is 1
                        "name".is "Iteration 1"
                        selfRelIs "http://localhost:8080/api/rest/latest/iterations/1"
                    }
                    "created_by".is "User 2B93"
                    "created_on".is "2017-02-04T10:05:42.000+0000"
                    "last_modified_by".is "User 2B93"
                    "last_modified_on".is "2017-03-04T12:00:00.000+0000"

                    "custom_fields".hasSize 1
                    "custom_fields[0]".test {
                        "label".is "My Custom Field"
                        "code".is "MY_CUF"
                        "value".is "blue"
                    }

                    "test_plan".hasSize 2
                    "test_plan[0]".test {
                        "_type".is "iteration-test-plan-item"
                        "id".is 15
                        "execution_status".is "READY"
                        "referenced_test_case".test {
                            "_type".is "test-case"
                            "id".is 11
                            "name".is "test case 11"
                            selfRelIs "http://localhost:8080/api/rest/latest/test-cases/11"
                        }
                        "referenced_dataset".is null
                        selfRelIs "http://localhost:8080/api/rest/latest/iteration-test-plan-items/15"
                    }
                    "test_plan[1]".test {
                        "_type".is "iteration-test-plan-item"
                        "id".is 13
                        "execution_status".is "READY"
                        "referenced_test_case".test {
                            "_type".is "keyword-test-case"
                            "id".is 17
                            "name".is "keyword test case 17"
                            selfRelIs "http://localhost:8080/api/rest/latest/test-cases/17"
                        }
                        "referenced_dataset".is null
                        selfRelIs "http://localhost:8080/api/rest/latest/iteration-test-plan-items/13"
                    }
                    selfRelIs "http://localhost:8080/api/rest/latest/test-suites/10"
                }
            }
            selfRelIs "http://localhost:8080/api/rest/latest/iterations/1/test-suites?page=1&size=2"
            "page".test {
                "size".is 2
                "totalElements".is 6
                "totalPages".is 3
                "number".is 1
            }
        }

        /*
        * Documentation
        * */
        res.andDo(doc.document(
                AllInOne.createListEntityContent("test-suites", "iteration")
        ))

    }


}
