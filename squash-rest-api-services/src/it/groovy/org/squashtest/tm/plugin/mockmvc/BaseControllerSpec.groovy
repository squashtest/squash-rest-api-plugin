/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.mockmvc

import org.codehaus.groovy.control.CompilerConfiguration
import org.codehaus.groovy.control.customizers.ImportCustomizer
import org.junit.Rule
import org.springframework.restdocs.JUnitRestDocumentation
import org.springframework.restdocs.mockmvc.RestDocumentationResultHandler
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.web.context.WebApplicationContext
import org.squashtest.tm.service.internal.configuration.CallbackUrlProvider
import spock.lang.Specification

import javax.inject.Inject

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessRequest
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessResponse
import static org.springframework.restdocs.operation.preprocess.Preprocessors.prettyPrint

/**
 * Base class for our controller specifications, which hide the details of the Spring plumbing
 * 
 * @author bsiri
 *
 */

@ContextConfiguration(classes=[MockMvcConfiguration, MockServicesConfiguration])
public abstract class BaseControllerSpec extends Specification {
	
	private static final String ENTITY_BUILDER = "org.squashtest.tm.test.domainbuilder.SquashEntityBuilder"

	@Rule
	protected JUnitRestDocumentation restDocumentation = new JUnitRestDocumentation("target/generated-snippets");
	
	protected  RestDocumentationResultHandler documentationHandler;
	
	/*
	 * When using @WebMvcTest, an instance of MockMvc could be injected. However we need to add a rule for spring-restdoc,
	 * that's why we still configure MockMvc instance manually.
	 *
	 */
	protected MockMvc mockMvc
	
	@Inject
	private WebApplicationContext context;

	@Inject
	private CallbackUrlProvider callbackUrlProvider
	
	def setup(){
		
		this.documentationHandler = document("{ClassName}/{method-name}",
			preprocessRequest(prettyPrint()),
			preprocessResponse(prettyPrint()));
		
		
		this.mockMvc = MockMvcBuilders.webAppContextSetup(this.context)
			.apply(documentationConfiguration(this.restDocumentation)) 
			.alwaysDo(this.documentationHandler)
			.build();

		callbackUrlProvider.getCallbackUrl() >> new URL("http://localhost:8080")
	}

	
	/*
	 *  getter for the document handler (we dont want to invoke the default one 
	 *  you usually obtain with static MockMvcRestDocumentation.document()  
	 */
	protected RestDocumentationResultHandler getDoc(){
		documentationHandler
	}
	
	
	/**
	 * Will load a mock definition from a groovy script (typically in resources/mocks/my-mock-script.groovy. 
	 * 
	 * 
	 * @param scriptPath
	 * @return
	 */
	def loadMock(String scriptPath, Collection<String> imports = [], Collection<String> staticImports = []){
		
		ImportCustomizer customizer = new ImportCustomizer()
		
		customizer.addImports (imports as String[])		
		staticImports.each { customizer.addStaticImport it  }
		
		customizer.addImport ENTITY_BUILDER
		customizer.addStaticStar ENTITY_BUILDER
		
		def conf = new CompilerConfiguration()
		conf.addCompilationCustomizers customizer 
			
		def classloader = Thread.currentThread().contextClassLoader
		
		def shell = new GroovyShell(classloader, new Binding(), conf)
		def script = new File(classloader.getResource(scriptPath).toURI())
		
		
		shell.evaluate script
	}

	def loadFile(String filePath){
		new File(Thread.currentThread().contextClassLoader.getResource(filePath).toURI()).text
	}
}
