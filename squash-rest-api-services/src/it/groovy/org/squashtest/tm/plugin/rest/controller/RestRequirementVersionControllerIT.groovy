/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.controller

import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import org.squashtest.tm.plugin.mockmvc.BaseControllerSpec
import org.squashtest.tm.plugin.rest.service.RestRequirementVersionService
import org.squashtest.tm.service.customfield.CustomFieldValueFinderService
import org.squashtest.tm.service.security.PermissionEvaluationService
import org.squashtest.tm.test.domainbuilder.SquashEntityBuilder

import javax.inject.Inject

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import static org.squashtest.tm.plugin.docutils.DocumentationSnippets.DescriptorLists
import static org.squashtest.tm.plugin.docutils.DocumentationSnippets.documentationBuilder
import static org.squashtest.tm.plugin.mockmvc.TestHelper.selfRelIs
import static org.squashtest.tm.plugin.mockmvc.TestHelper.withResult

@WebMvcTest(RestRequirementVersionController)
public class RestRequirementVersionControllerIT extends BaseControllerSpec {

    @Inject
    private RestRequirementVersionService service;

    @Inject
    private CustomFieldValueFinderService cufService

    @Inject
    private PermissionEvaluationService permService

    def "get-requirement-version"() {

        given:

        def reqVersion = SquashEntityBuilder.requirementVersion {
            id = 3l
            name = "sample requirement"
            reference = "SAMP_REQ_VER"
            versionNumber = 2

            createdBy "User-1"
            createdOn "2017/07/19"
            lastModifiedBy "User-2"
            lastModifiedOn "2017/07/20"

            criticality "CRITICAL"
            category "CAT_PERFORMANCE"
            status "APPROVED"
            description = "<p>Approved performance requirement-version</p>"

            testCases = [
                    SquashEntityBuilder.testCase {
                        id = 4l
                        name = "verifying test case 1"
                    },
                    SquashEntityBuilder.scriptedTestCase {
                        id = 9l
                        name = "verifying scripted test case 2"
                    },
                    SquashEntityBuilder.keywordTestCase {
                        id = 14l
                        name = "verifying keyword test case 3"
                    }
            ]
        }

        def requirement = SquashEntityBuilder.requirement {
            id = 64l
            project = SquashEntityBuilder.project {
                id = 85l
                name = "myProject"
            }
            versions = [reqVersion]
        }

        def cufs = [
                SquashEntityBuilder.cufValue {
                    label = "Cuf One"
                    inputType "PLAIN_TEXT"
                    code = "CUF1"
                    value = "value_1"
                },
                SquashEntityBuilder.cufValue {
                    label = "Cuf Two"
                    inputType "PLAIN_TEXT"
                    code = "CUF2"
                    value = "value_2"
                }
        ]

        and:

        service.findRequirementVersion(3) >> reqVersion

        cufService.findAllCustomFieldValues(reqVersion) >> cufs

        permService.canRead(_) >> true

        when:

        def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/requirement-versions/{id}", 3)
                .header("Accept", "application/json"))

        then:

        /*
       * Test (with TestHelper)
       */
        res.andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType("application/json;charset=UTF-8"))

        withResult(res) {
            "_type".is "requirement-version"
            "id".is 3
            "name".is "sample requirement"
            "reference".is "SAMP_REQ_VER"
            "version_number".is 2

            "requirement".test {
                "_type".is "requirement"
                "id".is 64
                "name".is "sample requirement"
                selfRelIs "http://localhost:8080/api/rest/latest/requirements/64"
            }

            "created_by".is "User-1"
            "created_on".is "2017-07-19T10:00:00.000+0000"
            "last_modified_by".is "User-2"
            "last_modified_on".is "2017-07-20T10:00:00.000+0000"
            "criticality".is "CRITICAL"
            "category".test { "code".is "CAT_PERFORMANCE" }
            "status".is "APPROVED"
            "description".is "<p>Approved performance requirement-version</p>"

            "verifying_test_cases".hasSize 3
            "verifying_test_cases".test {
                "[0]".test {
                    "_type".is "test-case"
                    "id".is 4
                    "name".is "verifying test case 1"
                    selfRelIs "http://localhost:8080/api/rest/latest/test-cases/4"
                }
                "[1]".test {
                    "_type".is "scripted-test-case"
                    "id".is 9
                    "name".is "verifying scripted test case 2"
                    selfRelIs "http://localhost:8080/api/rest/latest/test-cases/9"
                }
                "[2]".test {
                    "_type".is "keyword-test-case"
                    "id".is 14
                    "name".is "verifying keyword test case 3"
                    selfRelIs "http://localhost:8080/api/rest/latest/test-cases/14"
                }
            }

            "custom_fields".hasSize 2
            "custom_fields".test {
                "[0]".test {
                    "code".is "CUF1"
                    "label".is "Cuf One"
                    "value".is "value_1"
                }
                "[1]".test {
                    "code".is "CUF2"
                    "label".is "Cuf Two"
                    "value".is "value_2"
                }
            }

            "_links".test {
                "self.href".is "http://localhost:8080/api/rest/latest/requirement-versions/3"
                "project.href".is "http://localhost:8080/api/rest/latest/projects/85"
                "requirement.href".is "http://localhost:8080/api/rest/latest/requirements/64"
            }
        }

        /*
        * Documentation
         */
        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "id : the id of the requirement version"
                    }

                    requestParams {
                        add DescriptorLists.fieldsParams
                    }

                    fields {
                        add "id (number) : the id of the requirement version"
                        add "_type (string) : the type of the entity"
                        add "name (string) : the name of the requirement version"
                        add "reference (string) : the reference of the requirement version"
                        add "version_number (number) : the version number"
                        addAndStop "requirement (object) : the requirement of this requirement version"
                        add DescriptorLists.auditableFields
                        add "criticality (string) : the criticality of this requirement version"
                        addAndStop "category (object) : the category of this requirement version"
                        add "status (string) : the status of this requirement version"
                        add "description (string) : the description of this requirement version"
                        addAndStop "verifying_test_cases (array) : the test cases which cover this requirement version"
                        addAndStop "custom_fields (array) : the custom fields of this requirement version"
                        add "attachments (array) : the attachments of this requirement version"

                        add DescriptorLists.linksFields
                    }

                    _links {
                        add "self  : link to this requirement version"
                        add "project : link to the project this requirement version belongs to"
                        add "requirement : link to the requirement this requirement version belongs to"
                        add "attachments : link to the attachments this requirement version owns"

                    }

                }
        ))

    }
}
