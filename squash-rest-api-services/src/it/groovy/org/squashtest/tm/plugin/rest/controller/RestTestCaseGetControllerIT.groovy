/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.controller

import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.Sort
import org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders
import org.springframework.restdocs.payload.JsonFieldType
import org.squashtest.tm.domain.testcase.Dataset
import org.squashtest.tm.domain.testcase.Parameter
import org.squashtest.tm.domain.testcase.TestCase
import org.squashtest.tm.domain.testcase.TestStep
import org.squashtest.tm.plugin.mockmvc.BaseControllerSpec
import org.squashtest.tm.plugin.rest.core.service.NodeHierarchyHelpService
import org.squashtest.tm.plugin.rest.service.RestDatasetService
import org.squashtest.tm.plugin.rest.service.RestParameterService
import org.squashtest.tm.plugin.rest.service.RestTestCaseService
import org.squashtest.tm.service.customfield.CustomFieldValueFinderService
import org.squashtest.tm.service.internal.library.PathService
import org.squashtest.tm.service.security.PermissionEvaluationService
import org.squashtest.tm.test.domainbuilder.SquashEntityBuilder

import javax.inject.Inject

import static org.hamcrest.Matchers.hasSize
import static org.hamcrest.Matchers.is
import static org.springframework.restdocs.hypermedia.HypermediaDocumentation.halLinks
import static org.springframework.restdocs.hypermedia.HypermediaDocumentation.linkWithRel
import static org.springframework.restdocs.hypermedia.HypermediaDocumentation.links
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields
import static org.springframework.restdocs.payload.PayloadDocumentation.subsectionWithPath
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName
import static org.springframework.restdocs.request.RequestDocumentation.pathParameters
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import static org.squashtest.tm.plugin.docutils.DocumentationSnippets.AllInOne
import static org.squashtest.tm.plugin.docutils.DocumentationSnippets.DescriptorLists
import static org.squashtest.tm.plugin.docutils.DocumentationSnippets.Snippets
import static org.squashtest.tm.plugin.docutils.DocumentationSnippets.documentationBuilder
import static org.squashtest.tm.plugin.mockmvc.TestHelper.selfRelIs
import static org.squashtest.tm.plugin.mockmvc.TestHelper.withResult

@WebMvcTest(RestTestCaseController)
public class RestTestCaseGetControllerIT extends BaseControllerSpec {


    @Inject
    private RestTestCaseService restTestCaseService;

    @Inject
    private RestParameterService restParameterService;

    @Inject
    private RestDatasetService restDatasetService;

    @Inject
    private CustomFieldValueFinderService cufService

    @Inject
    private PathService pathService

    @Inject
    private NodeHierarchyHelpService hierService

    @Inject
    private PermissionEvaluationService permService


    // ****************** browsing **************************************

    def "browse-test-cases"() {

        given:
        restTestCaseService.getAllReadableTestCases(_) >> { args ->

            def tc1 = SquashEntityBuilder.testCase {
                id = 1l
                name = "sample standard test case"
                reference = "TC1"
                type "TYP_REGRESSION_TESTING"
            }

            def tc2 = SquashEntityBuilder.scriptedTestCase {
                id = 2l
                name = "sample script test case"
                reference = "TC2"
                type "TYP_REGRESSION_TESTING"
                script = "This is a new Gherkin test"
            }

            def tc3 = SquashEntityBuilder.keywordTestCase {
                id = 3l
                name = "sample keyword test case"
                reference = "TC3"
                type "TYP_REGRESSION_TESTING"
            }

            new PageImpl<TestCase>([tc1, tc2, tc3], args[0], 5)
        }


        when:
        def res = mockMvc.perform(get("/api/rest/latest/test-cases?page=1&size=2&fields=name,reference,script").header("Accept", "application/json"))


        then:

        /*
         * Test (style is plain Spring testmvc)
         */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))

        // embedded section
                .andExpect(jsonPath("_embedded.test-cases", hasSize(3)))

                .andExpect(jsonPath("_embedded.test-cases[0]._type", is("test-case")))
                .andExpect(jsonPath("_embedded.test-cases[0].id", is(1)))
                .andExpect(jsonPath("_embedded.test-cases[0].name", is("sample standard test case")))
                .andExpect(jsonPath("_embedded.test-cases[0].reference", is("TC1")))
                .andExpect(jsonPath("_embedded.test-cases[0]._links.self.href", is("http://localhost:8080/api/rest/latest/test-cases/1")))

                .andExpect(jsonPath("_embedded.test-cases[1]._type", is("scripted-test-case")))
                .andExpect(jsonPath("_embedded.test-cases[1].id", is(2)))
                .andExpect(jsonPath("_embedded.test-cases[1].name", is("sample script test case")))
                .andExpect(jsonPath("_embedded.test-cases[1].reference", is("TC2")))
                .andExpect(jsonPath("_embedded.test-cases[1].script", is("This is a new Gherkin test")))
                .andExpect(jsonPath("_embedded.test-cases[1]._links.self.href", is("http://localhost:8080/api/rest/latest/test-cases/2")))

                .andExpect(jsonPath("_embedded.test-cases[2]._type", is("keyword-test-case")))
                .andExpect(jsonPath("_embedded.test-cases[2].id", is(3)))
                .andExpect(jsonPath("_embedded.test-cases[2].name", is("sample keyword test case")))
                .andExpect(jsonPath("_embedded.test-cases[2].reference", is("TC3")))
                .andExpect(jsonPath("_embedded.test-cases[2]._links.self.href", is("http://localhost:8080/api/rest/latest/test-cases/3")))

        // links section
                .andExpect(jsonPath("_links.first.href", is("http://localhost:8080/api/rest/latest/test-cases?fields=name,reference,script&page=0&size=2")))
                .andExpect(jsonPath("_links.prev.href", is("http://localhost:8080/api/rest/latest/test-cases?fields=name,reference,script&page=0&size=2")))
                .andExpect(jsonPath("_links.self.href", is("http://localhost:8080/api/rest/latest/test-cases?fields=name,reference,script&page=1&size=2")))
                .andExpect(jsonPath("_links.next.href", is("http://localhost:8080/api/rest/latest/test-cases?fields=name,reference,script&page=2&size=2")))
                .andExpect(jsonPath("_links.last.href", is("http://localhost:8080/api/rest/latest/test-cases?fields=name,reference,script&page=2&size=2")))

        // page section
                .andExpect(jsonPath("page.size", is(2)))
                .andExpect(jsonPath("page.totalElements", is(5)))
                .andExpect(jsonPath("page.totalPages", is(3)))
                .andExpect(jsonPath("page.number", is(1)))

        /*
         * Documentation
         */
        res.andDo(doc.document(
                AllInOne.createBrowseAllEntities("test-cases")
        ))

    }


    // ********************** GETting ******************************

    def "get-test-case"() {

        given:
        def mocks = loadMock("mocks/get-test-case.groovy")

        def tc = mocks.tc

        and:

        restTestCaseService.getOne(238l) >> tc

        hierService.findParentFor(tc) >> SquashEntityBuilder.testCaseFolder {
            id = 237l
            name = "sample folder"
        }

        cufService.findAllCustomFieldValues(tc) >> mocks.tcCufs
        cufService.findAllCustomFieldValues(tc.steps[0]) >> mocks.step1Cufs
        cufService.findAllCustomFieldValues(tc.steps[1]) >> mocks.step2Cufs

        pathService.buildTestCasePath(238l) >> '/sample project/sample folder/walking test'

        permService.canRead(tc.steps[2].calledTestCase) >> true
        permService.canRead(tc.steps[3].calledTestCase) >> false
        permService.canRead(tc.requirementVersionCoverages[0].verifiedRequirementVersion) >> true
        permService.canRead(tc.requirementVersionCoverages[1].verifiedRequirementVersion) >> false

        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/test-cases/{id}", 238).header("Accept", "application/json"))


        then:

        /*
         * Test (using the TestHelper)
         *
         */

        // tests
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))

        withResult(res) {

            // simple body attributes
            "_type".is "test-case"
            "id".is 238
            "name".is "walking test"
            "path".is "/sample project/sample folder/walking test"
            "created_by".is "User-1"
            "created_on".is "2017-06-15T10:00:00.000+0000"
            "last_modified_by".is "User-1"
            "last_modified_on".is "2017-06-15T10:00:00.000+0000"
            "reference".is "TC1"
            "importance".is "LOW"
            "status".is "WORK_IN_PROGRESS"
            "nature.code".is "NAT_USER_TESTING"
            "type.code".is "TYP_EVOLUTION_TESTING"
            "prerequisite".is "<p>You must have legs with feet attached to them (one per leg)</p>\n"
            "description".is "<p>check that you can walk through the API (literally)</p>\n"
            "automated_test".test {
                "_type".is "automated-test"
                "id".is 2
                "name".is "script_custom_field_params_all.ta"
                selfRelIs "http://localhost:8080/api/rest/latest/automated-tests/2"
            }
            "script_auto".is "/ta-tests/script_custom_field_params_all.ta"
            "automated_test_technology".is "Cucumber"
            "scm_repository_url".is "https://github.com/test/repo01 (master)"
            "scm_repository_id".is 2
            "automated_test_reference".is "repo01/src/resources/script_custom_field_params_all.ta#Test_case_238"
            "uuid".is "2f7194ca-eb2e-4378-a82d-ddc207c866bd"
            // custom fields
            "custom_fields".hasSize 2
            "custom_fields".test {
                "[0]".test {
                    "code".is "CF_TXT"
                    "label".is "test level"
                    "value".is "mandatory"
                }
                "[1]".test {
                    "code".is "CF_TAGS"
                    "label".is "see also"
                    "value".contains "walking", "bipedal"
                }
            }

            // steps
            "steps".hasSize 4
            "steps".test {
                "[0]".test {
                    "_type".is "action-step"
                    "id".is 165
                    "action".is "<p>move \${first_foot} forward</p>\n"
                    "expected_result".is "<p>I just advanced by one step</p>\n"
                    "custom_fields".hasSize 2
                    "custom_fields".test {
                        "[0]".test {
                            "code".is "CF_TXT"
                            "label".is "test level"
                            "value".is "mandatory"
                        }
                        "[1]".test {
                            "code".is "CF_TAGS"
                            "label".is "see also"
                            "value".contains "basic", "walking"
                        }
                    }
                    selfRelIs "http://localhost:8080/api/rest/latest/test-steps/165"
                }
                "[1]".test {
                    "_type".is "action-step"
                    "id".is 166
                    "action".is "<p>move \${second_foot}&nbsp;forward</p>\n"
                    "expected_result".is "<p>and another step !</p>\n"
                    "custom_fields".hasSize 2
                    "custom_fields".test {
                        "[0]".test {
                            "code".is "CF_TXT"
                            "label".is "test level"
                            "value".is "mandatory"
                        }
                        "[1]".test {
                            "code".is "CF_TAGS"
                            "label".is "see also"
                            "value".contains "basic", "walking"
                        }
                    }
                    selfRelIs "http://localhost:8080/api/rest/latest/test-steps/166"
                }
                "[2]".test {
                    "_type".is "call-step"
                    "id".is 167
                    "delegate_parameter_values".is false
                    "called_dataset".is null
                    "called_test_case".test {
                        "_type".is "test-case"
                        "id".is 239
                        "name".is "victory dance"
                        selfRelIs "http://localhost:8080/api/rest/latest/test-cases/239"
                    }
                    selfRelIs "http://localhost:8080/api/rest/latest/test-steps/167"
                }
                "[3]".test {
                    "_type".is "call-step"
                    "id".is 168
                    "delegate_parameter_values".is false
                    "called_dataset".is null
                    "called_test_case".test {
                        "_type".is "unauthorized-resource"
                        "resource_type".is "test-case"
                        "resource_id".is 240
                        selfRelIs "http://localhost:8080/api/rest/latest/test-cases/240"
                    }
                    selfRelIs "http://localhost:8080/api/rest/latest/test-steps/168"
                }
            }

            //parameters
            "parameters".hasSize 2
            "parameters".test {
                "[0]".test {
                    "_type".is "parameter"
                    "id".is 1
                    "name".is "first_foot"
                    selfRelIs "http://localhost:8080/api/rest/latest/parameters/1"
                }
                "[1]".test {
                    "_type".is "parameter"
                    "id".is 2
                    "name".is "second_foot"
                    selfRelIs "http://localhost:8080/api/rest/latest/parameters/2"
                }
            }

            // datasets
            "datasets".hasSize 2
            "datasets".test {
                "[0]".test {
                    "_type".is "dataset"
                    "id".is 1
                    selfRelIs "http://localhost:8080/api/rest/latest/datasets/1"
                }
                "[1]".test {
                    "_type".is "dataset"
                    "id".is 2
                    selfRelIs "http://localhost:8080/api/rest/latest/datasets/2"
                }
            }

            // requirements
            "verified_requirements".hasSize 2
            "verified_requirements".test {
                "[0]".test {
                    "_type".is "requirement-version"
                    "id".is 255
                    "name".is "Must have legs"
                    selfRelIs "http://localhost:8080/api/rest/latest/requirement-versions/255"
                }
                "[1]".test {
                    "_type".is "unauthorized-resource"
                    "resource_type".is "requirement-version"
                    "resource_id".is 256
                    selfRelIs "http://localhost:8080/api/rest/latest/requirement-versions/256"
                }
            }

            // node hierarchy infos
            "project".test {
                "_type".is "project"
                "id".is 14
                "name".is "sample project"
                selfRelIs "http://localhost:8080/api/rest/latest/projects/14"
            }

            "parent".test {
                "_type".is "test-case-folder"
                "id".is 237
                "name".is "sample folder"
                selfRelIs "http://localhost:8080/api/rest/latest/test-case-folders/237"
            }

            // links
            selfRelIs "http://localhost:8080/api/rest/latest/test-cases/238"
            "_links".test {
                "project.href".is "http://localhost:8080/api/rest/latest/projects/14"
                "steps.href".is "http://localhost:8080/api/rest/latest/test-cases/238/steps"
                "parameters.href".is "http://localhost:8080/api/rest/latest/test-cases/238/parameters"
                "datasets.href".is "http://localhost:8080/api/rest/latest/test-cases/238/datasets"
            }

        }


        /*
         * Documentation
         */
        res.andDo(doc.document(

                pathParameters(
                        parameterWithName("id").description("the id of the test case")
                ),

                Snippets.fieldsParams,

                responseFields(
                        fieldWithPath("id").type(JsonFieldType.NUMBER).description("the id of the entity"),
                        fieldWithPath("_type").type(JsonFieldType.STRING).description("the type of the entity"),
                        fieldWithPath("name").type(JsonFieldType.STRING).description("name of the test case"),
                        subsectionWithPath("project").type(JsonFieldType.OBJECT).description("project of the test case"),
                        subsectionWithPath("parent").type(JsonFieldType.OBJECT).description("the location of the test case (either a folder or the project if located at the root of the library)"),
                        fieldWithPath("path").type(JsonFieldType.STRING).description("the path of the test case"),
                        fieldWithPath("created_by").type(JsonFieldType.STRING).description("user that created the test case"),
                        fieldWithPath("created_on").type(JsonFieldType.STRING).description("timestamp of the creation (ISO 8601)"),
                        fieldWithPath("last_modified_by").type(JsonFieldType.STRING).description("user that modified the test case the most recently"),
                        fieldWithPath("last_modified_on").type(JsonFieldType.STRING).description("timestamp of last modification (ISO 8601)"),
                        fieldWithPath("reference").type(JsonFieldType.STRING).description("a shorter identifier for that test case"),
                        fieldWithPath("importance").type(JsonFieldType.STRING).description("code of the importance"),
                        fieldWithPath("status").type(JsonFieldType.STRING).description("code of the status"),
                        fieldWithPath("nature.code").type(JsonFieldType.STRING).description("code of the nature"),
                        fieldWithPath("type.code").type(JsonFieldType.STRING).description("code of the type of test case"),
                        fieldWithPath("description").type(JsonFieldType.STRING).description("description of the test case (html)"),
                        fieldWithPath("prerequisite").type(JsonFieldType.STRING).description("prerequisites that should be met before the execution of the test script (html)"),

                        //automatisation
                        subsectionWithPath("automated_test").type(JsonFieldType.OBJECT).description("automated test of the test case (optional)"),
                        fieldWithPath("script_auto").type(JsonFieldType.STRING).description("automation script of the test case"),
                        fieldWithPath("automated_test_technology").type(JsonFieldType.STRING).description("automated test technology of the test case"),
                        fieldWithPath("scm_repository_url").type(JsonFieldType.STRING).description("scm repository url of the test case"),
                        fieldWithPath("scm_repository_id").type(JsonFieldType.NUMBER).description("scm repository id of the test case"),
                        fieldWithPath("automated_test_reference").type(JsonFieldType.STRING).description("automated test reference of the test case"),
                        fieldWithPath("uuid").type(JsonFieldType.STRING).description("uuid of the test case"),

                        fieldWithPath("custom_fields").type(JsonFieldType.ARRAY).description("array of custom fields"),
                        fieldWithPath("custom_fields[].code").type(JsonFieldType.STRING).description("code of the custom field"),
                        fieldWithPath("custom_fields[].label").type(JsonFieldType.STRING).description("label of the custom field"),
                        fieldWithPath("custom_fields[].value").type(JsonFieldType.VARIES).description("the value of the custom field. The value is either a string (for most custom fields), or an array of strings (for multivalued custom fields eg a tag list)"),

                        subsectionWithPath("steps").type(JsonFieldType.ARRAY).description("the step list that constitute the script. Please refer to the test steps documentation."),
                        subsectionWithPath("parameters").type(JsonFieldType.ARRAY).description("the list of parameters. Please refer to the parameters documentation."),
                        subsectionWithPath("datasets").type(JsonFieldType.ARRAY).description("the list of datasets. Please refer to the datasets documentation."),

                        subsectionWithPath("verified_requirements").type(JsonFieldType.ARRAY).description("the list of verified requirements. Please refer to the requirements documentation."),
                        fieldWithPath("attachments").type(JsonFieldType.ARRAY).description("the list of attachments."),

                        subsectionWithPath("_links").description("related links")


                ),

                links(halLinks(),
                        linkWithRel("self").description("link to this test case"),
                        linkWithRel("project").description("link to its project"),
                        linkWithRel("steps").description("link to the test script"),
                        linkWithRel("parameters").description("link to the parameters"),
                        linkWithRel("datasets").description("link to the datasets"),
                        linkWithRel("attachments").description("link to the attachments")

                )
        ))

    }


    def "get-scripted-test-case"() {

        given:
        def mocks = loadMock("mocks/get-scripted-test-case.groovy")

        def stc = mocks.tc

        and:

        restTestCaseService.getOne(1l) >> stc

        hierService.findParentFor(stc) >> SquashEntityBuilder.testCaseFolder {
            id = 237l
            name = "sample folder"
        }

        cufService.findAllCustomFieldValues(stc) >> mocks.tcCufs

        pathService.buildTestCasePath(1l) >> '/sample project/sample folder/coffee machine test'

        permService.canRead(stc.requirementVersionCoverages[0].verifiedRequirementVersion) >> true

        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/test-cases/{id}", 1).header("Accept", "application/json"))


        then:

        /*
         * Test (using the TestHelper)
         *
         */

        // tests
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))

        withResult(res) {

            // simple body attributes
            "_type".is "scripted-test-case"
            "id".is 1
            "name".is "coffee machine test"
            "path".is "/sample project/sample folder/coffee machine test"
            "created_by".is "admin"
            "created_on".is "2020-04-02T10:00:00.000+0000"
            "last_modified_by".is "admin"
            "last_modified_on".is "2020-04-03T10:00:00.000+0000"
            "reference".is "stc1"
            "importance".is "MEDIUM"
            "status".is "WORK_IN_PROGRESS"
            "nature.code".is "NAT_PERFORMANCE_TESTING"
            "type.code".is "TYP_EVOLUTION_TESTING"
            "prerequisite".is "<p>You must have a coffee machine and a coffee bag</p>\n"
            "description".is "<p>check that you can walk through the API (literally)</p>\n"
            "script".is "This is a default Gherkin script"
            "automated_test".test {
                "_type".is "automated-test"
                "id".is 2
                "name".is "script_custom_field_params_all.ta"
                selfRelIs "http://localhost:8080/api/rest/latest/automated-tests/2"
            }
            "script_auto".is "/ta-tests/script_custom_field_params_all.ta"
            "automated_test_technology".is "Cucumber"
            "scm_repository_url".is "https://github.com/test/repo01 (master)"
            "scm_repository_id".is 2
            "automated_test_reference".is "repo01/src/resources/script_custom_field_params_all.ta#Test_case_1"
            "uuid".is "4a7cdf59-d6ed-4380-89d6-cc393952e413"

            // custom fields
            "custom_fields".hasSize 2
            "custom_fields".test {
                "[0]".test {
                    "code".is "CF_TXT"
                    "label".is "test level"
                    "value".is "mandatory"
                }
                "[1]".test {
                    "code".is "CF_TAGS"
                    "label".is "see also"
                    "value".contains "expresso", "starbuck"
                }
            }

            //steps
            "steps".hasSize 0

            //parameters
            "parameters".hasSize 2
            "parameters".test {
                "[0]".test {
                    "_type".is "parameter"
                    "id".is 1
                    "name".is "trademark"
                    selfRelIs "http://localhost:8080/api/rest/latest/parameters/1"
                }
                "[1]".test {
                    "_type".is "parameter"
                    "id".is 2
                    "name".is "price"
                    selfRelIs "http://localhost:8080/api/rest/latest/parameters/2"
                }
            }

            // datasets
            "datasets".hasSize 2
            "datasets".test {
                "[0]".test {
                    "_type".is "dataset"
                    "id".is 1
                    "name".is "philips"
                    selfRelIs "http://localhost:8080/api/rest/latest/datasets/1"
                }
                "[1]".test {
                    "_type".is "dataset"
                    "id".is 2
                    "name".is "100"
                    selfRelIs "http://localhost:8080/api/rest/latest/datasets/2"
                }
            }

            // node hierarchy infos
            "project".test {
                "_type".is "project"
                "id".is 14
                "name".is "sample project"
                selfRelIs "http://localhost:8080/api/rest/latest/projects/14"
            }

            "parent".test {
                "_type".is "test-case-folder"
                "id".is 237
                "name".is "sample folder"
                selfRelIs "http://localhost:8080/api/rest/latest/test-case-folders/237"
            }

            // links
            selfRelIs "http://localhost:8080/api/rest/latest/test-cases/1"
            "_links".test {
                "project.href".is "http://localhost:8080/api/rest/latest/projects/14"
                "steps.href".is "http://localhost:8080/api/rest/latest/test-cases/1/steps"
                "parameters.href".is "http://localhost:8080/api/rest/latest/test-cases/1/parameters"
                "datasets.href".is "http://localhost:8080/api/rest/latest/test-cases/1/datasets"
            }

        }


        /*
         * Documentation
         */
        res.andDo(doc.document(

                pathParameters(
                        parameterWithName("id").description("the id of the test case")
                ),

                Snippets.fieldsParams,

                responseFields(
                        fieldWithPath("id").type(JsonFieldType.NUMBER).description("the id of the entity"),
                        fieldWithPath("_type").type(JsonFieldType.STRING).description("the type of the entity"),
                        fieldWithPath("name").type(JsonFieldType.STRING).description("name of the test case"),
                        subsectionWithPath("project").type(JsonFieldType.OBJECT).description("project of the test case"),
                        subsectionWithPath("parent").type(JsonFieldType.OBJECT).description("the location of the test case (either a folder or the project if located at the root of the library)"),
                        fieldWithPath("path").type(JsonFieldType.STRING).description("the path of the test case"),
                        fieldWithPath("created_by").type(JsonFieldType.STRING).description("user that created the test case"),
                        fieldWithPath("created_on").type(JsonFieldType.STRING).description("timestamp of the creation (ISO 8601)"),
                        fieldWithPath("last_modified_by").type(JsonFieldType.STRING).description("user that modified the test case the most recently"),
                        fieldWithPath("last_modified_on").type(JsonFieldType.STRING).description("timestamp of last modification (ISO 8601)"),
                        fieldWithPath("reference").type(JsonFieldType.STRING).description("a shorter identifier for that test case"),
                        fieldWithPath("importance").type(JsonFieldType.STRING).description("code of the importance"),
                        fieldWithPath("status").type(JsonFieldType.STRING).description("code of the status"),
                        fieldWithPath("nature.code").type(JsonFieldType.STRING).description("code of the nature"),
                        fieldWithPath("type.code").type(JsonFieldType.STRING).description("code of the type of test case"),
                        fieldWithPath("description").type(JsonFieldType.STRING).description("description of the test case (html)"),
                        fieldWithPath("prerequisite").type(JsonFieldType.STRING).description("prerequisites that should be met before the execution of the test script (html)"),

                        //automatisation
                        subsectionWithPath("automated_test").type(JsonFieldType.OBJECT).description("automated test of the test case (optional)"),
                        fieldWithPath("script_auto").type(JsonFieldType.STRING).description("automation script of the test case"),
                        fieldWithPath("automated_test_technology").type(JsonFieldType.STRING).description("automated test technology of the test case"),
                        fieldWithPath("scm_repository_url").type(JsonFieldType.STRING).description("scm repository url of the test case"),
                        fieldWithPath("scm_repository_id").type(JsonFieldType.NUMBER).description("scm repository id of the test case"),
                        fieldWithPath("automated_test_reference").type(JsonFieldType.STRING).description("automated test reference of the test case"),
                        fieldWithPath("uuid").type(JsonFieldType.STRING).description("uuid of the test case"),

                        fieldWithPath("custom_fields").type(JsonFieldType.ARRAY).description("array of custom fields"),
                        fieldWithPath("custom_fields[].code").type(JsonFieldType.STRING).description("code of the custom field"),
                        fieldWithPath("custom_fields[].label").type(JsonFieldType.STRING).description("label of the custom field"),
                        fieldWithPath("custom_fields[].value").type(JsonFieldType.VARIES).description("the value of the custom field. The value is either a string (for most custom fields), or an array of strings (for multivalued custom fields eg a tag list)"),

                        subsectionWithPath("steps").type(JsonFieldType.ARRAY).description("the step list that constitute the script. Please refer to the test steps documentation."),
                        subsectionWithPath("parameters").type(JsonFieldType.ARRAY).description("the list of parameters. Please refer to the parameters documentation."),
                        subsectionWithPath("datasets").type(JsonFieldType.ARRAY).description("the list of datasets. Please refer to the datasets documentation."),

                        //This attribute is now only for Scripted Test Case
                        fieldWithPath("script").type(JsonFieldType.STRING).description("the script of Gherkin test case."),

                        subsectionWithPath("verified_requirements").type(JsonFieldType.ARRAY).description("the list of verified requirements. Please refer to the requirements documentation."),
                        fieldWithPath("attachments").type(JsonFieldType.ARRAY).description("the list of attachments."),

                        subsectionWithPath("_links").description("related links")


                ),

                links(halLinks(),
                        linkWithRel("self").description("link to this test case"),
                        linkWithRel("project").description("link to its project"),
                        linkWithRel("steps").description("link to the test script"),
                        linkWithRel("parameters").description("link to the parameters"),
                        linkWithRel("datasets").description("link to the datasets"),
                        linkWithRel("attachments").description("link to the attachments")

                )
        ))


    }

    def "get-keyword-test-case"() {

        given:
        def mocks = loadMock("mocks/get-keyword-test-case.groovy")

        def ktc = mocks.tc

        and:

        restTestCaseService.getOne(1l) >> ktc

        hierService.findParentFor(ktc) >> SquashEntityBuilder.testCaseFolder {
            id = 237l
            name = "sample folder"
        }

        cufService.findAllCustomFieldValues(ktc) >> mocks.tcCufs

        pathService.buildTestCasePath(1l) >> '/sample project/sample folder/bdd-test'

        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/test-cases/{id}", 1).header("Accept", "application/json"))


        then:

        /*
         * Test (using the TestHelper)
         *
         */

        // tests
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))

        withResult(res) {

            // simple body attributes
            "_type".is "keyword-test-case"
            "id".is 1
            "name".is "bdd-test"
            "path".is "/sample project/sample folder/bdd-test"
            "created_by".is "admin"
            "created_on".is "2020-04-07T10:00:00.000+0000"
            "last_modified_by".is "admin"
            "last_modified_on".is "2020-04-07T10:00:00.000+0000"
            "reference".is "ktc1"
            "importance".is "HIGH"
            "status".is "WORK_IN_PROGRESS"
            "nature.code".is "NAT_PERFORMANCE_TESTING"
            "type.code".is "TYP_EVOLUTION_TESTING"
            "prerequisite".is "<p>You must go to work on monday</p>\n"
            "description".is "<p>check that you can walk through the API (literally)</p>\n"
            "automated_test".test {
                "_type".is "automated-test"
                "id".is 2
                "name".is "script_custom_field_params_all.ta"
                selfRelIs "http://localhost:8080/api/rest/latest/automated-tests/2"
            }
            "script_auto".is "/ta-tests/script_custom_field_params_all.ta"
            "automated_test_technology".is "Cucumber"
            "scm_repository_url".is "https://github.com/test/repo01 (master)"
            "scm_repository_id".is 2
            "automated_test_reference".is "repo01/src/resources/script_custom_field_params_all.ta#Test_case_1"
            "uuid".is "513ecac2-9865-4d0b-8b90-91831ff479ea"

            // custom fields
            "custom_fields".hasSize 0

            //steps
            "steps".hasSize 3
            "steps".test {
                "[0]".test {
                    "_type".is "keyword-step"
                    "id".is 165
                    "keyword".is "GIVEN"
                    "action".is "today is monday"
                    selfRelIs "http://localhost:8080/api/rest/latest/test-steps/165"
                }
                "[1]".test {
                    "_type".is "keyword-step"
                    "id".is 166
                    "keyword".is "WHEN"
                    "action".is "i go to work"
                    selfRelIs "http://localhost:8080/api/rest/latest/test-steps/166"
                }
                "[2]".test {
                    "_type".is "keyword-step"
                    "id".is 167
                    "keyword".is "THEN"
                    "action".is "i am at the office"
                    selfRelIs "http://localhost:8080/api/rest/latest/test-steps/167"
                }
            }

            //parameters
            "parameters".hasSize 0

            // datasets
            "datasets".hasSize 0

            // node hierarchy infos
            "project".test {
                "_type".is "project"
                "id".is 14
                "name".is "BDD project"
                selfRelIs "http://localhost:8080/api/rest/latest/projects/14"
            }

            "parent".test {
                "_type".is "test-case-folder"
                "id".is 237
                "name".is "sample folder"
                selfRelIs "http://localhost:8080/api/rest/latest/test-case-folders/237"
            }

            // links
            selfRelIs "http://localhost:8080/api/rest/latest/test-cases/1"
            "_links".test {
                "project.href".is "http://localhost:8080/api/rest/latest/projects/14"
                "steps.href".is "http://localhost:8080/api/rest/latest/test-cases/1/steps"
                "parameters.href".is "http://localhost:8080/api/rest/latest/test-cases/1/parameters"
                "datasets.href".is "http://localhost:8080/api/rest/latest/test-cases/1/datasets"
            }
        }

        /*
         * Documentation
         */
        res.andDo(doc.document(

                pathParameters(
                        parameterWithName("id").description("the id of the test case")
                ),

                Snippets.fieldsParams,

                responseFields(
                        fieldWithPath("id").type(JsonFieldType.NUMBER).description("the id of the entity"),
                        fieldWithPath("_type").type(JsonFieldType.STRING).description("the type of the entity"),
                        fieldWithPath("name").type(JsonFieldType.STRING).description("name of the test case"),
                        subsectionWithPath("project").type(JsonFieldType.OBJECT).description("project of the test case"),
                        subsectionWithPath("parent").type(JsonFieldType.OBJECT).description("the location of the test case (either a folder or the project if located at the root of the library)"),
                        fieldWithPath("path").type(JsonFieldType.STRING).description("the path of the test case"),
                        fieldWithPath("created_by").type(JsonFieldType.STRING).description("user that created the test case"),
                        fieldWithPath("created_on").type(JsonFieldType.STRING).description("timestamp of the creation (ISO 8601)"),
                        fieldWithPath("last_modified_by").type(JsonFieldType.STRING).description("user that modified the test case the most recently"),
                        fieldWithPath("last_modified_on").type(JsonFieldType.STRING).description("timestamp of last modification (ISO 8601)"),
                        fieldWithPath("reference").type(JsonFieldType.STRING).description("a shorter identifier for that test case"),
                        fieldWithPath("importance").type(JsonFieldType.STRING).description("code of the importance"),
                        fieldWithPath("status").type(JsonFieldType.STRING).description("code of the status"),
                        fieldWithPath("nature.code").type(JsonFieldType.STRING).description("code of the nature"),
                        fieldWithPath("type.code").type(JsonFieldType.STRING).description("code of the type of test case"),
                        fieldWithPath("description").type(JsonFieldType.STRING).description("description of the test case (html)"),
                        fieldWithPath("prerequisite").type(JsonFieldType.STRING).description("prerequisites that should be met before the execution of the test script (html)"),

                        //automatisation
                        subsectionWithPath("automated_test").type(JsonFieldType.OBJECT).description("automated test of the test case (optional)"),
                        fieldWithPath("script_auto").type(JsonFieldType.STRING).description("automation script of the test case"),
                        fieldWithPath("automated_test_technology").type(JsonFieldType.STRING).description("automated test technology of the test case"),
                        fieldWithPath("scm_repository_url").type(JsonFieldType.STRING).description("scm repository url of the test case"),
                        fieldWithPath("scm_repository_id").type(JsonFieldType.NUMBER).description("scm repository id of the test case"),
                        fieldWithPath("automated_test_reference").type(JsonFieldType.STRING).description("automated test reference of the test case"),
                        fieldWithPath("uuid").type(JsonFieldType.STRING).description("uuid of the test case"),

                        fieldWithPath("custom_fields").type(JsonFieldType.ARRAY).description("array of custom fields"),

                        subsectionWithPath("steps").type(JsonFieldType.ARRAY).description("the step list that constitute the script. Please refer to the test steps documentation."),
                        subsectionWithPath("parameters").type(JsonFieldType.ARRAY).description("the list of parameters. Please refer to the parameters documentation."),
                        subsectionWithPath("datasets").type(JsonFieldType.ARRAY).description("the list of datasets. Please refer to the datasets documentation."),

                        subsectionWithPath("verified_requirements").type(JsonFieldType.ARRAY).description("the list of verified requirements. Please refer to the requirements documentation."),
                        fieldWithPath("attachments").type(JsonFieldType.ARRAY).description("the list of attachments."),

                        subsectionWithPath("_links").description("related links")


                ),

                links(halLinks(),
                        linkWithRel("self").description("link to this test case"),
                        linkWithRel("project").description("link to its project"),
                        linkWithRel("steps").description("link to the test script"),
                        linkWithRel("parameters").description("link to the parameters"),
                        linkWithRel("datasets").description("link to the datasets"),
                        linkWithRel("attachments").description("link to the attachments")

                )
        ))


    }

    def "get-test-case-steps"() {

        given:
        def mocks = loadMock("mocks/get-test-case-steps.groovy")

        and:
        restTestCaseService.getTestCaseSteps(_, _) >> { args ->
            new PageImpl<TestStep>([mocks.step1,
                                    mocks.step2,
                                    mocks.step3,
                                    mocks.step4], args[1], 4)
        }
        cufService.findAllCustomFieldValues({ it in [mocks.step1, mocks.step3] }) >> [mocks.cufsFalse]
        cufService.findAllCustomFieldValues(mocks.step2) >> [mocks.cufsTrue]
        permService.canRead(mocks.step4.calledTestCase) >> true

        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/test-cases/{id}/steps", 239).header("Accept", "application/json"))

        then:
        // tests
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))

        def testCufsFalse = {
            "code".is "CHK_BODY_FEINT"
            "label".is "requires body feint"
            "value".is "false"
        }

        def testCufsTrue = {
            "code".is "CHK_BODY_FEINT"
            "label".is "requires body feint"
            "value".is "true"
        }

        withResult(res) {
            "_embedded.steps".hasSize 4
            "_embedded.steps".test {
                "[0]".test {
                    "_type".is "action-step"
                    "id".is 167
                    "action".is "<p>Quick step forward</p>\n"
                    "expected_result".is "<p>So does your opponent</p>\n"
                    "custom_fields[0]".test testCufsFalse
                    selfRelIs "http://localhost:8080/api/rest/latest/test-steps/167"
                }
                "[1]".test {
                    "_type".is "action-step"
                    "id".is 168
                    "action".is "<p>Another quick step forward, albeit smaller</p>\n"
                    "expected_result".is "<p>Opponent&nbsp;doubles his steps too then lunges forward for an attack</p>\n"
                    "custom_fields[0]".test testCufsTrue
                    selfRelIs "http://localhost:8080/api/rest/latest/test-steps/168"
                }
                "[2]".test {
                    "_type".is "action-step"
                    "id".is 169
                    "action".is "<p>Strong Quarte parry, possibly with a slight retreat.</p>\n"
                    "expected_result".is "<p>Opponent&#39;s attack gets blocked by your blade.</p>\n"
                    "custom_fields[0]".test testCufsFalse
                    selfRelIs "http://localhost:8080/api/rest/latest/test-steps/169"
                }
                "[3]".test {
                    "_type".is "call-step"
                    "id".is 170
                    "delegate_parameter_values".is true
                    "called_dataset".is null
                    "called_test_case".test {
                        "_type".is "test-case"
                        "id".is 240
                        "name".is "Compound riposte"
                        selfRelIs "http://localhost:8080/api/rest/latest/test-cases/240"
                    }
                    selfRelIs "http://localhost:8080/api/rest/latest/test-steps/170"
                }
            }


            selfRelIs "http://localhost:8080/api/rest/latest/test-cases/239/steps?page=0&size=20"

            "page".test {
                "size".is 20
                "totalElements".is 4
                "totalPages".is 1
                "number".is 0
            }

        }

        /*
         * Documentation
         */
        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "id : the id of the test case"
                    }
                    requestParams {
                        add DescriptorLists.paginationParams
                        add DescriptorLists.fieldsParams
                    }
                    fields {
                        embeddedAndStop "steps (array) : the steps of this test case"
                        add DescriptorLists.paginationFields
                        add DescriptorLists.linksFields
                    }
                    _links {
                        add DescriptorLists.paginationLinks
                    }
                }
        ))

    }

    def "get-keyword-test-case-steps"() {

        given:
        def mocks = loadMock("mocks/get-keyword-test-case-steps.groovy")

        and:
        restTestCaseService.getTestCaseSteps(_, _) >> { args ->
            new PageImpl<TestStep>([mocks.step1,
                                    mocks.step2,
                                    mocks.step3], args[1], 3)
        }

        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/test-cases/{id}/steps", 2).header("Accept", "application/json"))

        then:
        // tests
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))

        withResult(res) {
            "_embedded.steps".hasSize 3
            "_embedded.steps".test {
                "[0]".test {
                    "_type".is "keyword-step"
                    "id".is 180
                    "keyword".is "GIVEN"
                    "action".is "first \"good\" action word"
                    selfRelIs "http://localhost:8080/api/rest/latest/test-steps/180"
                }
                "[1]".test {
                    "_type".is "keyword-step"
                    "id".is 181
                    "keyword".is "WHEN"
                    "action".is "second action with \"5\" words"
                    selfRelIs "http://localhost:8080/api/rest/latest/test-steps/181"
                }
                "[2]".test {
                    "_type".is "keyword-step"
                    "id".is 182
                    "keyword".is "THEN"
                    "action".is "third action <attribute> word"
                    selfRelIs "http://localhost:8080/api/rest/latest/test-steps/182"
                }
            }

            selfRelIs "http://localhost:8080/api/rest/latest/test-cases/2/steps?page=0&size=20"

            "page".test {
                "size".is 20
                "totalElements".is 3
                "totalPages".is 1
                "number".is 0
            }

        }

        /*
         * Documentation
         */
        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "id : the id of the test case"
                    }
                    requestParams {
                        add DescriptorLists.paginationParams
                        add DescriptorLists.fieldsParams
                    }
                    fields {
                        embeddedAndStop "steps (array) : the steps of this test case"
                        add DescriptorLists.paginationFields
                        add DescriptorLists.linksFields
                    }
                    _links {
                        add DescriptorLists.paginationLinks
                    }
                }
        ))

    }

    def "nodoc-get-test-steps-sort-disabled"() {

        when:
        def res = mockMvc.perform(get("/api/rest/latest/test-cases/{id}/steps", 239).header("Accept", "application/json"))

        then:
        1 * restTestCaseService.getTestCaseSteps(_,
                { it.sort == Sort.unsorted() }        // <---- this is what we really test
        ) >> { args -> new PageImpl<>([], args[1], 0) }
    }


    def "get-test-case-parameters"() {

        given:
        def owner = SquashEntityBuilder.keywordTestCase {
            id = 238L
            name = "Chocolate cake"
        }

        def params = [SquashEntityBuilder.parameter {
            id = 1l
            name = "cocoa_purity"
            description = "<p>how refined the cocoa cream should be</p>"
            it.testCase = owner
        },
                      SquashEntityBuilder.parameter {
                          id = 2L
                          name = "number_of_layers"
                          description = "<p>how many times should the base pattern be repeated</p>"
                          it.testCase = owner
                      }
        ]

        and:
        restParameterService.findAllByTestCaseId(238, _) >> { args -> new PageImpl<Parameter>(params, args[1], 2) }

        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/test-cases/{id}/parameters", 238).header("Accept", "application/json"))

        then:
        /*
         * Test
         */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))

        def jsTestCaseValid = {
            "id".is 238
            "name".is "Chocolate cake"
            selfRelIs "http://localhost:8080/api/rest/latest/test-cases/238"
        }

        withResult(res) {
            "_embedded.parameters".hasSize 2
            "_embedded.parameters".test {
                "[0]".test {
                    "id".is 1
                    "name".is "cocoa_purity"
                    "description".is "<p>how refined the cocoa cream should be</p>"
                    "test_case".test jsTestCaseValid
                    selfRelIs "http://localhost:8080/api/rest/latest/parameters/1"
                }
                "[1]".test {
                    "id".is 2
                    "name".is "number_of_layers"
                    "description".is "<p>how many times should the base pattern be repeated</p>"
                    "test_case".test jsTestCaseValid
                    selfRelIs "http://localhost:8080/api/rest/latest/parameters/2"
                }
            }
            "page".test {
                "size".is 20
                "totalElements".is 2
                "totalPages".is 1
                "number".is 0
            }
            selfRelIs "http://localhost:8080/api/rest/latest/test-cases/238/parameters?page=0&size=20"

        }

        /*
         * Documentation
         */
        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "id : the id of the test case"
                    }
                    requestParams {
                        add DescriptorLists.paginationParams
                        add DescriptorLists.fieldsParams
                    }
                    fields {
                        embeddedAndStop "parameters (array) : the parameters of this test case"
                        add DescriptorLists.paginationFields
                        add DescriptorLists.linksFields
                    }
                    _links {
                        add DescriptorLists.paginationLinks
                    }
                }
        ))
    }


    def "get-test-case-datasets"() {

        given:
        def datasets = loadMock("mocks/get-test-case-datasets.groovy")

        and:
        restDatasetService.findAllByTestCaseId(238l, _) >> { args -> new PageImpl<Dataset>(datasets, args[1], 2) }

        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/test-cases/{id}/datasets", 238).header("Accept", "application/json"))


        then:
        /*
         * Test
         */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))

        def paramsValid = {
            "[0]".test {
                "_type".is "parameter"
                "id".is 1
                "name".is "cocoa_purity"
            }
            "[1]".test {
                "_type".is "parameter"
                "id".is 2
                "name".is "number_of_layers"
            }
        }

        def valuesValid = { value1, value2 ->
            "[0]".test {
                "parameter_test_case_id".is 238
                "parameter_value".is value1
                "parameter_name".is "cocoa_purity"
                "parameter_id".is 1
            }
            "[1]".test {
                "parameter_test_case_id".is 238
                "parameter_value".is value2
                "parameter_name".is "number_of_layers"
                "parameter_id".is 2
            }
        }

        withResult(res) {
            "_embedded.datasets".hasSize 2
            "_embedded.datasets".test {
                "[0]".test {
                    "_type".is "dataset"
                    "id".is 1
                    "name".is "big_cake"

                    "parameters".hasSize 2
                    "parameters".test paramsValid

                    "parameter_values".hasSize 2
                    "parameter_values".test valuesValid.curry("98%", "4")

                    selfRelIs "http://localhost:8080/api/rest/latest/datasets/1"
                }
                "[1]".test {
                    "_type".is "dataset"
                    "id".is 2
                    "name".is "biscuit"

                    "parameters".hasSize 2
                    "parameters".test paramsValid

                    "parameter_values".hasSize 2
                    "parameter_values".test valuesValid.curry("80%", "1")

                    selfRelIs "http://localhost:8080/api/rest/latest/datasets/2"
                }
            }

            "page".test {
                "size".is 20
                "totalElements".is 2
                "totalPages".is 1
                "number".is 0
            }

            selfRelIs "http://localhost:8080/api/rest/latest/test-cases/238/datasets?page=0&size=20"

        }

        /*
         * Documentation
         */
        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "id : the id of the test case"
                    }
                    requestParams {
                        add DescriptorLists.paginationParams
                        add DescriptorLists.fieldsParams
                    }
                    fields {
                        embeddedAndStop "datasets (array) : the datasets of this test case"
                        add DescriptorLists.paginationFields
                        add DescriptorLists.linksFields
                    }
                    _links {
                        add DescriptorLists.paginationLinks
                    }
                }
        ))


    }

}
