/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.controller

import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.data.domain.PageImpl
import org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders
import org.squashtest.tm.domain.testautomation.AutomatedExecutionExtender
import org.squashtest.tm.domain.testautomation.AutomatedSuite
import org.squashtest.tm.plugin.docutils.DocumentationSnippets
import org.squashtest.tm.plugin.mockmvc.BaseControllerSpec
import org.squashtest.tm.plugin.rest.service.RestAutomatedExecutionExtenderService
import org.squashtest.tm.plugin.rest.service.RestAutomatedSuiteService

import javax.inject.Inject

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import static org.squashtest.tm.plugin.docutils.DocumentationSnippets.documentationBuilder
import static org.squashtest.tm.plugin.mockmvc.TestHelper.selfRelIs
import static org.squashtest.tm.plugin.mockmvc.TestHelper.withResult
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.automatedExecutionExtender

@WebMvcTest(RestAutomatedSuiteController)
class RestAutomatedSuiteControllerIT extends BaseControllerSpec {

    @Inject
    RestAutomatedSuiteService automatedSuiteService;

    @Inject
    private RestAutomatedExecutionExtenderService automatedExecutionExtenderService;

    def "browse-automated-suite"() {
        given:
        def mocks = loadMock("mocks/get-automated-suite.groovy")

        and:
        automatedSuiteService.findAllReadable(_) >> { args -> new PageImpl<AutomatedSuite>([mocks.suite, mocks.suite1, mocks.suite2], args[0], 3) }

        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/automated-suites?size=3&page=1")
                .header("Accept", "application/json"))
        then:

        /*
        * Test (using TestHelper)
        * */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))

        withResult(res) {
            "_embedded.automated-suites".hasSize 3
            "_embedded.automated-suites".test {
                "[0]".test {
                    "_type".is "automated-suite"
                    "id".is "4028b88161e64f290161e6d832460019"
                    selfRelIs "http://localhost:8080/api/rest/latest/automated-suites/4028b88161e64f290161e6d832460019"
                }
                "[1]".test {
                    "_type".is "automated-suite"
                    "id".is "4028b881620b2a4a01620b31f2c60000"
                    selfRelIs "http://localhost:8080/api/rest/latest/automated-suites/4028b881620b2a4a01620b31f2c60000"
                }
                "[2]".test {
                    "_type".is "automated-suite"
                    "id".is "4028b88161e64f290161e6704c37000f"
                    selfRelIs "http://localhost:8080/api/rest/latest/automated-suites/4028b88161e64f290161e6704c37000f"
                }
            }
            selfRelIs "http://localhost:8080/api/rest/latest/automated-suites?page=1&size=3"
            "page".test {
                "size".is 3
                "totalElements".is 6
                "totalPages".is 2
                "number".is 1
            }
        }

        /*
        * Documentation
        * */
        res.andDo(doc.document(
                DocumentationSnippets.AllInOne.createBrowseAllEntities("automated-suites")
        ))
    }

    def "get-automated-suite"() {

        given:
        def mocks = loadMock("mocks/get-automated-suite.groovy")

        and:
        automatedSuiteService.findById(_) >> mocks.suite

        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/automated-suites/{id}", "4028b88161e64f290161e6d832460019").header("Accept", "application/json"))
        then:

        /*
        * Test (using TestHelper)
        * */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))

        withResult(res) {
            "_type".is "automated-suite"
            "id".is "4028b88161e64f290161e6d832460019"
            selfRelIs "http://localhost:8080/api/rest/latest/automated-suites/4028b88161e64f290161e6d832460019"
        }

        /*
        * Documentation
        * */
        res.andDo(doc.document(
                documentationBuilder {

                    pathParams {
                        add "id : the id of the automated suite"
                    }

                    requestParams {
                        add DocumentationSnippets.DescriptorLists.fieldsParams
                    }

                    fields {
                        add "id (string) : the uuid of the automated suite"
                        add "_type (string) : the type of the entity"
                        add DocumentationSnippets.DescriptorLists.linksFields
                    }

                    _links {
                        add "self : link to this automated test"
                        add "executions : link to the associated executions"
                    }
                }
        ))


    }

    def "get-automated-suite-executions"() {
        given:

        def mocks = loadMock("mocks/get-automated-suite.groovy")

        automatedExecutionExtenderService.findAutomatedExecutionExtenderByAutomatedSuiteId(_, _) >> { args ->
            def exec1 = automatedExecutionExtender {
                id = 778L
                automatedTest = mocks.autoTest
                execution = mocks.exec
                resultURL = new URL("http://1234:4567/jenkins/report")
                resultSummary = "all right"
                nodeName = "no root"
            }
            def exec2 = automatedExecutionExtender {
                id = 338L
                automatedTest = mocks.autoTest
                execution = mocks.exec
                resultURL = new URL("http://1234:4567/jenkins/report")
                resultSummary = "all wrong"
                nodeName = "your are right"
            }

            new PageImpl<AutomatedExecutionExtender>([exec1, exec2], args[1], 4)
        }

        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/automated-suites/{id}/executions", "4028b88161e64f290161e6d832460019").header("Accept", "application/json"))

        then:

        /*
        * Test (using TestHelper)
        * */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))

        withResult(res) {
            "_embedded.automated-execution-extenders".hasSize 2
            "_embedded.automated-execution-extenders".test {
                "[0]".test {
                    "_type".is "automated-execution-extender"
                    "id".is 778
                    "automated_test.id".is 569
                    selfRelIs "http://localhost:8080/api/rest/latest/automated-execution-extenders/778"
                }
                "[1]".test {
                    "_type".is "automated-execution-extender"
                    "id".is 338
                    selfRelIs "http://localhost:8080/api/rest/latest/automated-execution-extenders/338"
                }
            }
            selfRelIs "http://localhost:8080/api/rest/latest/automated-suites/4028b88161e64f290161e6d832460019/executions?page=0&size=20"
            "page".test {
                "size".is 20
                "totalElements".is 2
                "totalPages".is 1
                "number".is 0
            }
        }

        /*
        * Documentation
        * */
        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "id : the id of the automated suite"
                    }
                    requestParams {
                        add DocumentationSnippets.DescriptorLists.paginationAndSortParams
                        add DocumentationSnippets.DescriptorLists.fieldsParams
                    }
                    fields {
                        embeddedAndStop "automated-execution-extenders (array) : the automated execution extenders of this automated suite"
                        add DocumentationSnippets.DescriptorLists.paginationFields
                        add DocumentationSnippets.DescriptorLists.linksFields
                    }
                    _links {
                        add DocumentationSnippets.DescriptorLists.paginationLinks
                    }

                }
        ))


    }

    def "create-automated-suite-from-iteration"() {
        given:

        def mocks = loadMock("mocks/get-automated-suite.groovy")

        and:
        automatedSuiteService.createAutomatedSuiteFromIteration(_) >> mocks.suite

        when:

        def res = mockMvc.perform(RestDocumentationRequestBuilders.post("/api/rest/latest/automated-suite-utils/from-iteration")
                .accept("application/json")
                .contentType("application/json")
                .param("iterationId", "486"))
        then:

        /*
        * Test (using TestHelper)
        * */
        res.andExpect(status().isCreated())
                .andExpect(content().contentType("application/json;charset=UTF-8"))

        withResult(res) {
            "_type".is "automated-suite"
            "id".is "4028b88161e64f290161e6d832460019"
            selfRelIs "http://localhost:8080/api/rest/latest/automated-suites/4028b88161e64f290161e6d832460019"
        }

        /*
        * Documentation
        * */
        res.andDo(doc.document(
                documentationBuilder {

                    requestParams {
                        add "iterationId : the id of the iteration"
                    }

                    requestParams {
                        add DocumentationSnippets.DescriptorLists.fieldsParams
                    }

                    fields {
                        add "id (string) : the uuid of the automated suite"
                        add "_type (string) : the type of the entity"
                        add DocumentationSnippets.DescriptorLists.linksFields
                    }

                    _links {
                        add "self : link to this automated test"
                        add "executions : link to the associated executions"
                    }
                }
        ))

    }

    def "create-automated-suite-from-test-suite"() {
        given:

        def mocks = loadMock("mocks/get-automated-suite.groovy")

        and:
        automatedSuiteService.createAutomatedSuiteFromTestSuite(_) >> mocks.suite

        when:

        def res = mockMvc.perform(RestDocumentationRequestBuilders.post("/api/rest/latest/automated-suite-utils/from-test-suite")
                .accept("application/json")
                .contentType("application/json")
                .param("testSuiteId", "888"))
        then:

        /*
        * Test (using TestHelper)
        * */
        res.andExpect(status().isCreated())
                .andExpect(content().contentType("application/json;charset=UTF-8"))

        withResult(res) {
            "_type".is "automated-suite"
            "id".is "4028b88161e64f290161e6d832460019"
            selfRelIs "http://localhost:8080/api/rest/latest/automated-suites/4028b88161e64f290161e6d832460019"
        }

        /*
        * Documentation
        * */
        res.andDo(doc.document(
                documentationBuilder {

                    requestParams {
                        add "testSuiteId : the id of the test suite"
                    }

                    requestParams {
                        add DocumentationSnippets.DescriptorLists.fieldsParams
                    }

                    fields {
                        add "id (string) : the uuid of the automated suite"
                        add "_type (string) : the type of the entity"
                        add DocumentationSnippets.DescriptorLists.linksFields
                    }

                    _links {
                        add "self : link to this automated test"
                        add "executions : link to the associated executions"
                    }
                }
        ))

    }

    def "create-automated-suite-from-iteration-test-plan-items"() {
        given:

        def mocks = loadMock("mocks/get-automated-suite.groovy")

        and:
        automatedSuiteService.createAutomatedSuiteFromIterationTestPlanItems(_) >> mocks.suite

        when:

        def res = mockMvc.perform(RestDocumentationRequestBuilders.post("/api/rest/latest/automated-suite-utils/from-iteration-test-plan-items")
                .accept("application/json")
                .contentType("application/json")
                .param("itemIds", "888,777,555"))
        then:

        /*
        * Test (using TestHelper)
        * */
        res.andExpect(status().isCreated())
                .andExpect(content().contentType("application/json;charset=UTF-8"))

        withResult(res) {
            "_type".is "automated-suite"
            "id".is "4028b88161e64f290161e6d832460019"
            selfRelIs "http://localhost:8080/api/rest/latest/automated-suites/4028b88161e64f290161e6d832460019"
        }

        /*
        * Documentation
        * */
        res.andDo(doc.document(
                documentationBuilder {

                    requestParams {
                        add "itemIds : the ids of the iteration test plan items"
                    }

                    requestParams {
                        add DocumentationSnippets.DescriptorLists.fieldsParams
                    }

                    fields {
                        add "id (string) : the uuid of the automated suite"
                        add "_type (string) : the type of the entity"
                        add DocumentationSnippets.DescriptorLists.linksFields
                    }

                    _links {
                        add "self : link to this automated test"
                        add "executions : link to the associated executions"
                    }
                }
        ))

    }


    def "execute-automated-suite"() {
        given:

        def mocks = loadMock("mocks/get-automated-suite.groovy")

        and:
        automatedSuiteService.findById(_) >> mocks.suite

        when:

        def res = mockMvc.perform(RestDocumentationRequestBuilders.post("/api/rest/latest/automated-suite-utils/{suiteId}/executor", "4028b88161e64f290161e6d832460019").
                header("Accept", "application/json"))

        then:

        /*
        * Test (using TestHelper)
        * */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))

        withResult(res) {
            "_type".is "automated-suite"
            "id".is "4028b88161e64f290161e6d832460019"
            selfRelIs "http://localhost:8080/api/rest/latest/automated-suites/4028b88161e64f290161e6d832460019"
        }

        /*
        * Documentation
        * */
        res.andDo(doc.document(
                documentationBuilder {

                    pathParams {
                        add "suiteId : the id of the automated suite"
                    }
                    requestParams {
                        add DocumentationSnippets.DescriptorLists.fieldsParams
                    }

                    fields {
                        add "id (string) : the uuid of the automated suite"
                        add "_type (string) : the type of the entity"
                        add DocumentationSnippets.DescriptorLists.linksFields
                    }

                    _links {
                        add "self : link to this automated test"
                        add "executions : link to the associated executions"
                    }
                }
        ))

    }
}
