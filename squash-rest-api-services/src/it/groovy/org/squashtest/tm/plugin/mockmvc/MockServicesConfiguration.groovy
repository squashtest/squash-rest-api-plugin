/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.mockmvc

import com.fasterxml.jackson.datatype.hibernate5.Hibernate5Module
import com.google.common.reflect.ClassPath
import org.spockframework.spring.xml.SpockMockFactoryBean
import org.springframework.beans.BeansException
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory
import org.springframework.beans.factory.config.ConstructorArgumentValues
import org.springframework.beans.factory.support.BeanDefinitionRegistry
import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor
import org.springframework.beans.factory.support.RootBeanDefinition
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.squashtest.tm.plugin.rest.configuration.SquashRestApiJacksonModuleConfigurerImpl
import org.squashtest.tm.plugin.rest.core.configuration.SquashRestApiJacksonModuleConfigurer
import org.squashtest.tm.plugin.rest.service.RestRequirementVersionService
import org.squashtest.tm.plugin.rest.service.impl.RequirementVersionPatcher
import org.squashtest.tm.service.campaign.TestSuiteTestPlanManagerService
import org.squashtest.tm.service.configuration.ConfigurationService
import org.squashtest.tm.service.customfield.CustomFieldBindingFinderService
import org.squashtest.tm.service.customfield.CustomFieldValueFinderService
import org.squashtest.tm.service.denormalizedfield.DenormalizedFieldValueManager
import org.squashtest.tm.service.internal.configuration.CallbackUrlProvider
import org.squashtest.tm.service.internal.library.PathService
import org.squashtest.tm.service.internal.repository.IssueDao
import org.squashtest.tm.service.internal.repository.IterationDao
import org.squashtest.tm.service.internal.repository.TestSuiteDao
import org.squashtest.tm.service.security.PermissionEvaluationService
import org.squashtest.tm.service.testcase.ParameterModificationService

import javax.persistence.EntityManagerFactory

@Configuration
@Import([JacksonAutoConfiguration])
class MockServicesConfiguration implements BeanDefinitionRegistryPostProcessor {

	/*
	 *  packages in which services must be mocked.  
	 */
	private static final List<String> MOCK_PACKAGES = [
		"org.squashtest.tm.plugin.rest.service",
		"org.squashtest.tm.plugin.rest.validators"
	]
	
	/*
	 * Individual classes that must be mocked. Use instead of MOCK_PACKAGES if only a couple of classes in a package 
	 * needs a mock. 
	 */
	private static final List<Class<?>> MOCK_CLASSES = [
			EntityManagerFactory,
			CustomFieldValueFinderService,
			PathService,
			PermissionEvaluationService,
			RestRequirementVersionService,
			DenormalizedFieldValueManager,
			CustomFieldBindingFinderService,
			ParameterModificationService,
			RequirementVersionPatcher,
			/* see RestIssueControllerIT#get-issue */
			TestSuiteTestPlanManagerService,
			IssueDao,
			TestSuiteDao,
			IterationDao,
			CallbackUrlProvider,
			ConfigurationService
	]
	
	@Override
	public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {

	}
	
	
	@Override
	public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry registry) throws BeansException{
		
		MOCK_PACKAGES.each {
			def classes = listClasses it
			classes.each {
				registerBeanDefinition registry, it
			}
		}
		
		MOCK_CLASSES.each { registerBeanDefinition registry, it }
		
	}

	// *************** other beans ***************************
	
	@Bean
	public Hibernate5Module hibernate5Module(){
		Hibernate5Module bean = new Hibernate5Module();
		bean.configure(Hibernate5Module.Feature.FORCE_LAZY_LOADING, true);
		return bean;
	}
	
	@Bean
	public SquashRestApiJacksonModuleConfigurer basicRestObjectMapperConfigurer(){
		return new SquashRestApiJacksonModuleConfigurerImpl();
	}
	
	// **************** utilities *****************************
	
	
	private registerBeanDefinition(BeanDefinitionRegistry registry, Class<?> clazz){
		ConstructorArgumentValues cav = new ConstructorArgumentValues()
		cav.addGenericArgumentValue(clazz)
		
		RootBeanDefinition definition = new RootBeanDefinition(SpockMockFactoryBean, cav, null)


		def clazzname = clazz.canonicalName
		registry.registerBeanDefinition(clazzname, definition)
	}
	
	private Collection<Class<?>> listClasses(String packageName){
		def classloader = Thread.currentThread().contextClassLoader
		ClassPath cp = ClassPath.from classloader
		def classinfos = cp.getTopLevelClasses packageName
		return classinfos.collect { Class.forName(it.className) } 
	}
	
}
