/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.dataset
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.execution
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.iteration
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.iterationTestPlanItem
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.keywordExecution
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.keywordTestCase
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.scriptedExecution
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.scriptedTestCase
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.testCase

def itpi1 = iterationTestPlanItem {
    id = 1l
    executionStatus "BLOCKED"
    referencedTestCase = testCase {
        id = 1l
        name = "sample test case 1"
    }
    referencedDataset = dataset {
        id = 1l
        name = "sample dataset 1"
    }
    user = user {
        login = "User-1"
    }
    lastExecutedBy = "User-1"
    lastExecutedOn "2017/07/13"
    executions = [
            execution {
                id = 11l
                executionStatus "FAILURE"
                lastExecutedBy = "User-1"
                lastExecutedOn "2017/07/13"
            },
            execution {
                id = 21l
                executionStatus "BLOCKED"
                lastExecutedBy = "User-1"
                lastExecutedOn "2017/07/13"
            }
    ]
    iteration = iteration {
        id = 1L
        name = "sample iteration 1"
        reference = "IT1"
    }
}

def itpi2 = iterationTestPlanItem {
    id = 2l
    executionStatus "SUCCESS"
    referencedTestCase = scriptedTestCase {
        id = 2l
        name = "sample test case 2"
    }
    referencedDataset = dataset {
        id = 2l
        name = "sample dataset 2"
    }
    user = user {
        login = "User-2"
    }
    lastExecutedBy = "User-2"
    lastExecutedOn "2017/07/15"
    executions = [
            scriptedExecution {
                id = 12l
                executionStatus "SUCCESS"
                lastExecutedBy = "User-2"
                lastExecutedOn "2017/07/07"
            },
            scriptedExecution {
                id = 22l
                executionStatus "BLOCKED"
                lastExecutedBy = "User-2"
                lastExecutedOn "2017/07/15"
            },
            scriptedExecution {
                id = 32l
                executionStatus "RUNNING"
                lastExecutedBy = "User-2"
                lastExecutedOn "2017/07/20"
            }
    ]
    iteration = iteration {
        id = 2L
        name = "sample iteration 2"
        reference = "IT2"
    }
}

def itpi3 = iterationTestPlanItem {
    id = 3l
    executionStatus "SUCCESS"
    referencedTestCase = keywordTestCase {
        id = 3l
        name = "sample test case 3"
    }
    referencedDataset = dataset {
        id = 3l
        name = "sample dataset 3"
    }
    user = user {
        login = "User-3"
    }
    lastExecutedBy = "User-3"
    lastExecutedOn "2020/04/15"
    executions = [
            keywordExecution {
                id = 13l
                executionStatus "SUCCESS"
                lastExecutedBy = "User-3"
                lastExecutedOn "2020/04/15"
            },
            keywordExecution {
                id = 23l
                executionStatus "BLOCKED"
                lastExecutedBy = "User-3"
                lastExecutedOn "2020/04/15"
            },
            keywordExecution {
                id = 33l
                executionStatus "RUNNING"
                lastExecutedBy = "User-3"
                lastExecutedOn "2020/04/15"
            }
    ]
    iteration = iteration {
        id = 2L
        name = "sample iteration 2"
        reference = "IT2"
    }
}

/*
 * Now pack the mocks and return
 *
 */
[
        itpi1 : itpi1,
        itpi2 : itpi2,
        itpi3 : itpi3
]
