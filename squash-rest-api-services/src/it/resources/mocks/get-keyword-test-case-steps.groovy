/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
import org.squashtest.tm.domain.bdd.Keyword

import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.actionWord
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.actionWordParameter
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.actionWordParameterValue
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.actionWordText
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.keywordStep
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.keywordTestCase

def fragment1 = actionWordText {
    text = "first "
}
def fragment2 = actionWordParameter {
    name = "param1"
    defaultValue = "fine"
}
def value2 = actionWordParameterValue {
    value = "good"
    actionWordParam = fragment2
}
def fragment3 = actionWordText {
    text = " action word"
}

def step1 = keywordStep {
    id = 180L
    keyword = Keyword.GIVEN
    actionWord = actionWord {
        id = 20L
        fragments = [fragment1, fragment2, fragment3]
    }
    paramValues = [value2]
}

def fragment4 = actionWordText {
    text = "second action with "
}
def fragment5 = actionWordParameter {
    id = 1L
    name = "param1"
    defaultValue = "1"
}
def value5 = actionWordParameterValue {
    value = "5"
    actionWordParam = fragment5
}
def fragment6 = actionWordText {
    text = " words"
}

def step2 = keywordStep {
    id = 181L
    keyword = Keyword.WHEN
    actionWord = actionWord {
        id = 21L
        fragments = [fragment4, fragment5, fragment6]
    }
    paramValues = [value5]
}

def fragment7 = actionWordText {
    text = "third action "
}
def fragment8 = actionWordParameter {
    id = 1L
    name = "param1"
    defaultValue = "fine"
}
def value8 = actionWordParameterValue {
    value = "<attribute>"
    actionWordParam = fragment8
}
def fragment9 = actionWordText {
    text = " word"
}

def step3 = keywordStep {
    id = 182L
    keyword = Keyword.THEN
    actionWord = actionWord {
        id = 22L
        fragments = [fragment7, fragment8, fragment9]
    }
    paramValues = [value8]
}

def tc = keywordTestCase {
    id = 2L
    name = "who cares"
    steps = [step1, step2, step3]
}

return [
        step1    : step1,
        step2    : step2,
        step3    : step3
]
