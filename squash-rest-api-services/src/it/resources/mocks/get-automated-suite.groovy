/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
import org.squashtest.tm.domain.testautomation.TestAutomationServerKind

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.automatedExecutionExtender
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.automatedSuite
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.automatedTest
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.campaign
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.execution
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.executionStep
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.iteration
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.iterationTestPlanItem
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.project
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.testAutomatioProject
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.testAutomationServer
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.testCase

/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
def autoTest = automatedTest {
    id = 569L
    name = "auto test"
    project = testAutomatioProject {
        id = 569L
        jobName = "job 1"
        label = "wan wan"
        server = testAutomationServer {
            id = 569L
            name = "TA server"
            url = "http://1234:4567/jenkins/"
            login = "ajenkins"
            password = "password"
            kind = TestAutomationServerKind.jenkins
            description = "<p>nice try</p>"
        }
        tmProject = project {
            id = 367L
            description = "<p>This project is the main sample project</p>"
            label = "Main Sample Project"
            active = true
        }
        slaves = "no slavery"
    }
}

def exec = execution {

    referencedTestCase = testCase {
    }

    id = 56l
    name = "sample test case 56"
    executionOrder = 4
    executionStatus "BLOCKED"

    lastExecutedBy = "User-5"
    lastExecutedOn "2017/07/24"

    executionMode "MANUAL"
    reference = "SAMP_EXEC_56"
    datasetLabel = "sample dataset"

    steps = [
            executionStep {
                id = 22l
                executionStatus "SUCCESS"
                action = "<p>First action</p>"
                expectedResult = "<p>First result</p>"
            },
            executionStep {
                id = 23l
                executionStatus "BLOCKED"
                action = "<p>Second action</p>"
                expectedResult = "<p>Second result</p>"
            },
            executionStep {
                id = 27l
                executionStatus "SUCCESS"
                action = "<p>The Action</p>"
                expectedResult = "<p>The Result</p>"
            }
    ]

    description = "<p>I have no comment</p>"
    prerequisite = "<p>Being alive.</p>"
    tcdescription = "<p>This is nice.</p>"
    importance "LOW"
    nature "NAT_SECURITY_TESTING"
    type "TYP_EVOLUTION_TESTING"
    status "APPROVED"
    testPlan = iterationTestPlanItem {
        id = 15L
        iteration = iteration {
            campaign = campaign {
                project = project {
                    id = 87L
                }
            }
        }
    }

}

def suite = automatedSuite {
    id = "4028b88161e64f290161e6d832460019"
    executionExtenders = [
            automatedExecutionExtender {
                id = 778L
                automatedTest = autoTest
                execution = exec
                resultURL = new URL("http://1234:4567/jenkins/report")
                resultSummary = "all right"
                nodeName = "no root"
            }
    ]
}


def suite1 = automatedSuite {
    id = "4028b881620b2a4a01620b31f2c60000"
    executionExtenders = [
            automatedExecutionExtender {
                id = 358L
                automatedTest = autoTest
                execution = exec
                resultURL = new URL("http://1234:4567/jenkins/report")
                resultSummary = "all right"
                nodeName = "no root"
            }
    ]
}


def suite2 = automatedSuite {
    id = "4028b88161e64f290161e6704c37000f"
    executionExtenders = [
            automatedExecutionExtender {
                id = 985L
                automatedTest = autoTest
                execution = exec
                resultURL = new URL("http://1234:4567/jenkins/report")
                resultSummary = "all right"
                nodeName = "no root"
            }
    ]
}

/*
 * Now pack the mocks and return
 *
 */
[
        autoTest : autoTest,
        exec : exec,
        suite : suite,
        suite1 : suite1,
        suite2 : suite2
]
