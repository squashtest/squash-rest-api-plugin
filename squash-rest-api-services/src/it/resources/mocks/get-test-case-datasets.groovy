/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
def owner = SquashEntityBuilder.testCase {
    id = 238L
}


def parameters = [SquashEntityBuilder.parameter {
        id = 1L
        name = "cocoa_purity"
        testCase = owner
    },
      SquashEntityBuilder.parameter {
          id = 2L
          name = "number_of_layers"
          testCase = owner
}]


def datasets = [ SquashEntityBuilder.dataset {
        id = 1L
        name = "big_cake"
        parameterValues = [paramValue {
            id = 1L
            paramValue = "98%"
            parameter = parameters[0]
        },
           paramValue{
               id = 2L
               paramValue = "4"
               parameter = parameters[1]
           }
        ] as Set
    },
     SquashEntityBuilder.dataset {
         id = 2L
         name = "biscuit"
         parameterValues = [paramValue {
             id = 1L
             paramValue = "80%"
             parameter = parameters[0]
         },
                            paramValue{
                                id = 2L
                                paramValue = "1"
                                parameter = parameters[1]
                            }
         ]as Set
 }]

return datasets