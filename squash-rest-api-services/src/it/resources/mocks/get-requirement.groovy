/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.cufValue
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.keywordTestCase
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.project
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.requirement
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.requirementVersion
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.scriptedTestCase
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.testCase

def reqVer1 = requirementVersion {
    id = 78l
    name = "sample requirement 98-1"
    versionNumber = 1
}

def reqVer2 = requirementVersion {
    id = 88l
    name = "sample requirement 98-2"
    versionNumber = 2
}

def reqVer3 = requirementVersion {
    id = 98l
    name = "sample requirement 98-3"
    versionNumber = 3
    reference = "REQ01"

    createdBy "User-1"
    createdOn "2017/07/17"
    lastModifiedBy "User-1"
    lastModifiedOn "2017/07/17"

    criticality "MAJOR"
    category "CAT_FUNCTIONAL"
    description = "<p>Description of the sample requirement.</p>"

    testCases = [
            testCase {
                id =100l
                name = "sample test case 1"
            },
            scriptedTestCase {
                id =102l
                name = "sample scripted test case 2"
            },
            keywordTestCase {
                id =103l
                name = "sample keyword test case 3"
            }
    ]
}

def req = requirement {

    id = 624l
    name = "sample requirement"

    project = project {
        id = 44l
        name = "sample project"
    }

    resource = reqVer3

    versions = [
            reqVer1,
            reqVer2,
            reqVer3
    ]
}


def cufs = [
        cufValue {
            label = "cuf text"
            inputType "PLAIN_TEXT"
            code = "CF_TXT"
            value = "text value"
        },
        cufValue {
            label = "cuf tag"
            inputType  "TAG"
            code = "CF_TAG"
            value = ["tag_1", "tag_2"]
        }
]
/*
 * Now pack the mocks and return
 *
 */
[
        req : req,
        cufs : cufs
]
