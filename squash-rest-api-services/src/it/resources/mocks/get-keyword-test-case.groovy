/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
import org.squashtest.tm.domain.bdd.ActionWordText

import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.actionWord
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.automatedTest
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.automatedTestTechnology
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.keywordStep
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.keywordTestCase
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.project
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.scmRepository
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.scmServer
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.testAutomatioProject


def autoTest = automatedTest {
    id = 2l
    name = "script_custom_field_params_all.ta"
    project = testAutomatioProject {
        id = 2l
        jobName = "ta-tests"
    }
}

def automatedTestTechno = automatedTestTechnology {
    id = 2l
    name = "Cucumber"
}

def scmRepo = scmRepository {
    id = 2l
    name = "repo01"
    workingBranch = "master"
    scmServer = scmServer {
        id = 2l
        url = "https://github.com/test"
    }
}

def actionWord1 = actionWord {
    id = 1l
    fragments = [new ActionWordText("today is monday")]
}

def actionWord2 = actionWord {
    id = 2l
    fragments = [new ActionWordText("i go to work")]
}

def actionWord3 = actionWord {
    id = 3l
    fragments = [new ActionWordText("i am at the office")]
}

def ktc = keywordTestCase {
    project = project {
        id = 14l
        name = "BDD project"
    }

    id = 1l
    name = "bdd-test"

    createdBy "admin"
    createdOn "2020/04/07"
    lastModifiedBy "admin"
    lastModifiedOn "2020/04/07"

    reference = "ktc1"
    importance "HIGH"
    nature "NAT_PERFORMANCE_TESTING"
    type "TYP_EVOLUTION_TESTING"

    prerequisite = "<p>You must go to work on monday</p>\n"
    description = "<p>check that you can walk through the API (literally)</p>\n"

    steps = [
        keywordStep {
            id = 165l
            keyword "GIVEN"
            actionWord = actionWord1
        },
        keywordStep {
            id = 166l
            keyword "WHEN"
            actionWord = actionWord2
        },
        keywordStep {
            id = 167l
            keyword "THEN"
            actionWord = actionWord3
        }
    ]

    parameters = [
    ] as Set

    datasets = [
    ] as Set

    requirementVersions = [
    ]

    automatedTest = autoTest
    automatedTestTechnology = automatedTestTechno
    scmRepository = scmRepo
    automatedTestReference = "repo01/src/resources/script_custom_field_params_all.ta#Test_case_1"
    uuid = "513ecac2-9865-4d0b-8b90-91831ff479ea"
}

/*
 * The custom fields
 */
def tcCufs = [
]

/*
 * Now pack the mocks and return
 * 
 */
[
        tc    : ktc,
        tcCufs: tcCufs,
]
