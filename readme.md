# Squash TM REST API plugin


## User documentation

TODO : point to the url of the static documentation servec by Squash

## Developper documentation

### useful maven goals

* **from service module** : running the integration tests and generate the spring-restdoc : `mvn install -Pintegration-tests`
* **from root module** : create a distribution : `mvn install -Pdistro,integration-tests`. The profile `distro` explicitly
enforces the usage of `integration-tests` to make sure the documentation will be generated